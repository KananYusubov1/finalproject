/** @type {import('tailwindcss').Config} */
import withMT from "@material-tailwind/react/utils/withMT";

export default withMT({
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        modulus: ["Modulus"],
      },
      colors: {
        "brand-green": "#25B99A",
        "body-bg": "#f5f5f5",
        "dark-body-bg": "#1e2021",
        "component-bg": "#ffffff",
        "dark-component-bg": "#181a1b",
        "dark-component-second-bg": "#393e41",
        "border-clr": "#dfdfdf",
        "dark-border-clr": "#393e40",
        "hover-border-clr": "#a3a3a3",
        "hover-bg-clr": "#ececec",
        "dark-hover-bg-clr": "#393e41",
        "hover-blue-clr": "#e2e3f3",
        "dark-hover-blue-clr": "#1d1c4d",
        accent: "#3B49DF",
        "accent-dark": "#2f3ab2",
        "dark-text-accent": "#4a86e1",
        "second-text": "#575757",
        "dark-second-text": "#dfdfdf",
        "dark-text": "#717171",
        "dark-dark-text": "#717171",
        "github-bg": "#24292e",
        "github-hover-bg": "#000000",
        "google-bg": "#24292e",
        "google-hover-bg": "#000000",
        "build-selected-category-bg": "#ebedfc",
      },
      keyframes: {
        slideDownAndFade: {
          from: { opacity: 0, transform: "translateY(-2px)" },
          to: { opacity: 1, transform: "translateY(0)" },
        },
        slideLeftAndFade: {
          from: { opacity: 0, transform: "translateX(2px)" },
          to: { opacity: 1, transform: "translateX(0)" },
        },
        slideUpAndFade: {
          from: { opacity: 0, transform: "translateY(2px)" },
          to: { opacity: 1, transform: "translateY(0)" },
        },
        slideRightAndFade: {
          from: { opacity: 0, transform: "translateX(-2px)" },
          to: { opacity: 1, transform: "translateX(0)" },
        },
        slideOutBrandTr: {
          from: { opacity: 1, transform: "translateY(0) translateX(0)" },
          to: {
            opacity: 1,
            transform: "translateY(-100px) translateX(100px)",
          },
        },
        slideInBrandTr: {
          from: {
            opacity: 0,
            transform: "translateY(50px) translateX(-50px)",
          },
          to: {
            opacity: 1,
            transform: "translateY(0) translateX(0)",
          },
        },
      },
      animation: {
        slideInBrandTr: "slideInBrandTr 2s ease-out both",
        slideOutBrandTr: "slideOutBrandTr 5s ease-out both ",
        slideBrand:
          "slideOutBrandTr 5s ease-out 2s both slideOutBrandTr 5s ease-out both",
        slideDownAndFade:
          "slideDownAndFade 400ms cubic-bezier(0.16, 1, 0.3, 1)",
        slideLeftAndFade:
          "slideLeftAndFade 400ms cubic-bezier(0.16, 1, 0.3, 1)",
        slideUpAndFade: "slideUpAndFade 400ms cubic-bezier(0.16, 1, 0.3, 1)",
        slideRightAndFade:
          "slideRightAndFade 400ms cubic-bezier(0.16, 1, 0.3, 1)",
      },
    },
  },
  plugins: [],
});
