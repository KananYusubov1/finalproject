import EnterModal from "./modals/enter.jsx";
import EditProfilePhotoModal from "./modals/editProfilePhoto.jsx";
import PublishResourceModal from "./modals/publishResource.jsx";
import ResourceEditorModal from "./modals/resourceEditor.jsx";
import ContentDeleteModal from "./modals/contentDeleteModal.jsx";
import SignOutModal from "./modals/signOutModal.jsx";
import { TagEditor } from "./modals/tagEditor.jsx";
import ConfirmContentEditorBack from "./modals/confirmContentEditorBack.jsx";
import EntryEdit from "./modals/entryEdit.jsx";
import TagColorEditorModal from "./modals/tagColorEditorModal.jsx";
import BuyProductModal from "./modals/buyProductModal.jsx";
import ReportViewModal from "./modals/reportViewModal.jsx";
import ReportActionModal from "./modals/reportActionModal.jsx";

const modals = [
  {
    name: "enter",
    element: EnterModal,
  },
  {
    name: "editProfilePhoto",
    element: EditProfilePhotoModal,
  },
  {
    name: "tagEditor",
    element: TagEditor,
  },
  {
    name: "publishResource",
    element: PublishResourceModal,
  },
  {
    name: "editResource",
    element: ResourceEditorModal,
  },
  {
    name: "contentDelete",
    element: ContentDeleteModal,
  },
  {
    name: "signOut",
    element: SignOutModal,
  },
  {
    name: "confirmContentEditorBack",
    element: ConfirmContentEditorBack,
  },
  {
    name: "entryEdit",
    element: EntryEdit,
  },
  {
    name: "tagColorEditor",
    element: TagColorEditorModal,
  },
  {
    name: "buyProduct",
    element: BuyProductModal,
  },
  {
    name: "reportView",
    element: ReportViewModal,
  },
  {
    name: "reportAction",
    element: ReportActionModal,
  },
];

export default modals;
