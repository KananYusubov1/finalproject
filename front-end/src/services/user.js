import { get, patch } from "./request.js";

export const getStats = () => get("Users/GetStats");

export const getAppSettings = () => get("Users/GetAppSettings");

export const getDashboardStats = (endpoint) => get(`Users/${endpoint}`);

export const userExistsByEmail = (email) =>
  get(`Users/UserExistByEmail/${email}`);

export const userExistsByUsername = (username) =>
  get(`Users/UserExistByUsername/${username}`);

export const userResetPassword = (email, password) =>
  patch("Users/ResetPassword/", { email, newPassword: password });

export const userUpdatePassword = (currentPassword, newPassword) =>
  patch("Users/UpdatePassword/", { currentPassword, newPassword });

export const getPreviewUserById = (id) =>
  get(`Users/GetUserPreviewResponseBySelector/ById/${id}`);

export const getPreviewUserByUsername = (id) =>
  get(`Users/GetUserPreviewResponseBySelector/ByUsername/${id}`);

export const getViewUserByUsername = (username) =>
  get(`Users/GetUserViewResponseBySelector/ByUsername/${username}`);

export const getIsFollowed = (id) => get(`Users/IsFollowed/${id}`);

export const followUser = (id) => patch("Users/FollowUser", { id });

export const searchUser = (q) => get(`Users/SearchUsers/${q}`);

export const getFollowers = () => get("Users/GetFollowers");
export const getFollowings = () => get("Users/GetFollowings");
