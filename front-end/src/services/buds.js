import { get } from "./request.js";

export const getBuds = () => get("Users/GetAllTimeBuds");
export const getUsableBuds = () => get("Users/GetUsableBuds");

export const getLeaders = () => get("Users/GetLeaderBoard");

export const getBudsHistory = () =>
  get("Notifications/GetAllBudsRelatedNotifications");
