import { post, get, put, remove } from "./request.js";
import { generateAllUrl } from "../utils/common.js";

export const getResourcesCount = () => get("explore/count");

export const getResource = (resourceId) => get(`resource/${resourceId}`);
export const getAllUserResources = () => get(`explore/GetAllUserResources`);

// export const getAllResources = (q, category, flag, sort) =>
//   get(
//     `explore?${q && `SearchQuery=${q}`}${
//       category ? `&CategoryId=${category}` : ""
//     }
//     ${flag ? `&FlagId=${flag}` : ""}${sort ? `&Sort=${sort}` : ""}`
//   );

export const getAllResources = (q, category, flag, sort) => {
  let query = generateAllUrl("explore/?", {
    q,
    category,
    flag,
    sort,
  });
  return get(query);
};

export const getAllSavedResources = (q, category, flag, sort) => {
  let query = generateAllUrl("explore/saved?", {
    q,
    category,
    flag,
    sort,
  });
  return get(query);
};

// export const getAllSavedResources = (q, category, flag, sort) =>
//   get(`explore/saved?${q && `SearchQuery=${q}`}${
//     category && `&CategoryId=${category}`
//   }
//     ${flag && `&FlagId=${flag}`}${sort && `&Sort=${sort}`}`);

export const publishResource = (data) => post("explore/InsertResource", data);

export const editResource = (data) =>
  put("explore/EditResource", { editResource: data });

export const deleteResource = (resourceId) =>
  remove(`resource/delete/${resourceId}`);
