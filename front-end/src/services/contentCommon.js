import { get, patch, post, put, remove } from "./request.js";

/**
 * Deletes content of a specific type and ID.
 *
 * @param {string} contentType - The type of content to be deleted.
 * @param {string} contentId - The ID of the content to be deleted.
 * @return {Promise} A promise that resolves when the content is successfully deleted.
 */
export const deleteContent = (contentType, contentId) =>
  remove(`${contentType}/delete/${contentId}`);

/**
 * Vote on a content.
 *
 * @param {string} contentType - The type of content to vote on.
 * @param {string} voteStatus - The status of the vote.
 * @param {number} contentId - The ID of the content to vote on.
 * @return {Promise} A promise that resolves to the result of the patch request.
 */
export const voteContent = (contentType, voteStatus, contentId) =>
  patch(`${contentType}/vote/${voteStatus}/${contentId}`);

/**
 * Saves the content identified by the given contentType and contentId.
 *
 * @param {string} contentType - The type of content to save.
 * @param {string} contentId - The ID of the content to save.
 * @return {Promise} A promise that resolves to the saved content.
 */
export const saveContent = (contentType, contentId) =>
  patch(`${contentType}/save/${contentId}`);

/**
 * Fetches a resource visit by its content ID.
 *
 * @param {string} contentId - The ID of the resource content.
 * @return {Promise} A promise that resolves with the visit data.
 */
export const visitResource = (contentId) =>
  patch(`resource/visit/${contentId}`);

export const insertContent = (contentType, data) =>
  post(`${contentType}/insert`, data);

export const updateContent = (contentType, data) =>
  put(`${contentType}/update`, data);

export const getContent = (contentType, contentId) =>
  get(`${contentType}/${contentId}`);

export const getContentCount = (type) => get(`${type}/count`);

export const insertAnswer = (questionId, body) =>
  post(`answers/InsertAnswer`, { body, questionId });

export const insertComment = (contentId, content) =>
  post(`comments/MakeComment`, { content, contentId });

export const editEntry = (contentType, data) =>
  put(`${contentType}/edit`, data);
