import { get } from "./request.js";

export const lastArticles = () => get("Articles/GetLastArticles");
export const lastQuestion = () => get("questions/GetLastQuestions");
export const lastResources = () => get("explore/GetLastResources");
