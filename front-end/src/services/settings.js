import { get, patch } from "./request.js";

export const getProfileSettings = () => get("Users/GetProfileSettings");

export const updateProfileSettings = (settings) =>
  patch("Users/UpdateProfileSettings", { profileSettings: settings });

export const getCustomizationSettings = () =>
  get("Users/GetCustomizationSettings");

export const updateCustomizationSettings = (settings) =>
  patch("Users/UpdateCustomizationSettings", {
    customizationSettings: settings,
  });

export const getAccountEmails = () => get("Users/GetConnectedEmails");

export const addConnectedAccount = (github) =>
  patch("Users/AddConnectedAccount", github);

export const getAllRepositories = () => get("Users/GetAllRepositories");

export const updatePinnedRepositories = (repositoryId) =>
  patch("Users/UpdatePinnedRepositories", { repositoryId });

export const isConnectedAccountTypeExist = (type) =>
  get(`Users/IsConnectedAccountTypeExist/${type}`);

export const updatePrimaryEmail = (email) =>
  patch("Users/UpdatePrimaryEmail", { email });

export const updateDisplayEmail = (values) =>
  patch("Users/UpdateDisplayEmail", values);

export const passwordIsSet = () => get("Users/PasswordIsSet");
