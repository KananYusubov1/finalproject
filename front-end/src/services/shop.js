import { get, patch, post, put } from "./request.js";
import { generateAllUrl } from "../utils/common.js";

export const getProductRanking = (id) => {
  return get(`Product/GetAllRankCount/${id}`);
};

export const insertProduct = (data) => {
  return post("Product/InsertProduct", { insertProductDTO: data });
};

export const updateProduct = (data) => {
  return put("Product/UpdateProduct", { updateProductDTO: data });
};

export const getProducts = (q, sort) => {
  let query = generateAllUrl("Product/GetAllProducts?", {
    q,
    sort,
  });
  return get(query);
};

export const getFavProducts = (q, sort) => {
  let query = generateAllUrl("Product/GetAllFavouriteProducts?", {
    q,
    sort,
  });
  return get(query);
};

export const getProduct = (id) => {
  return get(`Product/${id}`);
};

export const getReviews = (id) => {
  return get(`Review/GetAllReviews/?ProductId=${id}`);
};

export const buyProduct = (data) => {
  return put(`Product/BuyProduct/`, data);
};

export const favouriteProduct = (id) => {
  return patch(`Product/Favourite/${id}`);
};

export const publishReview = (data) => {
  return post(`Review/InsertReview`, data);
};
