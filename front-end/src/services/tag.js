import { get, patch, post, put, remove } from "./request.js";

export const followCategory = (id) => patch(`Tag/FollowCategory/${id}`);

export const getAllBuildCategory = () => get("Tag/GetAllCategoryBuildProfile");

export const getAllCategory = (query) =>
  get(`Tag/categories${query && query.length > 0 ? `?KeyWord=${query}` : ""}`);

export const getAllFollowedCategory = (query) =>
  get(
    `Tag/GetAllFollowedCategory${
      query && query.length > 0 ? `?KeyWord=${query}` : ""
    }`
  );

export const getAllTags = (type) => get(`Tag/${type}`);

export const getTag = (type, id) => get(`Tag/${type}/${id}`);

export const insertTag = (type, data) =>
  post(`Tag/${type}/insert`, { insertTagDTO: data });

export const updateTag = (type, data) =>
  put(`Tag/${type}/update`, { updateTagDTO: data });

export const deleteTag = (type, id) => remove(`Tag/${type}/delete/${id}`);
