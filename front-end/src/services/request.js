async function parseData(data) {
  const formData = new FormData();

  for (let [key, value] of Object.entries(data)) {
    formData.append(key, value);
  }
  return formData;
}
async function request(url, method = "GET", data = false, type = "JSON") {
  const options = { method, headers: {} };

  if (data && method !== "GET") {
    options.body =
      type === "JSON" ? JSON.stringify(data) : await parseData(data);
    options.headers["Content-Type"] =
      type === "JSON" ? "application/json" : "multipart/form-data";
  }

  // Token Authentication
  options.withCredentials = true;
  options.credentials = "include";
  const token =
    localStorage.getItem("authToken") || sessionStorage.getItem("authToken");

  if (token) {
    options.headers["Authorization"] = `Bearer ${token}`;
  }

  const response = await fetch(import.meta.env.VITE_API_URL + url, options);

  let result = "";

  try {
    const contentType = response.headers.get("Content-Type");

    if (contentType.includes("text/plain")) {
      result = await response.text();
    } else if (contentType.includes("application/json")) {
      result = await response.json();
    }
  } catch (e) {
    console.log(e);
  }

  if (response.ok) {
    return result;
  } else if (response.status === 401) {
    return refreshAndRetry(url, data, method, type);
  } else {
    throw { status: response.status, message: result };
  }
}

async function refreshAndRetry(url, data, method, type) {
  const token =
    localStorage.getItem("authToken") || sessionStorage.getItem("authToken");
  const userJSON =
    localStorage.getItem("auth-info") || sessionStorage.getItem("auth-info");
  const user = JSON.parse(userJSON);

  const refreshOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ email: user.email, refreshToken: token }),
  };

  const refreshResponse = await fetch(
    import.meta.env.VITE_API_URL + `auth/refresh-token`,
    refreshOptions
  );

  if (refreshResponse.ok) {
    const newToken = await refreshResponse.text();
    if (localStorage.getItem("authToken")) {
      localStorage.setItem("authToken", newToken);
    } else {
      sessionStorage.setItem("authToken", newToken);
    }
    return request(url, data, method, type);
  } else {
    throw "Refresh token request failed";
  }
}

export const post = async (url, data) => await request(url, "POST", data);
export const put = async (url, data) => await request(url, "PUT", data);
export const patch = async (url, data) => await request(url, "PATCH", data);
export const get = async (url) => await request(url);
export const remove = async (url) => await request(url, "DELETE");
