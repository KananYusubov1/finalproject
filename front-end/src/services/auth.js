import { get, post } from "./request.js";

export const userBuild = (user) => post("auth/BuildUser", { buildUser: user });

export const sigInWithProvider = (dto) => post("auth/provider-enter", dto);

export const login = (data) => post("auth/login", data);

export const getMe = () => get("Users/GetMe");

export const signOutApi = () => get("auth/logout");
export const register = (data) => post("auth/register", { user: data });

export const sendOtp = (email, name) =>
  post("auth/SendOTPCode", { toEmail: email, toName: name });

export const verifyOtp = (data) => post("auth/VerifyEmail", data);
