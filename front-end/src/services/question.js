import { post, get, put, remove, patch } from "./request.js";
import { generateAllUrl } from "../utils/common.js";

export const getAllUserQuestion = () => get("questions/GetAllUserQuestion");

export const publishQuestion = (data) => post("questions/InsertQuestion", data);

export const getAllQuestion = (q, category, sort) => {
  let query = generateAllUrl("questions/GetAllQuestion?", {
    q,
    category,
    sort,
  });
  return get(query);
};

export const getAllSavedQuestion = (q, category, sort, page) => {
  let query = generateAllUrl("questions/GetAllUserSavedQuestion?", {
    q,
    category,
    sort,
    page,
  });
  return get(query);
};

export const getAllWatchedQuestion = (q, category, sort, page) => {
  let query = generateAllUrl("questions/GetAllUserWatchedQuestion?", {
    q,
    category,
    sort,
    page,
  });
  return get(query);
};

export const watchQuestion = (id) => patch(`questions/watch/${id}`);

export const getQuestion = (id) => get(`questions/GetQuestionById/${id}`);

export const setCorrectAnswer = (questionId, answerId) =>
  patch(`questions/SetCorrectAnswer/${questionId}/${answerId}`);
