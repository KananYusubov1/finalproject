import SideBarNavTitle from "../side-bar-nav-title/index.js";
import SideBarNavHeader from "../side-bar-nav-header/index.js";
import BaseLinkButton from "../../buttons/base-link-button/index.js";
import { useUserSignedIn } from "../../../utils/auth.js";
import classNames from "classnames";
import PropTypes from "prop-types";

const SideBar = ({ drawerMode = false }) => {
  const signedIn = useUserSignedIn();
  return (
    <div
      className={classNames({
        "w-full h-fit  2xl:w-60 flex flex-col gap-3 p-1 box-border": true,
        "overflow-scroll": drawerMode,
      })}
    >
      <div className={"h-fit  flex flex-col gap-1 "}>
        <SideBarNavHeader title={"Discover"} />
        <BaseLinkButton
          redirectUrl={"/"}
          renderTitle={<SideBarNavTitle title={"Home"} iconName={"house"} />}
          startFlex={true}
        />

        {signedIn && (
          <BaseLinkButton
            redirectUrl={"/flow"}
            renderTitle={<SideBarNavTitle title={"Flow"} iconName={"flow"} />}
            startFlex={true}
          />
        )}
        <BaseLinkButton
          redirectUrl={"/shop"}
          renderTitle={<SideBarNavTitle title={"Shop"} iconName={"shop"} />}
          startFlex={true}
        />
        <BaseLinkButton
          redirectUrl={"/categories"}
          renderTitle={
            <SideBarNavTitle title={"Categories"} iconName={"categories"} />
          }
          startFlex={true}
        />
        <BaseLinkButton
          redirectUrl={"/leaders"}
          renderTitle={
            <SideBarNavTitle title={"Leader Board"} iconName={"leader-board"} />
          }
          startFlex={true}
        />
      </div>

      <div className={"h-fit  flex flex-col gap-1"}>
        <SideBarNavHeader title={"Contribute"} />

        <BaseLinkButton
          redirectUrl={"/explore"}
          renderTitle={
            <SideBarNavTitle title={"Explore"} iconName={"explore"} />
          }
          startFlex={true}
        />
        <BaseLinkButton
          redirectUrl={"/form"}
          renderTitle={<SideBarNavTitle title={"Form"} iconName={"form"} />}
          startFlex={true}
        />
        <BaseLinkButton
          redirectUrl={"/articles"}
          renderTitle={
            <SideBarNavTitle title={"Articles"} iconName={"articles"} />
          }
          startFlex={true}
        />
        {/*<BaseLinkButton*/}
        {/*  renderTitle={*/}
        {/*    <SideBarNavTitle title={"Courses"} iconName={"courses"} />*/}
        {/*  }*/}
        {/*  startFlex={true}*/}
        {/*/>*/}
      </div>

      <div className={"h-fit  flex flex-col gap-1"}>
        <SideBarNavHeader title={"Platform Pages"} />

        <BaseLinkButton
          redirectUrl={"/faq"}
          renderTitle={<SideBarNavTitle title={"FAQ"} iconName={"faq"} />}
          startFlex={true}
        />
        <BaseLinkButton
          redirectUrl={"/about"}
          renderTitle={
            <SideBarNavTitle title={"About"} iconName={"logo"} type={"svg"} />
          }
          startFlex={true}
        />
        <BaseLinkButton
          redirectUrl={"/code-of-conduct"}
          renderTitle={
            <SideBarNavTitle
              title={"Code Of Conduct"}
              iconName={"code-of-conduct"}
            />
          }
          startFlex={true}
        />
        <BaseLinkButton
          redirectUrl={"/contact"}
          renderTitle={
            <SideBarNavTitle title={"Contact"} iconName={"contact"} />
          }
          startFlex={true}
        />
        <BaseLinkButton
          redirectUrl={"/privacy"}
          renderTitle={
            <SideBarNavTitle title={"Privacy & Policy"} iconName={"privacy"} />
          }
          startFlex={true}
        />
        <BaseLinkButton
          redirectUrl={"/terms"}
          renderTitle={
            <SideBarNavTitle title={"Terms of use"} iconName={"terms"} />
          }
          startFlex={true}
        />
      </div>
    </div>
  );
};

SideBar.propTypes = {
  drawerMode: PropTypes.bool,
};

export default SideBar;
