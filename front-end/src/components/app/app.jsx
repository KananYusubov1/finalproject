import {
  BrowserRouter as Router,
  Outlet,
  Route,
  Routes,
} from "react-router-dom";
import Layout from "../layout/layout/index.js";
import NotFoundPage from "../../pages/not-found-page/index.js";
import HomePage from "../../pages/home-page/index.js";
import EnterPage from "../../pages/enter-page/index.js";
import ForgotPasswordPage from "../../pages/forgot-password-page/index.js";
import BuildProfile from "../../pages/build-profile/index.js";
import TermsPage from "../../pages/terms-page/index.js";
import PrivacyPage from "../../pages/privacy-page/index.js";
import CodeOfConductPage from "../../pages/code-of-conduct-page/index.js";
import ContactPage from "../../pages/contact-page/index.js";
import CategoriesPage from "../../pages/categories-page/index.js";
import SideBarLayout from "../layout/side-bar-layout/index.js";
import Modal from "../../modals/index.jsx";
import { useModals } from "../../utils/modal.js";
import WebRouteLayout from "../layout/web-route-layout/index.js";
import ExplorePage from "../../pages/explore-page/index.js";
import ReportAbusePage from "../../pages/report-abuse-page/index.js";
import AdminMemePage from "../../pages/admin-meme-page/index.js";
import { useLoading } from "../../utils/process.js";
import ProviderEnterPage from "../../pages/provider-enter-page/index.js";
import { signOutSetting, useThemeSetting } from "../../utils/appSettings.js";
import { useEffect } from "react";
import UserProfileViewPage from "../../pages/user-profile-view-page/index.js";
import SettingsLayout from "../layout/settings-layout/index.js";
import ProfileSettings from "../../pages/settings/profile-settings/index.js";
import CustomizationSettings from "../../pages/settings/customization-settings/index.js";
import AccountSettings from "../../pages/settings/account-settings/index.js";
import ExtensionsSettings from "../../pages/settings/extensions-settings/index.js";
import ManagerLayout from "../layout/manager-layout/index.js";
import ReportsPage from "../../pages/managers/reports-page/index.js";
import MyActivity from "../../pages/managers/my-activity/index.js";
import ManageTags from "../../pages/managers/manage-tags/index.js";
import FormPage from "../../pages/form-page/index.js";
import ArticlesPage from "../../pages/articles-page/index.js";
import useSystemTheme from "react-use-system-theme";
import PublishContent from "../../pages/publish-content/index.js";
import Container from "../common/container/index.js";
import ProjectSearchPage from "../../pages/project-search-page/index.js";
import NotificationsPage from "../../pages/notifications-page/index.js";
import ContentViewPage from "../../pages/content-view-page/index.js";
import EditContentPage from "../../pages/edit-content-page/index.js";
import FlowPage from "../../pages/flow-page/index.js";
import LeaderBoardPage from "../../pages/leader-board-page/index.js";
import AboutPage from "../../pages/about-page/index.js";
import FaqPage from "../../pages/faq-page/index.js";
import DashboardPage from "../../pages/dashboard-page/index.js";
import FullScreenLoader from "../loaders/full-screen-loader/index.js";
import HamburgerMenu from "../common/hamburger-menu/index.js";
import PlatformPageLayout from "../layout/platform-page-layout/index.js";
import TreeLoader from "../loaders/tree-loader/index.js";
import ShopPage from "../../pages/shop-page/index.js";
import ProductViewPage from "../../pages/product-view-page/index.js";
import { getMe, signOutApi } from "../../services/auth.js";
import { logOutUser, useUserSignedIn } from "../../utils/auth.js";
import { removeUser } from "../../utils/user.js";
import ManageProducts from "../../pages/manage-products/index.js";
import PublishProduct from "../../pages/publish-product/index.js";
import ManageProductView from "../../pages/manage-product-view/index.js";
import ManageProductUpdate from "../../pages/manage-product-update/index.js";

const App = () => {
  const systemTheme = useSystemTheme("dark");
  const theme = useThemeSetting();

  const signedIn = useUserSignedIn();

  useEffect(() => {
    if (signedIn)
      getMe()
        .then((res) => {})
        .catch(() => {
          logOutUser();
          removeUser();
          signOutSetting();
        });
  }, []);

  useEffect(() => {
    let currentTheme = "";
    if (theme.toLowerCase() === "system") {
      currentTheme = systemTheme;
    } else {
      currentTheme = theme;
    }

    if (currentTheme.toLowerCase() === "dark") {
      document.body.classList.add("dark");
      document.body.setAttribute("data-color-mode", "dark");
      document.body.classList.remove("light");
    } else {
      document.body.classList.add("light");
      document.body.setAttribute("data-color-mode", "light");
      document.body.classList.remove("dark");
    }
  }, [theme, systemTheme]);

  return (
    <Router>
      <Routes>
        <Route path={"/"} element={<WebRouteLayout />}>
          <Route path={"/"} element={<SideBarLayout />}>
            <Route index={true} element={<HomePage />} />
            <Route path={"flow"} element={<FlowPage />} />
            <Route path={"leaders"} element={<LeaderBoardPage />} />
          </Route>

          {/*Shop Pages*/}
          <Route path={"/shop"} element={<Layout />}>
            <Route index={true} element={<ShopPage />} />
            <Route path={":id"} element={<ProductViewPage />} />
          </Route>

          {/*Settings Pages*/}
          <Route path={"/settings"} element={<SettingsLayout />}>
            <Route index={true} element={<ProfileSettings />} />
            <Route path={"profile"} element={<ProfileSettings />} />
            <Route path={"customization"} element={<CustomizationSettings />} />
            <Route path={"account"} element={<AccountSettings />} />
            <Route path={"extensions"} element={<ExtensionsSettings />} />
          </Route>

          <Route path={"/"} element={<Layout />}>
            <Route path={":username"} element={<UserProfileViewPage />} />
            <Route path={"notifications"} element={<NotificationsPage />} />
            <Route path={"dashboard"} element={<DashboardPage />} />
            <Route path={"search"} element={<ProjectSearchPage />} />
            <Route path={"explore"} element={<ExplorePage />} />
            <Route path={"form"} element={<FormPage />} />
            <Route path={"articles"} element={<ArticlesPage />} />
            <Route path={":type/:id"} element={<ContentViewPage />} />
            <Route path={"enter"} element={<EnterPage />} />
            <Route path={"forgot-password"} element={<ForgotPasswordPage />} />
            <Route path={"report-abuse"} element={<ReportAbusePage />} />
            <Route path={"categories"} element={<CategoriesPage />} />
          </Route>

          {/*Platform Pages*/}
          <Route path={"/"} element={<PlatformPageLayout />}>
            <Route path={"faq"} element={<FaqPage />} />
            <Route path={"about"} element={<AboutPage />} />
            <Route path={"/code-of-conduct"} element={<CodeOfConductPage />} />
            <Route path={"/contact"} element={<ContactPage />} />
            <Route path={"/privacy"} element={<PrivacyPage />} />
            <Route path={"/terms"} element={<TermsPage />} />
          </Route>

          {/*Admin Panel*/}
          <Route path={"/manage/:role"} element={<ManagerLayout />}>
            <Route index={true} element={<ReportsPage />} />
            <Route path={"reports"} element={<ReportsPage />} />
            <Route path={"products"}>
              <Route index={true} element={<ManageProducts />} />
              <Route path={"publish"} element={<PublishProduct />} />
              <Route path={":id"} element={<ProductViewPage />} />
              <Route path={":id/update"} element={<ManageProductUpdate />} />
            </Route>

            <Route path={"my-activity"} element={<MyActivity />} />
            <Route path={"manage-tags"} element={<ManageTags />} />
          </Route>

          <Route
            path={"/publish/:type"}
            element={
              <Container>
                <Outlet />
              </Container>
            }
          >
            <Route index={true} element={<PublishContent />} />
          </Route>

          <Route
            path={"/edit/:type/:id"}
            element={
              <Container>
                <Outlet />
              </Container>
            }
          >
            <Route index={true} element={<EditContentPage />} />
          </Route>
        </Route>

        <Route path={"/admin"} element={<AdminMemePage />} />
        <Route path={"/build-profile"} element={<BuildProfile />} />
        <Route path={"provider-enter"} element={<ProviderEnterPage />} />
        <Route path={"*"} element={<NotFoundPage />} />
      </Routes>

      <HamburgerMenu />
      {useLoading().isVisible && <TreeLoader />}
      {useModals().length > 0 && <Modal />}
    </Router>
  );
};

export default App;
