import TooltipContainer from "../../common/tooltip-container/index.js";
import PropTypes from "prop-types";
import { BsCaretDownSquare, BsCaretDownSquareFill } from "react-icons/bs";
import { useUserSignedIn } from "../../../utils/auth.js";
import { createModal } from "../../../utils/modal.js";

const DownVoteButton = ({ status, click }) => {
  const singendIn = useUserSignedIn();

  return (
    <button
      className={"h-[38px] w-fit "}
      onClick={(e) => {
        e.stopPropagation();
        if (singendIn) {
          click();
        } else {
          createModal("enter");
        }
      }}
    >
      {/*Down Vote*/}
      <TooltipContainer tooltip={"Down Vote"}>
        {status === "DownVote" ? (
          <BsCaretDownSquareFill
            size={38}
            className={
              "hover:text-red-700  hover:bg-red-200 hover:dark:bg-red-300 p-2 rounded-md cursor-pointer transition-all duration-150 text-[#c32222]"
            }
          />
        ) : (
          <BsCaretDownSquare
            size={38}
            className={
              "hover:text-red-700 dark:text-gray-300  hover:bg-red-200 hover:dark:bg-red-400 p-2 rounded-md cursor-pointer transition-all duration-150"
            }
          />
        )}
      </TooltipContainer>
    </button>
  );
};

DownVoteButton.propTypes = {
  status: PropTypes.string,
  click: PropTypes.func,
};

export default DownVoteButton;
