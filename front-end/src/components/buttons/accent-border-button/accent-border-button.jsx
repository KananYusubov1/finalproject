import PropTypes from "prop-types";

const AccentBorderButton = ({
  disabled = false,
  title,
  click,
  type = "button",
}) => {
  return (
    <button
      type={type}
      disabled={disabled}
      onClick={(e) => {
        e.stopPropagation();
        if (click) {
          click();
        }
      }}
      className={
        "w-full text-sm dark:text-dark-text-accent  font-semibold text-accent border border-accent  py-2 px-4 rounded-md hover:bg-accent hover:text-white hover:dark:text-white hover:underline smooth-animation disabled:cursor-not-allowed disabled:opacity-70 "
      }
    >
      {title}
    </button>
  );
};

AccentBorderButton.propTypes = {
  title: PropTypes.string,
  click: PropTypes.func,
  disabled: PropTypes.bool,
  type: PropTypes.string,
};

export default AccentBorderButton;
