import PropTypes from "prop-types";
import { BiLike, BiSolidLike } from "react-icons/bi";
import TooltipContainer from "../../common/tooltip-container/index.js";
import { useState } from "react";
import { useUserSignedIn } from "../../../utils/auth.js";
import { createModal } from "../../../utils/modal.js";
import { voteContent } from "../../../services/contentCommon.js";
import { errorToast } from "../../../utils/toast.js";
import { ThreeDots } from "react-loader-spinner";

const LikeButton = ({ contentId, contentType, vote }) => {
  const signedIn = useUserSignedIn();

  const [isLoading, setIsLoading] = useState(false);
  const [localStatus, setLocalStatus] = useState(vote.status);
  const [localCount, setLocalCount] = useState(vote.voteCount);
  const handleLike = (e) => {
    e.stopPropagation();
    if (!signedIn) {
      createModal("enter");
      return;
    }
    setIsLoading(true);
    voteContent(contentType, "UpVote", contentId)
      .then(() => {
        setLocalCount(
          localStatus === "UnVote" ? localCount + 1 : localCount - 1
        );
        setLocalStatus(localStatus === "UnVote" ? "UpVote" : "UnVote");
      })
      .catch((error) => errorToast(error.message))
      .finally(() => {
        setIsLoading(false);
      });
  };
  return isLoading ? (
    <div className={"w-16 flex items-center justify-center"}>
      <ThreeDots
        height="24"
        width="24"
        color={"#3B49DF"}
        wrapperClass="radio-wrapper"
        radius="9"
        ariaLabel="three-dots-loading"
        wrapperStyle={{}}
        wrapperClassName=""
        visible={true}
      />
    </div>
  ) : (
    <TooltipContainer tooltip={localStatus === "UnVote" ? "Like" : "Un Like"}>
      <button
        type={"button"}
        onClick={handleLike}
        className={
          "w-fit flex items-center gap-4 text-second-text dark:text-gray-500 px-2 py-1 hover:bg-hover-blue-clr hover:dark:bg-dark-hover-blue-clr rounded-md"
        }
      >
        {localStatus === "UnVote" ? (
          <BiLike size={20} />
        ) : (
          <BiSolidLike size={20} fill={"#2f3ab2"} />
        )}
        {localCount > 0 && (
          <span>
            {localCount} {localCount === 1 ? "like" : "likes"}
          </span>
        )}
      </button>
    </TooltipContainer>
  );
};

LikeButton.propTypes = {
  contentId: PropTypes.string,
  contentType: PropTypes.string,
  vote: PropTypes.object,
};

export default LikeButton;
