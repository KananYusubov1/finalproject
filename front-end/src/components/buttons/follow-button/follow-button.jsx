import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import { UserService } from "../../../services";
import PrimaryAccentButton from "../primary-accent-button/index.js";
import { ThreeDots } from "react-loader-spinner";
import { useUserSignedIn } from "../../../utils/auth.js";
import { createModal } from "../../../utils/modal.js";
import AccentBorderButton from "../accent-border-button/index.js";
import { useUser } from "../../../utils/user.js";

const FollowButton = ({ targetId }) => {
  const [followed, setFollowed] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const userId = useUser()?.id;

  const signedIn = useUserSignedIn();

  if (userId === targetId) {
    return null;
  }

  const followUser = () => {
    if (signedIn) {
      setIsLoading(true);
      UserService.followUser(targetId)
        .then(() => {
          setFollowed(!followed);
        })
        .catch((error) => {
          console.log(error);
        })
        .finally(() => {
          setIsLoading(false);
        });
    } else {
      createModal("enter");
    }
  };

  useEffect(() => {
    if (signedIn) {
      setIsLoading(true);
      UserService.getIsFollowed(targetId)
        .then(() => {
          setFollowed(true);
        })
        .catch(() => {
          setFollowed(false);
        })
        .finally(() => {
          setIsLoading(false);
        });
    } else {
      setFollowed(false);
    }
  }, []);

  return (
    <div>
      {isLoading ? (
        <div className={"w-full flex items-center justify-center"}>
          <ThreeDots
            height="24"
            width="24"
            color={"#3B49DF"}
            wrapperClass="radio-wrapper"
            radius="9"
            ariaLabel="three-dots-loading"
            wrapperStyle={{}}
            wrapperClassName=""
            visible={true}
          />
        </div>
      ) : followed ? (
        <AccentBorderButton title={"Unfollow"} click={followUser} />
      ) : (
        <PrimaryAccentButton title={"Follow"} click={followUser} />
      )}
    </div>
  );
};

FollowButton.propTypes = {
  targetId: PropTypes.string,
};
export default FollowButton;
