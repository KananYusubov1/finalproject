import PropTypes from "prop-types";
import classNames from "classnames";
import { useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";

const SubpageButton = ({ page, basePart }) => {
  const [isActiveLocal, setIsActiveLocal] = useState(false);

  const { title, iconName, customUrl, url } = page;

  const location = useLocation();

  useEffect(() => {
    const parts = location.pathname.trim().split("/");
    const lastPart = parts.length > 2 ? parts[parts.length - 1] : "";

    if (page.index && (lastPart === "" || lastPart === basePart))
      setIsActiveLocal(true);
    else setIsActiveLocal(lastPart === url);
  }, [location]);

  return (
    <Link
      to={page.url}
      className={classNames({
        "py-2 px-3 dark:text-gray-300  rounded-md  hover:bg-hover-blue-clr hover:dark:bg-dark-hover-blue-clr smooth-animation text-start": true,
        "font-semibold bg-white dark:bg-dark-component-second-bg ":
          isActiveLocal,
      })}
    >
      <div className={"flex items-center gap-2"}>
        <img
          src={customUrl ? customUrl : `/${iconName}.png`}
          alt={"icon"}
          className={"w-6 h-6 rounded-full object-cover"}
        />
        {title}
      </div>
    </Link>
  );
};

SubpageButton.propTypes = {
  page: PropTypes.object,
  basePart: PropTypes.string,
  isActive: PropTypes.bool,
};

export default SubpageButton;
