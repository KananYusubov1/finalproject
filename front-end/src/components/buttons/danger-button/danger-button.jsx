import PropTypes from "prop-types";

const DangerButton = ({ disabled = false, title, click, type = "button" }) => {
  return (
    <button
      type={type}
      disabled={disabled}
      onClick={(e) => {
        e.stopPropagation();
        if (click) {
          click();
        }
      }}
      className={
        "w-full text-sm  font-semibold text-gray-200  py-2 px-4 rounded-md bg-red-700 hover:bg-red-900 transition-all duration-70 disabled:cursor-not-allowed disabled:opacity-70 "
      }
    >
      {title}
    </button>
  );
};

DangerButton.propTypes = {
  title: PropTypes.string,
  click: PropTypes.func,
  disabled: PropTypes.bool,
  type: PropTypes.string,
};

export default DangerButton;
