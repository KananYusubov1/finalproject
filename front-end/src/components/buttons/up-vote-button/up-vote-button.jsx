import TooltipContainer from "../../common/tooltip-container/index.js";
import PropTypes from "prop-types";
import { BsCaretUpSquare, BsCaretUpSquareFill } from "react-icons/bs";
import { useUserSignedIn } from "../../../utils/auth.js";
import { createModal } from "../../../utils/modal.js";

const UpVoteButton = ({ status, click }) => {
  const singendIn = useUserSignedIn();

  return (
    <button
      className={"h-[38px] w-fit "}
      onClick={(e) => {
        e.stopPropagation();
        if (singendIn) {
          click();
        } else {
          createModal("enter");
        }
      }}
    >
      {/*Up Vote*/}
      <TooltipContainer tooltip={"Up vote"}>
        {status === "UpVote" ? (
          <BsCaretUpSquareFill
            size={38}
            className={
              "hover:bg-[#1ddc6f3d] p-2 rounded-md cursor-pointer transition-all duration-150  text-[#15ce5c]"
            }
          />
        ) : (
          <BsCaretUpSquare
            size={38}
            className={
              "hover:text-[#15ce5c] dark:text-gray-300 hover:bg-[#1ddc6f3d] hover:dark:bg-green-400 p-2 rounded-md cursor-pointer transition-all duration-150"
            }
          />
        )}
      </TooltipContainer>
    </button>
  );
};

UpVoteButton.propTypes = {
  status: PropTypes.string,
  click: PropTypes.func,
};

export default UpVoteButton;
