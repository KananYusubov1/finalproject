import PropTypes from "prop-types";

const PrimaryButton = ({ title, click, type = "button" }) => {
  return (
    <button
      type={type}
      onClick={(e) => {
        e.stopPropagation();
        if (click) {
          click();
        }
      }}
      className={
        "w-full text-sm dark:text-gray-300 font-semibold py-2 px-3 rounded-lg hover:bg-hover-bg-clr hover:dark:bg-dark-hover-bg-clr"
      }
    >
      {title}
    </button>
  );
};

PrimaryButton.propTypes = {
  title: PropTypes.string,
  click: PropTypes.func,
  type: PropTypes.string,
};

export default PrimaryButton;
