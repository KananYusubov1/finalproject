import TooltipContainer from "../../common/tooltip-container/index.js";
import { BsBookmark, BsFillBookmarkFill } from "react-icons/bs";
import PropTypes from "prop-types";
import { useState } from "react";
import { ContentCommonService } from "../../../services";
import { successToast } from "../../../utils/toast.js";
import { useUserSignedIn } from "../../../utils/auth.js";
import { createModal } from "../../../utils/modal.js";
import classNames from "classnames";

const SaveButton = ({
  isSaved,
  contentType,
  contentId,
  verticalMode,
  saveCount,
  showNumber,
}) => {
  const [saved, setSaved] = useState(isSaved);
  const [count, setCount] = useState(saveCount);

  const signedIn = useUserSignedIn();

  const handleSaveChange = (e) => {
    e.stopPropagation();
    if (signedIn) {
      setSaved(!saved);
      ContentCommonService.saveContent(contentType, contentId).catch(
        (error) => {
          console.log(error);
        }
      );
      setCount(saved ? count - 1 : count + 1);
      successToast(`Content successfully ${saved ? "UnSaved" : "Saved"}`);
    } else {
      createModal("enter");
    }
  };

  return (
    <button onClick={handleSaveChange}>
      {saved ? (
        <TooltipContainer tooltip={"UnSave Content"}>
          <div
            className={classNames({
              "flex flex-col gap-3 justify-center p-2 hover:bg-hover-blue-clr hover:dark:bg-dark-hover-bg-clr rounded-md cursor-pointer dark:text-gray-300": true,
              "!flex-row items-center gap-3": !verticalMode || showNumber,
            })}
          >
            <BsFillBookmarkFill size={22} className={""} />
            {(verticalMode || showNumber) && <span>{count}</span>}
          </div>
        </TooltipContainer>
      ) : (
        <TooltipContainer tooltip={"Save Content"}>
          <div
            className={classNames({
              "flex flex-col justify-center gap-3 p-2 hover:bg-hover-blue-clr hover:dark:bg-dark-hover-bg-clr rounded-md cursor-pointer dark:text-gray-300": true,
              "!flex-row  items-center gap-3": !verticalMode || showNumber,
            })}
          >
            <BsBookmark size={22} className={""} />
            {(verticalMode || showNumber) && <span>{count}</span>}
          </div>
        </TooltipContainer>
      )}
    </button>
  );
};

SaveButton.propTypes = {
  verticalMode: PropTypes.bool,
  saveCount: PropTypes.number,
  isSaved: PropTypes.bool,
  contentType: PropTypes.string,
  contentId: PropTypes.string,
  showNumber: PropTypes.bool,
};

export default SaveButton;
