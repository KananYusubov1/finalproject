import PropTypes from "prop-types";
import { IconContext } from "react-icons";

const IconAccentButton = ({
  icon,
  disabled = false,
  click,
  type = "button",
}) => {
  return (
    <button
      type={type}
      disabled={disabled}
      onClick={(e) => {
        e.stopPropagation();
        if (click) {
          click();
        }
      }}
      className={
        "w-full py-2 px-3 rounded-md hover:bg-hover-blue-clr hover:text-accent hover:underline transition-all duration-70 disabled:cursor-not-allowed disabled:opacity-70 hover:dark:bg-dark-hover-blue-clr hover:dark:text-dark-text-accent dark:text-gray-300"
      }
    >
      <IconContext.Provider value={{ size: 22 }}>{icon}</IconContext.Provider>
    </button>
  );
};

IconAccentButton.propTypes = {
  icon: PropTypes.element,
  click: PropTypes.func,
  disabled: PropTypes.bool,
  type: PropTypes.string,
};

export default IconAccentButton;
