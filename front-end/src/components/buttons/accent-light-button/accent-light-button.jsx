import PropTypes from "prop-types";

const AccentLightButton = ({ title, click, type = "button" }) => {
  return (
    <button
      type={type}
      onClick={(e) => {
        e.stopPropagation();
        if (click) {
          click();
        }
      }}
      className={
        " bg-transparent flex items-center text-gray-700 dark:text-gray-300  rounded-lg py-2 px-4 hover:text-accent hover:bg-hover-blue-clr hover:dark:text-dark-text-accent hover:dark:bg-dark-hover-blue-clr hover:underline transition-all duration-150 "
      }
    >
      {title}
    </button>
  );
};

AccentLightButton.propTypes = {
  title: PropTypes.string,
  click: PropTypes.func,
  type: PropTypes.string,
};

export default AccentLightButton;
