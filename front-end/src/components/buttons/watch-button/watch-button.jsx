import { useState } from "react";
import { successToast } from "../../../utils/toast.js";
import TooltipContainer from "../../common/tooltip-container/index.js";
import { BsEye, BsEyeFill, BsFillBookmarkFill } from "react-icons/bs";
import { watchQuestion } from "../../../services/question.js";
import PropTypes from "prop-types";
import { useUserSignedIn } from "../../../utils/auth.js";
import { createModal } from "../../../utils/modal.js";

const WatchButton = ({ isWatched, contentId, verticalMode, watchCount }) => {
  const [watched, setWatched] = useState(isWatched);
  const [count, setCount] = useState(watchCount);

  const isSigned = useUserSignedIn();

  const handleSaveChange = (e) => {
    e.stopPropagation();
    if (isSigned) {
      setWatched(!watched);
      watchQuestion(contentId).catch((error) => {
        console.log(error);
      });
      setCount(watched ? count - 1 : count + 1);
      successToast(
        `Question successfully ${watched ? "UnWatched" : "Watched"}`
      );
    } else {
      createModal("enter");
    }
  };
  return (
    <button onClick={handleSaveChange}>
      {watched ? (
        <TooltipContainer tooltip={"UnWatch Question"}>
          <div
            className={
              "p-2 hover:bg-hover-blue-clr hover:dark:bg-dark-hover-bg-clr rounded-md cursor-pointer dark:text-gray-300"
            }
          >
            <BsEyeFill size={22} className={""} />
            {verticalMode && <span>{count}</span>}
          </div>
        </TooltipContainer>
      ) : (
        <TooltipContainer tooltip={"Watch Question"}>
          <div
            className={
              "p-2 hover:bg-hover-blue-clr hover:dark:bg-dark-hover-bg-clr rounded-md cursor-pointer dark:text-gray-300"
            }
          >
            <BsEye size={22} className={""} />
            {verticalMode && <span>{watchCount}</span>}
          </div>
        </TooltipContainer>
      )}
    </button>
  );
};

WatchButton.propTypes = {
  isWatched: PropTypes.bool,
  contentId: PropTypes.string,
  verticalMode: PropTypes.bool,
  watchCount: PropTypes.number,
};

export default WatchButton;
