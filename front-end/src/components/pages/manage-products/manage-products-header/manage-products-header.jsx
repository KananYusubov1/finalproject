import PrimaryAccentButton from "../../../buttons/primary-accent-button/index.js";
import { MdAddBusiness } from "react-icons/md";
import Search from "../../../common/search/index.js";
import { useNavigate } from "react-router-dom";
import PropTypes from "prop-types";

const ManageProductsHeader = ({
  defaultSearchValue,
  onSearchChange,
  onSearch,
}) => {
  const navigate = useNavigate();

  return (
    <div
      className={
        "w-full px-2 flex flex-col items-start  sm:flex-row sm:items-center sm:justify-between"
      }
    >
      {/*Title*/}
      <h1 className={"font-bold text-2xl dark:text-gray-300"}>Products</h1>

      {/*Action*/}
      <div
        className={
          "w-full sm:w-fit flex flex-col items-start sm:items-end    md:flex-row gap-3"
        }
      >
        <div className={"w-fit ml-[-8px]"}>
          <PrimaryAccentButton
            title={"Add Product"}
            icon={<MdAddBusiness />}
            click={() => {
              navigate("publish");
            }}
          />
        </div>

        <div className={"w-full sm:w-[300px]"}>
          <Search
            searchValue={defaultSearchValue}
            placeHolder={"Search for product"}
            onChange={onSearchChange}
            onSearch={onSearch}
          />
        </div>
      </div>
    </div>
  );
};

ManageProductsHeader.propTypes = {
  defaultSearchValue: PropTypes.string,
  onSearchChange: PropTypes.func,
  onSearch: PropTypes.func,
};

export default ManageProductsHeader;
