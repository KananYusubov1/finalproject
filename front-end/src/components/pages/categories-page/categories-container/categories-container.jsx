import PropTypes from "prop-types";
import Category from "../category/index.js";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import NoResultFound from "../../../common/no-result-found/index.js";

const CategoriesContainer = ({ categoriesData }) => {
  const [animationParent] = useAutoAnimate();

  return (
    <div
      className={"grid grid-cols-1 md:grid-cols-2 xl:grid-cols-4 gap-4 "}
      ref={animationParent}
    >
      {categoriesData.length > 0 ? (
        categoriesData.map((category) => (
          <Category key={category.id} category={category} />
        ))
      ) : (
        <div className={"w-full"}>
          <NoResultFound />
        </div>
      )}
    </div>
  );
};

CategoriesContainer.propTypes = {
  categoriesData: PropTypes.array,
};

export default CategoriesContainer;
