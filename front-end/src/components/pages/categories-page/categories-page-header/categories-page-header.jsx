import Search from "../../../common/search/index.js";
import PropTypes from "prop-types";
import { useUserSignedIn } from "../../../../utils/auth.js";
import PageFilterContainer from "../../common/page-filter-container/index.js";

const CategoriesPageHeader = ({
  defaultSearchValue,
  onSearchChange,
  onSearch,
  searchParams,
  setSearchParam,
}) => {
  const isSignedIn = useUserSignedIn();

  return (
    <div
      className={
        "w-full px-2 flex flex-col items-start  sm:flex-row sm:items-center sm:justify-between"
      }
    >
      {/*Title*/}
      <h1 className={"font-bold text-2xl dark:text-gray-300"}>Categories</h1>

      {/*Action*/}
      <div
        className={
          "w-full sm:w-fit flex flex-col items-start sm:items-end    md:flex-row gap-3"
        }
      >
        {isSignedIn && (
          <div className={"w-fit ml-[-8px]"}>
            <PageFilterContainer
              pageFilters={[
                {
                  label: "Followed",
                  paramName: "followed",
                },
              ]}
              onSearch={onSearch}
              searchParams={searchParams}
              setSearchParam={setSearchParam}
              paramName={"filter"}
              isRemovable={true}
            />
          </div>
        )}

        <div className={"w-full sm:w-[300px]"}>
          <Search
            searchValue={defaultSearchValue}
            placeHolder={"Search for category"}
            onChange={onSearchChange}
            onSearch={onSearch}
          />
        </div>
      </div>
    </div>
  );
};

CategoriesPageHeader.propTypes = {
  defaultSearchValue: PropTypes.string,
  onSearchChange: PropTypes.func,
  onSearch: PropTypes.func,
  searchParams: PropTypes.object,
  setSearchParam: PropTypes.func,
};

export default CategoriesPageHeader;
