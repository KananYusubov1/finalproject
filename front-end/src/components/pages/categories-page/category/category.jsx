import PropTypes from "prop-types";
import LimitedText from "../../../common/limited-text/index.js";
import PrimaryAccentButton from "../../../buttons/primary-accent-button/index.js";
import { useUserSignedIn } from "../../../../utils/auth.js";
import { createModal } from "../../../../utils/modal.js";
import PrimaryButton from "../../../buttons/primary-button/index.js";
import { divideNumber } from "../../../../utils/common.js";
import { useState } from "react";
import { TagService } from "../../../../services";
import TagPreview from "../../../common/tag-preview/index.js";
import { ThreeDots } from "react-loader-spinner";

const Category = ({ category }) => {
  //   id: 2,
  //   title: "webdev",
  //   description: "",
  //   useCount: 123,
  //   iconLink: "",
  //   accentColor: "",
  //   isFollowed

  const userSignedIn = useUserSignedIn();

  const [isFollowed, setIsFollowed] = useState(category.isFollowed);
  const [loading, setLoading] = useState(false);

  const handleFollowCategory = () => {
    if (!userSignedIn) createModal("enter");
    else {
      setLoading(true);
      TagService.followCategory(category.id)
        .catch((error) => {
          console.log(error);
        })
        .finally(() => {
          setLoading(false);
        });
      setIsFollowed(!isFollowed);
    }
  };

  return (
    <div
      className={
        "w-full bg-white dark:bg-dark-component-bg dark:border-dark-border-clr rounded-lg border border-border-clr px-5 py-4 flex flex-col justify-between gap-3 shadow hover:drop-shadow-lg smooth-animation "
      }
    >
      <div className={"flex flex-col gap-1"}>
        {/*Header*/}
        <div className={"flex items-center justify-between gap-1 "}>
          {/*Title*/}
          <h1>
            <TagPreview
              isSecond={false}
              title={category.title}
              colorId={category.accentColor}
            />
          </h1>
          {/*Used Count*/}
          <p className={"text-dark-text text-xs"}>
            <span>{divideNumber(category.useCount)}</span> time used
          </p>
        </div>
        {/*Description*/}
        <div className={"text-second-text dark:text-gray-600 text-sm"}>
          <LimitedText text={category.description} limit={110} />
        </div>
        {/*Footer*/}
      </div>
      <div className={"flex items-center justify-between"}>
        {/*Follow button*/}
        <div className={"w-fit"}>
          {loading ? (
            <div className={"w-full flex items-center justify-center"}>
              <ThreeDots
                height="24"
                width="24"
                color={"#3B49DF"}
                wrapperClass="radio-wrapper"
                radius="9"
                ariaLabel="three-dots-loading"
                wrapperStyle={{}}
                wrapperClassName=""
                visible={true}
              />
            </div>
          ) : isFollowed ? (
            <>
              <PrimaryButton click={handleFollowCategory} title={"Following"} />
            </>
          ) : (
            <PrimaryAccentButton
              title={"Follow"}
              click={handleFollowCategory}
            />
          )}
        </div>
        {/*Icon*/}
        {category.iconLink && (
          <img
            className={"w-12 rounded-md"}
            src={category.iconLink}
            alt={`${category.title} icon`}
          />
        )}
      </div>
    </div>
  );
};

Category.propTypes = {
  category: PropTypes.object,
};

export default Category;
