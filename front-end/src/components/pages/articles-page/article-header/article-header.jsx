import PropTypes from "prop-types";
import { formatDate } from "../../../../utils/common.js";
import UserPreviewHoverCard from "../../../common/user-preview-hover-card/index.js";

const ArticleHeader = ({ article, mode }) => {
  return (
    <div className={""}>
      {/*Image*/}
      {mode === "component" && article.header.image && (
        <img
          className={" w-full sm:h-64 object-cover rounded-t-md"}
          src={article.header.image}
          alt={"Cover Image"}
        />
      )}

      {/*Bar*/}
      <div
        className={
          "w-fit flex items-center gap-2  px-6 pt-4 pb-2 dark:text-gray-300 "
        }
      >
        {/*User*/}
        <img
          src={article.userContent.profilePhoto}
          alt="avatar"
          className={"w-9 h-9 rounded-full object-cover"}
        />
        <div className={"flex flex-col"}>
          <UserPreviewHoverCard userId={article.userContent.userId}>
            <div
              className={
                "w-fit rounded-md dark:text-gray-300 p-1 hover:bg-border-clr hover:dark:bg-dark-hover-bg-clr smooth-animation"
              }
            >
              <span className={"font-medium cursor-pointer"}>
                {article.userContent.name}
              </span>
            </div>
          </UserPreviewHoverCard>
          <span className={"text-xs pl-1"}>
            {formatDate(article.updateTime)}
          </span>
        </div>
      </div>
    </div>
  );
};

ArticleHeader.propTypes = {
  article: PropTypes.object,
  mode: PropTypes.string,
};

export default ArticleHeader;
