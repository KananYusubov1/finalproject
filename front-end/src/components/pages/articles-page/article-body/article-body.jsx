import TagPreview from "../../../common/tag-preview/index.js";
import { Link } from "react-router-dom";
import classNames from "classnames";
import PropTypes from "prop-types";

const ArticleBody = ({ article }) => {
  return (
    <div className={"flex flex-col gap-1 px-[6%]"}>
      {/*Title*/}
      <Link
        to={`/articles/${article.id}`}
        className={classNames({
          "dark:text-gray-300 font-bold sm:text-3xl text-xl hover:text-accent hover:dark:text-dark-text-accent": true,
          "text-dark-text dark:text-gray-500": article.isViewed,
        })}
      >
        {article.header.title}
      </Link>
      {/*Categories & Flag*/}
      <div className={"flex gap-6"}>
        {article.articleCategories.map((category) => (
          <TagPreview
            key={category.id}
            title={category.title}
            colorId={category.accentColor}
          />
        ))}
        {
          <TagPreview
            key={article.articleFlag.id}
            title={article.articleFlag.title}
            colorId={article.articleFlag.accentColor}
          />
        }
      </div>
    </div>
  );
};

ArticleBody.propTypes = {
  article: PropTypes.object,
};

export default ArticleBody;
