import ArticleBody from "../article-body/index.js";
import ArticleHeader from "../article-header/index.js";
import ArticleFooter from "../article-footer/index.js";
import PropTypes from "prop-types";

const Article = ({ article, mode = "component" }) => {
  return (
    <div className={"full-card-schema w-full !p-0 !pb-3 !gap-1"}>
      <ArticleHeader article={article} mode={mode} />
      <ArticleBody article={article} />
      {mode === "component" && <ArticleFooter article={article} mode={mode} />}
    </div>
  );
};

Article.propTypes = {
  article: PropTypes.object,
  mode: PropTypes.string,
};

export default Article;
