import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { nanoid } from "nanoid";

const LastListings = ({ title, data, seeAllUrl }) => {
  if (!data || !data.length) return <div></div>;
  return (
    <div
      className={
        "w-fit 2xl:w-80  !pb-2 !p-0 !flex-col !gap-1 card-color-schema card-shadow-schema "
      }
    >
      {/*Header*/}
      <div
        className={
          "w-full flex items-center justify-between p-3 px-4 dark:text-white"
        }
      >
        {/*Title*/}
        <span className={"text-lg font-bold"}>{title}</span>
        {/*See all*/}
        <Link
          to={seeAllUrl}
          className={
            "text-sm text-accent dark:text-dark-text-accent underline-animation"
          }
        >
          See all
        </Link>
      </div>
      <div className={"w-full flex flex-col"}>
        {data.map((item) => (
          <Link
            to={item.url}
            target={"_blank"}
            key={nanoid()}
            className={
              "group p-4 cursor-pointer border-t border-border-clr dark:border-dark-border-clr text-sm text-gray-700 dark:text-gray-400 flex flex-col gap-1"
            }
          >
            <p
              className={
                "w-full group-hover:text-accent group-hover:dark:text-dark-text-accent hover:dark:text-dark-text-accent ellipsis-overflow"
              }
            >
              {item.title}
            </p>
            <span className={"text-xs whitespace-pre ellipsis-overflow"}>
              {item.description}
            </span>
          </Link>
        ))}
      </div>
    </div>
  );
};

LastListings.propTypes = {
  title: PropTypes.string,
  data: PropTypes.array,
  seeAllUrl: PropTypes.string,
};

export default LastListings;
