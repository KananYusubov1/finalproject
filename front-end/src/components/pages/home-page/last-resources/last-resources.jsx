import { useEffect, useState } from "react";
import { lastResources } from "../../../../services/lastListings.js";
import { ThreeDots } from "react-loader-spinner";
import LastListings from "../last-listings/index.js";
import { useMediaQuery } from "react-responsive";

const LastResources = () => {
  const [resources, setResources] = useState([]);

  const [isLoading, setIsLoading] = useState(false);

  const isSmall = useMediaQuery({ query: "(max-width: 1024px)" });

  useEffect(() => {
    if (isSmall) return;

    setIsLoading(true);
    lastResources()
      .then((res) => {
        let data = res.map((r) => {
          return {
            title: r.title,
            url: r.url,
            description: r.url,
          };
        });
        setResources(data);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, []);

  if (isSmall) return null;

  return (
    <div>
      {isLoading ? (
        <div className={"!w-80 full-card-schema items-center"}>
          <ThreeDots
            height="24"
            width="24"
            color={"#3B49DF"}
            wrapperClass="radio-wrapper"
            radius="9"
            ariaLabel="three-dots-loading"
            wrapperStyle={{}}
            wrapperClassName=""
            visible={true}
          />
        </div>
      ) : (
        <LastListings
          key={"Last Resources"}
          title={"Last Resources"}
          data={resources}
          seeAllUrl={"/explore"}
        />
      )}
    </div>
  );
};

export default LastResources;
