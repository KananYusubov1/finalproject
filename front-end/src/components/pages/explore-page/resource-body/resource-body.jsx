import VerticalVoteContainer from "../../../common/vote-container/index.js";
import WebView from "../../../common/web-view/index.js";
import PropTypes from "prop-types";
import { ContentCommonService } from "../../../../services";

const ResourceBody = ({ resource, mode }) => {
  const { title, url, vote, id } = resource;
  const { voteCount, status } = vote;

  const handleResourceVisit = () => {
    ContentCommonService.visitResource(id).catch((error) => {
      console.log(error);
    });
  };

  return (
    <div className={" flex items-center justify-between gap-2"}>
      {mode === "component" && (
        <VerticalVoteContainer
          voteStatus={status}
          voteCount={voteCount}
          contentType={"resource"}
          contentId={id}
        />
      )}

      <div className={"w-full flex flex-col gap-3"}>
        <p
          className={
            "text-2xl font-bold mt-[1px] text-gray-800 dark:text-gray-300"
          }
        >
          {title}
        </p>

        <WebView url={url} onClick={handleResourceVisit} />
      </div>
    </div>
  );
};

ResourceBody.propTypes = {
  resource: PropTypes.object,
  mode: PropTypes.string,
};

export default ResourceBody;
