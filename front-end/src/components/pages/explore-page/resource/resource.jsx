import PropTypes from "prop-types";
import ResourceHeader from "../resource-header/index.js";
import ResourceBody from "../resource-body/index.js";
import { useState } from "react";
import ResourceSkeleton from "../../../skeleton-loaders/resource-skeleton/index.js";

const Resource = ({ resource, onDeleteResource, mode = "component" }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [innerResource, setInnerResource] = useState(resource);

  const handleResourceUpdate = (updatedResource) => {
    setIsLoading(true);
    setTimeout(() => {
      setIsLoading(false);
    }, 1000);

    const innerUpdatedResource = { ...innerResource };
    innerUpdatedResource.title = updatedResource.title;
    innerUpdatedResource.category =
      updatedResource.category ?? innerResource.category;
    innerUpdatedResource.flag = updatedResource.flag ?? innerResource.flag;
    setInnerResource(innerUpdatedResource);
  };

  return isLoading ? (
    <ResourceSkeleton />
  ) : (
    <div
      className={
        "bg-white dark:bg-dark-component-bg py-4 px-5 rounded-md flex flex-col gap-2  hover:drop-shadow-lg hover:dark:shadow-gray-800 shadow smooth-animation"
      }
    >
      <ResourceHeader
        resource={innerResource}
        updateResource={handleResourceUpdate}
        onDeleteResource={onDeleteResource}
        mode={mode}
      />
      <ResourceBody resource={innerResource} mode={mode} />
    </div>
  );
};

Resource.propTypes = {
  resource: PropTypes.object,
  onDeleteResource: PropTypes.func,
  mode: PropTypes.string,
};

export default Resource;
