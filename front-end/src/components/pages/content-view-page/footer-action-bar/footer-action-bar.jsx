import { useMediaQuery } from "react-responsive";
import { getFieldsForContentViewAction } from "../../../../utils/common.js";
import { generateOptionsForContentViewAction } from "../../../../utils/commonForComponent.jsx";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useUser } from "../../../../utils/user.js";
import VoteContainer from "../../../common/vote-container/index.js";
import TooltipContainer from "../../../common/tooltip-container/index.js";
import IconButton from "../../../buttons/icon-button/index.js";
import { FaRegComment } from "react-icons/fa";
import { RiQuestionAnswerLine } from "react-icons/ri";
import WatchButton from "../../../buttons/watch-button/index.js";
import SaveButton from "../../../buttons/save-button/index.js";
import MoreOptions from "../../../common/more-options/index.js";
import PropTypes from "prop-types";

const FooterActionBar = ({ content, type, id }) => {
  const {
    vote,
    isSaved,
    actionCount,
    watchCount,
    userId,
    saveCount,
    isWatched,
  } = getFieldsForContentViewAction(content, type);

  const [dropdownItems, setDropdownItems] = useState([]);
  const navigate = useNavigate();

  const currentId = useUser().id;

  const isSmall = useMediaQuery({ query: "(min-width: 767px)" });

  useEffect(() => {
    if (isSmall) return;

    const options = generateOptionsForContentViewAction(
      content,
      type,
      id,
      currentId,
      userId,
      navigate
    );

    setDropdownItems(options);
  }, []);

  if (isSmall) return null;

  return (
    <div
      className={
        "bg-body-bg dark:bg-dark-body-bg  w-full h-fit p-1 absolute  bottom-[-1px]  flex items-center justify-between"
      }
    >
      <VoteContainer
        contentId={id}
        contentType={type}
        voteStatus={vote.status}
        voteCount={vote.voteCount}
        contentViewMode={true}
        direction={"horizontal"}
      />

      {type === "articles" ? (
        <TooltipContainer tooltip={"Comments"}>
          <IconButton
            icon={<FaRegComment />}
            title={actionCount}
            verticalMode={false}
            size={22}
            click={() => {
              document.getElementById("conversation-area").scrollIntoView({
                behavior: "smooth",
              });
            }}
          />
        </TooltipContainer>
      ) : (
        <TooltipContainer tooltip={"Answers"}>
          <IconButton
            icon={<RiQuestionAnswerLine />}
            title={actionCount}
            verticalMode={false}
            size={22}
            click={() => {
              document.getElementById("conversation-area").scrollIntoView({
                behavior: "smooth",
              });
            }}
          />
        </TooltipContainer>
      )}

      {type === "questions" && (
        <WatchButton
          verticalMode={false}
          contentId={id}
          isWatched={isWatched}
          watchCount={watchCount}
        />
      )}

      <SaveButton
        verticalMode={false}
        showNumber={true}
        saveCount={saveCount}
        contentType={type}
        contentId={id}
        isSaved={isSaved}
      />
      <MoreOptions items={dropdownItems} />
    </div>
  );
};

FooterActionBar.propTypes = {
  type: PropTypes.string,
  id: PropTypes.string,
  content: PropTypes.object,
};

export default FooterActionBar;
