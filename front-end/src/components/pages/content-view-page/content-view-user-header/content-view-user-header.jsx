import { Link } from "react-router-dom";
import { formatDate } from "../../../../utils/common.js";
import PropTypes from "prop-types";

const ContentViewUserHeader = ({ user, updateTime }) => {
  return (
    <div>
      <div
        className={"w-fit flex items-center gap-2 py-2  dark:text-gray-300  "}
      >
        {/*User*/}
        <img
          src={user.profilePhoto}
          alt="avatar"
          className={"w-9 h-9 rounded-full object-cover"}
        />
        <div className={"flex flex-col"}>
          <span className={"font-medium text-sm"}>
            <Link
              to={`/${user.username}`}
              className={
                "rounded-md dark:text-gray-300 hover:text-accent hover:dark:text-dark-text-accent smooth-animation"
              }
            >
              <span className={"font-semibold "}>{user.name}</span>
            </Link>
          </span>
          <span className={"text-xs"}>{formatDate(updateTime)}</span>
        </div>
      </div>
    </div>
  );
};

ContentViewUserHeader.propTypes = {
  user: PropTypes.object,
  updateTime: PropTypes.string,
};

export default ContentViewUserHeader;
