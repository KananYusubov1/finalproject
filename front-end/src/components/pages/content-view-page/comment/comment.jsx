import { useUserSignedIn } from "../../../../utils/auth.js";
import { useUser } from "../../../../utils/user.js";
import { useEffect, useState } from "react";
import UserPreviewHoverCard from "../../../common/user-preview-hover-card/index.js";
import TooltipContainer from "../../../common/tooltip-container/index.js";
import { SlBadge } from "react-icons/sl";
import DotDivider from "../../../common/dot-divider/index.js";
import { formatDate } from "../../../../utils/common.js";
import MoreOptions from "../../../common/more-options/index.js";
import MarkdownViewer from "../../../markdown-viewer/index.js";
import LikeButton from "../../../buttons/like-button/index.js";
import PropTypes from "prop-types";
import { createModal } from "../../../../utils/modal.js";

const Comment = ({ comment, authorId, onDelete }) => {
  const isAuthorAnswer = comment.userContent.userId === authorId;

  const signedIn = useUserSignedIn();
  const userId = useUser()?.id;
  const [body, setBody] = useState(comment.body);

  const [dropdownItems, setDropdownItems] = useState([]);
  const handleOnUpdate = (values) => {
    setBody(values.body);
  };

  const handleEditComment = () => {
    console.log("comment: ", comment);
    createModal("entryEdit", {
      type: "comments",
      title: "comment",
      id: comment.id,
      body: body,
      onUpdate: handleOnUpdate,
    });
  };
  useEffect(() => {
    const options = [];
    if (comment.userContent.userId === userId) {
      options.push(
        {
          label: "Edit comment",
          onClick: () => {
            handleEditComment();
          },
          type: "item",
        },
        {
          label: "Delete comment",
          onClick: () => {
            createModal("contentDelete", {
              contentId: comment.id,
              contentType: "comments",
              onDelete: () => {
                onDelete(comment.id);
              },
            });
          },
          type: "danger-item",
        }
      );
    }

    setDropdownItems(options);
  }, []);

  return (
    <div className={"w-full flex gap-2"}>
      <img
        src={comment.userContent.profilePhoto}
        alt="Avatar"
        className={"w-8 h-8 rounded-full object-cover mt-2"}
      />
      <div className={"w-full flex flex-col gap-2"}>
        <div
          className={
            "full-card-schema !p-3 !border-[1px] dark:hover:border-gray-700"
          }
        >
          {/*Avatar*/}
          <div className={"flex items-center justify-between"}>
            <div className={"flex items-center gap-1"}>
              <UserPreviewHoverCard userId={comment.userContent.userId}>
                <div
                  className={
                    "rounded-md dark:text-gray-300 p-1 hover:bg-border-clr hover:dark:bg-dark-hover-bg-clr smooth-animation"
                  }
                >
                  <span className={" font-semibold cursor-pointer"}>
                    {comment.userContent.name}
                  </span>
                </div>
              </UserPreviewHoverCard>
              {isAuthorAnswer && (
                <TooltipContainer tooltip={"Author"}>
                  <div className={"dark:text-gray-300"}>
                    <SlBadge />
                  </div>
                </TooltipContainer>
              )}
              <DotDivider size={20} />
              <span className={"text-sm text-second-text dark:text-gray-500"}>
                {formatDate(comment.updateTime)}
              </span>
            </div>
            {signedIn && dropdownItems && dropdownItems.length > 0 && (
              <MoreOptions items={dropdownItems} />
            )}
          </div>
          <div className={"pl-1"}>
            <MarkdownViewer source={body} />
          </div>
          {/*Body*/}
        </div>
        <LikeButton
          contentType={"comments"}
          contentId={comment.id}
          vote={comment.voteDTO}
        />
      </div>
    </div>
  );
};

Comment.propTypes = {
  comment: PropTypes.object,
  authorId: PropTypes.string,
  onDelete: PropTypes.func,
};

export default Comment;
