import PropTypes from "prop-types";
import { useUserSignedIn } from "../../../../utils/auth.js";
import { useUser } from "../../../../utils/user.js";
import { useState } from "react";
import { getUtcNowDataFormat } from "../../../../utils/common.js";
import Comment from "../comment/index.js";
import WriteComment from "../write-comment/index.js";

const CommentArea = ({ articleId, authorId, comments }) => {
  const signedIn = useUserSignedIn();

  const { id, name, avatar } = useUser();
  const [localComments, setLocalComments] = useState(comments);

  const handleCommentPublished = (comment) => {
    const newComment = { ...comment };
    newComment.voteDTO = { voteCount: 0, status: "UnVote" };
    newComment.updateTime = getUtcNowDataFormat();
    newComment.userContent = {
      userId: id,
      name: name,
      profilePhoto: avatar,
    };
    const newComments = [newComment, ...localComments];
    setLocalComments(newComments);
  };

  const handleOnDelete = (id) => {
    const newComments = [...localComments];

    setLocalComments(newComments.filter((a) => a.id !== id));
  };

  return (
    <div
      className={
        "flex flex-col gap-6 card-size-schema !border-t-[1px] !border-transparent  card-color-schema  !rounded-t-none"
      }
    >
      <span className={"dark:text-gray-300 text-2xl font-semibold"}>
        Total Comments ({localComments?.length ?? 0})
      </span>

      {signedIn && (
        <WriteComment
          articleId={articleId}
          onCommentPublished={handleCommentPublished}
        />
      )}

      <div className={"flex flex-col gap-6"}>
        {localComments &&
          localComments.map((comment) => (
            <Comment
              key={comment.id}
              comment={comment}
              authorId={authorId}
              onDelete={handleOnDelete}
            />
          ))}
      </div>
    </div>
  );
};

CommentArea.propTypes = {
  articleId: PropTypes.string,
  authorId: PropTypes.string,
  comments: PropTypes.array,
};
export default CommentArea;
