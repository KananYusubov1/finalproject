import Answer from "../answer/index.js";
import PropTypes from "prop-types";
import { Badge } from "@material-tailwind/react";
import WriteAnswer from "../write-answer/index.js";
import { useUserSignedIn } from "../../../../utils/auth.js";
import { useState } from "react";
import { getUtcNowDataFormat } from "../../../../utils/common.js";
import { useUser } from "../../../../utils/user.js";

const AnswerArea = ({ questionId, answers, correctAnswerId, authorId }) => {
  const signedIn = useUserSignedIn();

  console.log(questionId);

  const { id, name, avatar } = useUser();

  const [localAnswers, setLocalAnswers] = useState(answers);

  const handleAnswerPublished = (answer) => {
    const newAnswer = { ...answer };
    newAnswer.voteDTO = { voteCount: 0, status: "UnVote" };
    newAnswer.updateTime = getUtcNowDataFormat();
    newAnswer.userContent = {
      userId: id,
      name: name,
      profilePhoto: avatar,
    };
    const newAnswers = [newAnswer, ...localAnswers];
    setLocalAnswers(newAnswers);
  };

  const handleOnDelete = (id) => {
    const newAnswers = [...localAnswers];

    setLocalAnswers(newAnswers.filter((a) => a.id !== id));
  };

  return (
    <div
      className={
        "flex flex-col gap-12 card-size-schema   card-color-schema !border-t-[1px] !rounded-t-none"
      }
    >
      <span className={"dark:text-gray-300 text-2xl font-semibold"}>
        Total Answers ({localAnswers?.length ?? 0})
      </span>

      {signedIn && (
        <WriteAnswer
          questionId={questionId}
          onAnswerPublished={handleAnswerPublished}
        />
      )}

      {correctAnswerId && (
        <Badge
          placement="top-start"
          content={"Correct Answer"}
          className={
            "w-fit rounded-md bg-green-400 dark:bg-green-500 px-6 py-1 min-w-[20px] min-h-[20px] top-[-2%] left-[14%]"
          }
        >
          <Answer
            answer={localAnswers.find((a) => a.id === correctAnswerId)}
            authorId={authorId}
            correctAnswerId={correctAnswerId}
            questionId={questionId}
          />
        </Badge>
      )}

      <div className={"flex flex-col gap-6"}>
        {localAnswers &&
          localAnswers
            .filter((answer) => answer.id !== correctAnswerId)
            .map((answer) => (
              <Answer
                key={answer.id}
                answer={answer}
                authorId={authorId}
                correctAnswerId={correctAnswerId}
                questionId={questionId}
                onDelete={handleOnDelete}
              />
            ))}
      </div>
    </div>
  );
};

AnswerArea.propTypes = {
  answers: PropTypes.array,
  correctAnswerId: PropTypes.string,
  questionId: PropTypes.string,
  authorId: PropTypes.string,
};

export default AnswerArea;
