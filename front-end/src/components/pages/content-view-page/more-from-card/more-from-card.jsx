import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import { ThreeDots } from "react-loader-spinner";
import { Link } from "react-router-dom";
import { nanoid } from "nanoid";
import category from "../../categories-page/category/index.js";
import { getMoreFromArticle } from "../../../../services/article.js";

const MoreFromCard = ({ userId, name }) => {
  const [articles, setArticles] = useState();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    console.log(userId);
    if (!userId) return;
    setLoading(true);
    getMoreFromArticle(userId)
      .then((res) => {
        setArticles(res);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [userId]);

  return (
    <div>
      {!loading ? (
        <div className={"w-80 !pb-2 !p-0 !flex-col !gap-1 full-card-schema"}>
          {/*Header*/}
          <div
            className={
              "flex items-center justify-between p-3 px-4 dark:text-white"
            }
          >
            {/*Title*/}
            <span className={"flex items-center gap-2 text-lg font-bold"}>
              More from
              <Link
                to={""}
                className={" text-accent dark:text-dark-text-accent"}
              >
                {name}
              </Link>
            </span>
            {/*See all*/}
          </div>

          <div className={"flex flex-col"}>
            {articles &&
              articles.length > 0 &&
              articles.map((item) => (
                <Link
                  to={`/articles/${item.id}`}
                  key={item.id}
                  className={
                    "group p-4 cursor-pointer border-t border-border-clr dark:border-dark-border-clr text-sm text-gray-700 dark:text-gray-400 flex flex-col gap-1"
                  }
                >
                  <p
                    className={
                      "group-hover:text-accent group-hover:dark:text-dark-text-accent hover:dark:text-dark-text-accent"
                    }
                  >
                    {item.title}
                  </p>
                  <span className={"text-xs flex items-center gap-2"}>
                    {item.categories.map((category) => (
                      <p key={nanoid()}>#{category.title}</p>
                    ))}
                    <p>{item.flag.title}</p>
                  </span>
                </Link>
              ))}
          </div>
        </div>
      ) : (
        <div className={"!w-80 full-card-schema items-center"}>
          <ThreeDots
            height="24"
            width="24"
            color={"#3B49DF"}
            wrapperClass="radio-wrapper"
            radius="9"
            ariaLabel="three-dots-loading"
            wrapperStyle={{}}
            wrapperClassName=""
            visible={true}
          />
        </div>
      )}
    </div>
  );
};

MoreFromCard.propTypes = {
  userId: PropTypes.string,
  name: PropTypes.string,
};
export default MoreFromCard;
