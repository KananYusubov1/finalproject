import UserPreviewCard from "../../../common/user-preview-card/index.js";
import MoreFromCard from "../more-from-card/index.js";
import { useEffect, useState } from "react";
import { getPreviewUserById } from "../../../../services/user.js";
import PropTypes from "prop-types";
import { useMediaQuery } from "react-responsive";

const ContentViewSideBar = ({ userId, userData = null }) => {
  const [user, setUser] = useState(userData);

  const isSmall = useMediaQuery({ query: "(max-width: 1024px)" });

  useEffect(() => {
    if (isSmall) return;

    if (!userData)
      getPreviewUserById(userId)
        .then((res) => {
          console.log(res);
          setUser(res);
        })
        .catch((error) => {
          console.log(error);
        });
  }, []);

  if (isSmall) return null;

  return (
    <div className={"w-[400px] flex flex-col gap-5"}>
      <UserPreviewCard user={user} />
      <MoreFromCard userId={userId} name={user.name} />
    </div>
  );
};

ContentViewSideBar.propTypes = {
  userId: PropTypes.string,
  userData: PropTypes.object,
};

export default ContentViewSideBar;
