import VerticalVoteContainer from "../../../common/vote-container/index.js";
import IconButton from "../../../buttons/icon-button/index.js";
import { FaRegComment } from "react-icons/fa";
import TooltipContainer from "../../../common/tooltip-container/index.js";
import SaveButton from "../../../buttons/save-button/index.js";
import MoreOptions from "../../../common/more-options/index.js";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import PropTypes from "prop-types";
import { getFieldsForContentViewAction } from "../../../../utils/common.js";
import { generateOptionsForContentViewAction } from "../../../../utils/commonForComponent.jsx";
import { useUser } from "../../../../utils/user.js";
import { RiQuestionAnswerLine } from "react-icons/ri";
import WatchButton from "../../../buttons/watch-button/index.js";
import { useMediaQuery } from "react-responsive";

const ContentViewActionBar = ({ content, type, id }) => {
  const {
    vote,
    isSaved,
    actionCount,
    watchCount,
    userId,
    saveCount,
    isWatched,
  } = getFieldsForContentViewAction(content, type);

  const [dropdownItems, setDropdownItems] = useState([]);
  const navigate = useNavigate();

  const currentId = useUser().id;

  const isSmall = useMediaQuery({ query: "(min-width: 767px)" });

  useEffect(() => {
    if (!isSmall) return;

    const options = generateOptionsForContentViewAction(
      content,
      type,
      id,
      currentId,
      userId,
      navigate
    );

    setDropdownItems(options);
  }, []);

  if (!isSmall) return null;

  return (
    <div
      className={"h-fit w-26 flex flex-col gap-4 items-center sticky top-36 "}
    >
      <VerticalVoteContainer
        contentId={id}
        contentType={type}
        voteStatus={vote.status}
        voteCount={vote.voteCount}
        contentViewMode={true}
      />

      {type === "articles" ? (
        <TooltipContainer tooltip={"Comments"}>
          <IconButton
            icon={<FaRegComment />}
            title={actionCount}
            verticalMode={true}
            size={20}
            click={() => {
              document.getElementById("conversation-area").scrollIntoView({
                behavior: "smooth",
              });
            }}
          />
        </TooltipContainer>
      ) : (
        <TooltipContainer tooltip={"Answers"}>
          <IconButton
            icon={<RiQuestionAnswerLine />}
            title={actionCount}
            verticalMode={true}
            size={20}
            click={() => {
              document.getElementById("conversation-area").scrollIntoView({
                behavior: "smooth",
              });
            }}
          />
        </TooltipContainer>
      )}

      {type === "questions" && (
        <WatchButton
          verticalMode={true}
          contentId={id}
          isWatched={isWatched}
          watchCount={watchCount}
        />
      )}

      <SaveButton
        verticalMode={true}
        saveCount={saveCount}
        contentType={type}
        contentId={id}
        isSaved={isSaved}
      />
      <MoreOptions items={dropdownItems} />
    </div>
  );
};

ContentViewActionBar.propTypes = {
  type: PropTypes.string,
  id: PropTypes.string,
  content: PropTypes.object,
};

export default ContentViewActionBar;
