import { useUser } from "../../../../utils/user.js";
import MarkdownEditor from "../../../common/markdown-editor/index.js";
import { useState } from "react";
import { Form, Formik } from "formik";
import PrimaryAccentButton from "../../../buttons/primary-accent-button/index.js";
import { insertAnswer } from "../../../../services/contentCommon.js";
import { errorToast, successToast } from "../../../../utils/toast.js";
import PropTypes from "prop-types";
import { ThreeDots } from "react-loader-spinner";

const WriteAnswer = ({ questionId, onAnswerPublished }) => {
  const user = useUser();

  const [isLoading, setIsLoading] = useState(false);

  return (
    <div>
      <div className={"w-full  !flex justify-center "}>
        <Formik
          initialValues={{ body: "" }}
          onSubmit={(values, { resetForm }) => {
            setIsLoading(true);
            insertAnswer(questionId, values.body)
              .then((answerId) => {
                successToast("Answer submitted successfully");
                onAnswerPublished({ id: answerId.result, body: values.body });
                resetForm();
              })
              .catch(() => {
                errorToast("A problem accrued, please try again later");
              })
              .finally(() => {
                setIsLoading(false);
              });
          }}
        >
          {({ values }) => (
            <Form className={" flex  gap-4 w-full p-3"}>
              {/*Avatar*/}
              <img
                src={user.avatar}
                alt="Avatar"
                className={"w-8 h-8 rounded-full object-cover mt-4"}
              />
              <div className={"w-full  flex flex-col gap-2"}>
                <div className={"h-[200px]"}>
                  <MarkdownEditor
                    size={200}
                    name={"body"}
                    placeholder={"Write your answer here..."}
                  />
                </div>

                <div className={"w-fit "}>
                  {isLoading ? (
                    <div className={"w-16 flex items-center justify-center"}>
                      <ThreeDots
                        height="24"
                        width="24"
                        color={"#3B49DF"}
                        wrapperClass="radio-wrapper"
                        radius="9"
                        ariaLabel="three-dots-loading"
                        wrapperStyle={{}}
                        wrapperClassName=""
                        visible={true}
                      />
                    </div>
                  ) : (
                    <PrimaryAccentButton
                      type={"submit"}
                      title={"Submit"}
                      disabled={!values.body}
                    />
                  )}
                </div>
              </div>
            </Form>
          )}
        </Formik>
        {/*Body*/}
      </div>
    </div>
  );
};

WriteAnswer.propTypes = {
  questionId: PropTypes.string,
  onAnswerPublished: PropTypes.func,
};

export default WriteAnswer;
