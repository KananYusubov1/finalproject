import PropTypes from "prop-types";
import { IconContext } from "react-icons";
import classNames from "classnames";

const UserProfileViewInfo = ({ icon, data, type }) => {
  return (
    data && (
      <div
        className={
          "flex items-center gap-2 text-dark-text dark:text-gray-600 smooth-animation"
        }
      >
        <IconContext.Provider value={{ size: 24 }}>{icon}</IconContext.Provider>
        {type === "url" || type === "mail" ? (
          <a
            href={type === "mail" ? `mailto:${data}` : data}
            target={"_blank"}
            rel="noreferrer"
            className={"hover:text-dark-text-accent    underline-animation"}
          >
            {data}
          </a>
        ) : (
          <span>{data}</span>
        )}
      </div>
    )
  );
};

UserProfileViewInfo.propTypes = {
  icon: PropTypes.element,
  data: PropTypes.string,
  type: PropTypes.string,
};

export default UserProfileViewInfo;
