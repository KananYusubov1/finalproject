import PropTypes from "prop-types";
import classNames from "classnames";

const UserProfileViewPageAsideInfo = ({ title, data, customData }) => {
  if (!data && !customData) return null;
  return (
    <div className={"rounded-md card-shadow-schema"}>
      {title && (
        <div
          className={
            "p-4 card-color-schema !border-b-0 !rounded-b-none font-bold dark:text-gray-300 "
          }
        >
          {title}
        </div>
      )}

      <div
        className={classNames({
          "flex flex-col gap-4 p-4  card-color-schema ": true,
          "!border-t-[1px] !rounded-t-none": title,
        })}
      >
        {customData ? (
          customData
        ) : (
          <p className={"text-second-text dark:text-gray-400"}>{data}</p>
        )}
      </div>
    </div>
  );
};

UserProfileViewPageAsideInfo.propTypes = {
  title: PropTypes.string,
  data: PropTypes.string,
  customData: PropTypes.element,
};

export default UserProfileViewPageAsideInfo;
