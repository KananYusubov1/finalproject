import PropTypes from "prop-types";

const UserProfileViewSecondaryInfo = ({ label, data }) => {
  if (data.length === 0) return null;
  return (
    <div className={"flex flex-col gap-1 items-center"}>
      <span
        className={"font-semibold text-dark-text dark:text-gray-600 text-sm"}
      >
        {label}
      </span>
      <span className={" dark:text-gray-500"}>{data}</span>
    </div>
  );
};

UserProfileViewSecondaryInfo.propTypes = {
  label: PropTypes.string,
  data: PropTypes.string,
};

export default UserProfileViewSecondaryInfo;
