import PropTypes from "prop-types";
import { IconContext } from "react-icons";

const UserProfileViewPageAsideCountInfo = ({ icon, data, label }) => {
  return (
    <div
      className={"flex items-center gap-2 text-second-text dark:text-gray-400"}
    >
      {/*icon*/}
      <IconContext.Provider value={{ size: 20 }}>{icon}</IconContext.Provider>
      <div>
        {data} <span>{label}</span>
      </div>
    </div>
  );
};

UserProfileViewPageAsideCountInfo.propTypes = {
  icon: PropTypes.elementType,
  data: PropTypes.number,
  label: PropTypes.string,
};

export default UserProfileViewPageAsideCountInfo;
