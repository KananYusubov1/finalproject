import PropTypes from "prop-types";
import UserProfileViewPageAsideInfo from "../user-profile-view-page-aside-info/index.js";
import { HiOutlineNewspaper } from "react-icons/hi";
import UserProfileViewPageAsideCountInfo from "../user-profile-view-page-aside-count-info/index.js";
import { HiMiniHashtag } from "react-icons/hi2";
import UserProfileViewRepos from "../user-profile-view-repos/index.js";
import { TbMessage2Question } from "react-icons/tb";
import { RiQuestionAnswerLine } from "react-icons/ri";

const UserProfileViewPageAside = ({ user }) => {
  return (
    <div className={"flex flex-col gap-4 pb-3 h-[500px]"}>
      <UserProfileViewRepos
        repos={user.personalInfo?.pinnedRepositories ?? []}
      />

      <UserProfileViewPageAsideInfo
        title={"Available For"}
        data={user.personalInfo?.availableFor}
      />

      <UserProfileViewPageAsideInfo
        title={"Currently Hacking On"}
        data={user.personalInfo?.currentlyWorking}
      />

      <UserProfileViewPageAsideInfo
        title={"Currently Learning"}
        data={user.personalInfo?.currentlyLearning}
      />

      <UserProfileViewPageAsideInfo
        customData={
          <div className={"flex flex-col gap-2"}>
            <UserProfileViewPageAsideCountInfo
              icon={<HiMiniHashtag />}
              data={user.followedCategoryCount}
              label={"tags followed"}
            />
            <UserProfileViewPageAsideCountInfo
              icon={<HiOutlineNewspaper />}
              data={user.publishedArticlesCount}
              label={"posts published"}
            />
            {user.publishedQuestionCount ? (
              <UserProfileViewPageAsideCountInfo
                icon={<TbMessage2Question />}
                data={user.publishedQuestionCount}
                label={"question asked"}
              />
            ) : null}
            {user.publishedAnswerCount ? (
              <UserProfileViewPageAsideCountInfo
                icon={<RiQuestionAnswerLine />}
                data={user.publishedAnswerCount}
                label={"question answered"}
              />
            ) : null}
          </div>
        }
      />
    </div>
  );
};

UserProfileViewPageAside.propTypes = {
  user: PropTypes.object,
};

export default UserProfileViewPageAside;
