import PropTypes from "prop-types";
import DotDivider from "../../../common/dot-divider/index.js";

const UserProfileViewPageAsideRepo = ({ repo }) => {
  // title: "github.com/efpage",
  // description: "Some description",
  // language: "CSharp",
  // starCount: 141234,
  // url: "https://github.com/efpage",
  const { url, name, description, topProgrammingLanguage, stars, fork } = repo;
  return (
    <a
      href={url}
      target={"_blank"}
      className={
        "group w-full h-full  text-second-text dark:text-gray-400 flex flex-col gap-1 border-t border-border-clr dark:border-dark-border-clr p-4"
      }
      rel="noreferrer"
    >
      {/*Title*/}
      <span
        className={
          "group-hover:text-accent group-hover:dark:text-dark-text-accent text-black dark:text-gray-300 font-bold smooth-animation"
        }
      >
        {name}
      </span>
      <p className={"text-sm"}>{description}</p>
      <div className={"flex items-center gap-2 text-xs"}>
        {fork && (
          <span
            className={
              "text-sm bg-gray-500 dark:bg-gray-700 dark:text-gray-400 rounded-md  px-1 text-black"
            }
          >
            fork
          </span>
        )}
        <span className={""}>{topProgrammingLanguage}</span>

        {stars ? (
          <>
            <DotDivider size={14} />
            <span className={""}>{stars}</span>
          </>
        ) : null}
      </div>
    </a>
  );
};

UserProfileViewPageAsideRepo.propTypes = {
  repo: PropTypes.object,
};
export default UserProfileViewPageAsideRepo;
