import classNames from "classnames";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import BuyCheckoutInfo from "../../../common/buy-checkout-info/index.js";
import DotDivider from "../../../common/dot-divider/index.js";
import { formatDate } from "../../../../utils/common.js";

const BuyNotification = ({ data }) => {
  const body = JSON.parse(data.body);
  const product = body.Product;
  return (
    <div
      className={classNames({
        "full-card-schema ": true,
        "!border-l-4 !border-l-accent": !data.isViewed,
      })}
    >
      <div className={"flex items-center gap-1"}>
        <p>You purchased </p>
        <Link
          to={`/shop/${product.Id}`}
          className={
            "text-lg font-bold dark:text-gray-300 hover:text-accent hover:dark:text-dark-text-accent"
          }
        >
          {product.Name}
        </Link>
        <DotDivider size={20} />
        <p>{formatDate(data.createdAt)}</p>
      </div>
      <div
        className={
          " dark:bg-dark-component-second-bg card-border-color-schema card-size-schema !flex-row !p-2"
        }
      >
        <img
          src={product.Photos[0].Path}
          alt="product"
          className={"w-28 h-28 object-cover rounded-md"}
        />
        <div className={"w-full"}>
          <BuyCheckoutInfo label={"Price"} value={product.Price} />
          <BuyCheckoutInfo label={"Discount"} value={product.Discount} />
          <BuyCheckoutInfo
            label={"Discounted Price"}
            value={product.DiscountedPrice}
          />
          <BuyCheckoutInfo
            label={"Purchase Quantity"}
            value={body.PurchaseQuantity}
          />

          <BuyCheckoutInfo label={"Total Sum"} value={body.TotalSum} />
        </div>
      </div>
    </div>
  );
};

BuyNotification.propTypes = {
  data: PropTypes.object,
};

export default BuyNotification;
