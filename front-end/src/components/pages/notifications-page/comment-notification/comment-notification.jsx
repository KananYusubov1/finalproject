import PropTypes from "prop-types";
import classNames from "classnames";
import { Link } from "react-router-dom";
import { formatDate } from "../../../../utils/common.js";
import MarkdownViewer from "../../../markdown-viewer/index.js";

const CommentNotification = ({ data }) => {
  const comment = data.content.commentDTO;
  return (
    <div
      className={classNames({
        "full-card-schema !flex-row justify-between items-center  ": true,
        "!border-l-4 !border-l-accent": !data.isViewed,
      })}
    >
      <div className={"w-full flex  gap-3"}>
        <img
          src={comment.userContent.profilePhoto}
          alt="avatar"
          className={"w-12 h-12 rounded-full object-cover mt-1"}
        />
        <div className={"w-full flex flex-col gap-2"}>
          <div className={"flex flex-col"}>
            <div className={"flex items-center gap-2"}>
              <Link
                to={`/${comment.userContent.username}`}
                className={
                  "text-lg font-bold dark:text-gray-300 hover:text-accent hover:dark:text-dark-text-accent"
                }
              >
                {comment.userContent.name}
              </Link>
              <p>commented on</p>
              <Link
                to={`/questions/${data.content.articleDTO.id}`}
                className={
                  "text-lg font-bold dark:text-gray-300 hover:text-accent hover:dark:text-dark-text-accent"
                }
              >
                {data.content.articleDTO.header.title}
              </Link>
            </div>
            <p>{formatDate(data.createdAt)}</p>
          </div>
          <span
            className={
              "w-full rounded-md border-[1px] border-border-clr dark:border-dark-border-clr card-size-schema"
            }
          >
            <MarkdownViewer source={comment.body} />
          </span>
        </div>
      </div>
    </div>
  );
};

CommentNotification.propTypes = {
  data: PropTypes.object,
};

export default CommentNotification;
