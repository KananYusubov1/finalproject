import StepperStat from "../../../common/stepper-stat/index.js";
import classNames from "classnames";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import BuildProfileProfilePhoto from "../build-profile-profile-photo/index.js";
import PropTypes from "prop-types";
import { Form, Formik } from "formik";
import TextInput from "../../../form/text-input/index.js";
import TextAreaInput from "../../../form/text-area-input/index.js";
import ErrorView from "../../../common/error-view/index.js";
import { useEffect, useState } from "react";
import { AiOutlineArrowLeft } from "react-icons/ai";
import { UserService } from "../../../../services";
import { UserBuildProfileSchema } from "../../../../validation/Schemas/UserBuildProfileSchema.js";
import {
  saveBuildFormValues,
  useBuildFormValues,
} from "../../../../utils/process.js";
import InlineLoader from "../../../loaders/inline-loader/index.js";
import { Radio } from "react-loader-spinner";
import UrlInput from "../../../form/url-input/index.js";

const BuildProfileStep = ({ onStepForward, onStepBack }) => {
  const [animationParent] = useAutoAnimate();

  const [userName, setUserName] = useState("");

  const [userNameIsAvailable, setUserNameIsAvailable] = useState(false);
  const [userNameErrorMessage, setUserNameErrorMessage] = useState("");

  const projectUrlPaths = [
    "settings",
    "manage",
    "explore",
    "form ",
    "articles",
    "categories",
    "code-of-conduct",
    "contact",
    "privacy",
    "terms",
    "leaders",
    "about",
    "faq",
    "dashboard",
  ];

  useEffect(() => {
    setUserNameIsAvailable(true);
    setUserNameErrorMessage("");

    if (userName.length <= 2) return;

    if (projectUrlPaths.includes(userName)) {
      setUserNameIsAvailable(false);
      setUserNameErrorMessage("Username can't be taken");
      return;
    }

    setTimeout(() => {
      UserService.userExistsByUsername(userName)
        .then(() => {
          setUserNameIsAvailable(false);
          setUserNameErrorMessage("Username is already taken");
        })
        .catch(() => {
          setUserNameIsAvailable(true);
        });
    }, 1000);
  }, [userName]);

  return (
    <div className={"bg-white rounded-lg"} ref={animationParent}>
      <Formik
        initialValues={
          useBuildFormValues() ?? {
            userName: "",
            profilePhoto: "",
            bio: "",
            websiteUrl: "",
            work: "",
            education: "",
          }
        }
        onSubmit={(values) => {
          if (!userNameIsAvailable) return;

          let data = values;

          saveBuildFormValues(data);
          onStepForward(data);
        }}
        validationSchema={UserBuildProfileSchema}
      >
        {({ values }) => {
          setUserName(values.userName);
          return (
            <>
              <Form>
                <div
                  className={
                    "py-4 px-12 flex items-center justify-between  border-b-2 border-b-black"
                  }
                  ref={animationParent}
                >
                  <button
                    type={"button"}
                    onClick={() => {
                      saveBuildFormValues(values);
                      onStepBack();
                    }}
                    className={
                      "w-[190px]  flex items-center self-center gap-4 hover:bg-hover-blue-clr p-2 rounded-lg cursor-pointer smooth-animation"
                    }
                  >
                    {/*Icon*/}
                    <AiOutlineArrowLeft />

                    <p>Back to Categories</p>
                  </button>

                  <StepperStat totalStep={2} currentStep={2} />

                  <div className={"flex justify-end"} ref={animationParent}>
                    <button
                      type="submit"
                      className={classNames({
                        "py-[10px] w-[170px] self-center px-10 text-black  font-medium   rounded-lg text-sm transition-all duration-500": true,
                        "bg-accent hover:bg-blue-800 text-white": true,
                        "bg-[#d6d6d6] hover:bg-[#a3a3a3] ": false,
                      })}
                    >
                      <p>Build Profile</p>
                    </button>
                  </div>
                </div>
                <div className={"bg-black h-[1px] w-full"}></div>
                <div className={"w-[780px] py-4 px-12"} ref={animationParent}>
                  {/*Container*/}
                  <div
                    className={
                      "flex flex-col gap-10 overflow-x-hidden overflow-scroll"
                    }
                    ref={animationParent}
                  >
                    {/*Header*/}
                    <div>
                      {/*Title*/}
                      <div className={"flex flex-col gap-1"}>
                        <p className={"font-extrabold text-3xl"}>
                          Build your profile
                        </p>
                        <p className={"font-medium text-xl"}>
                          Tell us a little bit about yourself — this is how
                          others will see you on Clubrick Community.
                          You&lsquo;ll always be able to edit this later in your
                          Settings.
                        </p>
                      </div>
                    </div>
                    <div
                      className={" w-full h-[410px] pr-2 py-2"}
                      ref={animationParent}
                    >
                      <div className={"flex flex-col gap-5 p-1"}>
                        <BuildProfileProfilePhoto name={"profilePhoto"} />

                        <TextInput
                          isRequired={true}
                          name={"userName"}
                          title={"Username"}
                          placeholder={"Enter your username"}
                          autoComplete={"username"}
                        />
                        {userNameErrorMessage && (
                          <ErrorView message={userNameErrorMessage} />
                        )}

                        <TextAreaInput
                          name={"bio"}
                          title={"Bio"}
                          maxLength={200}
                          placeholder={"Tell us a little about yourself"}
                        />

                        <UrlInput
                          name={"websiteUrl"}
                          title={"Website Url"}
                          placeholder={"Enter your website url"}
                          autoComplete={"url"}
                        />

                        <div className={"flex flex-col gap-3"}>
                          {/*Header*/}
                          <p className={"font-bold text-2xl"}>Work</p>
                          <TextInput
                            name={"work"}
                            title={"Work"}
                            placeholder={
                              "What do you do? Example: CEO at Google Inc."
                            }
                            autoComplete={"organization-title"}
                          />
                          <TextInput
                            name={"education"}
                            title={"Education"}
                            placeholder={
                              "Where did you go to school? Example: Student at Step It Academy"
                            }
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Form>
            </>
          );
        }}
      </Formik>
    </div>
  );
};

BuildProfileStep.propTypes = {
  onStepForward: PropTypes.func,
  onStepBack: PropTypes.func,
};

export default BuildProfileStep;
