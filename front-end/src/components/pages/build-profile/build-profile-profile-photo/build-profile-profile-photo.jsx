import PropTypes from "prop-types";
import { ErrorMessage, useField } from "formik";
import { useEffect, useState } from "react";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import FirstAvatarEditor from "../first-avatar-editor/index.js";
import ProviderAvatarSelect from "../provider-avatar-select/index.js";
import { useUserAvatar } from "../../../../utils/auth.js";

const BuildProfileProfilePhoto = ({ name }) => {
  const [field, meta] = useField(name);
  const [animationParent] = useAutoAnimate();

  const [avatar, setAvatar] = useState();

  const userAvatarUrl = useUserAvatar();

  const handleAvatarChange = (newAvatar) => {
    setAvatar(newAvatar);
  };

  useEffect(() => {
    field.onChange({ target: { name: name, value: avatar } });
  }, [avatar]);

  return (
    <div className={"flex justify-center"} ref={animationParent}>
      <ErrorMessage
        name={field.name}
        component={"small"}
        className={"text-xs block text-red-700"}
      />
      {userAvatarUrl ? (
        <ProviderAvatarSelect
          avatarUrl={userAvatarUrl}
          avatarChanged={handleAvatarChange}
          error={meta.error}
        />
      ) : (
        <FirstAvatarEditor avatarChanged={handleAvatarChange} />
      )}
    </div>
  );
};

BuildProfileProfilePhoto.propTypes = {
  name: PropTypes.string,
};

export default BuildProfileProfilePhoto;
