import PropTypes from "prop-types";
import { createModal } from "../../../../utils/modal.js";
import { createAvatar } from "@dicebear/core";
import { botttsNeutral } from "@dicebear/collection";
import { useName } from "../../../../utils/auth.js";
import { useEffect, useState } from "react";
import {
  saveBuildFormFirstAvatarValues,
  useBuildFormFirstAvatarValues,
} from "../../../../utils/process.js";

const FirstAvatarEditor = ({ avatarChanged }) => {
  const [avatar, setAvatar] = useState();
  const userName = useName();

  const avatarState = useBuildFormFirstAvatarValues();

  useEffect(() => {
    if (avatarState) {
      setAvatar(avatarState);
    } else {
      const diceAvatar = createAvatar(botttsNeutral, {
        seed: userName,
        radius: 50,
      });

      diceAvatar
        .jpeg()
        .toDataUri()
        .then((uri) => setAvatar(uri));
    }
  }, []);

  useEffect(() => {
    saveBuildFormFirstAvatarValues(avatar);
    avatarChanged(avatar);
  }, [avatar]);

  const handleSetAvatar = (newAvatar) => {
    setAvatar(newAvatar);
  };

  return (
    <div className={"flex gap-4"}>
      {/*Profile Photo*/}
      <div>
        <img
          src={avatar}
          alt="avatar"
          className={"w-20 h-20 rounded-full object-cover"}
        />
      </div>

      <div className={"flex flex-col items-center gap-2"}>
        {/*Name*/}
        <p className={"text-xl"}>{userName}</p>

        {/*Edit image*/}
        <button
          type={"button"}
          onClick={() => {
            createModal("editProfilePhoto", handleSetAvatar);
          }}
          className={
            "p-2 px-3  text-black  font-medium rounded-lg transition-all duration-500 bg-[#d6d6d6] hover:bg-[#a3a3a3] "
          }
        >
          <p>Edit profile image</p>
        </button>
      </div>
    </div>
  );
};

FirstAvatarEditor.propTypes = {
  avatarChanged: PropTypes.func,
};

export default FirstAvatarEditor;
