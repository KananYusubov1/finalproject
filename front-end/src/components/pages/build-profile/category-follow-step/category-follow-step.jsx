import { useEffect, useState } from "react";
import BuildProfileCategoryFollow from "../build-profile-category-follow/index.js";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import StepperStat from "../../../common/stepper-stat/index.js";
import classNames from "classnames";
import PropTypes from "prop-types";
import { TagService } from "../../../../services";
import {
  saveFollowedCategories,
  useFollowedCategories,
} from "../../../../utils/process.js";
import CategoryBuildProfileSkeleton from "../../../skeleton-loaders/category-build-profile-skeleton/index.js";

const CategoryFollowStep = ({ onStepForward }) => {
  const [animationParent] = useAutoAnimate();
  const [isLoading, setIsLoading] = useState(false);

  const [categories, setCategories] = useState([]);
  const [followedCount, setFollowedCount] = useState(0);

  const followedCategories = useFollowedCategories();

  const handleCategoryFollowChange = (title) => {
    const updatedCategories = categories.map((category) =>
      category.title === title
        ? { ...category, isFollowed: !category.isFollowed }
        : category
    );

    setCategories(updatedCategories);
  };

  function addNewPropToEachElement(array, propName, propValue) {
    return array.map((item) => ({
      ...item,
      [propName]: propValue,
    }));
  }

  useEffect(() => {
    setIsLoading(true);
    TagService.getAllBuildCategory()
      .then((res) => {
        const newArray = addNewPropToEachElement(res, "isFollowed", false);
        setCategories(newArray);

        followedCategories.forEach((followedCategory) => {
          const matchedCategory = newArray.find(
            (category) => category.title === followedCategory.title
          );

          if (matchedCategory && !matchedCategory.isFollowed) {
            matchedCategory.isFollowed = true;
          }
        });

        setIsLoading(false);

        setCategories(newArray);
      })
      .catch((err) => {
        setIsLoading(false);
        console.log(err);
      });
  }, []);

  useEffect(() => {
    const followedCategories = categories.filter(
      (category) => category.isFollowed
    );
    setFollowedCount(followedCategories.length);
  }, [categories]);

  return (
    <div className={"bg-white rounded-lg"} ref={animationParent}>
      <div
        className={
          "py-4 px-12 grid grid-cols-2 items-center justify-end  border-b-2 border-b-black"
        }
        ref={animationParent}
      >
        <div className={"w-full flex justify-end"}>
          <StepperStat totalStep={2} currentStep={1} />
        </div>

        <div className={"w-full flex justify-end"} ref={animationParent}>
          <button
            onClick={() => {
              saveFollowedCategories(
                categories.filter((category) => category.isFollowed)
              );
              onStepForward();
            }}
            className={classNames({
              "py-[10px] w-[170px] self-center px-10 text-black  font-medium   rounded-lg text-sm transition-all duration-500": true,
              "bg-accent hover:bg-blue-800 text-white": followedCount,
              "bg-[#d6d6d6] hover:bg-[#a3a3a3] ": !followedCount,
            })}
          >
            {!followedCount ? <p>Skip for now</p> : <p>Continue</p>}
          </button>
        </div>
      </div>
      <div className={"bg-black h-[1px] w-full"}></div>
      <div className={"py-4 px-12"} ref={animationParent}>
        {/*Stepper*/}
        {/*Header*/}
        <div></div>
        {/*Container*/}
        <div
          className={"flex flex-col gap-4 overflow-x-hidden overflow-scroll"}
          ref={animationParent}
        >
          {/*Header*/}
          <div>
            {/*Title*/}
            <div>
              <p className={"font-extrabold text-3xl"}>
                What are you interested in?
              </p>
              <p className={"font-medium text-xl"}>
                Follow tags to customize your feed
              </p>
            </div>
            {/*Stat*/}
            <div className={"flex items-center gap-1 text-gray-500 "}>
              <span>{followedCount}</span>
              <p>tags selected</p>
            </div>
          </div>
          <div
            className={
              " w-[660px] max-h-[410px]  flex items-center justify-center flex-wrap gap-4  "
            }
            ref={animationParent}
          >
            {isLoading ? (
              <div className={"flex "}>
                <CategoryBuildProfileSkeleton />
                <CategoryBuildProfileSkeleton />
                <CategoryBuildProfileSkeleton />
              </div>
            ) : (
              categories.map((tag) => (
                <BuildProfileCategoryFollow
                  key={tag.id}
                  tag={tag}
                  categoryFollowChange={handleCategoryFollowChange}
                />
              ))
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

CategoryFollowStep.propTypes = {
  onStepForward: PropTypes.func,
};

export default CategoryFollowStep;
