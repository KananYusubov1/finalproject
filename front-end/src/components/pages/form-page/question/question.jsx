import QuestionHeader from "../question-header/index.js";
import QuestionBody from "../question-body/index.js";
import QuestionFooter from "../question-footer/index.js";
import classNames from "classnames";
import { Badge } from "@material-tailwind/react";
import PropTypes from "prop-types";

const Question = ({ question, mode = "component" }) => {
  return (
    <Badge
      placement="top-start"
      content={"Solved"}
      invisible={!(question.status === "Solved")}
      className={
        "w-fit rounded-md bg-green-400 dark:bg-green-500 px-6 py-1 min-w-[20px] min-h-[20px] top-[0%] left-[5%]"
      }
    >
      <div
        className={classNames({
          " full-card-schema !p-0 !pb-3 !gap-1": true,
          "!border-2 !border-green-400 dark:!border-green-500":
            question.status === "Solved",
        })}
      >
        <QuestionHeader question={question} />
        <QuestionBody question={question} />
        {mode === "component" && (
          <QuestionFooter mode={mode} question={question} />
        )}
      </div>
    </Badge>
  );
};

Question.propTypes = {
  question: PropTypes.object,
  mode: PropTypes.string,
};

export default Question;
