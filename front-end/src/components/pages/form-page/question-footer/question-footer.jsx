import IconButton from "../../../buttons/icon-button/index.js";
import { RiExpandUpDownFill, RiQuestionAnswerLine } from "react-icons/ri";
import SaveButton from "../../../buttons/save-button/index.js";
import WatchButton from "../../../buttons/watch-button/index.js";
import PropTypes from "prop-types";
import { useUserSignedIn } from "../../../../utils/auth.js";
import { useNavigate } from "react-router-dom";

const QuestionFooter = ({ question, mode }) => {
  const { isSaved, isWatched, answerCount, voteDTO } = question;

  const url = `/questions/${question.id}`;

  const navigate = useNavigate();

  return (
    <div className={"w-full flex items-center justify-between px-6"}>
      <div className={" flex items-center gap-3"}>
        {/*Vote*/}
        {voteDTO.voteCount !== 0 && (
          <IconButton
            icon={<RiExpandUpDownFill />}
            title={`${voteDTO.voteCount} Votes`}
            click={() => {
              navigate(url);
            }}
          />
        )}
        {/*Comment*/}
        <IconButton
          icon={<RiQuestionAnswerLine />}
          title={answerCount > 0 ? `${answerCount} Answers` : "Add answers"}
          click={() => {
            navigate(url);
          }}
        />
      </div>

      <div className={"flex items-center gap-3"}>
        {/*Reading time*/}
        <span className={"text-sm dark:text-gray-400"}>
          {question.readingTime} min read
        </span>

        {/*Save*/}
        <SaveButton
          isSaved={isSaved}
          contentType={"questions"}
          contentId={question.id}
        />
        {/*Watch*/}
        <WatchButton isWatched={isWatched} contentId={question.id} />
      </div>
    </div>
  );
};

QuestionFooter.propTypes = {
  question: PropTypes.object,
  mode: PropTypes.string,
};

export default QuestionFooter;
