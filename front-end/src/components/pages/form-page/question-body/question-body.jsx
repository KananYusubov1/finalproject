import { Link } from "react-router-dom";
import classNames from "classnames";
import TagPreview from "../../../common/tag-preview/index.js";
import PropTypes from "prop-types";

const QuestionBody = ({ question }) => {
  const { title, isViewed } = question;
  return (
    <div className={" flex flex-col gap-1 px-8"}>
      {/*Title*/}
      <Link
        to={`/questions/${question.id}`}
        className={classNames({
          "!hover:text-accent dark:text-gray-300 font-bold text-3xl hover:text-accent hover:dark:text-dark-text-accent": true,
          "!hover:text-accent text-dark-text dark:text-gray-500": isViewed,
        })}
      >
        {question.header.title}
      </Link>
      {/*Categories & Flag*/}
      <div className={"flex gap-6"}>
        {question.questionCategories.map((category) => (
          <TagPreview
            key={category.id}
            title={category.title}
            colorId={category.accentColor}
          />
        ))}
      </div>
    </div>
  );
};

QuestionBody.propTypes = {
  question: PropTypes.object,
};
export default QuestionBody;
