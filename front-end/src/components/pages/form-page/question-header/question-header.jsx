import PropTypes from "prop-types";
import { formatDate } from "../../../../utils/common.js";
import UserPreviewHoverCard from "../../../common/user-preview-hover-card/index.js";

const QuestionHeader = ({ question }) => {
  return (
    <div className={""}>
      {/*Bar*/}
      <div
        className={
          "w-fit flex items-center gap-2  px-6 py-4 dark:text-gray-300 "
        }
      >
        {/*User*/}
        <img
          src={question.userContent.profilePhoto}
          alt="avatar"
          className={"w-9 h-9 rounded-full object-cover"}
        />
        <div className={"flex flex-col"}>
          <span className={"font-medium text-sm"}>
            <UserPreviewHoverCard userId={question.userContent.userId}>
              <div
                className={
                  "rounded-md dark:text-gray-300 p-1 hover:bg-border-clr hover:dark:bg-dark-hover-bg-clr smooth-animation"
                }
              >
                <span className={"font-medium cursor-pointer"}>
                  {question.userContent.name}
                </span>
              </div>
            </UserPreviewHoverCard>
          </span>
          <span className={"text-xs pl-1"}>
            {formatDate(question.updateTime)}
          </span>
        </div>
      </div>
    </div>
  );
};

QuestionHeader.propTypes = {
  question: PropTypes.object,
};
export default QuestionHeader;
