import PropTypes from "prop-types";
import PageFilterContainerItem from "../page-filter-container-item/index.js";
import { useEffect } from "react";

const PageFilterContainer = ({
  pageFilters,
  paramName,
  onSearch,
  defaultParam,
  setSearchParam,
  searchParams,
  isRemovable = false,
}) => {
  const handleOptionClick = (name) => {
    if (isRemovable) {
      const exist = searchParams.get(paramName) === name;
      console.log(`exist: ${exist}`);
      if (exist) {
        setSearchParam((prev) => {
          prev.set(paramName, "");
          return prev;
        });
      } else {
        setSearchParam((prev) => {
          prev.set(paramName, name);
          return prev;
        });
      }
    } else {
      setSearchParam((prev) => {
        prev.set(paramName, name);
        return prev;
      });
    }
    onSearch();
  };

  useEffect(() => {
    if (defaultParam && defaultParam.length > 0)
      handleOptionClick(defaultParam);
  }, [defaultParam]);

  return (
    <div className={"w-full flex flex-col gap-1"}>
      {pageFilters.map((pageFilter) => {
        return (
          <PageFilterContainerItem
            key={pageFilter.paramName}
            paramName={pageFilter.paramName}
            title={pageFilter.label}
            click={handleOptionClick}
            isActive={
              searchParams.get(paramName) === pageFilter.paramName ||
              (searchParams.get(paramName) === "" &&
                defaultParam === pageFilter.paramName)
            }
          />
        );
      })}
    </div>
  );
};

PageFilterContainer.propTypes = {
  pageFilters: PropTypes.array,
  paramName: PropTypes.string,
  onSearch: PropTypes.func,
  setSearchParam: PropTypes.func,
  searchParams: PropTypes.object,
  defaultParam: PropTypes.string,
  isRemovable: PropTypes.bool,
};

export default PageFilterContainer;
