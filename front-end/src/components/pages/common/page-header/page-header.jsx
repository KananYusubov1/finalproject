import PropTypes from "prop-types";
import { IconContext } from "react-icons";

const PageHeader = ({
  accentColor,
  title,
  description,
  actionSide,
  icon,
  iconName,
}) => {
  return (
    <div
      className={
        "w-full rounded-md border-[1px] border-t-[16px] border-border-clr bg-white dark:bg-dark-component-bg dark:border-dark-border-clr p-4 flex items-center justify-between gap-3"
      }
      style={{ borderTopColor: accentColor }}
    >
      <div className={"flex items-center gap-6"}>
        {iconName ? (
          <img src={`/${iconName}.png`} alt={title} className={"h-16"} />
        ) : icon ? (
          <IconContext.Provider
            value={{
              size: "62",
              color: accentColor,
            }}
          >
            {icon}
          </IconContext.Provider>
        ) : (
          <></>
        )}

        <div className={"flex flex-col gap-4"}>
          <p className={"text-3xl dark:text-gray-300 font-bold"}>{title}</p>
          <p className={"whitespace-pre text-second-text dark:text-gray-500"}>
            {description}
          </p>
        </div>
      </div>
      {actionSide && actionSide}
    </div>
  );
};

PageHeader.propTypes = {
  accentColor: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  actionSide: PropTypes.element,
  iconName: PropTypes.string,
  icon: PropTypes.element,
};

export default PageHeader;
