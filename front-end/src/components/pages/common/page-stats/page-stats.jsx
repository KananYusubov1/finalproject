import PropTypes from "prop-types";
import CountUp from "react-countup";

const PageStats = ({ stat, title }) => {
  return (
    <div
      className={
        "flex items-center pl-3 dark:text-gray-500 gap-1 text-[15px] font-bold text-second-text "
      }
      style={{ fontFamily: "Consolas" }}
    >
      <CountUp end={stat} separator={""} />
      <span>{title}</span>
    </div>
  );
};

PageStats.propTypes = {
  stat: PropTypes.number,
  title: PropTypes.string,
};

export default PageStats;
