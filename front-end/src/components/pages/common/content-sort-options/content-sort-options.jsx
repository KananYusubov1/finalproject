import FilterOption from "../../../common/filter-option/index.js";

const ContentSortOptions = () => {
  return (
    <div className={"w-full max-w-[500px] flex items-center justify-between"}>
      <div>
        <FilterOption title={"Relevant"} />
        <FilterOption title={"Latest"} isActive={true} />
        <FilterOption title={"Top"} />
      </div>
      <div className={"flex items-center gap-1 "}>
        <FilterOption title={"Week"} />
        <FilterOption title={"Month"} />
        <FilterOption title={"Year"} />
        <FilterOption title={"Infinity"} />
      </div>
    </div>
  );
};

export default ContentSortOptions;
