import PropTypes from "prop-types";

const PageGuidelines = ({ title, guidelines }) => {
  return (
    <div
      className={
        "flex flex-col gap-4 italic p-4 dark:text-gray-500 dark:border-dark-border-clr border-y-[2px] border-border-clr"
      }
    >
      <p className={"font-bold text-sm"} style={{ fontFamily: "monospace" }}>
        {title}
      </p>
      <p
        className={"whitespace-pre text-[15px]"}
        style={{ fontFamily: "Segoe UI" }}
      >
        {guidelines}
      </p>
    </div>
  );
};

PageGuidelines.propTypes = {
  title: PropTypes.string,
  guidelines: PropTypes.string,
};

export default PageGuidelines;
