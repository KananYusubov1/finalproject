import PropTypes from "prop-types";
import FollowButton from "../../../buttons/follow-button/index.js";

const UserSearchCard = ({ user }) => {
  return (
    <div className={"h-36 full-card-schema !flex-row"}>
      {/*Header*/}
      <div className={"w-10 h-full"}>
        <img
          src={user.profilePhoto}
          alt=""
          className={"w-8 h-8 rounded-full object-cover"}
        />
      </div>
      <div className={"w-full h-full flex flex-col gap-5"}>
        <a
          href={`/${user.username}`}
          className={"font-medium cursor-pointer dark:text-gray-300"}
        >
          {user.username}
        </a>
        <a
          href={`/${user.username}`}
          className={
            "text-2xl font-bold dark:text-gray-300 hover:text-accent hover:dark:text-dark-text-accent"
          }
        >
          {user.name}
        </a>
      </div>
      <div className={"w-20 h-full flex justify-center items-end"}>
        <FollowButton targetId={user.userId} isFollowes={user.isUserFollowed} />
      </div>
    </div>
  );
};

UserSearchCard.propTypes = {
  user: PropTypes.object,
};
export default UserSearchCard;
