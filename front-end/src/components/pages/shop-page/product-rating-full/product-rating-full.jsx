import Rating from "react-rating";
import { FaRegStar, FaStar } from "react-icons/fa6";
import PropTypes from "prop-types";
import RatingIndicator from "../../../common/rating-indicator/index.js";

const ProductRatingFull = ({ rating, ...props }) => {
  return (
    <div
      className={"flex justify-center items-center grow-0 shrink-0 gap-1"}
      {...props}
    >
      <Rating
        className="!flex  !items-center"
        initialRating={rating}
        readonly={true}
        emptySymbol={<FaRegStar fill={"#a0aec0"} size={18} />}
        fullSymbol={<FaStar fill={"#fde047"} size={18} />}
        fractions={2}
      />
      <RatingIndicator rating={rating} />
    </div>
  );
};

ProductRatingFull.propTypes = {
  rating: PropTypes.number,
};

export default ProductRatingFull;
