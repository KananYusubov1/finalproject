import PropTypes from "prop-types";
import { useUserSignedIn } from "../../../../utils/auth.js";
import { useUser } from "../../../../utils/user.js";
import { useEffect, useState } from "react";
import { createModal } from "../../../../utils/modal.js";
import UserPreviewHoverCard from "../../../common/user-preview-hover-card/index.js";
import DotDivider from "../../../common/dot-divider/index.js";
import { formatDate } from "../../../../utils/common.js";
import MoreOptions from "../../../common/more-options/index.js";
import MarkdownViewer from "../../../markdown-viewer/index.js";
import ProductRatingFull from "../product-rating-full/index.js";

const ReviewView = ({ review, onDelete }) => {
  const signedIn = useUserSignedIn();
  const userId = useUser()?.id;
  const [values, seValues] = useState({ body: review.body, rank: review.rank });

  const [dropdownItems, setDropdownItems] = useState([]);
  const handleOnUpdate = (newValues) => {
    seValues(newValues);
  };

  const handleEditReview = () => {
    console.log("comment: ", review);
    createModal("entryEdit", {
      type: "review",
      title: "review",
      id: review.id,
      body: values.body,
      rank: values.rank,
      onUpdate: handleOnUpdate,
    });
  };
  useEffect(() => {
    const options = [];
    if (review.userContent.userId === userId) {
      options.push(
        {
          label: "Edit review",
          onClick: () => {
            handleEditReview();
          },
          type: "item",
        },
        {
          label: "Delete review",
          onClick: () => {
            createModal("contentDelete", {
              contentId: review.id,
              contentType: "review",
              onDelete: () => {
                onDelete(review.id);
              },
            });
          },
          type: "danger-item",
        }
      );
    }

    setDropdownItems(options);
  }, []);

  return (
    <div className={"w-full flex gap-2"}>
      <img
        src={review.userContent.profilePhoto}
        alt="Avatar"
        className={"w-8 h-8 rounded-full object-cover mt-2"}
      />
      <div className={"w-full flex flex-col gap-2"}>
        <div
          className={
            "full-card-schema !p-3 !border-[1px] dark:hover:border-gray-700"
          }
        >
          {/*Avatar*/}
          <div className={"flex items-center justify-between"}>
            <div className={"flex items-center gap-1"}>
              <UserPreviewHoverCard userId={review.userContent.userId}>
                <div
                  className={
                    "rounded-md dark:text-gray-300 p-1 hover:bg-border-clr hover:dark:bg-dark-hover-bg-clr smooth-animation"
                  }
                >
                  <span className={" font-semibold cursor-pointer"}>
                    {review.userContent.name}
                  </span>
                </div>
              </UserPreviewHoverCard>

              <DotDivider size={20} />

              <span className={"text-sm text-second-text dark:text-gray-500"}>
                {formatDate(review.updatedAt)}
              </span>

              <DotDivider size={20} />

              <ProductRatingFull rating={values.rank} />
            </div>
            {signedIn && dropdownItems && dropdownItems.length > 0 && (
              <MoreOptions items={dropdownItems} />
            )}
          </div>
          <div className={"pl-1"}>
            <MarkdownViewer source={values.body} />
          </div>
          {/*Body*/}
        </div>
      </div>
    </div>
  );
};

ReviewView.propTypes = {
  review: PropTypes.object,
  onDelete: PropTypes.func,
};

export default ReviewView;
