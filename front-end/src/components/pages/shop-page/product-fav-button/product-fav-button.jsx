import PropTypes from "prop-types";
import TooltipContainer from "../../../common/tooltip-container/index.js";
import { FaHeart, FaRegHeart } from "react-icons/fa";
import { useState } from "react";
import { useUserSignedIn } from "../../../../utils/auth.js";
import { createModal } from "../../../../utils/modal.js";
import { errorToast, successToast } from "../../../../utils/toast.js";
import classNames from "classnames";
import { favouriteProduct } from "../../../../services/shop.js";
import { ThreeDots } from "react-loader-spinner";

const ProductFavButton = ({
  productId,
  isFavourited,
  transparent,
  ...props
}) => {
  const [favourited, setFavourited] = useState(isFavourited);
  const [loading, setLoading] = useState(false);

  const signedIn = useUserSignedIn();

  const handleFavouriteChange = (e) => {
    e.stopPropagation();
    if (!signedIn) {
      createModal("enter");
      return;
    }

    setLoading(true);
    favouriteProduct(productId)
      .then(() => {
        setFavourited(!favourited);
        successToast(`${favourited ? "Removed from" : "Added to"} Favourite`);
      })
      .catch(() => {
        errorToast("Something went wrong");
      })
      .finally(() => {
        setLoading(false);
      });
  };
  return (
    <button
      {...props}
      onClick={handleFavouriteChange}
      className={classNames({
        "bg-white rounded-full p-1.5 flex items-center": true,
        "!bg-transparent": transparent,
      })}
    >
      {loading ? (
        <div className={"w-[24px] flex items-center justify-center"}>
          <ThreeDots
            height="24"
            width="24"
            color={"#3B49DF"}
            wrapperClass="radio-wrapper"
            radius="9"
            ariaLabel="three-dots-loading"
            wrapperStyle={{}}
            wrapperClassName=""
            visible={true}
          />
        </div>
      ) : favourited ? (
        <TooltipContainer tooltip={"Remove from Favourite"}>
          <FaHeart size={20} className={"text-red-500"} />
        </TooltipContainer>
      ) : (
        <TooltipContainer tooltip={"Add to Favourite"}>
          <FaRegHeart
            size={20}
            className={classNames({
              "text-second-text": true,
              "dark:text-white": transparent,
            })}
          />
        </TooltipContainer>
      )}
    </button>
  );
};

ProductFavButton.propTypes = {
  isFavourited: PropTypes.bool,
  productId: PropTypes.string,
  transparent: PropTypes.bool,
};

export default ProductFavButton;
