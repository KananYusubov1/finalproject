import PropTypes from "prop-types";
import ProductImageGallery from "../product-image-gallery/index.js";

const ProductViewPagePhotos = ({ product }) => {
  const { photos } = product;

  const images = photos.map((item) => {
    return {
      original: item.path,
      thumbnail: item.path,
    };
  });

  return (
    <div className={"card-color-schema w-full  p-8"}>
      <ProductImageGallery images={images} />
    </div>
  );
};

ProductViewPagePhotos.propTypes = {
  product: PropTypes.object,
};

export default ProductViewPagePhotos;
