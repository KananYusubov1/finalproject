import { useUser } from "../../../../utils/user.js";
import { useState } from "react";
import { Form, Formik } from "formik";
import { insertComment } from "../../../../services/contentCommon.js";
import { errorToast, successToast } from "../../../../utils/toast.js";
import MarkdownEditor from "../../../common/markdown-editor/index.js";
import { ThreeDots } from "react-loader-spinner";
import PrimaryAccentButton from "../../../buttons/primary-accent-button/index.js";
import RatingInput from "../../../form/rating-input/index.js";
import { publishReview } from "../../../../services/shop.js";
import PropTypes from "prop-types";

const WriteReview = ({ productId, onReviewPublished }) => {
  const user = useUser();

  const [isLoading, setIsLoading] = useState(false);

  return (
    <div>
      <div className={"w-full  !flex justify-center "}>
        <Formik
          initialValues={{ body: "", rank: 0 }}
          onSubmit={(values, { resetForm }) => {
            setIsLoading(true);
            publishReview({
              productId: productId,
              body: values.body,
              rank: values.rank,
            })
              .then((reviewId) => {
                successToast("Comment published successfully");
                onReviewPublished({
                  id: reviewId,
                  body: values.body,
                  rank: values.rank,
                });
                resetForm();
              })
              .catch(() => {
                errorToast("A problem accrued, please try again later");
              })
              .finally(() => {
                setIsLoading(false);
              });
          }}
        >
          {({ values }) => (
            <Form className={" flex  gap-4 w-full p-3"}>
              {/*Avatar*/}
              <img
                src={user.avatar}
                alt="Avatar"
                className={"w-10 h-10 rounded-full object-cover mt-4"}
              />
              <div className={"w-full flex flex-col  gap-3"}>
                <div className={"h-[140px]"}>
                  <MarkdownEditor
                    size={140}
                    name={"body"}
                    placeholder={"Write your review here..."}
                  />
                </div>

                <RatingInput name={"rank"} />

                <div className={"w-fit "}>
                  {isLoading ? (
                    <div className={"w-16 flex items-center justify-center"}>
                      <ThreeDots
                        height="24"
                        width="24"
                        color={"#3B49DF"}
                        wrapperClass="radio-wrapper"
                        radius="9"
                        ariaLabel="three-dots-loading"
                        wrapperStyle={{}}
                        wrapperClassName=""
                        visible={true}
                      />
                    </div>
                  ) : (
                    <PrimaryAccentButton
                      type={"submit"}
                      title={"Publish"}
                      disabled={!values.body && !values.rank}
                    />
                  )}
                </div>
              </div>
            </Form>
          )}
        </Formik>
        {/*Body*/}
      </div>
    </div>
  );
};

WriteReview.propTypes = {
  productId: PropTypes.number,
  onReviewPublished: PropTypes.func,
};

export default WriteReview;
