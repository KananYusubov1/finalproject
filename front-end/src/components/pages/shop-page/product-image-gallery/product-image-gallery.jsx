import { useState } from "react";
import ImageGallery from "react-image-gallery";
import "react-image-gallery/styles/css/image-gallery.css";
import PropTypes from "prop-types";

const ProductImageGallery = ({ images }) => {
  const [isFullScreen, setIsFullscreen] = useState(false);

  const customRenderItem = (item) => {
    return (
      <>
        {!isFullScreen ? (
          <div className={"w-full flex items-center justify-center"}>
            <img
              className={
                " h-[440px] backdrop-blur-lg object-contain rounded-lg "
              }
              src={item.original}
              alt={"Product image"}
            />
          </div>
        ) : (
          <img
            className={"image-gallery-image w-full h-full object-contain"}
            src={item.original}
            alt={"Product image"}
          />
        )}
      </>
    );
  };

  const customRenderThumbInner = (item) => {
    return (
      <img
        className={"  object-cover "}
        src={item.thumbnail}
        alt={"Product image"}
      />
    );
  };
  return (
    <div className={"w-full h-[567px] max-h-[567px]  rounded-lg"}>
      <ImageGallery
        items={images}
        infinite={true}
        showPlayButton={false}
        slideOnThumbnailOver={true}
        lazyLoad={true}
        renderCustomControls={() => null}
        renderItem={customRenderItem}
        renderThumbInner={customRenderThumbInner}
        onScreenChange={() => setIsFullscreen(!isFullScreen)}
        thumbnailClass={"bg-red-500 rounded-lg"}
      />
    </div>
  );
};

ProductImageGallery.propTypes = {
  images: PropTypes.array,
};

export default ProductImageGallery;
