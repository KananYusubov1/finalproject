import IconButton from "../../buttons/icon-button/index.js";
import { Drawer } from "@material-tailwind/react";
import { AiOutlineClose } from "react-icons/ai";
import { toggleDrawerProcess, useDrawer } from "../../../utils/process.js";
import SideBar from "../../side-bar/side-bar/index.js";

const HamburgerMenu = () => {
  const open = useDrawer();
  const closeDrawer = () => toggleDrawerProcess();

  return (
    <>
      <Drawer
        open={open}
        onClose={closeDrawer}
        className=" p-4 bg-white dark:bg-dark-component-bg"
      >
        <div className="mb-4 flex items-center justify-between">
          <h1 className={"text-xl font-bold dark:text-white"}>
            Clubrick Community
          </h1>
          <IconButton icon={<AiOutlineClose />} click={closeDrawer} />
        </div>
        <SideBar drawerMode={true} />
      </Drawer>
    </>
  );
};

export default HamburgerMenu;
