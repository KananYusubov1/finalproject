import { LuSearch } from "react-icons/lu";
import PropTypes from "prop-types";
import { useEffect } from "react";

const Search = ({ searchValue, placeHolder, onChange, onSearch }) => {
  useEffect(() => {
    if (searchValue) onSearch();
  }, []);

  return (
    <div className={"w-full h-9"}>
      <label
        htmlFor=""
        className={"relative  flex justify-center items-center  "}
      >
        <input
          className={
            "w-full h-9 bg-white dark:text-gray-300 dark:bg-dark-component-bg dark:border-dark-border-clr placeholder-gray-700 text-sm border border-border-clr rounded-md py-2 pl-3 pr-[38px] focus:outline-none hover:border-hover-border-clr focus:outline-accent focus:border-none transition-all duration-75"
          }
          value={searchValue}
          type="text"
          placeholder={placeHolder}
          onKeyDown={(e) => {
            if (e.key === "Enter") {
              onSearch();
            }
          }}
          onChange={(e) => {
            onChange(e.target.value);
          }}
        />
        <LuSearch
          onClick={() => {
            onSearch();
          }}
          size="38"
          className={
            "absolute top-[0px] dark:text-gray-400 right-[0px] flex items-center px-2 h-[36px] cursor-pointer hover:text-accent hover:bg-hover-blue-clr hover:dark:bg-dark-hover-blue-clr hover:dark:text-dark-text-accent rounded-lg z-10 box-border"
          }
        />
      </label>
    </div>
  );
};

Search.propTypes = {
  searchValue: PropTypes.string,
  placeHolder: PropTypes.string,
  onChange: PropTypes.func,
  onSearch: PropTypes.func,
};

export default Search;
