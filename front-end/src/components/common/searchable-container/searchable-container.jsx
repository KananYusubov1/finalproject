import PropTypes from "prop-types";

const SearchableContainer = ({ data, handleSelectedItem, renderItem }) => {
  return (
    <div>
      <div>{data.map((item) => renderItem(item))}</div>
    </div>
  );
};

SearchableContainer.propTypes = {
  data: PropTypes.array,
  handleSelectedItem: PropTypes.func,
  renderItem: PropTypes.func,
};
export default SearchableContainer;
