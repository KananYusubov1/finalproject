import React from "react";
import PropTypes from "prop-types";

const BuyCheckoutInfo = ({ label, value, renderValue }) => {
  return (
    <div className={"w-full flex items-center justify-between dark:text-white"}>
      <p className={"ellipsis-overflow text-sm"}>{label}:</p>
      {renderValue ? (
        renderValue
      ) : (
        <span className={"ellipsis-overflow text-sm"}>{value}</span>
      )}
    </div>
  );
};

BuyCheckoutInfo.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string,
  renderValue: PropTypes.node,
};

export default BuyCheckoutInfo;
