import PropTypes from "prop-types";
import classNames from "classnames";
import { getTagColor } from "../../../utils/common.js";

const TagPreview = ({ title, colorId, isSecond = true }) => {
  const color = getTagColor(colorId);

  return (
    <>
      <button
        type={"button"}
        id={title}
        style={{
          "--tag-bg-hover": color.bgHover,
          "--tag-border-hover": color.borderHover,
          "--tag-text": color.text,
        }}
        className={classNames({
          "category tag-color mx-[-8px] text-sm font-bold transition-all duration-70 rounded-md py-1 px-2 border border-transparent bg-transparent group ": true,
        })}
      >
        <span>#</span>
        <span
          className={classNames({
            "group-hover:text-black dark:group-hover:text-[#e0e0e0] lowercase ellipsis-overflow": true,
            "text-[#717171] dark:text-[#9e9e9e]": isSecond,
            "text-black dark:text-[#e0e0e0] ": !isSecond,
          })}
        >
          {title}
        </span>
      </button>
    </>
  );
};

TagPreview.propTypes = {
  title: PropTypes.string,
  colorId: PropTypes.string,
  isSecond: PropTypes.bool,
};

export default TagPreview;
