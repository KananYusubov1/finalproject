import { Link } from "react-router-dom";
import MediaQuery from "react-responsive";

const Brand = () => {
  return (
    <Link
      to={"/"}
      className={
        "w-fit min-w-[32px] mr-2  h-10 flex gap-1 items-center select-none"
      }
      onClick={() => {}}
    >
      <img
        className={"w-8   hover:animate-slideOutBrandTr"}
        src={`/logo.svg`}
        alt="logo"
      />

      <MediaQuery minWidth={430}>
        <span className={"text-2xl text-brand-green font-modulus"}>
          Clubrick
        </span>
      </MediaQuery>
    </Link>
  );
};

export default Brand;
