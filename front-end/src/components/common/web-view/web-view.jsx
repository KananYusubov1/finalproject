import { useEffect, useState } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import { load } from "cheerio";
import LimitedText from "../limited-text/index.js";

const WebView = ({ url, onClick }) => {
  const [previewData, setPreviewData] = useState(null);

  useEffect(() => {
    try {
      /*request url html document*/

      axios.get(url).then((response) => {
        const { data } = response;

        //load html document in cheerio
        const $ = load(data);

        /*function to get needed values from meta tags to generate preview*/
        const getMetaTag = (name) => {
          return (
            $(`meta[name=${name}]`).attr("content") ||
            $(`meta[propety="twitter${name}"]`).attr("content") ||
            $(`meta[property="og:${name}"]`).attr("content")
          );
        };

        /*Fetch values into an object */
        setPreviewData({
          url,
          title: $("title").first().text(),
          favicon:
            $('link[rel="shortcut icon"]').attr("href") ||
            $('link[rel="alternate icon"]').attr("href"),
          description: getMetaTag("description"),
          image: getMetaTag("image"),
          author: getMetaTag("author"),
        });
      });
    } catch (error) {
      console.log(error);
    }
  }, []);

  return (
    <a
      href={url}
      onClick={onClick}
      target={"_blank"}
      rel="noreferrer"
      className={
        "w-full rounded-md py-2 px-4 bg-[#f5f8fc] dark:bg-dark-component-second-bg"
      }
    >
      {previewData ? (
        previewData.image ? (
          <div className={"flex gap-6"}>
            <img
              className={"h-16 rounded-md"}
              src={previewData.image}
              alt="resource image"
            />
            <div className={"flex flex-col gap-2 "}>
              <span
                className={
                  "text-second-text dark:text-gray-300 text-lg font-semibold"
                }
              >
                <LimitedText text={previewData.title ?? url} limit={70} />
              </span>
              <span className={"text-dark-text dark:text-gray-500"}>
                <LimitedText text={previewData.description ?? url} limit={78} />
              </span>
            </div>
          </div>
        ) : (
          <div className={"flex flex-col gap-2 "}>
            <span
              className={
                "text-second-text dark:text-gray-300 text-lg font-semibold"
              }
            >
              <LimitedText text={previewData.title ?? url} limit={70} />
            </span>
            <span className={"text-dark-text dark:text-gray-500"}>
              <LimitedText text={previewData.description ?? url} limit={78} />
            </span>
          </div>
        )
      ) : (
        <div className={"flex flex-col gap-2 "}>
          <span
            className={
              "text-second-text dark:text-gray-300 text-lg font-semibold"
            }
          >
            <LimitedText text={url} limit={87} />
          </span>
          <span className={"text-dark-text dark:text-gray-500"}>
            <LimitedText text={url} limit={95} />
          </span>
        </div>
      )}
    </a>
  );
};

WebView.propTypes = {
  url: PropTypes.string,
  onClick: PropTypes.func,
};

export default WebView;
