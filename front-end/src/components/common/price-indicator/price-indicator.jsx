import PropTypes from "prop-types";
import classNames from "classnames";

const PriceIndicator = ({ price, overLine = false, isBig = false }) => {
  return (
    <div className={"flex items-center gap-1 "}>
      <p
        className={classNames({
          "dark:text-white text-md font-medium text-gray-800 mt-0 ": true,
          " dark:!text-dark-text text-sm  line-through": overLine,
          "!text-xl": isBig,
        })}
      >
        {price}
      </p>
      <img
        src={`/bud.png`}
        alt={"bud icon"}
        className={classNames({
          "h-4": !overLine,
          "h-3 grayscale-[60%] ": overLine,
          "!h-4": isBig,
        })}
      />
    </div>
  );
};

PriceIndicator.propTypes = {
  price: PropTypes.number,
  overLine: PropTypes.bool,
  isBig: PropTypes.bool,
};

export default PriceIndicator;
