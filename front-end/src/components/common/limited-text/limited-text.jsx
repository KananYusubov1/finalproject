import PropTypes from "prop-types";

const LimitedText = ({ text, limit }) => {
  return (
    <div className={"dark:text-gray-300"}>
      {text.length <= limit ? <p>{text}</p> : <p>{text.slice(0, limit)}...</p>}
    </div>
  );
};

LimitedText.propTypes = { text: PropTypes.string, limit: PropTypes.number };

export default LimitedText;
