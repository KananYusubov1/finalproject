import * as Tooltip from "@radix-ui/react-tooltip";
import PropTypes from "prop-types";
import "./tooltip-container.css";
const TooltipContainer = ({ tooltip, children }) => {
  const childrenType = children.type;
  return (
    <Tooltip.Provider>
      <Tooltip.Root>
        <Tooltip.Trigger asChild>
          {childrenType === "button" ? children : <button>{children}</button>}
        </Tooltip.Trigger>
        <Tooltip.Portal>
          <Tooltip.Content className="tt-content" sideOffset={5}>
            {tooltip}
            <Tooltip.Arrow className="fill-white dark:fill-dark-border-clr" />
          </Tooltip.Content>
        </Tooltip.Portal>
      </Tooltip.Root>
    </Tooltip.Provider>
  );
};

TooltipContainer.propTypes = {
  tooltip: PropTypes.string,
  children: PropTypes.element,
};

export default TooltipContainer;
