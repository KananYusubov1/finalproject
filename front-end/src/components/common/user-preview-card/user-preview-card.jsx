import { Link } from "react-router-dom";
import FollowButton from "../../buttons/follow-button/index.js";
import UserPreviewHoverCardInfo from "../user-preview-hover-card-info/index.js";
import LimitedText from "../limited-text/index.js";
import { formatDate } from "../../../utils/common.js";
import { useUser } from "../../../utils/user.js";
import PropTypes from "prop-types";
import { ThreeDots } from "react-loader-spinner";
import classNames from "classnames";

const UserPreviewCard = ({ user, isHoverCard = false }) => {
  const currentUser = useUser();

  return (
    <div>
      {user ? (
        <div
          className={classNames({
            "min-w-[300px] max-w-[400px] flex flex-col border-2 dark:border-dark-border-clr border-border-clr rounded-md shadow  drop-shadow-lg ": true,
            "dark:shadow-gray-800": isHoverCard,
          })}
        >
          {/*Brand*/}
          <div
            className={"rounded-t-md  h-6"}
            style={{ backgroundColor: user.brand?.accentColor ?? "black" }}
          ></div>
          {/*Main Content*/}
          <div
            className={
              "rounded-b-md bg-white dark:bg-dark-component-bg p-3 flex flex-col gap-2"
            }
          >
            {/*Header | Avatar & Name*/}
            <div className={"flex items-center justify-center gap-3"}>
              <img
                className={"w-10 h-10 rounded-full object-cover"}
                src={user.profilePhoto}
                alt="avatar"
              />

              <Link
                to={`/${user.username}`}
                className={
                  "font-semibold text-lg dark:text-gray-300 hover:text-accent-dark hover:dark:text-dark-text-accent  cursor-pointer"
                }
              >
                {user.name}
              </Link>
            </div>

            {user.id !== currentUser.id && <FollowButton targetId={user.id} />}

            {user.bio && <LimitedText limit={100} text={user.bio} />}

            <UserPreviewHoverCardInfo
              label={"Email"}
              value={user.displayEmail}
              type={"email"}
            />

            <UserPreviewHoverCardInfo label={"work"} value={user.work} />

            <UserPreviewHoverCardInfo
              label={"location"}
              value={user.location}
            />
            <UserPreviewHoverCardInfo
              label={"education"}
              value={user.education}
            />

            <UserPreviewHoverCardInfo
              label={"joined"}
              value={formatDate(user.joined)}
            />
          </div>
        </div>
      ) : (
        <div className={"full-card-schema items-center"}>
          <ThreeDots
            height="24"
            width="24"
            color={"#3B49DF"}
            wrapperClass="radio-wrapper"
            radius="9"
            ariaLabel="three-dots-loading"
            wrapperStyle={{}}
            wrapperClassName=""
            visible={true}
          />
        </div>
      )}
    </div>
  );
};

UserPreviewCard.propTypes = {
  user: PropTypes.object,
  isHoverCard: PropTypes.bool,
};

export default UserPreviewCard;
