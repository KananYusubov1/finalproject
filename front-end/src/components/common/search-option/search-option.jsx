import PropTypes from "prop-types";
import { IconContext } from "react-icons";
import { useEffect, useState } from "react";
import SelectComponent from "../select-component/index.js";
import { TagService } from "../../../services";

const SearchOption = ({ getSearchParam, option, onChange }) => {
  const [value, setValue] = useState("");
  const [options, setOptions] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);

    switch (option.type) {
      case "sort":
        setOptions([
          {
            label: "Relevant",
            value: "Relevant",
          },
          {
            label: "Newest",
            value: "Newest",
          },
          {
            label: "Top Week",
            value: "TopWeek",
          },

          {
            label: "Top Month",
            value: "TopMonth",
          },
          {
            label: "Top Year",
            value: "TopYear",
          },
          {
            label: "Top Infinity",
            value: "TopInfinity",
          },
        ]);
        setIsLoading(false);
        break;
      case "productSort":
        setOptions([
          {
            label: "Ranking",
            value: "Ranking",
          },
          {
            label: "LowestPrice",
            value: "LowestPrice",
          },
          {
            label: "HighestPrice",
            value: "HighestPrice",
          },

          {
            label: "Newest",
            value: "Newest",
          },
          {
            label: "BestSellers",
            value: "BestSellers",
          },
          {
            label: "MostReviewed",
            value: "MostReviewed",
          },
          {
            label: "TopRated(Favorite)",
            value: "TopRated",
          },
        ]);
        setIsLoading(false);
        break;
      case "category":
        TagService.getAllTags("categories")
          .then((res) => {
            const categories = res.map((category) => {
              return {
                label: category.title,
                value: category.id,
              };
            });
            setOptions(categories);
          })
          .catch((error) => {
            console.log(error);
          })
          .finally(() => {
            setIsLoading(false);
          });
        break;
      case "resource-flag":
        TagService.getAllTags("resource-flags")
          .then((res) => {
            const categories = res.map((category) => {
              return {
                label: category.title,
                value: category.id,
              };
            });
            setOptions(categories);
          })
          .catch((error) => {
            console.log(error);
          })
          .finally(() => {
            setIsLoading(false);
          });
        break;
      case "article-flag":
        TagService.getAllTags("article-flags")
          .then((res) => {
            const categories = res.map((category) => {
              return {
                label: category.title,
                value: category.id,
              };
            });
            setOptions(categories);
          })
          .catch((error) => {
            console.log(error);
          })
          .finally(() => {
            setIsLoading(false);
          });
        break;
    }
  }, []);

  const handleOnChange = (selected) => {
    setValue(selected?.value ?? "");
    onChange(selected?.value ?? "", option.paramName);
  };

  return (
    <div className={"w-fit z-50"}>
      <SelectComponent
        options={options}
        onChange={handleOnChange}
        defaultOptionValue={getSearchParam(option.paramName)}
        isLoading={isLoading}
        renderPlaceholder={
          <div className={"flex items-center gap-2"}>
            <IconContext.Provider value={{ color: "#3B49DF" }}>
              {option.icon}
            </IconContext.Provider>
            <span className={"font-medium dark:text-gray-400"}>
              {value
                ? options.find((item) => item.value === value)?.label
                : option.label}
            </span>
          </div>
        }
      />
      {/*<DropDownMenu*/}
      {/*  trigger={*/}

      {/*  }*/}
      {/*  items={option.options}*/}
      {/*/>*/}
    </div>
  );
};

SearchOption.propTypes = {
  getSearchParam: PropTypes.func,
  option: PropTypes.object,
  onChange: PropTypes.func,
};
export default SearchOption;
