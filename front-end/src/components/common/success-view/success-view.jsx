import classNames from "classnames";
import { BsInfoSquareFill } from "react-icons/bs";
import PropTypes from "prop-types";

const SuccessView = ({ message, version }) => {
  return (
    <div
      className={classNames({
        "flex items-center  gap-2 p-1": true,
        "rounded-md  border-[1px] border-border-clr select-none shadow-none hover:shadow smooth-animation":
          version === "input-like",
      })}
    >
      <BsInfoSquareFill fill={"#3B49DF"} size={20} />
      <p>{message}</p>
    </div>
  );
};

SuccessView.propTypes = {
  message: PropTypes.string,
  version: PropTypes.string,
};

export default SuccessView;
