import { MdOutlineSearchOff } from "react-icons/md";
import PropTypes from "prop-types";
import { IconContext } from "react-icons";

const NoResultFound = ({
  label = "No results found",
  icon = <MdOutlineSearchOff />,
}) => {
  return (
    <div
      className={
        "py-4 px-5 flex flex-col items-center justify-center gap-1 card-color-schema card-shadow-schema"
      }
    >
      <IconContext.Provider value={{ size: 43, color: "#3B49DF" }}>
        {icon}
      </IconContext.Provider>

      <span className={"font-semibold dark:text-gray-300 text-xl select-none"}>
        {label}
      </span>
    </div>
  );
};

NoResultFound.propTypes = {
  label: PropTypes.string,
  icon: PropTypes.element,
};

export default NoResultFound;
