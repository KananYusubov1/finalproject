const BrandHeader = () => {
  return (
    <div
      className={
        "w-full flex flex-col  items-center justify-center gap-1 select-none"
      }
    >
      <img className={"w-16 "} src={`logo.svg`} alt="logo" />
      <span className={"text-5xl text-[#25B99A] font-modulus"}>Clubrick</span>
    </div>
  );
};

export default BrandHeader;
