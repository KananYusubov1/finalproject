import { BsDot } from "react-icons/bs";
import PropTypes from "prop-types";

const DotDivider = ({ size = 16 }) => {
  return (
    <div>
      <BsDot size={size} fill={"#575757"} />
    </div>
  );
};

DotDivider.propTypes = {
  size: PropTypes.number,
};

export default DotDivider;
