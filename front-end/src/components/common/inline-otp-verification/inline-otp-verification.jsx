import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import { AuthService } from "../../../services";
import ErrorView from "../error-view/index.js";
import InlineLoader from "../../loaders/inline-loader/index.js";
import { Radio } from "react-loader-spinner";
import PrimaryAccentButton from "../../buttons/primary-accent-button/index.js";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import { useUser } from "../../../utils/user.js";

const InlineOtpVerification = ({ email, emailVerified }) => {
  const { animationParent } = useAutoAnimate();

  const { name } = useUser();

  const [otp, setOtp] = useState("");

  const [isLoading, setLoading] = useState(false);
  const [loadingMessage, setLoadingMessage] = useState("");

  const [errorMessage, setErrorMessage] = useState("");

  const resetOtp = () => {
    sendOtp();
  };

  useEffect(() => {
    if (otp.length === 6) {
      setLoading(true);
      setLoadingMessage("OTP code verification...");
      AuthService.verifyOtp({ email, otp })
        .then(() => {
          console.log("succes");
          setLoading(false);
          setErrorMessage("");
          console.log("before callback");
          emailVerified();
          console.log("after callback");
        })
        .catch((err) => {
          setLoading(false);
          switch (err.status) {
            case 400:
              setErrorMessage("Wrong OTP");
              break;
            case 410:
              setErrorMessage("Otp Code expired");
              break;
          }
          console.log(err);
        });
    }
  }, [otp]);

  const sendOtp = () => {
    setLoading(true);
    setErrorMessage("");
    setOtp("");
    setLoadingMessage("OTP code sending...");
    AuthService.sendOtp(email)
      .then(() => {
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
        console.log(err);
      });
  };

  useEffect(() => {
    sendOtp();
  }, []);

  return (
    <div
      className={
        "w-80 flex flex-col gap-2 border-[1px] border-border-clr p-2 rounded-md bg-component-bg dark:bg-dark-body-bg dark:border-dark-border-clr"
      }
      ref={animationParent}
    >
      <span className={"font-semibold dark:text-gray-300"}>
        Email Verification
      </span>
      <div className={"flex items-center gap-2"}>
        <input
          value={otp}
          onChange={(e) => setOtp(e.target.value)}
          autoComplete={"one-time-code"}
          maxLength={6}
          placeholder={"Write Otp code"}
          className={
            "peer w-full bg-white dark:bg-dark-component-bg dark:border-dark-border-clr dark:text-gray-300 h-9 border border-border-clr rounded-md py-2 pl-3  focus:outline-none hover:border-hover-border-clr focus:outline-accent focus:border-none transition-all duration-75"
          }
        />

        <PrimaryAccentButton title={"Resend"} click={resetOtp} />
      </div>
      {errorMessage && <ErrorView message={errorMessage} />}
      {isLoading && (
        <InlineLoader
          loader={
            <Radio
              visible={true}
              height="24"
              width="24"
              colors={["#3B49DF", "#3B49DF", "#3B49DF"]}
              ariaLabel="radio-loading"
              wrapperStyle={{}}
              wrapperClass="radio-wrapper"
            />
          }
          message={loadingMessage ?? "loading ..."}
          version={"input-like"}
        />
      )}
    </div>
  );
};

InlineOtpVerification.propTypes = {
  email: PropTypes.string,
  emailVerified: PropTypes.func,
};

export default InlineOtpVerification;
