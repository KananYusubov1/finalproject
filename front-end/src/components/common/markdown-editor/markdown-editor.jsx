import MDEditor, { commands } from "@uiw/react-md-editor";
import rehypeSanitize from "rehype-sanitize";
import MermaidCodeParser from "../mermaid-code-parser/index.js";
import PropTypes from "prop-types";
import { useField } from "formik";
import classNames from "classnames";

const MarkdownEditor = ({
  name,
  placeholder,
  onChange,
  initialValue = "",
  bigSize = true,
  transparent = true,
  size,
}) => {
  const [field] = useField(name ?? "");

  const help = {
    name: "help",
    keyCommand: "help",
    buttonProps: { "aria-label": "Insert help" },
    icon: (
      <svg viewBox="0 0 16 16" width="12px" height="12px">
        <path
          d="M8 0C3.6 0 0 3.6 0 8s3.6 8 8 8 8-3.6 8-8-3.6-8-8-8Zm.9 13H7v-1.8h1.9V13Zm-.1-3.6v.5H7.1v-.6c.2-2.1 2-1.9 1.9-3.2.1-.7-.3-1.1-1-1.1-.8 0-1.2.7-1.2 1.6H5c0-1.7 1.2-3 2.9-3 2.3 0 3 1.4 3 2.3.1 2.3-1.9 2-2.1 3.5Z"
          fill="currentColor"
        />
      </svg>
    ),
    execute: () => {
      window.open("https://www.markdownguide.org/basic-syntax/", "_blank");
    },
  };

  const handleChange = (markdown) => {
    if (name) field.onChange({ target: { name: name, value: markdown } });
    else onChange(markdown);
  };

  return (
    <div>
      <MDEditor
        value={name ? field?.value : initialValue}
        visibleDragbar={!(size || bigSize)}
        onChange={handleChange}
        height={size}
        maxHeight={size ? size : bigSize ? 600 : 300}
        className={classNames({
          "!bg-transparent !border-none": transparent,
          "": !transparent,
        })}
        preview="edit"
        commands={[...commands.getCommands(), help]}
        textareaProps={{
          placeholder: placeholder,
        }}
        previewOptions={{
          rehypePlugins: [[rehypeSanitize]],
          components: {
            code: MermaidCodeParser,
          },
        }}
      />
    </div>
  );
};

MarkdownEditor.propTypes = {
  name: PropTypes.string,
  bigSize: PropTypes.bool,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  initialValue: PropTypes.string,
  transparent: PropTypes.bool,
  size: PropTypes.number,
};

export default MarkdownEditor;
