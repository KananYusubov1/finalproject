import {
  Table,
  Header,
  HeaderRow,
  Body,
  Row,
  HeaderCell,
  Cell,
} from "@table-library/react-table-library/table";
import { useRowSelect } from "@table-library/react-table-library/select";
import { useTheme } from "@table-library/react-table-library/theme";
import {
  DEFAULT_OPTIONS,
  getTheme,
} from "@table-library/react-table-library/material-ui";
import { useEffect, useState } from "react";
import { createModal } from "../../../utils/modal.js";
import "./tag-table.css";
import PropTypes from "prop-types";
import { TagService } from "../../../services";
import NoResultFound from "../../common/no-result-found/index.js";
import { ThreeDots } from "react-loader-spinner";
import InlineLoader from "../../loaders/inline-loader/index.js";
import PrimaryAccentButton from "../../buttons/primary-accent-button/index.js";
import { getTagTypeTitle } from "../../../utils/common.js";

const TagTable = ({ type }) => {
  /*
   * Id
   * Title
   * Description
   * AccentColor
   * UseCount
   * */

  const [nodes, setNodes] = useState([]);

  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    handleRefresh();
  }, []);

  let data = { nodes };

  const select = useRowSelect(data, {
    onChange: onSelectChange,
  });

  function onSelectChange(action, state) {
    console.log(action, state);
    const selectedTag = data.nodes.find((item) => item.id === state.id);
    createModal("tagEditor", {
      type,
      tag: selectedTag,
      refresh: handleRefresh,
    });
  }

  const materialTheme = getTheme(DEFAULT_OPTIONS);
  const theme = useTheme(materialTheme);

  const [search, setSearch] = useState("");

  const handleSearch = (event) => {
    setSearch(event.target.value);
  };

  data = {
    nodes: data.nodes.filter((item) =>
      item.title.toLowerCase().includes(search.toLowerCase())
    ),
  };

  const handleRefresh = () => {
    setIsLoading(true);

    TagService.getAllTags(type)
      .then((res) => {
        setIsLoading(false);
        setNodes(res);
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  return (
    <div className={"!z-[1]"}>
      <div className={"w-fit mb-3"}>
        <PrimaryAccentButton
          title={`Create ${getTagTypeTitle(type)}`}
          click={() => {
            createModal("tagEditor", {
              type: type,
              refresh: handleRefresh,
            });
          }}
        />
      </div>
      <div>
        <input
          placeholder="Search Tag"
          value={search}
          onChange={handleSearch}
          className={"text-input"}
        />
      </div>
      <br />
      <Table
        data={data}
        select={select}
        className={"!max-h-[410px]"}
        theme={theme}
      >
        {(tableList) => (
          <>
            <Header>
              <HeaderRow
                className={
                  "dark:bg-dark-component-second-bg dark:text-gray-300"
                }
              >
                <HeaderCell>Id</HeaderCell>
                <HeaderCell>Title</HeaderCell>
                <HeaderCell>Description</HeaderCell>
                <HeaderCell>AccentColor</HeaderCell>
                <HeaderCell>UseCount</HeaderCell>
              </HeaderRow>
            </Header>

            <Body className={"dark:bg-dark-body-bg"}>
              {tableList.map((item) => (
                <Row
                  item={item}
                  key={item.id}
                  className={
                    "!rounded-md dark:bg-dark-body-bg  dark:text-gray-300"
                  }
                >
                  <Cell>{item.id}</Cell>
                  <Cell>{item.title}</Cell>
                  <Cell>{item.description}</Cell>
                  <Cell>{item.accentColor}</Cell>
                  <Cell>{item.useCount}</Cell>
                </Row>
              ))}
            </Body>
          </>
        )}
      </Table>
      <br />

      {isLoading ? (
        <>
          <InlineLoader
            loader={
              <ThreeDots
                height="24"
                width="24"
                color={"#3B49DF"}
                wrapperClass="radio-wrapper"
                radius="9"
                ariaLabel="three-dots-loading"
                wrapperStyle={{}}
                wrapperClassName=""
                visible={true}
              />
            }
            message={"Tags loading..."}
            version={"input-like"}
          />
        </>
      ) : (
        nodes.length <= 0 && <NoResultFound />
      )}
    </div>
  );
};

TagTable.propTypes = {
  type: PropTypes.string,
};

export default TagTable;
