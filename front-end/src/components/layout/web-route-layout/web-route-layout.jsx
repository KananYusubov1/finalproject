import { useEffect } from "react";
import { Outlet, useNavigate } from "react-router-dom";
import { useUserProfileBuilt, useUserSignedIn } from "../../../utils/auth.js";

const WebRouteLayout = () => {
  const signedIn = useUserSignedIn();
  const profileBuilt = useUserProfileBuilt();
  const navigate = useNavigate();

  useEffect(() => {
    if (signedIn && !profileBuilt) {
      navigate("/build-profile");
    }
  });

  return <Outlet />;
};

export default WebRouteLayout;
