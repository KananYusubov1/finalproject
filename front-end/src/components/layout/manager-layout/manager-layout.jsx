import Container from "../../common/container/index.js";
import { Outlet, useNavigate, useParams } from "react-router-dom";
import ManagerHeader from "../manager-header/index.js";
import SubpageContainer from "../../pages/common/subpage-container/index.js";
import { useAuthData } from "../../../utils/auth.js";
import NotFoundPage from "../../../pages/not-found-page/index.js";

const ManagerLayout = () => {
  const subPages = [
    {
      index: true,
      title: "Reports",
      iconName: "reports",
      url: "reports",
    },
    {
      title: "MyActivity",
      iconName: "my-activity",
      url: "my-activity",
    },
    {
      title: "Products",
      iconName: "shop",
      url: "products",
    },
    {
      title: "ManageTags",
      iconName: "categories",
      url: "manage-tags",
    },
  ];

  const navigate = useNavigate();

  const params = useParams();

  const { role } = useAuthData();

  console.log(role);
  if (role !== "Moderator" && role !== "Admin") return <NotFoundPage />;

  return (
    <div>
      <ManagerHeader />
      <Container>
        <div className={"flex"}>
          <aside className={"w-60 py-4 flex flex-col gap-4"}>
            <SubpageContainer pages={subPages} basePart={params.role} />
          </aside>
          <div className={"w-full p-4"}>
            <Outlet />
          </div>
        </div>
      </Container>
    </div>
  );
};

export default ManagerLayout;
