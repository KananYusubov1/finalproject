import { Outlet } from "react-router-dom";
import Header from "../header/index.js";
import Container from "../../common/container/index.js";
import "./layout.css";

const Layout = () => {
  return (
    <>
      <Header />
      <Container>
        <Outlet />
      </Container>
    </>
  );
};

export default Layout;
