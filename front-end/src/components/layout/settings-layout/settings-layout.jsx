import { useUserSignedIn } from "../../../utils/auth.js";
import { useEffect } from "react";
import { Link, Outlet, useNavigate } from "react-router-dom";
import Header from "../header/index.js";
import Container from "../../common/container/index.js";
import SubpageContainer from "../../pages/common/subpage-container/index.js";
import { useUser } from "../../../utils/user.js";
import EnterPage from "../../../pages/enter-page/index.js";

const SettingsLayout = () => {
  document.title = "Settings - Clubrick";

  const signedIn = useUserSignedIn();

  const { avatar, username } = useUser();
  const navigate = useNavigate();
  if (!signedIn) {
    return navigate("/enter");
  }

  const subPages = [
    {
      index: true,
      title: "Profile",
      iconName: "profile",
      url: "profile",
      customUrl: avatar,
    },
    {
      title: "Customization",
      iconName: "customization",
      url: "customization",
    },
    {
      title: "Account",
      iconName: "account",
      url: "account",
    },
    {
      title: "Extensions",
      iconName: "extensions",
      url: "extensions",
    },
  ];

  return (
    <div>
      <Header />
      <Container>
        <div className={"flex px-32 py-6 gap-4"}>
          <div className={"w-60 flex flex-col "}>
            <SubpageContainer pages={subPages} />
          </div>
          <div className={"w-full flex flex-col gap-4"}>
            <Link
              to={`/${username}`}
              className={
                "w-fit text-3xl font-bold text-accent dark:text-dark-text-accent"
              }
            >
              @{username}
            </Link>
            <Outlet />
          </div>
        </div>
      </Container>
    </div>
  );
};

export default SettingsLayout;
