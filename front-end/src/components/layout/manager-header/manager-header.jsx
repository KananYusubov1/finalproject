import Container from "../../common/container/index.js";
import PrimaryAccentButton from "../../buttons/primary-accent-button/index.js";
import { useNavigate } from "react-router-dom";
import { useUser } from "../../../utils/user.js";

const ManagerHeader = () => {
  const { avatar, username } = useUser();

  const navigate = useNavigate();

  return (
    <div
      className={
        "w-full h-14 bg-white dark:bg-dark-component-bg drop-shadow-sm"
      }
    >
      <Container>
        <div className={"w-full h-14 flex items-center "}>
          <div className={"w-fit flex items-center gap-4"}>
            <img
              className={"w-10 h-10 rounded-full object-cover"}
              src={avatar}
              alt="avatar"
            />
            <span className={"font-bold text-lg dark:text-gray-300"}>
              @{username}
            </span>
          </div>
          <div className={"w-full  flex items-center justify-end gap-4"}>
            <div className={"w-fit"}>
              <PrimaryAccentButton
                title={"Back to Home"}
                click={() => {
                  navigate("/");
                }}
              />
            </div>
          </div>
        </div>
      </Container>
    </div>
  );
};

export default ManagerHeader;
