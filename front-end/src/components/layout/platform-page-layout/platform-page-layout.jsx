import Header from "../header/index.js";
import Container from "../../common/container/index.js";
import { Outlet } from "react-router-dom";
import BrandHeader from "../../common/brand-header/index.js";

const PlatformPageLayout = () => {
  return (
    <div>
      <Header />
      <Container>
        <div className={"flex justify-center"}>
          <div
            className={
              "sm:mt-3 sm:px-16 py-8 p-3 mt-1 bg-component-bg dark:bg-dark-component-bg rounded-lg flex flex-col gap-4 items-center max-w-[1000px]"
            }
          >
            <BrandHeader />
            <Outlet />
          </div>
        </div>
      </Container>
    </div>
  );
};

export default PlatformPageLayout;
