const ProductSkeletonLoader = () => {
  return (
    <div
      className={
        "w-[300px] h-[252px] rounded-md bg-[#a3a3a3] p-2 animate-pulse"
      }
    >
      <div className={"w-full h-[170px] rounded-md bg-gray-300"} />
      <div className={"flex py-2 items-center justify-between"}>
        <div className={"flex flex-col  gap-2"}>
          <div className={"w-[170px] h-[20px] rounded-md bg-gray-300"} />
          <div className={"w-[140px] h-[20px] rounded-md bg-gray-300"} />
        </div>
        <div className={"w-[80px] h-[40px] rounded-md bg-gray-300"} />
      </div>
    </div>
  );
};

export default ProductSkeletonLoader;
