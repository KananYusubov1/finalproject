import React from "react";
import ContentLoader from "react-content-loader";

const ArticleBigSkeleton = () => {
  return (
    <div>
      <ContentLoader
        speed={2}
        width={720}
        height={415}
        viewBox="0 0 720 415"
        backgroundColor="#575757"
        foregroundColor="#ecebeb"
      >
        <rect x="7" y="9" rx="8" ry="8" width="700" height="218" />
        <rect x="4" y="372" rx="5" ry="5" width="65" height="30" />
        <rect x="6" y="243" rx="5" ry="5" width="700" height="51" />
        <rect x="7" y="310" rx="5" ry="5" width="700" height="46" />
        <rect x="75" y="371" rx="5" ry="5" width="65" height="30" />
        <rect x="150" y="371" rx="5" ry="5" width="65" height="30" />
        <rect x="223" y="371" rx="5" ry="5" width="65" height="30" />
      </ContentLoader>
    </div>
  );
};

export default ArticleBigSkeleton;
