import ContentLoader from "react-content-loader";

const CategorySkeleton = () => {
  return (
    <div>
      <ContentLoader
        speed={2}
        width={300}
        height={124}
        viewBox="0 0 300 124"
        backgroundColor="#a3a3a3"
        foregroundColor="#ecebeb"
      >
        <rect x="49" y="30" rx="3" ry="3" width="100" height="6" />
        <rect x="49" y="80" rx="3" ry="3" width="90" height="21" />
        <rect x="48" y="57" rx="3" ry="3" width="200" height="7" />
        <rect x="200" y="30" rx="3" ry="3" width="50" height="6" />
      </ContentLoader>
    </div>
  );
};

export default CategorySkeleton;
