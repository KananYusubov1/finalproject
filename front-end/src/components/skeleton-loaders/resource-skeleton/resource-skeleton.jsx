import ContentLoader from "react-content-loader";

const ResourceSkeleton = () => {
  return (
    <div>
      <ContentLoader
        speed={2}
        width={1000}
        height={124}
        viewBox="0 0 1000 124"
        backgroundColor="#a3a3a3"
        foregroundColor="#ecebeb"
      >
        <rect x="50" y="14" rx="3" ry="3" width="52" height="6" />
        <rect x="49" y="53" rx="3" ry="3" width="1000" height="17" />
        <rect x="155" y="85" rx="3" ry="3" width="806" height="6" />
        <circle cx="20" cy="20" r="20" />
        <rect x="150" y="13" rx="3" ry="3" width="52" height="6" />
        <circle cx="128" cy="17" r="3" />
        <rect x="245" y="12" rx="3" ry="3" width="52" height="6" />
        <circle cx="219" cy="14" r="3" />
        <rect x="318" y="12" rx="3" ry="3" width="52" height="6" />
        <rect x="157" y="106" rx="3" ry="3" width="700" height="4" />
        <rect x="45" y="75" rx="7" ry="7" width="102" height="45" />
        <rect x="6" y="52" rx="8" ry="8" width="24" height="15" />
        <rect x="5" y="101" rx="8" ry="8" width="24" height="15" />
        <rect x="5" y="82" rx="3" ry="3" width="27" height="3" />
      </ContentLoader>
    </div>
  );
};

export default ResourceSkeleton;
