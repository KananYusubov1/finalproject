import ContentLoader from "react-content-loader";

const ArticleSkeleton = () => {
  return (
    <div>
      <ContentLoader
        speed={2}
        width={1020}
        height={300}
        viewBox="0 0 1020 300"
        backgroundColor="#575757"
        foregroundColor="#ecebeb"
      >
        <rect x="7" y="9" rx="8" ry="8" width="1000" height="111" />
        <rect x="5" y="240" rx="5" ry="5" width="65" height="30" />
        <rect x="7" y="128" rx="5" ry="5" width="1000" height="44" />
        <rect x="7" y="183" rx="5" ry="5" width="1000" height="38" />
        <rect x="83" y="240" rx="5" ry="5" width="65" height="30" />
        <rect x="163" y="241" rx="5" ry="5" width="65" height="30" />
        <rect x="240" y="243" rx="5" ry="5" width="65" height="30" />
      </ContentLoader>
    </div>
  );
};

export default ArticleSkeleton;
