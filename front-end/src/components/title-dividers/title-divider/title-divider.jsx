import PropTypes from "prop-types";

const TitleDivider = ({ title }) => {
  return (
    <div className="w-full flex  items-center space-x-4">
      <div className="flex-1 h-px bg-gray-300 dark:bg-gray-700"></div>
      <div className={"text-dark-text dark:text-gray-500"}>{title}</div>
      <div className="flex-1 h-px bg-gray-300 dark:bg-gray-700"></div>
    </div>
  );
};

TitleDivider.propTypes = { title: PropTypes.any };

export default TitleDivider;
