import { Link } from "react-router-dom";
import { useUserSignedIn } from "../../../utils/auth.js";
import { useThemeSetting } from "../../../utils/appSettings.js";
import useSystemTheme from "react-use-system-theme";

const DarkModeBillboard = () => {
  const signedIn = useUserSignedIn();

  const systemTheme = useSystemTheme("dark");
  const theme = useThemeSetting();
  let currentTheme = "";
  if (theme.toLowerCase() === "system") {
    currentTheme = systemTheme;
  } else {
    currentTheme = theme;
  }
  if (currentTheme.toLowerCase() === "dark") return null;

  return (
    <div className={"full-card-schema !p-3"}>
      <div className={"w-full flex flex-col gap-4"}>
        <span className={"text-sm text-second-text dark:text-gray-400"}>
          Clubrick Community
        </span>
        <img
          src={`/dark-mode-billboard.png`}
          alt="billdboard image"
          className={"rounded-md"}
        />
        <p className={"dark:text-gray-300 font-bold text-xl"}>
          Life is too short to browse without <br />
          <Link
            to={signedIn ? "/settings/customization" : "/enter"}
            className={"text-accent-dark underline"}
          >
            dark mode.
          </Link>
        </p>
        <p className={"text-sm text-second-text dark:text-gray-400"}>
          You can customize your theme, and more{" "}
          {!signedIn && (
            <Link to={"/enter"} className={"text-accent-dark underline"}>
              when you are signed in.
            </Link>
          )}
        </p>
      </div>
    </div>
  );
};

export default DarkModeBillboard;
