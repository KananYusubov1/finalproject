import PropTypes from "prop-types";
import classNames from "classnames";

const FormThemeCard = ({ theme }) => {
  return theme === "System" ? (
    <div className={"flex flex-col gap-2 items-center"}>
      <span className={" font-semibold"}>System</span>
      <div
        className={
          "w-48 h-20 bg-body-bg rounded-md border-[1px] border-border-clr flex flex-col overflow-hidden"
        }
      >
        {/*Header*/}
        <div
          className={
            "w-full h-4 rounded-t-md bg-component-bg flex items-center"
          }
        >
          {/*Left*/}
          <div
            className={
              "w-24 h-full flex items-center gap-1 px-2 border-b-[1px] border-border-clr"
            }
          >
            {/*Logo*/}
            <div
              className={
                "w-3 h-2 rounded-sm bg-white border-[1px] border-border-clr"
              }
            ></div>
            {/*Search Bar*/}
            <div
              className={classNames({
                "w-12 h-2 rounded-sm bg-white border-[1px] border-border-clr ": true,
                "bg-dark-component-bg border-dark-border-clr": theme === "Dark",
              })}
            ></div>
          </div>

          {/*Right*/}
          <div
            className={
              "w-24 h-full flex items-center justify-end rounded-tr-md  bg-dark-component-bg px-2 border-b-[1px] border-dark-border-clr"
            }
          >
            {/*UserProfile*/}
            <div
              className={
                "w-2 h-2 rounded-full bg-dark-component-bg border-dark-border-clr border-[1px]"
              }
            ></div>
          </div>
        </div>
        {/*Body*/}
        <div className={"w-full h-full flex justify-between"}>
          <div
            className={"w-[95px] h-full bg-component-bg flex  gap-1 pl-2 py-1"}
          >
            {/*Left Aside*/}
            <div
              className={
                "w-8 h-full bg-component-bg border-dark-border-clr rounded-md border-[1px]"
              }
            ></div>
            <div className={"w-14 flex flex-col items-end"}>
              {/*Sort Options*/}
              <div className={"text-xs"}>~ ~</div>
              {/*Box*/}
              <div
                className={
                  "w-full h-full bg-component-bg border-dark-border-clr rounded-l-md rounded-y-md border-l-[1px] border-y-[1px]"
                }
              ></div>
            </div>
          </div>

          <div
            className={
              "w-[95px] h-full flex bg-dark-component-bg gap-1 pr-2 py-1"
            }
          >
            <div className={"w-14 flex flex-col"}>
              {/*Sort Options*/}
              <div className={"text-xs text-gray-300"}>~ ~</div>
              {/*Box*/}
              <div
                className={
                  "w-full h-full bg-dark-component-bg border-dark-border-clr rounded-r-md rounded-y-md border-r-[1px] border-y-[1px]"
                }
              ></div>
            </div>
            {/*Right Aside*/}
            <div
              className={
                "w-8 h-full bg-dark-component-bg border-dark-border-clr rounded-md border-[1px]"
              }
            ></div>
          </div>
        </div>
      </div>
    </div>
  ) : (
    <div className={"flex flex-col gap-2 items-start    "}>
      <span className={" font-semibold"}>
        {theme === "Dark" ? "Dark" : "Light"}
      </span>

      <div
        className={classNames({
          "w-44 h-20 bg-body-bg rounded-md border-[1px] border-border-clr flex flex-col": true,
          "bg-dark-body-bg border-dark-border-clr": theme === "Dark",
        })}
      >
        {/*Header*/}
        <div
          className={classNames({
            "w-full h-4 rounded-t-md border-b-[1px] border-border-clr  bg-component-bg flex items-center justify-between px-2": true,
            "bg-dark-component-bg border-dark-border-clr": theme === "Dark",
          })}
        >
          {/*Left*/}
          <div className={"flex items-center gap-1"}>
            {/*Logo*/}
            <div
              className={classNames({
                "w-3 h-2 rounded-sm bg-white border-[1px] border-border-clr": true,
                "!bg-dark-component-bg border-dark-border-clr":
                  theme === "Dark",
              })}
            ></div>
            {/*Search Bar*/}
            <div
              className={classNames({
                "w-12 h-2 rounded-sm bg-white border-[1px] border-border-clr": true,
                "!bg-dark-component-bg border-dark-border-clr":
                  theme === "Dark",
              })}
            ></div>
          </div>
          {/*Right*/}
          <div>
            {/*UserProfile*/}
            <div
              className={classNames({
                "w-2 h-2 rounded-full bg-white border-[1px] border-border-clr": true,
                "!bg-dark-component-bg border-dark-border-clr":
                  theme === "Dark",
              })}
            ></div>
          </div>
        </div>
        {/*Body*/}
        <div className={"w-full h-full flex justify-between gap-1 px-2 py-1 "}>
          {/*Left Aside*/}
          <div
            className={classNames({
              "w-8 h-full bg-component-bg rounded-md border-[1px] border-border-clr": true,
              "bg-dark-component-bg border-dark-border-clr": theme === "Dark",
            })}
          ></div>
          {/*Main*/}
          <div
            className={"w-24 h-full flex flex-col items-center justify-between"}
          >
            {/*Sort Options*/}
            <div
              className={classNames({
                "text-xs": true,
                "text-gray-300": theme === "Dark",
              })}
            >
              ~ ~ ~ ~
            </div>
            {/*Box*/}
            <div
              className={classNames({
                "w-full h-full bg-component-bg rounded-md border-[1px] border-border-clr": true,
                "bg-dark-component-bg border-dark-border-clr": theme === "Dark",
              })}
            ></div>
          </div>
          {/*Right Aside*/}
          <div
            className={classNames({
              "w-8 h-full bg-component-bg rounded-md border-[1px] border-border-clr": true,
              "bg-dark-component-bg border-dark-border-clr": theme === "Dark",
            })}
          ></div>
        </div>
      </div>
    </div>
  );
};

FormThemeCard.propTypes = {
  theme: PropTypes.string,
};

export default FormThemeCard;
