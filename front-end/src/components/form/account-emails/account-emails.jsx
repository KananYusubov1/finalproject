import FormHeader from "../form-header/index.js";
import { SettingsService } from "../../../services";
import { useEffect, useState } from "react";
import InlineLoader from "../../loaders/inline-loader/index.js";
import { ThreeDots } from "react-loader-spinner";
import AccountEmail from "../account-email/index.js";

const AccountEmails = () => {
  const [emails, setEmails] = useState([]);

  useEffect(() => {
    SettingsService.getAccountEmails().then((res) => {
      setEmails(res);
    });
  }, []);

  return (
    <div className={"full-card-schema"}>
      <FormHeader title={"Account Emails"} />
      {emails && emails.length > 0 ? (
        emails.map((email) => <AccountEmail key={email.title} email={email} />)
      ) : (
        <InlineLoader
          loader={
            <ThreeDots
              height="24"
              width="24"
              color={"#3B49DF"}
              wrapperClass="radio-wrapper"
              radius="9"
              ariaLabel="three-dots-loading"
              wrapperStyle={{}}
              wrapperClassName=""
              visible={true}
            />
          }
          message={"Emails loading..."}
          version={"input-like"}
        />
      )}
    </div>
  );
};

export default AccountEmails;
