import { useEffect, useState } from "react";
import { getAllTags } from "../../../services/tag.js";
import { ErrorMessage, useField } from "formik";
import Select from "react-select";
import PropTypes from "prop-types";
import "./category-select-input.css";

const CategorySelectInput = ({ name }) => {
  const [field] = useField(name);

  const handleOnChange = (options) => {
    field.onChange({
      target: { name: name, value: options?.map((o) => o.value) ?? [] },
    });
  };

  const [categories, setCategories] = useState([]);
  const [categoriesIsLoading, setCategoriesIsLoading] = useState(false);

  useEffect(() => {
    setCategoriesIsLoading(true);
    getAllTags("categories")
      .then((res) => {
        const categories = res.map((category) => {
          return {
            label: category.title,
            value: category.id,
          };
        });
        setCategories(categories);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setCategoriesIsLoading(false);
      });
  }, []);

  return (
    <div>
      <div className={"w-full flex flex-col gap-1"}>
        <Select
          {...field}
          className={"w-full z-[60]"}
          classNamePrefix={"react-select"}
          placeholder={"Select categories"}
          isClearable={true}
          isSearchable={true}
          isLoading={categoriesIsLoading}
          isMulti
          defaultValue={field.value}
          value={categories.find((option) => option.value === field.value)}
          onChange={handleOnChange}
          options={categories}
        />

        <ErrorMessage
          name={field.name}
          component={"small"}
          className={"text-xs block text-red-700"}
        />
      </div>
    </div>
  );
};

CategorySelectInput.propTypes = {
  name: PropTypes.string,
};

export default CategorySelectInput;
