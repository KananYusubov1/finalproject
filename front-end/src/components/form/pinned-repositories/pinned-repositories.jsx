import FormHeader from "../form-header/index.js";
import FormPinnedRepository from "../form-pinned-repository/index.js";
import { useEffect, useState } from "react";
import { SettingsService } from "../../../services";
import InlineLoader from "../../loaders/inline-loader/index.js";
import { ThreeDots } from "react-loader-spinner";
import InfoView from "../../common/info-view/index.js";

const PinnedRepositories = () => {
  const [repos, setRepos] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [errorMessage, setErrorMessage] = useState(null);

  useEffect(() => {
    SettingsService.isConnectedAccountTypeExist("GitHub")
      .then(() => {
        SettingsService.getAllRepositories()
          .then((res) => {
            setIsLoading(false);
            setRepos(res);
          })
          .catch((error) => {
            console.log(error);
            setIsLoading(false);
          });
      })
      .catch(() => {
        setIsLoading(false);
        setErrorMessage(
          "Please connect your GitHub account for using this feature."
        );
      });
  }, []);

  return (
    <div className={"full-card-schema"}>
      <FormHeader
        title={"GitHub"}
        description={
          "Pin your GitHub repositories to your profile.\nRepositories will disappear from your profile if you remove the OAuth association with GitHub."
        }
      />
      <div
        className={
          "w-full min-h-fit  max-h-96  flex flex-col gap-5 overflow-y-auto"
        }
      >
        {isLoading ? (
          <InlineLoader
            loader={
              <ThreeDots
                height="24"
                width="24"
                color={"#3B49DF"}
                wrapperClass="radio-wrapper"
                radius="9"
                ariaLabel="three-dots-loading"
                wrapperStyle={{}}
                wrapperClassName=""
                visible={true}
              />
            }
            message={"Repositories loading..."}
            version={"input-like"}
          />
        ) : errorMessage ? (
          <InfoView message={errorMessage} version={"input-like"} />
        ) : (
          repos.map((repo) => (
            <FormPinnedRepository key={repo.repoId} repo={repo} />
          ))
        )}
      </div>
    </div>
  );
};

export default PinnedRepositories;
