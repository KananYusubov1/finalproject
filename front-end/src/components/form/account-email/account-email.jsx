import PropTypes from "prop-types";

const AccountEmail = ({ email }) => {
  return (
    <div className={"flex gap-4"}>
      <span className={"font-bold text-dark-text dark:text-gray-500"}>
        {email.title} email
      </span>
      <span className={"dark:text-gray-300"}>{email.email}</span>
    </div>
  );
};

AccountEmail.propTypes = {
  email: PropTypes.object,
};

export default AccountEmail;
