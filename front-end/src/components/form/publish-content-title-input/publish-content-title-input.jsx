import PropTypes from "prop-types";
import { useField } from "formik";

const PublishContentTitleInput = ({
  autoComplete,
  placeholder,
  maxLength,
  ...props
}) => {
  const [field] = useField(props);

  return (
    <label className={"w-full h-fit flex flex-col justify-between gap-2"}>
      <input
        {...field}
        {...props}
        autoComplete={autoComplete}
        placeholder={placeholder}
        maxLength={maxLength}
        className={
          "border-none ring-0 !outline-none text-4xl font-bold !p-0 text-input"
        }
      />
    </label>
  );
};

PublishContentTitleInput.propTypes = {
  title: PropTypes.string,
  placeholder: PropTypes.string,
  autoComplete: PropTypes.string,
  maxLength: PropTypes.number,
};

export default PublishContentTitleInput;
