import PropTypes from "prop-types";
import { useState } from "react";
import { SettingsService } from "../../../services";
import { successToast } from "../../../utils/toast.js";
import { ThreeDots } from "react-loader-spinner";

const FormPinnedRepository = ({ repo }) => {
  const [isSelectedLocal, setIsSelectedLocal] = useState(repo.isSelected);
  const [isLoading, setIsLoading] = useState(false);

  const handleUpdatePinnedRepository = () => {
    setIsLoading(true);
    SettingsService.updatePinnedRepositories(repo.repoId)
      .then(() => {
        setIsLoading(false);
        setIsSelectedLocal(!isSelectedLocal);
        successToast(
          !isSelectedLocal ? "Repository pinned" : "Repository unpinned"
        );
      })
      .catch((error) => {
        setIsLoading(false);
        console.log(error);
      });
  };
  return (
    <div
      className={
        "w-full h-fit   flex items-center justify-between px-3 py-3 dark:text-gray-300"
      }
    >
      <div className={"flex items-center gap-2"}>
        <span className={"font-semibold font-sans"}>{repo.repoName}</span>
        {repo.fork && (
          <span className={"text-sm bg-orange-300 rounded-md  px-1 text-black"}>
            fork
          </span>
        )}
      </div>
      <button
        className={"font-semibold"}
        onClick={handleUpdatePinnedRepository}
      >
        {isLoading ? (
          <ThreeDots
            height="24"
            width="24"
            color={"#3B49DF"}
            wrapperClass="radio-wrapper"
            radius="9"
            ariaLabel="three-dots-loading"
            wrapperStyle={{}}
            wrapperClassName=""
            visible={true}
          />
        ) : isSelectedLocal ? (
          "Remove"
        ) : (
          "Select"
        )}
      </button>
    </div>
  );
};

FormPinnedRepository.propTypes = {
  repo: PropTypes.object,
};

export default FormPinnedRepository;
