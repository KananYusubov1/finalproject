import { useField } from "formik";
import { useEffect, useState } from "react";
import { convertToBase64 } from "../../../utils/common.js";
import UploadImageButton from "../../action-buttons/upload-image-button/index.js";
import PropTypes from "prop-types";
import DangerButton from "../../buttons/danger-button/index.js";
import PrimaryButton from "../../buttons/primary-button/index.js";

const CoverImageInput = ({ name }) => {
  const [field] = useField(name);

  const [img, setImg] = useState(field.value);

  useEffect(() => {
    if (!img) return;
    convertToBase64([img, ""]).then((res) => {
      field.onChange({ target: { name: name, value: res[0] } });
    });
  }, [img]);

  const handleFileUpload = (blobUrl) => {
    setImg(blobUrl);
  };

  const handleFileSelect = async () => {
    const pickerOpts = {
      types: [
        {
          description: "Images",
          accept: {
            "custom/*": [".png", ".jpeg", ".jpg", ".gif"],
          },
        },
      ],
      excludeAcceptAllOption: true,
      multiple: false,
    };

    const [fileHandle] = await window.showOpenFilePicker(pickerOpts);
    const file = await fileHandle.getFile();
    const blobURL = URL.createObjectURL(file);
    setImg(blobURL);
  };
  return (
    <div>
      {img ? (
        <div className={"w-fit  flex items-center gap-14"}>
          <img
            src={img}
            className={"w-32 h-20 rounded-md object-cover"}
            alt="Cover image"
          />
          <div className={"w-fit flex items-center gap-4"}>
            <PrimaryButton title={"Change"} click={handleFileSelect} />
            <DangerButton
              title={"Remove"}
              click={() => {
                setImg("");
              }}
            />
          </div>
        </div>
      ) : (
        <div>
          <UploadImageButton
            onFileUpload={handleFileUpload}
            horizontal={true}
            label={"Add a cover image"}
          />
        </div>
      )}
    </div>
  );
};

CoverImageInput.propTypes = {
  name: PropTypes.string,
};

export default CoverImageInput;
