import PropTypes from "prop-types";

const FormHeader = ({ title, description }) => {
  return (
    <div className={"flex flex-col gap-2"}>
      <span className={"text-2xl font-bold dark:text-gray-300 select-none"}>
        {title}
      </span>
      <span className={"text-second-text dark:text-gray-500 "}>
        {description}
      </span>
    </div>
  );
};

FormHeader.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
};

export default FormHeader;
