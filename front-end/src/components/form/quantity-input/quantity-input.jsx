import PropTypes from "prop-types";
import IconButton from "../../buttons/icon-button/index.js";
import { FaMinus, FaPlus } from "react-icons/fa";
import TooltipContainer from "../../common/tooltip-container/index.js";

const QuantityInput = ({ max, value, setValue }) => {
  // const [value, setValue] = useState(value);
  return (
    <div
      className={
        "flex items-center gap-2 card-border-color-schema card-size-schema !p-3 !w-fit !flex-row"
      }
    >
      <IconButton
        icon={<FaMinus />}
        size={20}
        click={() =>
          setValue((prev) => {
            const newValue = prev - 1;
            if (newValue >= 1) return newValue;
            else return prev;
          })
        }
      />

      <TooltipContainer tooltip={"Product quantity"}>
        <span className={"w-10 text-center text-xl font-semibold"}>
          {value}
        </span>
      </TooltipContainer>

      <IconButton
        icon={<FaPlus />}
        size={20}
        click={() =>
          setValue((prev) => {
            const newValue = prev + 1;
            if (newValue <= max) return newValue;
            else return prev;
          })
        }
      />
    </div>
  );
};

QuantityInput.propTypes = {
  max: PropTypes.number,
  value: PropTypes.number,
  setValue: PropTypes.func,
};

export default QuantityInput;
