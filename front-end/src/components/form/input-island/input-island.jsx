import PropTypes from "prop-types";
import TextInput from "../text-input/index.js";
import TextAreaInput from "../text-area-input/index.js";
import FormAvatarEditor from "../form-avatar-editor/index.js";
import RadioButtonGroup from "../radio-button-group/index.js";
import FormThemeCard from "../form-theme-card/index.js";
import ColorInput from "../color-input/index.js";
import UsernameInput from "../username-input/index.js";
import UrlInput from "../url-input/index.js";

const InputIsland = ({ island }) => {
  return (
    <div className={"full-card-schema"}>
      <span className={"text-2xl font-bold dark:text-gray-300 select-none"}>
        {island.name}
      </span>

      <div className={"w-full flex flex-col gap-4"}>
        {island.inputs.map((input) => {
          return (
            <div key={input.stateName}>
              {input.type === "text" ? (
                <TextInput name={input.stateName} {...input} bigTitle={true} />
              ) : input.type === "text-area" ? (
                <TextAreaInput
                  name={input.stateName}
                  {...input}
                  bigTitle={true}
                />
              ) : input.type === "avatar" ? (
                <FormAvatarEditor name={input.stateName} {...input} />
              ) : input.type === "theme" ? (
                <RadioButtonGroup
                  name={input.stateName}
                  isHorizontal={true}
                  label={"Platform Theme"}
                  options={[
                    {
                      value: "System",
                      data: <FormThemeCard key={"system"} theme={"System"} />,
                    },
                    {
                      value: "Light",
                      data: <FormThemeCard key={"light"} theme={"Light"} />,
                    },
                    {
                      value: "Dark",
                      data: <FormThemeCard key={"dark"} theme={"Dark"} />,
                    },
                  ]}
                />
              ) : input.type === "contentPerPage" ? (
                <RadioButtonGroup
                  name={input.stateName}
                  isHorizontal={true}
                  label={"Content Per Page"}
                  options={[
                    {
                      value: 10,
                      data: (
                        <span className={"text-2xl font-semibold"}>10</span>
                      ),
                    },
                    {
                      value: 15,
                      data: (
                        <span className={"text-2xl font-semibold"}>15</span>
                      ),
                    },
                    {
                      value: 20,
                      data: (
                        <span className={"text-2xl font-semibold"}>20</span>
                      ),
                    },
                    {
                      value: 25,
                      data: (
                        <span className={"text-2xl font-semibold"}>25</span>
                      ),
                    },
                    {
                      value: 30,
                      data: (
                        <span className={"text-2xl font-semibold"}>30</span>
                      ),
                    },
                  ]}
                />
              ) : input.type === "color" ? (
                <ColorInput
                  name={input.stateName}
                  isProfile={true}
                  {...input}
                />
              ) : input.type === "username" ? (
                <UsernameInput
                  name={input.stateName}
                  {...input}
                  bigTitle={true}
                />
              ) : input.type === "url" ? (
                <UrlInput name={input.stateName} {...input} bigTitle={true} />
              ) : (
                <></>
              )}
            </div>
          );
        })}
      </div>
    </div>
  );
};

InputIsland.propTypes = {
  island: PropTypes.object,
};

export default InputIsland;
