import { useEffect, useState } from "react";
import { ErrorMessage, useField } from "formik";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import FirstAvatarEditor from "../../pages/build-profile/first-avatar-editor/index.js";
import PropTypes from "prop-types";
import { useUser } from "../../../utils/user.js";
import { createModal } from "../../../utils/modal.js";

const FormAvatarEditor = ({ name }) => {
  const [field] = useField(name);
  const [animationParent] = useAutoAnimate();

  const [avatar, setAvatar] = useState();

  const handleSetAvatar = (newAvatar) => {
    setAvatar(newAvatar);
  };

  useEffect(() => {
    setAvatar(field.value);
  }, []);

  useEffect(() => {
    field.onChange({ target: { name: name, value: avatar } });
  }, [avatar]);

  return (
    <div className={"flex flex-col  gap-3"} ref={animationParent}>
      <ErrorMessage
        name={field.name}
        component={"small"}
        className={"text-xs block text-red-700"}
      />

      <span className={"font-semibold dark:text-gray-400"}>Avatar</span>

      <div className={"flex justify-start items-center gap-4"}>
        {/*Profile Photo*/}
        <div>
          <img
            src={avatar}
            alt="avatar"
            className={"w-14 h-14 rounded-full object-cover"}
          />
        </div>

        {/*Edit image*/}
        <button
          type={"button"}
          onClick={() => {
            createModal("editProfilePhoto", handleSetAvatar);
          }}
          className={
            "p-2 px-3  text-black  font-medium rounded-lg transition-all duration-500 bg-[#d6d6d6] hover:bg-[#a3a3a3] "
          }
        >
          <p>Edit profile image</p>
        </button>
      </div>
    </div>
  );
};

FormAvatarEditor.propTypes = {
  name: PropTypes.string,
};

export default FormAvatarEditor;
