import PropTypes from "prop-types";
import { MdOutlineAddAPhoto } from "react-icons/md";

const PondImagesAddButton = ({ onAddImage }) => {
  return (
    <div
      className={
        "flex  items-center justify-center w-[144px] h-[118px]  mb-[15px] border-2 border-[#26365a]  bg-[#1b1d1e] cursor-pointer text-[#8ca6c3] hover:text-[#409bfb] hover:border-[#0337a1] rounded-md"
      }
      onClick={onAddImage}
    >
      <MdOutlineAddAPhoto size={54} />
    </div>
  );
};

PondImagesAddButton.propTypes = {
  onAddImage: PropTypes.func,
};

export default PondImagesAddButton;
