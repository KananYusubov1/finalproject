import PropTypes from "prop-types";
import { useField } from "formik";
import { FaRegStar, FaStar } from "react-icons/fa6";
import Rating from "react-rating";

const RatingInput = ({ name }) => {
  const [field] = useField(name);
  return (
    <>
      <Rating
        className="!flex  !items-center"
        initialRating={field.value ?? 0}
        emptySymbol={<FaRegStar fill={"#a0aec0"} size={22} />}
        fullSymbol={<FaStar fill={"#fde047"} size={22} />}
        onChange={(value) =>
          field.onChange({ target: { name: name, value: value } })
        }
      />
    </>
  );
};

RatingInput.propTypes = {
  name: PropTypes.string,
};

export default RatingInput;
