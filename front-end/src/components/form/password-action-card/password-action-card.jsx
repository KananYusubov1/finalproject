import { useEffect, useState } from "react";
import InlineLoader from "../../loaders/inline-loader/index.js";
import { ThreeDots } from "react-loader-spinner";
import UpdatePasswordForm from "../update-password-form/index.js";
import SetPasswordForm from "../set-password-form/index.js";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import { passwordIsSet } from "../../../services/settings.js";

const PasswordActionCard = () => {
  const { animationParent } = useAutoAnimate();

  const [passwordSet, setPasswordSet] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    handleRefresh();
  }, []);

  const handleRefresh = () => {
    setLoading(true);
    passwordIsSet()
      .then(() => {
        setLoading(false);
        setPasswordSet(true);
      })
      .catch(() => {
        setLoading(false);
        setPasswordSet(false);
      });
  };

  return (
    <div>
      <div ref={animationParent}>
        {loading ? (
          <div className={"full-card-schema"}>
            <InlineLoader
              loader={
                <ThreeDots
                  height="24"
                  width="24"
                  color={"#3B49DF"}
                  wrapperClass="radio-wrapper"
                  radius="9"
                  ariaLabel="three-dots-loading"
                  wrapperStyle={{}}
                  wrapperClassName=""
                  visible={true}
                />
              }
              message={"Loading..."}
              version={"input-like"}
            />
          </div>
        ) : passwordSet ? (
          <UpdatePasswordForm refresh={handleRefresh} />
        ) : (
          <SetPasswordForm refresh={handleRefresh} />
        )}
      </div>
    </div>
  );
};

export default PasswordActionCard;
