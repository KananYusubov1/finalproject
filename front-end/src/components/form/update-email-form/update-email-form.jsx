import FormHeader from "../form-header/index.js";
import { Form, Formik } from "formik";
import { updateUser, useUser } from "../../../utils/user.js";
import TextInput from "../text-input/index.js";
import PrimaryAccentButton from "../../buttons/primary-accent-button/index.js";
import { useState } from "react";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import InlineLoader from "../../loaders/inline-loader/index.js";
import { ThreeDots } from "react-loader-spinner";
import ErrorView from "../../common/error-view/index.js";
import InlineOtpVerification from "../../common/inline-otp-verification/index.js";
import { UpdateEmailFormSchema } from "../../../validation/Schemas/UpdateEmailFormSchema.js";
import { updatePrimaryEmail } from "../../../services/settings.js";
import { successToast } from "../../../utils/toast.js";

const UpdateEmailForm = () => {
  const [animationParent] = useAutoAnimate();

  const user = useUser();

  const [errorMessage, setErrorMessage] = useState("");
  const [loadingMessage, setLoadingMessage] = useState("");
  const [otpStep, setOtpStep] = useState(false);
  const [isEmailVerified, setIsEmailVerified] = useState(false);

  return (
    <div className={"full-card-schema"}>
      <FormHeader
        title={"Update Email"}
        description={"Update your primary email."}
      />
      <Formik
        initialValues={{
          email: "",
        }}
        onSubmit={(values, actions) => {
          setErrorMessage("");
          if (!isEmailVerified) {
            setOtpStep(true);
            return;
          }

          console.log("start update email");
          setLoadingMessage("Updating email...");
          updatePrimaryEmail(values.email)
            .then(() => {
              successToast("Email updated successfully.");
              setLoadingMessage("");
              setErrorMessage("");
              setOtpStep(false);
              setIsEmailVerified(false);
              updateUser({ email: values.email });
              actions.resetForm();
            })
            .catch((error) => {
              setLoadingMessage("");
              console.log(error);
              setErrorMessage(error.message);
            });
        }}
        validationSchema={UpdateEmailFormSchema}
        ref={animationParent}
      >
        {({ isValid, values, submitForm, dirty }) => (
          <Form className={"flex flex-col gap-5"} ref={animationParent}>
            <TextInput
              name={"email"}
              autoComplete={"email"}
              title={"Primary Email"}
              bigTitle={true}
            />

            {loadingMessage && (
              <InlineLoader
                loader={
                  <ThreeDots
                    height="24"
                    width="24"
                    color={"#3B49DF"}
                    wrapperClass="radio-wrapper"
                    radius="9"
                    ariaLabel="three-dots-loading"
                    wrapperStyle={{}}
                    wrapperClassName=""
                    visible={true}
                  />
                }
                message={loadingMessage}
                version={"input-like"}
              />
            )}
            {errorMessage && (
              <ErrorView message={errorMessage} version={"input-like"} />
            )}

            <div className={"w-fit"}>
              <PrimaryAccentButton
                title={"Update Email"}
                type={"submit"}
                disabled={
                  !dirty || !isValid || values.email === user.email || otpStep
                }
              />
            </div>

            {otpStep && (
              <InlineOtpVerification
                email={values.email}
                emailVerified={() => {
                  console.log("email verified");
                  setIsEmailVerified(true);
                  setOtpStep(false);
                  submitForm();
                }}
              />
            )}
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default UpdateEmailForm;
