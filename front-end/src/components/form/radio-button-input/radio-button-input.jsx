import PropTypes from "prop-types";

const RadioButtonInput = ({ id, option }) => {
  return (
    <div>
      <input type={"radio"} />
    </div>
  );
};

RadioButtonInput.propTypes = {
  id: PropTypes.string,
  option: PropTypes.object,
};

export default RadioButtonInput;
