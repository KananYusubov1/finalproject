import PrimaryAccentButton from "../../buttons/primary-accent-button/index.js";
import PropTypes from "prop-types";
import classNames from "classnames";
//formIsDirty, dirty, touched
const SubmitIsland = ({ formIsDirty, label = "Save Profile Information" }) => {
  return (
    <div
      className={classNames({
        "full-card-schema": true,
        "sticky bottom-0 smooth-animation animate-slideUpAndFade z-50":
          formIsDirty,
      })}
    >
      <PrimaryAccentButton
        title={label}
        type={"submit"}
        disabled={!formIsDirty}
      />
    </div>
  );
};

SubmitIsland.propTypes = {
  formIsDirty: PropTypes.bool,
  label: PropTypes.string,
};

export default SubmitIsland;
