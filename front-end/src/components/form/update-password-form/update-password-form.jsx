import { useAutoAnimate } from "@formkit/auto-animate/react";
import { useState } from "react";
import { useUser } from "../../../utils/user.js";
import FormHeader from "../form-header/index.js";
import { Form, Formik } from "formik";
import PasswordInput from "../password-input/index.js";
import InlineLoader from "../../loaders/inline-loader/index.js";
import { ThreeDots } from "react-loader-spinner";
import ErrorView from "../../common/error-view/index.js";
import PrimaryAccentButton from "../../buttons/primary-accent-button/index.js";
import InlineOtpVerification from "../../common/inline-otp-verification/index.js";
import { UpdatePasswordSchema } from "../../../validation/Schemas/UpdatePasswordSchema.js";
import { userUpdatePassword } from "../../../services/user.js";
import { successToast } from "../../../utils/toast.js";
import PropTypes from "prop-types";

const UpdatePasswordForm = ({ refresh }) => {
  const [animationParent] = useAutoAnimate();

  const [errorMessage, setErrorMessage] = useState("");
  const [loadingMessage, setLoadingMessage] = useState("");
  const [otpStep, setOtpStep] = useState(false);
  const [emailVerified, setEmailVerified] = useState(false);

  const { email } = useUser();
  return (
    <div className={"full-card-schema"}>
      <FormHeader
        title={"Update password"}
        description={"Update your password."}
      />
      <Formik
        initialValues={{
          currentPassword: "",
          password: "",
          confirmPassword: "",
        }}
        onSubmit={(values) => {
          setErrorMessage("");
          if (values.password !== values.confirmPassword) {
            setErrorMessage("Password and Confirm Password do not match");
            return;
          }

          if (values.currentPassword === values.confirmPassword) {
            setErrorMessage("New password cannot be same as old password");
            return;
          }

          setLoadingMessage("Updating password...");
          userUpdatePassword(values.currentPassword, values.password)
            .then(() => {
              setLoadingMessage("");
              successToast("Password updated successfully!");
              refresh();
            })
            .catch((error) => {
              setLoadingMessage("");
              setErrorMessage(error.message);
            });
        }}
        validationSchema={UpdatePasswordSchema}
      >
        {({ submitForm }) => (
          <Form className={"flex flex-col gap-4"} ref={animationParent}>
            {emailVerified && (
              <>
                <PasswordInput
                  title={"Current password"}
                  name={"currentPassword"}
                  autoComplete={"current-password"}
                  bigTitle={true}
                />
                <PasswordInput
                  title={"Password"}
                  name={"password"}
                  autoComplete={"new-password"}
                  isNewPassword={true}
                  bigTitle={true}
                />
                <PasswordInput
                  title={"Confirm new password"}
                  name={"confirmPassword"}
                  bigTitle={true}
                />
              </>
            )}
            <div className={"flex flex-col gap-3"}>
              {loadingMessage && (
                <InlineLoader
                  loader={
                    <ThreeDots
                      height="24"
                      width="24"
                      color={"#3B49DF"}
                      wrapperClass="radio-wrapper"
                      radius="9"
                      ariaLabel="three-dots-loading"
                      wrapperStyle={{}}
                      wrapperClassName=""
                      visible={true}
                    />
                  }
                  message={loadingMessage}
                  version={"input-like"}
                />
              )}
              {errorMessage && (
                <ErrorView message={errorMessage} version={"input-like"} />
              )}

              <div className={"w-fit"}>
                <PrimaryAccentButton
                  title={emailVerified ? "Update Password" : "Verify Email"}
                  type={"submit"}
                  disabled={otpStep}
                  click={() => {
                    if (!emailVerified) {
                      setOtpStep(true);
                    }
                  }}
                />
              </div>
            </div>

            {otpStep && (
              <InlineOtpVerification
                email={email}
                emailVerified={() => {
                  setEmailVerified(true);
                  setOtpStep(false);
                  submitForm();
                }}
              />
            )}
          </Form>
        )}
      </Formik>
    </div>
  );
};

UpdatePasswordForm.propTypes = {
  refresh: PropTypes.func,
};

export default UpdatePasswordForm;
