import Select from "react-select";
import PropTypes from "prop-types";
import { ErrorMessage, useField } from "formik";
import "./select-input.css";

const SelectInput = ({ name, placeholder, data, isLoading = false }) => {
  const [field] = useField(name);

  const handleOnChange = (option) => {
    field.onChange({ target: { name: name, value: option?.value ?? "" } });
  };

  return (
    <div className={"w-full flex flex-col gap-1"}>
      <Select
        className={"w-full"}
        classNamePrefix={"react-select"}
        placeholder={placeholder}
        isClearable={true}
        isSearchable={true}
        isLoading={isLoading}
        defaultValue={data.find((option) => option.value === field.value)}
        value={data.find((option) => option.value === field.value)}
        onChange={handleOnChange}
        options={data}
      />

      <ErrorMessage
        name={field.name}
        component={"small"}
        className={"text-xs block text-red-700"}
      />
    </div>
  );
};

SelectInput.propTypes = {
  name: PropTypes.string,
  placeholder: PropTypes.string,
  data: PropTypes.array,
  isLoading: PropTypes.bool,
};

export default SelectInput;
