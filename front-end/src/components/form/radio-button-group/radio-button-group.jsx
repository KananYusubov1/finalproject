import * as RadioGroup from "@radix-ui/react-radio-group";
import PropTypes from "prop-types";
import { nanoid } from "nanoid";
import "./radio-button-group.css";
import classNames from "classnames";
import { useField } from "formik";

const RadioButtonGroup = ({ name, options, label, isHorizontal = false }) => {
  const [field] = useField(name);

  const handleOnChange = (newValue) => {
    field.onChange({ target: { name: name, value: newValue } });
  };

  return (
    <div>
      <form>
        <RadioGroup.Root
          className={"flex  flex-col gap-2.5"}
          defaultValue={field.value}
          onValueChange={handleOnChange}
        >
          {label && (
            <span className={"font-semibold dark:text-gray-400 select-none"}>
              {label}
            </span>
          )}
          <div
            className={classNames({
              "flex gap-3": true,
              "flex-col": !isHorizontal,
            })}
          >
            {options.map((option) => {
              const _id = nanoid();

              return (
                <label
                  htmlFor={_id}
                  key={_id}
                  className={classNames({
                    "w-full text-base cursor-pointer  select-none flex bg-body-bg dark:bg-dark-body-bg  hover:bg-hover-blue-clr hover:dark:bg-dark-hover-bg-clr  p-2 rounded-lg transition-all duration-150 ease-in": true,
                    "items-start gap-3": isHorizontal,
                    "items-center": !isHorizontal,
                  })}
                >
                  <RadioGroup.Item
                    className=" w-4 h-4 rounded-full shadow dark:shadow-gray-800 shadow-gray-600 focus:shadow-lg focus:shadow-black outline-none cursor-pointer data-[state=checked]:bg-accent smooth-animation flex items-center justify-center "
                    value={option.value}
                    id={_id}
                  >
                    <RadioGroup.Indicator className="w-[6px] h-[6px]     bg-white rounded-full  cursor-pointer" />
                  </RadioGroup.Item>
                  <label
                    className="text-black dark:text-gray-300 text-base font-medium leading-none pl-[15px] cursor-pointer w-full h-full"
                    htmlFor={_id}
                  >
                    {option.data}
                  </label>
                </label>
              );
            })}{" "}
          </div>
        </RadioGroup.Root>
      </form>
    </div>
  );
};

RadioButtonGroup.propTypes = {
  name: PropTypes.string,
  label: PropTypes.string,
  options: PropTypes.array,
  isHorizontal: PropTypes.bool,
};

export default RadioButtonGroup;
