import PropTypes from "prop-types";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import { ErrorMessage, useField } from "formik";
import { useState } from "react";
import { convertToBase64 } from "../../../utils/common.js";
import PondImageCard from "../pond-image-card/index.js";
import PondImagesAddButton from "../pond-images-add-button/index.js";

const ProductImageInput = ({ name }) => {
  const [animationParent] = useAutoAnimate();
  const [field] = useField(name);

  const [actionIndex, setActionIndex] = useState(0);
  const [images, setImages] = useState(field.value ?? []);

  const handleUploadImages = (imageIndex) => {
    setActionIndex(imageIndex);
    document.getElementById("fileInput").click();
  };

  const handleOnDelete = (index) => {
    const filteredImages = [
      ...images.slice(0, index),
      ...images.slice(index + 1),
    ];
    convertToBase64(filteredImages).then((data) => {
      field.onChange({ target: { name: name, value: data } });
      setImages(filteredImages);
    });
  };

  const uploadImages = (e) => {
    const uploadImages = Array.from(e.target.files).map((f) =>
      URL.createObjectURL(f)
    );

    if (uploadImages.length <= 0) return;

    const mergedImages = [
      ...images.slice(0, actionIndex),
      ...uploadImages,
      ...images.slice(actionIndex + 1),
    ];

    convertToBase64(mergedImages).then((data) => {
      field.onChange({ target: { name: name, value: data } });
      setImages(mergedImages);
    });
  };

  function handleAddImages() {
    setActionIndex(images.length);
    document.getElementById("fileInput").click();
  }

  return (
    <div className={" w-full flex-col my-8"} ref={animationParent}>
      <div className={"flex-col mb-4"}>
        <div className={"w-full dark:text-white mb-2 font-semibold text-xl"}>
          Product Photos
        </div>
      </div>
      <ErrorMessage
        name={field.name}
        component={"small"}
        className={"text-xs block mt-2 text-red-400"}
      />
      <input
        className={"hidden"}
        type={"file"}
        id={"fileInput"}
        name="img"
        multiple
        accept={"image/ .jpg,.png,.jpeg,.jpg,"}
        onChange={uploadImages}
      />
      <div
        className={
          "flex flex-wrap w-full mt-[10px] mb-[20px] pt-[15px] pl-[15px] pr-[5px] rounded-md card-border-color-schema"
        }
        ref={animationParent}
      >
        {images.map((image, index) => (
          <PondImageCard
            uploadImages={handleUploadImages}
            key={index}
            index={index}
            onDeleteClick={handleOnDelete}
            selectedImage={image}
          />
        ))}
        <PondImagesAddButton onAddImage={handleAddImages} />
      </div>
    </div>
  );
};

ProductImageInput.propTypes = {
  name: PropTypes.string,
};

export default ProductImageInput;
