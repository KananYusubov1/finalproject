import { AiFillGithub } from "react-icons/ai";
import {
  connectGithub,
  getEnterWithProviderDTO,
  signOut,
} from "../../../supabase.js";
import { useEffect, useState } from "react";
import { supabase } from "../../../client.js";
import {
  startLoadingProcess,
  stopLoadingProcess,
} from "../../../utils/process.js";
import { SettingsService } from "../../../services";
import { successToast } from "../../../utils/toast.js";
import PropTypes from "prop-types";

const ConnectGithubAccount = () => {
  const handleConnectGitHub = () => {
    connectGithub(window.location.pathname);
  };

  const [githubConnected, setGithubConnected] = useState(true);

  useEffect(() => {
    SettingsService.isConnectedAccountTypeExist("GitHub")
      .then(() => {
        setGithubConnected(true);
      })
      .catch(() => {
        setGithubConnected(false);

        supabase.auth
          .getUser()
          .then(({ data }) => {
            const dto = getEnterWithProviderDTO(data);
            startLoadingProcess("GitHub Connecting");
            SettingsService.addConnectedAccount(dto.providerEnter)
              .then(() => {
                stopLoadingProcess();
                signOut();
                setGithubConnected(true);
                successToast("Connected GitHub Account");
                window.location.reload();
              })
              .catch((error) => {
                stopLoadingProcess();
                console.log(error);
              });
          })
          .catch(() => {
            stopLoadingProcess();
          });
      });
  }, []);

  return githubConnected ? null : (
    <div className={"full-card-schema"}>
      <button
        onClick={handleConnectGitHub}
        className={`w-fit min-h-6 p-3 px-4 rounded-lg flex justify-center items-center gap-2 text-white font-medium transition-all duration-150 bg-github-bg hover:bg-github-hover-bg `}
      >
        <AiFillGithub size={24} />
        {"Connect GitHub Account"}
      </button>
    </div>
  );
};

export default ConnectGithubAccount;
