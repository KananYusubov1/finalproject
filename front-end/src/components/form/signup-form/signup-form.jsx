import { Form, Formik } from "formik";
import TextInput from "../text-input/index.js";
import PasswordInput from "../password-input/index.js";
import Checkbox from "../checkbox/index.js";
import { SignUpSchema } from "../../../validation/Schemas/SignUpSchema.js";
import { useState } from "react";
import ErrorView from "../../common/error-view/index.js";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import PropTypes from "prop-types";

import { UserService } from "../../../services";
import {
  startLoadingProcess,
  stopLoadingProcess,
} from "../../../utils/process.js";

const SignupForm = ({ openSignupOtp }) => {
  const [animationParent] = useAutoAnimate();
  const [errorMessage, setErrorMessage] = useState("");

  return (
    <Formik
      onSubmit={(values) => {
        if (values.password !== values.confirmPassword) {
          setErrorMessage("Password and Confirm Password do not match");
          return;
        }

        startLoadingProcess();
        UserService.userExistsByEmail(values.email)
          .then(() => {
            setErrorMessage("Account already exists");
          })
          .catch(() => {
            // start email verification process
            openSignupOtp(values);
          })
          .finally(() => {
            stopLoadingProcess();
          });
      }}
      initialValues={{
        name: "",
        email: "",
        password: "",
        confirmPassword: "",
        rememberMe: true,
      }}
      validationSchema={SignUpSchema}
      ref={animationParent}
    >
      {() => (
        <Form className={"flex flex-col gap-4 "} ref={animationParent}>
          <TextInput title={"Name"} name={"name"} autoComplete={"name"} />
          <TextInput title={"Email"} name={"email"} autoComplete={"email"} />
          <PasswordInput
            title={"Password"}
            name={"password"}
            autoComplete={"new-password"}
            isNewPassword
          />
          <PasswordInput
            title={"Confirm Password"}
            name={"confirmPassword"}
            autoComplete={"current-password"}
          />

          <Checkbox name={"rememberMe"} checked={true} title={"Remember me"} />

          {errorMessage && <ErrorView message={errorMessage} />}

          <button
            type="submit"
            className={
              "py-[10px] w-full px-10 text-white  font-medium  bg-accent hover:bg-tz-red-hover-dark rounded-lg text-sm transition-all duration-150 ease-in hover:bg-blue-800"
            }
          >
            Sign Up
          </button>
        </Form>
      )}
    </Formik>
  );
};

SignupForm.propTypes = {
  openSignupOtp: PropTypes.func,
};

export default SignupForm;
