import FormHeader from "../form-header/index.js";
import PasswordInput from "../password-input/index.js";
import { Form, Formik } from "formik";
import PrimaryAccentButton from "../../buttons/primary-accent-button/index.js";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import { useState } from "react";
import ErrorView from "../../common/error-view/index.js";
import { SetPasswordFormSchema } from "../../../validation/Schemas/SetPasswordFormSchema.js";
import InlineOtpVerification from "../../common/inline-otp-verification/index.js";
import { useUser } from "../../../utils/user.js";
import InlineLoader from "../../loaders/inline-loader/index.js";
import { ThreeDots } from "react-loader-spinner";
import { userResetPassword } from "../../../services/user.js";
import { successToast } from "../../../utils/toast.js";
import PropTypes from "prop-types";

const SetPasswordForm = ({ refresh }) => {
  const [animationParent] = useAutoAnimate();

  const [errorMessage, setErrorMessage] = useState("");
  const [loadingMessage, setLoadingMessage] = useState("");
  const [otpStep, setOtpStep] = useState(false);
  const [emailVerified, setEmailVerified] = useState(false);

  const { email } = useUser();

  console.log(email);

  const handleAction = (password) => {
    setLoadingMessage("Resetting password...");
    userResetPassword(email, password)
      .then(() => {
        setLoadingMessage("");
        successToast("Password reset successfully!");
        refresh();
      })
      .catch((error) => {
        setLoadingMessage("");
        setErrorMessage(error.message);
      });
  };

  return (
    <div className={"full-card-schema"}>
      <FormHeader
        title={"Set new password"}
        description={
          "If your account was created using social account authentication, you may prefer to add an email log in. If you signed up with a social media account, please set the password for your primary email address in order to enable this. Please note that email login is in addition to social login rather than a replacement for it, so your authenticated social account will continue to be linked to your account."
        }
      />
      <Formik
        initialValues={{
          password: "",
          confirmPassword: "",
        }}
        onSubmit={(values) => {
          console.log("submit start");
          setErrorMessage("");
          if (values.password !== values.confirmPassword) {
            setErrorMessage("Password and Confirm Password do not match");
            return;
          }

          if (!emailVerified) {
            console.log("email not verified");
            setOtpStep(true);
            return;
          }

          handleAction(values.password);
        }}
        validationSchema={SetPasswordFormSchema}
      >
        {({ dirty, submitForm, values }) => (
          <Form className={"flex flex-col gap-4"} ref={animationParent}>
            <PasswordInput
              title={"Password"}
              name={"password"}
              autoComplete={"new-password"}
              isNewPassword={true}
              bigTitle={true}
            />
            <PasswordInput
              title={"Confirm new password"}
              name={"confirmPassword"}
              bigTitle={true}
            />

            {loadingMessage && (
              <InlineLoader
                loader={
                  <ThreeDots
                    height="24"
                    width="24"
                    color={"#3B49DF"}
                    wrapperClass="radio-wrapper"
                    radius="9"
                    ariaLabel="three-dots-loading"
                    wrapperStyle={{}}
                    wrapperClassName=""
                    visible={true}
                  />
                }
                message={loadingMessage}
                version={"input-like"}
              />
            )}
            {errorMessage && (
              <ErrorView message={errorMessage} version={"input-like"} />
            )}

            <div className={"w-fit"}>
              <PrimaryAccentButton
                title={"Set Password"}
                type={"submit"}
                disabled={!dirty || otpStep}
              />
            </div>
            {otpStep && (
              <InlineOtpVerification
                email={email}
                emailVerified={() => {
                  setEmailVerified(true);
                  setOtpStep(false);
                  handleAction(values.password);
                }}
              />
            )}
          </Form>
        )}
      </Formik>
    </div>
  );
};

SetPasswordForm.propTypes = {
  refresh: PropTypes.func,
};

export default SetPasswordForm;
