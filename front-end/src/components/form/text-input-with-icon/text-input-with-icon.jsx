import TextInput from "../text-input/index.js";
import { IconContext } from "react-icons";
import PropTypes from "prop-types";
import "./text-input-with-icon.css";

const TextInputWithIcon = ({
  name,
  autoComplete,
  placeholder,
  isRequired,
  maxLength,
  icon,
  ...props
}) => {
  return (
    <div className={"w-full relative"}>
      <IconContext.Provider
        value={{ color: "#717171", className: "text-input-icon", size: "22" }}
      >
        {icon}
      </IconContext.Provider>
      <TextInput
        {...props}
        name={name}
        placeholder={placeholder}
        isRequired={isRequired}
        autoComplete={autoComplete}
        maxLength={maxLength}
        paddingLeft={"35px"}
      />
    </div>
  );
};

TextInputWithIcon.propTypes = {
  name: PropTypes.string,
  autoComplete: PropTypes.string,
  placeholder: PropTypes.string,
  isRequired: PropTypes.bool,
  maxLength: PropTypes.number,
  icon: PropTypes.element,
};

export default TextInputWithIcon;
