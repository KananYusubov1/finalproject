import ErrorView from "../../common/error-view/index.js";
import InlineLoader from "../../loaders/inline-loader/index.js";
import { Radio } from "react-loader-spinner";
import { useEffect, useState } from "react";
import classNames from "classnames";
import { ErrorMessage, useField } from "formik";
import PropTypes from "prop-types";
import { checkUrlIsAvailable } from "../../../services/common.js";
import { checkUrlValidation } from "../../../utils/common.js";
import { IconContext } from "react-icons";

const UrlInput = ({
  title,
  autoComplete,
  placeholder,
  isRequired,
  maxLength,
  paddingLeft,
  icon,
  bigTitle = false,
  ...props
}) => {
  const [field, meta, helpers] = useField(props);

  useEffect(() => {
    setWebsiteUrlIsAvailable(null);

    const url = field.value;
    if (!url) return;

    // console.log("meta.error: ", meta.error);
    //
    if (!checkUrlValidation(url)) {
      // helpers.setError("Url is not valid");
      setWebsiteUrlIsAvailable("Url is not valid");

      return;
    }

    setIsCheckingUrl(true);

    checkUrlIsAvailable(url)
      .then(() => {
        setWebsiteUrlIsAvailable(null);
        // helpers.setError();
      })
      .catch(() => {
        setWebsiteUrlIsAvailable("This Url is not working");
        helpers.setError("This Url is not working");
      })
      .finally(() => {
        setIsCheckingUrl(false);
      });
  }, [field.value]);

  const [isCheckingUrl, setIsCheckingUrl] = useState(false);
  const [websiteUrlIsAvailable, setWebsiteUrlIsAvailable] = useState(null);

  return (
    <div className={"w-full"}>
      <div className={"w-full relative"}>
        <IconContext.Provider
          value={{ color: "#717171", className: "text-input-icon", size: "22" }}
        >
          {icon}
        </IconContext.Provider>
        <label className={"w-full h-fit flex flex-col justify-between gap-2"}>
          <div className={"flex gap-3 items-center"}>
            <span
              className={classNames({
                " font-medium dark:text-gray-400 select-none": true,
                "text-sm": !bigTitle,
                "font-semibold": bigTitle,
              })}
            >
              {title}
            </span>
            {isRequired && <span className={"font-bold text-red-900"}>*</span>}
            <ErrorMessage
              name={field.name}
              component={"small"}
              className={"text-xs block text-red-700 dark:text-red-500"}
            />
          </div>
          <div>
            <input
              {...props}
              {...field}
              autoComplete={autoComplete}
              placeholder={placeholder}
              maxLength={maxLength}
              className={"text-input"}
              type={"peer text"}
              style={{
                paddingLeft: icon
                  ? paddingLeft
                    ? paddingLeft
                    : "35px"
                  : paddingLeft,
              }}
            />
          </div>
        </label>
      </div>
      {websiteUrlIsAvailable !== null && (
        <ErrorView message={websiteUrlIsAvailable} />
      )}

      {isCheckingUrl && (
        <InlineLoader
          loader={
            <Radio
              visible={true}
              height="24"
              width="24"
              colors={["#3B49DF", "#3B49DF", "#3B49DF"]}
              ariaLabel="radio-loading"
              wrapperStyle={{}}
              wrapperClass="radio-wrapper"
            />
          }
          message={"Url Checking.."}
          version={"input-like"}
        />
      )}
    </div>
  );
};

UrlInput.propTypes = {
  title: PropTypes.string,
  placeholder: PropTypes.string,
  autoComplete: PropTypes.string,
  isRequired: PropTypes.bool,
  maxLength: PropTypes.number,
  paddingLeft: PropTypes.string,
  bigTitle: PropTypes.bool,
  icon: PropTypes.element,
};

export default UrlInput;
