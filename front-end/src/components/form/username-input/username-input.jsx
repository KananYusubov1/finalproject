import PropTypes from "prop-types";
import { ErrorMessage, useField } from "formik";
import { useEffect, useState } from "react";
import { UserService } from "../../../services";
import InlineLoader from "../../loaders/inline-loader/index.js";
import { Radio } from "react-loader-spinner";
import ErrorView from "../../common/error-view/index.js";

const UsernameInput = ({ name }) => {
  const [field] = useField(name);
  const [username, setUsername] = useState(field.value);

  const [isLoading, setIsLoading] = useState(false);
  const [userNameIsAvailable, setUserNameIsAvailable] = useState(true);

  useEffect(() => {
    if (username === field.value) return;
    setTimeout(() => {
      setIsLoading(true);
      UserService.userExistsByUsername(username)
        .then(() => {
          setIsLoading(false);
          setUserNameIsAvailable(false);
        })
        .catch(() => {
          setIsLoading(false);
          setUserNameIsAvailable(true);
          field.onChange({ target: { name: name, value: username } });
        });
    }, 1000);
  }, [username]);

  return (
    <label className={"w-full h-fit flex flex-col justify-between gap-2"}>
      <div className={"flex gap-3 items-center"}>
        <span className={"dark:text-gray-400 select-none font-semibold"}>
          Username
        </span>
        <ErrorMessage
          name={field.name}
          component={"small"}
          className={"text-xs block text-red-700 dark:text-red-500"}
        />
        {isLoading && (
          <InlineLoader
            loader={
              <Radio
                visible={true}
                height="24"
                width="24"
                colors={["#3B49DF", "#3B49DF", "#3B49DF"]}
                ariaLabel="radio-loading"
                wrapperStyle={{}}
                wrapperClass="radio-wrapper"
              />
            }
            message={"Username checking ..."}
            version={"input-like"}
          />
        )}
        {!userNameIsAvailable && (
          <ErrorView message={"This Username is not available"} />
        )}
      </div>
      <div>
        <input
          value={username}
          onChange={(e) => {
            setUsername(e.target.value);
          }}
          autoComplete={"user-name"}
          maxLength={100}
          className={
            "peer w-full bg-white dark:bg-dark-component-bg dark:border-dark-border-clr dark:text-gray-300 h-9 border border-border-clr rounded-md py-2 pl-3  focus:outline-none hover:border-hover-border-clr focus:outline-accent focus:border-none transition-all duration-75"
          }
          type={"text"}
        />
      </div>
    </label>
  );
};

UsernameInput.propTypes = {
  name: PropTypes.string,
  bigTitle: PropTypes.bool,
};

export default UsernameInput;
