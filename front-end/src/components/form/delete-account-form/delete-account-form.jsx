import DangerButton from "../../buttons/danger-button/index.js";
import { useEffect, useState } from "react";
import { successToast } from "../../../utils/toast.js";
import { SettingsService } from "../../../services";
import { createModal } from "../../../utils/modal.js";
import { removeUser, useUser } from "../../../utils/user.js";
import { useNavigate } from "react-router-dom";
import { logOutUser } from "../../../utils/auth.js";
import { signOutSetting } from "../../../utils/appSettings.js";

const DeleteAccountForm = () => {
  const [githubConnected, setGithubConnected] = useState(false);

  const { id } = useUser();

  const navigate = useNavigate();

  useEffect(() => {
    SettingsService.isConnectedAccountTypeExist("GitHub")
      .then(() => {
        setGithubConnected(true);
      })
      .catch(() => {
        setGithubConnected(false);
      });
  }, []);

  const onDeleteUser = () => {
    successToast("Account deleted");
    logOutUser();
    removeUser();
    signOutSetting();
    navigate("/");
  };

  return (
    <div className={"full-card-schema dark:text-gray-300"}>
      <span className={"text-2xl font-bold text-red-600"}>Delete Zone</span>
      <span>Deleting your account will:</span>
      {githubConnected && (
        <>
          <p>
            Delete your profile, along with your authentication associations.
            This does not include applications permissions. You will have to
            remove them yourself:
          </p>

          <a
            href="https://github.com/settings/applications"
            target={"_blank"}
            rel="noreferrer"
            className={
              "w-fit text-accent dark:text-dark-text-accent underline-animation"
            }
          >
            GitHub profile settings
          </a>
        </>
      )}
      <p>
        Delete any and all content you have, such as articles, comments, or your
        reading list. <br />
        Allow your username to become available to anyone.
      </p>

      <div className={"w-fit"}>
        <DangerButton
          title={"Delete Account"}
          click={() => {
            createModal("contentDelete", {
              contentId: id,
              contentType: "users",
              onDelete: onDeleteUser,
            });
          }}
        />
      </div>
    </div>
  );
};

export default DeleteAccountForm;
