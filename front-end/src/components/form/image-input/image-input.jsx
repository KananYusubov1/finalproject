import { useEffect, useState } from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { useField } from "formik";
import { convertToBase64 } from "../../../utils/common.js";

const ImageInput = ({ name, title, label, bigPreview, bigTitle }) => {
  const [field] = useField(name);

  const [img, setImg] = useState(field.value);

  useEffect(() => {
    if (!img) return;
    convertToBase64([img, ""]).then((res) => {
      field.onChange({ target: { name: name, value: res[0] } });
    });
  }, [img]);

  const loadFile = (event) => {
    setImg(URL.createObjectURL(event.target.files[0]));
  };

  return (
    <div className={"flex flex-col  gap-3"}>
      <span
        className={classNames({
          " font-medium dark:text-gray-400 select-none": true,
          "text-sm": !bigTitle,
          "font-semibold": bigTitle,
        })}
      >
        {title}
      </span>
      <div className={"flex  items-center gap-3"}>
        <div className=" shrink-0">
          {img ? (
            <img
              id="preview_img"
              className={classNames({
                "h-12 w-12 object-cover rounded-full": true,
                "h-20 w-32 object-cover rounded-md": bigPreview,
              })}
              src={img}
              alt="Current profile photo"
            />
          ) : (
            <div>{label}</div>
          )}
        </div>
        <label className="block">
          <span className="sr-only">Choose profile photo</span>
          <input
            type="file"
            accept="custom/*,.png,.jpg,.jpeg"
            onChange={loadFile}
            className="block w-full text-sm text-slate-500 file:mr-4 file:py-2 file:px-4 file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-violet-50 file:text-violet-700 hover:file:bg-violet-100 dark:text-second-text"
          />
        </label>
      </div>
    </div>
  );
};

ImageInput.propTypes = {
  name: PropTypes.string,
  title: PropTypes.string,
  bigTitle: PropTypes.bool,
};

export default ImageInput;
