import { useEffect } from "react";
import TooltipContainer from "../../common/tooltip-container/index.js";
import { BsClockHistory, BsFillFileTextFill } from "react-icons/bs";
import { useReadingTime } from "react-hook-reading-time";
import { useField, useFormikContext } from "formik";
import PropTypes from "prop-types";

const BodyStat = ({ name }) => {
  const [field] = useField(name);

  const { values, submitForm } = useFormikContext();

  const {
    text, // 1 min read
    minutes, // 1
    words, // 168
  } = useReadingTime(values.body);

  useEffect(() => {
    console.log(minutes);
    field.onChange({
      target: { name: name, value: minutes < 1 ? 1 : minutes },
    });
  }, [minutes, values.body]);

  return (
    <div
      className={
        "w-fit card-color-schema dark:text-white py-1 px-4 rounded-md flex items-center justify-center  gap-6"
      }
    >
      <TooltipContainer tooltip={"Words count"}>
        <p className={"font-semibold flex items-center gap-2"}>
          <BsFillFileTextFill />
          <span>{words}</span>
        </p>
      </TooltipContainer>
      <TooltipContainer tooltip={"Estimated reading time"}>
        <p className={"font-semibold flex items-center gap-2"}>
          <BsClockHistory />
          <span>{text}</span>
        </p>
      </TooltipContainer>
    </div>
  );
};

BodyStat.propTypes = {
  name: PropTypes.string,
};

export default BodyStat;
