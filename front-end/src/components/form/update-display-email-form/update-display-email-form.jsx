import { useEffect, useState } from "react";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import { updateUser } from "../../../utils/user.js";
import FormHeader from "../form-header/index.js";
import { Form, Formik } from "formik";
import { UpdateEmailFormSchema } from "../../../validation/Schemas/UpdateEmailFormSchema.js";
import TextInput from "../text-input/index.js";
import InlineLoader from "../../loaders/inline-loader/index.js";
import { ThreeDots } from "react-loader-spinner";
import ErrorView from "../../common/error-view/index.js";
import PrimaryAccentButton from "../../buttons/primary-accent-button/index.js";
import InlineOtpVerification from "../../common/inline-otp-verification/index.js";
import Checkbox from "../checkbox/index.js";
import { updateDisplayEmail } from "../../../services/settings.js";
import { successToast } from "../../../utils/toast.js";
import DangerButton from "../../buttons/danger-button/index.js";
import { SettingsService } from "../../../services";

const UpdateDisplayEmailForm = () => {
  const [animationParent] = useAutoAnimate();

  const [errorMessage, setErrorMessage] = useState("");
  const [loadingMessage, setLoadingMessage] = useState("");
  const [otpStep, setOtpStep] = useState(false);
  const [emailVerified, setEmailVerified] = useState(false);
  const [displayEmail, setDisplayEmail] = useState("");

  const refreshDisplayEmail = () => {
    setLoadingMessage("Display email loading ...");
    SettingsService.getAccountEmails()
      .then((res) => {
        setLoadingMessage("");
        setDisplayEmail(res.find((item) => item.title === "Display"));
      })
      .catch((error) => {
        setLoadingMessage("");
        console.log(error);
      });
  };

  useEffect(() => {
    refreshDisplayEmail();
  }, []);

  const handleAction = (values) => {
    setLoadingMessage("Updating email...");
    updateDisplayEmail(values)
      .then(() => {
        successToast("Email updated successfully.");
        setLoadingMessage("");
        setErrorMessage("");
        setOtpStep(false);
        setEmailVerified(false);
        updateUser({ email: values.email });
        refreshDisplayEmail();
      })
      .catch((error) => {
        setLoadingMessage("");
        console.log(error);
        setErrorMessage(error.message);
      });
  };

  return (
    <div className={"full-card-schema"}>
      <FormHeader
        title={"Update Display Email"}
        description={
          "Update your display email.Everyone can see your display email."
        }
      />
      <Formik
        initialValues={{
          email: "",
          usePrimaryEmail: false,
          removeDisplayEmail: false,
        }}
        onSubmit={(values, actions) => {
          setErrorMessage("");
          console.log("submit statted");
          console.log(
            "!(values.removeDisplayEmail || values.usePrimaryEmail) &&!emailVerified: " +
              !(values.removeDisplayEmail || values.usePrimaryEmail) &&
              !emailVerified
          );

          if (
            !(values.removeDisplayEmail || values.usePrimaryEmail) &&
            !emailVerified
          ) {
            setOtpStep(true);
            return;
          }

          handleAction(values);
        }}
        validationSchema={UpdateEmailFormSchema}
      >
        {({ isValid, values, submitForm, dirty, setFieldValue }) => (
          <Form className={"flex flex-col gap-5"} ref={animationParent}>
            <TextInput
              name={"email"}
              autoComplete={"email"}
              title={"Display Email"}
              disabled={values.usePrimaryEmail}
              bigTitle={true}
            />

            <Checkbox name={"usePrimaryEmail"} title={"Use primary email"} />

            {loadingMessage && (
              <InlineLoader
                loader={
                  <ThreeDots
                    height="24"
                    width="24"
                    color={"#3B49DF"}
                    wrapperClass="radio-wrapper"
                    radius="9"
                    ariaLabel="three-dots-loading"
                    wrapperStyle={{}}
                    wrapperClassName=""
                    visible={true}
                  />
                }
                message={loadingMessage}
                version={"input-like"}
              />
            )}
            {errorMessage && (
              <ErrorView message={errorMessage} version={"input-like"} />
            )}

            <div className={"flex  gap-4"}>
              <div className={"w-fit"}>
                <PrimaryAccentButton
                  title={"Update Email"}
                  type={"submit"}
                  disabled={
                    values.usePrimaryEmail
                      ? false
                      : !dirty || !isValid || otpStep
                  }
                />
              </div>

              <div>
                <DangerButton
                  type={"button"}
                  title={"Remove Display Email"}
                  disabled={!displayEmail}
                  click={() => {
                    setFieldValue("removeDisplayEmail", true);
                    submitForm();
                  }}
                />
              </div>
            </div>

            {otpStep && (
              <InlineOtpVerification
                email={values.email}
                emailVerified={() => {
                  setEmailVerified(true);
                  setOtpStep(false);
                  handleAction(values);
                }}
              />
            )}
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default UpdateDisplayEmailForm;
