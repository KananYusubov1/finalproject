import TextInput from "../text-input/index.js";
import Checkbox from "../checkbox/index.js";
import { Form, Formik } from "formik";
import PasswordInput from "../password-input/index.js";
import { Link, useNavigate } from "react-router-dom";
import { LoginSchema } from "../../../validation/Schemas/LoginSchema.js";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import PropTypes from "prop-types";
import { AuthService } from "../../../services/index.js";
import { signInUser } from "../../../utils/auth.js";
import { useState } from "react";
import ErrorView from "../../common/error-view/index.js";
import { saveUser } from "../../../utils/user.js";
import {
  startLoadingProcess,
  stopLoadingProcess,
} from "../../../utils/process.js";
import { getAppSettings } from "../../../services/user.js";
import { updateSettings } from "../../../utils/appSettings.js";

const LoginForm = ({ openLoginOtp }) => {
  const [errorMessage, setErrorMessage] = useState("");
  const [animationParent] = useAutoAnimate();
  const navigate = useNavigate();
  return (
    <Formik
      onSubmit={(values) => {
        // openLoginOtp(values);
        startLoadingProcess("Login Loading");
        AuthService.login(values)
          .then((result) => {
            setErrorMessage("");

            console.log("salam");
            signInUser(result, values.rememberMe);

            if (result.isProfileBuilt) {
              saveUser(result);
              startLoadingProcess("Reading App Settings");
              getAppSettings()
                .then((res) => {
                  updateSettings(res);
                })
                .catch(() => {})
                .finally(() => {
                  stopLoadingProcess();
                });
            }
            stopLoadingProcess();

            navigate("/");
          })
          .catch((err) => {
            stopLoadingProcess();
            console.log(JSON.stringify(err, null, 2));
            switch (err.status) {
              case 404:
                setErrorMessage("User not found");
                break;
              case 400:
                setErrorMessage("Invalid login");
                break;
              default:
                setErrorMessage(
                  "A problem has occurred please try again later"
                );
                break;
            }
          });
      }}
      initialValues={{ email: "", password: "", rememberMe: true }}
      validationSchema={LoginSchema}
      ref={animationParent}
    >
      {() => (
        <Form className={"flex flex-col gap-4 "} ref={animationParent}>
          <TextInput title={"Email"} name={"email"} autoComplete={"email"} />
          <PasswordInput
            title={"Password"}
            name={"password"}
            autoComplete={"current-password"}
          />

          <div className={"flex items-center justify-between"}>
            <div className={"w-fit"}>
              <Checkbox
                name={"rememberMe"}
                checked={true}
                title={"Remember me"}
              />
            </div>

            <Link
              to={"/forgot-password"}
              className={
                "w-fit text-sm underline-animation text-accent self-center"
              }
            >
              I forgot my password
            </Link>
          </div>
          {errorMessage && <ErrorView message={errorMessage} />}
          <button
            type="submit"
            className={
              "py-[10px] w-full px-10 text-white  font-medium  bg-accent hover:bg-tz-red-hover-dark rounded-lg text-sm transition-all duration-150 ease-in hover:bg-blue-800"
            }
          >
            Continue
          </button>
        </Form>
      )}
    </Formik>
  );
};

LoginForm.propTypes = {
  openLoginOtp: PropTypes.func,
};

export default LoginForm;
