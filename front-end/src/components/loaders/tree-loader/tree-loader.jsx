import { motion } from "framer-motion";
import { useLoading } from "../../../utils/process.js";
import "./tree-loader.css";

const TreeLoader = () => {
  const loader = useLoading();

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={
        "w-full h-full fixed absolute inset-0 bg-black/70 backdrop-blur-sm flex flex-col items-center justify-center gap-20 z-[99]"
      }
    >
      <div className="container">
        <div className="tree">
          <div className="branch" style={{ "--x": 0 }}>
            <span style={{ "--i": 0 }}></span>
            <span style={{ "--i": 1 }}></span>
            <span style={{ "--i": 2 }}></span>
            <span style={{ "--i": 3 }}></span>
          </div>
          <div className="branch" style={{ "--x": 1 }}>
            <span style={{ "--i": 0 }}></span>
            <span style={{ "--i": 1 }}></span>
            <span style={{ "--i": 2 }}></span>
            <span style={{ "--i": 3 }}></span>
          </div>
          <div className="branch" style={{ "--x": 2 }}>
            <span style={{ "--i": 0 }}></span>
            <span style={{ "--i": 1 }}></span>
            <span style={{ "--i": 2 }}></span>
            <span style={{ "--i": 3 }}></span>
          </div>
          <div className="branch" style={{ "--x": 3 }}>
            <span style={{ "--i": 0 }}></span>
            <span style={{ "--i": 1 }}></span>
            <span style={{ "--i": 2 }}></span>
            <span style={{ "--i": 3 }}></span>
          </div>
          <div className="stem">
            <span style={{ "--i": 0 }}></span>
            <span style={{ "--i": 1 }}></span>
            <span style={{ "--i": 2 }}></span>
            <span style={{ "--i": 3 }}></span>
          </div>
          <span className="tree-shadow"></span>
        </div>
      </div>
      <div className={""}>
        <p className={"font-medium text-[30px] text-white  font-modulus"}>
          {loader.label ?? "Loading..."}
        </p>
      </div>
    </motion.div>
  );
};

export default TreeLoader;
