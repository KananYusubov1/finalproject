import { motion } from "framer-motion";
import { useLoading } from "../../../utils/process.js";
import { BiSolidQuoteLeft, BiSolidQuoteRight } from "react-icons/bi";
import { TailSpin } from "react-loader-spinner";
import TitleDivider from "../../title-dividers/title-divider/index.js";
import { useEffect, useState } from "react";
import quotas from "../../../quotas.js";
const FullScreenLoader = () => {
  const loader = useLoading();
  const [quote, setQuote] = useState({});

  useEffect(() => {
    const randomIndex = Math.floor(Math.random() * quotas.length);
    setQuote(quotas[randomIndex]);
  }, []);

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={
        "w-full h-full fixed absolute inset-0 bg-black/75 backdrop-blur-sm z-[99] flex flex-col items-center justify-center"
      }
    >
      <div
        className={
          "max-w-[950px] flex flex-col items-center justify-center gap-10 "
        }
      >
        <img className={"w-20"} src={`/logo.svg`} alt="logo" />

        <div className={"flex"}>
          <BiSolidQuoteLeft className={"fill-accent"} size={32} />

          <p
            className={
              "max-w-[900px] mx-6 w-fit text-center font-medium text-3xl text-white  font-modulus"
            }
          >
            {quote.content}
          </p>

          <BiSolidQuoteRight className={"fill-accent self-end"} size={32} />
        </div>

        <TitleDivider
          title={
            <span className={"font-bold text-3xl text-accent  font-modulus"}>
              {quote.author}
            </span>
          }
        />

        <div className={"flex items-center gap-4"}>
          <p className={"font-medium text-2xl text-white  font-modulus"}>
            {loader.label ?? "Loading"}
          </p>
          <div className={"flex items-center justify-center"}>
            <TailSpin
              height="24"
              width="24"
              color={"#3B49DF"}
              wrapperClass="radio-wrapper"
              radius="3"
              ariaLabel="three-dots-loading"
              wrapperStyle={{}}
              wrapperClassName=""
              visible={true}
            />
          </div>
        </div>
      </div>
    </motion.div>
  );
};

export default FullScreenLoader;
