import PropTypes from "prop-types";
import classNames from "classnames";

const InlineLoader = ({ loader, message, version }) => {
  return (
    <>
      <div
        className={classNames({
          "w-full flex items-center gap-2 p-1 dark:text-gray-300": true,
          "rounded-md  border-[1px] border-border-clr dark:border-dark-border-clr select-none shadow-none hover:shadow":
            version === "input-like",
        })}
      >
        {loader}
        <p>{message}</p>
      </div>
    </>
  );
};

InlineLoader.propTypes = {
  loader: PropTypes.element,
  message: PropTypes.string,
  version: PropTypes.string,
};

export default InlineLoader;
