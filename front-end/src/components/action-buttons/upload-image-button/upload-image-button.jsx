import PropTypes from "prop-types";
import Resizer from "react-image-file-resizer";
import classNames from "classnames";

const UploadImageButton = ({
  onFileUpload,
  horizontal,
  label,
  resize = false,
}) => {
  const handleFileUpload = async () => {
    const pickerOpts = {
      types: [
        {
          description: "Images",
          accept: {
            "custom/*": [".png", ".jpeg", ".jpg", ".gif"],
          },
        },
      ],
      excludeAcceptAllOption: true,
      multiple: false,
    };

    try {
      const [fileHandle] = await window.showOpenFilePicker(pickerOpts);
      const file = await fileHandle.getFile();

      if (resize) {
        Resizer.imageFileResizer(
          file,
          350,
          350,
          "JPEG",
          100,
          0,
          (blob) => {
            const blobURL = URL.createObjectURL(blob);
            onFileUpload(blobURL);
          },
          "blob"
        );
      } else {
        const blobURL = URL.createObjectURL(file);
        onFileUpload(blobURL);
      }
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <button
      type={"button"}
      onClick={handleFileUpload}
      className={classNames({
        "group  border-dashed border-2 border-blue-400 rounded-md  hover:border-accent-dark hover:border-solid smooth-animation": true,
        "w-fit px-5 h-16 flex items-center justify-center gap-6": horizontal,
        "w-56 h-56 flex flex-col items-center justify-center": !horizontal,
      })}
    >
      <img
        className={classNames({
          "w-16 h-16": !horizontal,
          "h-9": horizontal,
        })}
        src={`/upload-image-icon.png`}
        alt="Upload Image Icon"
      />
      <p className={"text-md font-semibold dark:text-gray-300"}>{label}</p>
    </button>
  );
};

UploadImageButton.propTypes = {
  onFileUpload: PropTypes.func,
  horizontal: PropTypes.bool,
  label: PropTypes.string,
  resize: PropTypes.bool,
};

export default UploadImageButton;
