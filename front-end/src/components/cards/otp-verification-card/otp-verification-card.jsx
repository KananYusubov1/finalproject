import { useAutoAnimate } from "@formkit/auto-animate/react";
import { useEffect, useState } from "react";
import { useTimer } from "react-timer-hook";
import { LuMailOpen } from "react-icons/lu";
import OtpInput from "react-otp-input";
import PropTypes from "prop-types";
import "./otp-verification-card.css";
import { SiMicrosoftoutlook } from "react-icons/si";
import { BiLogoGmail } from "react-icons/bi";
import { AiOutlineArrowLeft } from "react-icons/ai";
import ResetPasswordHeader from "../../reset-password/reset-password-header/index.js";
import TimeShower from "../../common/time-shower/index.js";
import ErrorView from "../../common/error-view/index.js";
import AccordionCollapse from "../../common/accordion/index.js";
import {
  startLoadingProcess,
  stopLoadingProcess,
  useEmailVerify,
} from "../../../utils/process.js";
import { AuthService } from "../../../services";

const OtpVerificationCard = ({
  onStepForward,
  onStepBack,
  onStepBackTitle,
}) => {
  const [animationParent] = useAutoAnimate();

  const addSeconds = 120;

  const [otp, setOtp] = useState("");
  const [resendEnabled, setResendEnabled] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const email = useEmailVerify();

  const time = new Date();
  time.setSeconds(time.getSeconds() + addSeconds);

  const { seconds, minutes, restart } = useTimer({
    expiryTimestamp: time,
    onExpire: () => {
      setResendEnabled(true);
      console.warn("onExpire called");
    },
  });

  const resetOtp = () => {
    AuthService.sendOtp(email)
      .then(() => {
        setOtp("");
        setResendEnabled(false);
        const time = new Date();
        time.setSeconds(time.getSeconds() + addSeconds);
        restart(time);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    if (otp.length === 6) {
      startLoadingProcess("OTP code verification");
      AuthService.verifyOtp({ email, otp })
        .then(() => {
          stopLoadingProcess();
          setErrorMessage("");
          onStepForward();
        })
        .catch((err) => {
          stopLoadingProcess();
          switch (err.status) {
            case 400:
              setErrorMessage("Wrong OTP");
              break;
            case 410:
              setErrorMessage("Otp Code expired");
              break;
          }
          console.log(err);
        });
    }
  }, [otp]);

  useEffect(() => {
    AuthService.sendOtp(email).catch((err) => {
      setResendEnabled(false);
      console.log(err);
    });
  }, []);

  return (
    <div
      className="bg-white rounded-lg flex flex-col gap-4 p-4 min-w-[400px]"
      ref={animationParent}
    >
      <div className={"flex flex-col gap-4"} ref={animationParent}>
        <ResetPasswordHeader
          title={"Password Reset"}
          description={
            <p className={"text-gray-900"}>
              We sent a code to <span className={"font-bold"}>{email}</span>
            </p>
          }
          icon={
            <LuMailOpen size={30} className={"reset-password-header-icon"} />
          }
        />
        <div className={"flex items-center text-center gap-4 justify-center"}>
          <TimeShower title={"Minutes"} time={minutes} />

          <span className={"h-[35px] p-0 m-0 text-center text-2xl"}>:</span>

          <TimeShower title={"Seconds"} time={seconds} />
        </div>

        <OtpInput
          value={otp}
          onChange={setOtp}
          numInputs={6}
          renderSeparator={<span>-</span>}
          containerStyle={"flex gap-4 justify-center items-center"}
          renderInput={(props) => <input name={"otp"} {...props} />}
          inputStyle={
            "text-[2rem]  w-12 h-12 px-2 py-1 box-content border-[1px] border-border-clr  rounded-lg"
          }
          inputType={"number"}
        />

        {errorMessage && (
          <div className={"w-fit self-center"}>
            <ErrorView message={errorMessage} />
          </div>
        )}

        <button
          className={"group flex gap-2 text-sm self-center"}
          onClick={resetOtp}
          disabled={!resendEnabled}
        >
          <p> Didn&#39;t receive the email?</p>
          <span
            className={
              "text-accent font-medium group-disabled:text-gray-400 underline-animation"
            }
          >
            Click to resend
          </span>
        </button>
        <button
          onClick={() => {
            onStepBack();
          }}
          className={
            "flex items-center self-center gap-4 hover:bg-hover-blue-clr p-2 rounded-lg cursor-pointer smooth-animation"
          }
        >
          {/*Icon*/}
          <AiOutlineArrowLeft />
          {/*Link*/}

          <p>{onStepBackTitle}</p>
        </button>
        <AccordionCollapse title={"Quick view OTP"}>
          <div className={"flex flex-col gap-1"}>
            <a
              href={"https://outlook.com"}
              target="_blank"
              rel="noreferrer"
              className={`w-full min-h-6 p-3 pl-3 pr-5 rounded-lg flex justify-center items-center gap-2 text-white font-medium transition-all duration-150 bg-accent hover:bg-blue-900 `}
            >
              <SiMicrosoftoutlook size={24} />
              Open Outlook Web
            </a>
            <a
              href={"https://mail.google.com/"}
              target="_blank"
              rel="noreferrer"
              className={`w-full min-h-6 p-3 pl-3 pr-5 rounded-lg flex justify-center items-center gap-2 text-white font-medium transition-all duration-150 bg-red-700 hover:bg-red-900 `}
            >
              <BiLogoGmail size={24} />
              Open Gmail Web
            </a>
          </div>
        </AccordionCollapse>
      </div>
    </div>
  );
};

OtpVerificationCard.propTypes = {
  onStepForward: PropTypes.func,
  onStepBack: PropTypes.func,
  email: PropTypes.string,
  onStepBackTitle: PropTypes.string,
};

export default OtpVerificationCard;
