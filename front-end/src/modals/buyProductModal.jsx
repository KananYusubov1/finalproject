import { useNavigate } from "react-router-dom";
import ModalHeader from "../components/modals/modal-header/index.js";
import PrimaryAccentButton from "../components/buttons/primary-accent-button/index.js";
import { destroyModal } from "../utils/modal.js";
import PrimaryButton from "../components/buttons/primary-button/index.js";
import PropTypes from "prop-types";
import BuyCheckout from "../components/common/buy-checkout/index.js";
import { useEffect, useState } from "react";
import { getUsableBuds } from "../services/buds.js";
import { ThreeDots } from "react-loader-spinner";
import BuyCheckoutInfo from "../components/common/buy-checkout-info/index.js";
import PriceIndicator from "../components/common/price-indicator/index.js";
import ErrorView from "../components/common/error-view/index.js";
import { buyProduct } from "../services/shop.js";
import { errorToast, successToast } from "../utils/toast.js";

export default function BuyProductModal({ data }) {
  const { product, buyQuantity } = data;
  const [buds, setBuds] = useState(-1);

  const [loading, setLoading] = useState(false);

  const totalPrice = buyQuantity * product.discountedPrice;
  const canBuy = totalPrice <= buds;

  const navigate = useNavigate();

  useEffect(() => {
    getUsableBuds().then((res) => setBuds(res));
  }, []);

  return (
    <div className={"mx-2 w-max-[700px] min-w-[400px]"}>
      <ModalHeader title={`Buy ${product.name}`} />
      <div
        className={
          "flex flex-col items-center justify-center bg-white dark:bg-dark-component-bg gap-4 px-6 pb-3 rounded-b-md"
        }
      >
        <BuyCheckout product={product} buyQuantity={buyQuantity} />

        {buds === -1 ? (
          <div className={"w-[100px] flex items-center justify-center"}>
            <ThreeDots
              height="24"
              width="24"
              color={"#3B49DF"}
              wrapperClass="radio-wrapper"
              radius="9"
              ariaLabel="three-dots-loading"
              wrapperStyle={{}}
              wrapperClassName=""
              visible={true}
            />
          </div>
        ) : (
          <div className={"w-full flex flex-col gap-1"}>
            <BuyCheckoutInfo
              label={"Your usable buds"}
              renderValue={<PriceIndicator price={buds} />}
            />

            {!canBuy && (
              <ErrorView
                message={"You don't have enough buds"}
                version={"input-like"}
              />
            )}
          </div>
        )}

        <div className={"w-full flex flex-col gap-1"}>
          {loading ? (
            <div className={"w-full flex items-center justify-center"}>
              <ThreeDots
                height="24"
                width="24"
                color={"#3B49DF"}
                wrapperClass="radio-wrapper"
                radius="9"
                ariaLabel="three-dots-loading"
                wrapperStyle={{}}
                wrapperClassName=""
                visible={true}
              />
            </div>
          ) : (
            <PrimaryAccentButton
              title={"Buy"}
              disabled={!canBuy}
              click={() => {
                setLoading(true);
                buyProduct({ quantity: buyQuantity, productId: product.id })
                  .then(() => {
                    successToast("Product bought successfully");
                    destroyModal();
                  })
                  .catch((error) => {
                    errorToast(
                      error.message ??
                        "An error occurred, please try again later"
                    );
                  })
                  .finally(() => {
                    setLoading(false);
                  });
              }}
            />
          )}
          <PrimaryButton
            title={"Cancel"}
            click={() => {
              destroyModal();
            }}
          />
        </div>
      </div>
    </div>
  );
}

BuyProductModal.propTypes = {
  data: PropTypes.object,
};
