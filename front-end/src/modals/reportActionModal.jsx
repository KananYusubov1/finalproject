import PropTypes from "prop-types";
import ModalHeader from "../components/modals/modal-header/index.js";
import { useState } from "react";
import { Form, Formik } from "formik";
import TextAreaInput from "../components/form/text-area-input/index.js";
import RadioButtonGroup from "../components/form/radio-button-group/index.js";
import PrimaryAccentButton from "../components/buttons/primary-accent-button/index.js";
import { ReportActionSchema } from "../validation/Schemas/ReportActionSchema.js";
import { submitReport } from "../services/report.js";
import { errorToast, successToast } from "../utils/toast.js";
import { ThreeDots } from "react-loader-spinner";

export default function ReportActionModal({ data }) {
  const [loading, setLoading] = useState(false);

  return (
    <div className={"mx-2 max-w-[550px] min-w-[400px]"}>
      <ModalHeader title={"Report Action"} />
      <div
        className={
          "flex flex-col items-center justify-center gap-4 card-color-schema px-6 py-3 rounded-b-md"
        }
      >
        <Formik
          initialValues={{ reportStatus: "", moderatorMessage: "" }}
          onSubmit={(values) => {
            setLoading(true);
            submitReport({ ...values, reportId: data.id })
              .then(() => {
                successToast("Report action taken!");
              })
              .catch((err) => {
                console.log(err);
                errorToast("Failed to take report action");
              })
              .finally(() => {
                setLoading(false);
              });
          }}
        >
          {() => (
            <Form className={"w-full flex flex-col gap-2"}>
              <RadioButtonGroup
                options={[
                  {
                    data: "Decline",
                    value: "Declined",
                  },
                  {
                    data: "Freeze",
                    value: "ResultedWithContentFreeze",
                  },
                ]}
                name={"reportStatus"}
              />

              <TextAreaInput
                name={"moderatorMessage"}
                title={"Moderator Message"}
              />

              {loading ? (
                <div className={"w-full flex items-center justify-center"}>
                  <ThreeDots
                    height="24"
                    width="24"
                    color={"#3B49DF"}
                    wrapperClass="radio-wrapper"
                    radius="9"
                    ariaLabel="three-dots-loading"
                    wrapperStyle={{}}
                    wrapperClassName=""
                    visible={true}
                  />
                </div>
              ) : (
                <PrimaryAccentButton type={"submit"} title={"Take action"} />
              )}
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
}

ReportActionModal.propTypes = {
  data: PropTypes.object,
};
