import ModalHeader from "../components/modals/modal-header/index.js";
import { useEffect, useState } from "react";
import UploadImageButton from "../components/action-buttons/upload-image-button/index.js";
import PrimaryAccentButton from "../components/buttons/primary-accent-button/index.js";
import TitleDivider from "../components/title-dividers/title-divider/index.js";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import AvatarEditor from "../components/modals/avatar-editor/index.js";
import { createAvatar } from "@dicebear/core";
import { botttsNeutral } from "@dicebear/collection";
import PrimaryButton from "../components/buttons/primary-button/index.js";
import { useName } from "../utils/auth.js";
import PropTypes from "prop-types";
import { destroyModal } from "../utils/modal.js";
import { convertToBase64 } from "../utils/common.js";

EditProfilePhotoModal.propTypes = {
  data: PropTypes.func,
};

export default function EditProfilePhotoModal({ data }) {
  const [animationPatent] = useAutoAnimate();
  const [urlSrc, setUrlSrc] = useState(null);
  const [isAvatarEditMode, setIsAvatarEditMode] = useState(true);
  const [dataUri, setDataUri] = useState();
  const name = useName();

  const handleClose = () => {
    setUrlSrc(null);
    destroyModal();
  };

  const handleFileLoad = (url) => {
    setUrlSrc(url);
  };
  const handleToggleAvatarEditMode = () => {
    setIsAvatarEditMode(!isAvatarEditMode);
  };

  const handleSetAvatar = () => {
    if (isAvatarEditMode) {
      if (urlSrc) {
        convertToBase64([urlSrc, ""]).then((res) => {
          data(res[0]);

          handleClose();
        });
      } else {
        handleClose();
      }
    } else {
      if (dataUri) data(dataUri);
      handleClose();
    }
  };

  useEffect(() => {
    const avatar = createAvatar(botttsNeutral, {
      seed: name,
      radius: 50,
    });

    avatar
      .jpeg()
      .toDataUri()
      .then((uri) => setDataUri(uri));
  }, []);

  return (
    <div className={"mx-2 w-full"} ref={animationPatent}>
      <ModalHeader title={"Edit Your Avatar!"} />
      <div
        className={
          "w-full flex flex-col items-center justify-center gap-4 bg-white dark:bg-dark-component-bg px-6 py-3 rounded-b-md"
        }
      >
        <div>
          {isAvatarEditMode ? (
            <div
              className={
                "w-full flex flex-col items-center justify-center gap-4"
              }
            >
              {urlSrc ? (
                <AvatarEditor src={urlSrc} onClose={handleClose} />
              ) : (
                <UploadImageButton
                  onFileUpload={handleFileLoad}
                  label={"Upload your Avatar!"}
                  resize={true}
                />
              )}
              <TitleDivider
                title={<p className=" font-medium text-center">Or</p>}
              />
              <PrimaryButton
                click={handleToggleAvatarEditMode}
                title={"Generate Your Avatar!"}
              />
            </div>
          ) : (
            <div
              className={
                "w-full flex flex-col items-center justify-center gap-4"
              }
            >
              <p className={"font-semibold dark:text-gray-400"}>
                Your Generated Avatar
              </p>

              <img src={dataUri} alt="avatar" className={"w-20 rounded-lg"} />

              <TitleDivider
                title={<p className=" font-medium text-center">Or</p>}
              />
              <PrimaryButton
                click={handleToggleAvatarEditMode}
                title={"Edit yours!"}
              />
            </div>
          )}
        </div>
        <PrimaryAccentButton click={handleSetAvatar} title={"Save"} />
      </div>
    </div>
  );
}
