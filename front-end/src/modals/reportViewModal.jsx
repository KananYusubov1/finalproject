import PropTypes from "prop-types";
import ModalHeader from "../components/modals/modal-header/index.js";
import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import { getReport } from "../services/report.js";
import { errorToast } from "../utils/toast.js";
import { TailSpin } from "react-loader-spinner";
import { getContent } from "../services/contentCommon.js";
import { getResource } from "../services/resources.js";
import { getPreviewUserByUsername } from "../services/user.js";
import BuyCheckoutInfo from "../components/common/buy-checkout-info/index.js";
import MarkdownViewer from "../components/markdown-viewer/index.js";
import Divider from "../components/common/divider/index.js";
import PrimaryButton from "../components/buttons/primary-button/index.js";
import AccentBorderButton from "../components/buttons/accent-border-button/index.js";
import PrimaryAccentButton from "../components/buttons/primary-accent-button/index.js";
import { createModal, destroyModal } from "../utils/modal.js";

export default function ReportViewModal({ data }) {
  const navigate = useNavigate();

  const [loading, setLoading] = useState(true);
  const [report, setReport] = useState(null);
  const [reportContent, setReportContent] = useState(null);

  useEffect(() => {
    setLoading(true);
    getReport(data.id)
      .then((res) => {
        setReport(res);
      })
      .catch((err) => {
        console.log(err);
        errorToast("Failed to get report");
        setLoading(false);
      });
  }, []);

  useEffect(() => {
    if (report === null) return;
    const type = report.contentType;
    const id = report.contentId;
    switch (type) {
      case "Article":
        getContent("articles", id)
          .then((res) => {
            setReportContent(res);
          })
          .catch((err) => {
            console.log(err);
            errorToast("Failed to get report content");
          })
          .finally(() => {
            setLoading(false);
          });
        break;
      case "Question":
        getContent("questions", id)
          .then((res) => {
            setReportContent(res);
          })
          .catch((err) => {
            console.log(err);
            errorToast("Failed to get report content");
          })
          .finally(() => {
            setLoading(false);
          });
        break;
      case "Resource":
        getResource(id)
          .then((res) => {
            setReportContent(res);
          })
          .catch((err) => {
            console.log(err);
            errorToast("Failed to get report content");
          })
          .finally(() => {
            setLoading(false);
          });
        break;
      case "User":
        getPreviewUserByUsername(id)
          .then((res) => {
            setReportContent(res);
          })
          .catch((err) => {
            console.log(err);
            errorToast("Failed to get report content");
          })
          .finally(() => {
            setLoading(false);
          });
    }
  }, [report]);

  return (
    <div className={"mx-2 max-w-[550px] min-w-[400px]"}>
      <ModalHeader title={"Report View"} />
      <div
        className={
          "flex flex-col items-center justify-center gap-4 card-color-schema px-6 py-3 rounded-b-md"
        }
      >
        {!loading ? (
          <div className={"w-full flex flex-col gap-2"}>
            <BuyCheckoutInfo label={"Report Id"} value={report.id} />
            <BuyCheckoutInfo label={"Report Type"} value={report.reportType} />
            <BuyCheckoutInfo label={"Content Id"} value={report.contentId} />
            <BuyCheckoutInfo
              label={"Content Type"}
              value={report.contentType}
            />

            <Divider />

            <MarkdownViewer source={report.reportMessage} />

            <Divider />

            <PrimaryAccentButton
              title={"Take Action"}
              click={() => {
                createModal("reportAction", { id: report.id });
              }}
            />
            <AccentBorderButton
              title={"View Content"}
              click={() => {
                switch (report.contentType) {
                  case "Article":
                    navigate(`/articles/${reportContent.id}`);
                    destroyModal();
                    break;
                  case "Question":
                    navigate(`/questions/${reportContent.id}`);
                    destroyModal();
                    break;
                  case "Resource":
                    window.open(reportContent.url);
                    destroyModal();
                    break;
                  case "User":
                    navigate(`/${reportContent.username}`);
                    destroyModal();
                    break;
                }
              }}
            />
            {/*<p>{JSON.stringify(reportContent, null, 2)}</p>*/}
          </div>
        ) : (
          <>
            <div className={"w-full h-full flex items-center justify-center"}>
              <TailSpin
                height="60"
                width="60"
                color={"#3B49DF"}
                wrapperClass="radio-wrapper"
                radius="2"
                ariaLabel="three-dots-loading"
                wrapperStyle={{}}
                wrapperClassName=""
                visible={true}
              />
            </div>
          </>
        )}
      </div>
    </div>
  );
}

ReportViewModal.propTypes = {
  data: PropTypes.object,
};
