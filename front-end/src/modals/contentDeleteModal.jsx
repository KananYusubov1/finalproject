import PropTypes from "prop-types";
import ModalHeader from "../components/modals/modal-header/index.js";
import DangerButton from "../components/buttons/danger-button/index.js";
import PrimaryAccentButton from "../components/buttons/primary-accent-button/index.js";
import { destroyModal } from "../utils/modal.js";
import InfoView from "../components/common/info-view/index.js";
import { ContentCommonService } from "../services";
import { successToast } from "../utils/toast.js";
import { useState } from "react";
import InlineLoader from "../components/loaders/inline-loader/index.js";
import { Triangle } from "react-loader-spinner";

export default function ContentDeleteModal({ data }) {
  const [isLoading, setIsLoading] = useState(false);

  return (
    <div className={"mx-2 w-max-[500px]"}>
      <ModalHeader title={`Delete ${data.contentType}`} />
      <div
        className={
          "flex flex-col items-center justify-center gap-4 bg-white dark:bg-dark-component-bg px-6 py-3 rounded-b-md"
        }
      >
        <p className={"dark:text-gray-300"}>
          Are you sure you want to delete this content ?
        </p>

        <InfoView
          message={"If you delete this content, you will be able to recover it"}
          version={"input-like"}
        />

        {isLoading && (
          <InlineLoader
            loader={
              <Triangle
                visible={true}
                height="24"
                width="24"
                color={"#3B49DF"}
                ariaLabel="radio-loading"
                wrapperStyle={{}}
                wrapperClass="radio-wrapper"
              />
            }
            message={`Deleting ${data.contentType}...`}
            version={"input-like"}
          />
        )}

        <div className={"w-full flex items-center gap-4"}>
          <DangerButton
            title={`Delete ${data.contentType}`}
            click={() => {
              setIsLoading(true);
              ContentCommonService.deleteContent(
                data.contentType,
                data.contentId
              )
                .then(() => {
                  setIsLoading(false);
                  successToast(`${data.contentType} deleted successfully`);
                  data.onDelete(data.contentId);
                  destroyModal();
                })
                .catch((err) => {
                  setIsLoading(false);
                  console.log(err);
                });
            }}
          />
          <PrimaryAccentButton
            title={"Cancel"}
            click={() => {
              destroyModal();
            }}
          />
        </div>
      </div>
    </div>
  );
}

ContentDeleteModal.propTypes = {
  data: PropTypes.object,
};
