import ModalHeader from "../components/modals/modal-header/index.js";
import PropTypes from "prop-types";
import defaultColors from "../defaultTagColors.js";
import { useState } from "react";
import { getTagColor } from "../utils/common.js";
import PrimaryAccentButton from "../components/buttons/primary-accent-button/index.js";
import { destroyModal } from "../utils/modal.js";

export default function TagColorEditorModal({ data }) {
  const { colorId, setColor } = data;

  const [selectedColor, setSelectedColor] = useState(getTagColor(colorId));

  return (
    <div className={"mx-2 w-max-[400px]"}>
      <ModalHeader title={"Tag Color Editor"} />
      <div
        className={
          "flex flex-col items-center justify-center gap-8 card-color-schema dark:bg-dark-component-bg px-6 py-3 rounded-b-md"
        }
      >
        <div
          className={"w-full flex  flex-col gap-1 items-center justify-center"}
        >
          <p className={"font-medium text-white "}>Selected Color</p>

          <button
            type={"button"}
            key={selectedColor.id}
            className={
              "w-9 h-7 rounded-md dark:border-[1px] dark:border-dark-border-clr"
            }
            style={{ backgroundColor: selectedColor.text }}
          />
        </div>
        <div className={"grid grid-cols-8  gap-3 flex-wrap"}>
          {defaultColors.map((color) => (
            <button
              type={"button"}
              key={color.id}
              className={
                "w-9 h-7 rounded-md dark:border-[1px] dark:border-dark-border-clr"
              }
              style={{ backgroundColor: color.text }}
              onClick={() => {
                setSelectedColor(color);
              }}
            />
          ))}
        </div>

        <PrimaryAccentButton
          title={"Save"}
          type={"button"}
          click={() => {
            setColor(selectedColor);
            destroyModal();
          }}
        />
      </div>
    </div>
  );
}

TagColorEditorModal.propTypes = {
  data: PropTypes.any,
};
