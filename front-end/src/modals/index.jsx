import modalData from "../modal.js";
import { useModals } from "../utils/modal.js";
import { motion } from "framer-motion";
import { useAutoAnimate } from "@formkit/auto-animate/react";

export default function Modal() {
  const modals = useModals();
  const [animationPatent] = useAutoAnimate();

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={
        "w-full h-full top-0 fixed  inset-0 bg-black/50 backdrop-blur-sm flex items-center justify-center z-[98]"
      }
      ref={animationPatent}
    >
      {modals.map((modal) => {
        const m = modalData.find((m) => m.name === modal.name);

        return (
          <div key={m.name} className={"hidden last:block z-[99]"}>
            <m.element data={modal.data} />
          </div>
        );
      })}
    </motion.div>
  );
}
