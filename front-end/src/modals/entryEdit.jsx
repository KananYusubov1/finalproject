import ModalHeader from "../components/modals/modal-header/index.js";
import PrimaryAccentButton from "../components/buttons/primary-accent-button/index.js";
import { destroyModal } from "../utils/modal.js";
import { Form, Formik } from "formik";
import MarkdownEditor from "../components/common/markdown-editor/index.js";
import PropTypes from "prop-types";
import { ThreeDots } from "react-loader-spinner";
import { useState } from "react";
import { editEntry } from "../services/contentCommon.js";
import { errorToast, successToast } from "../utils/toast.js";
import RatingInput from "../components/form/rating-input/index.js";

export default function EntryEdit({ data }) {
  const [isLoading, setIsLoading] = useState(false);

  const handleEditEntry = (values) => {
    setIsLoading(true);
    editEntry(data.type, values)
      .then(() => {
        successToast(`Successfully updated ${data.title}`);
        data.onUpdate(values);
        destroyModal();
      })
      .catch((error) => {
        errorToast(error.message);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  return (
    <div className={"mx-2 w-max-[550px]"}>
      <ModalHeader title={`Edit ${data.title}`} />
      <div
        className={
          "flex flex-col items-center justify-center gap-4 card-color-schema px-6 py-3 rounded-b-md"
        }
      >
        <Formik
          initialValues={{
            contentId: data.id,
            body: data.body,
            rank: data.rank ?? 0,
          }}
          onSubmit={() => {}}
        >
          {({ dirty, values }) => (
            <Form className={"flex flex-col gap-6"}>
              {data.type === "review" && <RatingInput name={"rank"} />}
              <MarkdownEditor name={"body"} size={300} />
              <div className={"w-full flex flex-col gap-1"}>
                {isLoading ? (
                  <div className={"w-full flex items-center justify-center"}>
                    <ThreeDots
                      height="24"
                      width="24"
                      color={"#3B49DF"}
                      wrapperClass="radio-wrapper"
                      radius="9"
                      ariaLabel="three-dots-loading"
                      wrapperStyle={{}}
                      wrapperClassName=""
                      visible={true}
                    />
                  </div>
                ) : (
                  <PrimaryAccentButton
                    disabled={!dirty || !values.body.length > 0}
                    title={"Save changes"}
                    click={() => {
                      handleEditEntry(values);
                    }}
                  />
                )}
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
}

EntryEdit.propTypes = {
  data: PropTypes.object,
};
