import ModalHeader from "../components/modals/modal-header/index.js";
import { Form, Formik, useFormikContext } from "formik";
import TextInput from "../components/form/text-input/index.js";
import { BiLink } from "react-icons/bi";
import PrimaryAccentButton from "../components/buttons/primary-accent-button/index.js";
import ErrorView from "../components/common/error-view/index.js";
import InfoView from "../components/common/info-view/index.js";
import { useEffect, useState } from "react";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import { PublishResourceSchema } from "../validation/Schemas/PublishResourceShema.js";
import SelectInput from "../components/form/select-input/index.js";
import {
  savePublishResourceForm,
  usePublishResourceForm,
} from "../utils/process.js";
import InlineLoader from "../components/loaders/inline-loader/index.js";
import { Radio } from "react-loader-spinner";
import { TagService, ResourceService } from "../services";
import { successToast } from "../utils/toast.js";
import PropTypes from "prop-types";
import { destroyModal } from "../utils/modal.js";
import UrlInput from "../components/form/url-input/index.js";

function AutoStateSave() {
  const { values } = useFormikContext();

  useEffect(() => {
    savePublishResourceForm(values);
  }, [values]);
  return null;
}

PublishResourceModal.propTypes = {
  data: PropTypes.any,
};

export default function PublishResourceModal({ data }) {
  const [isLoading, setIsLoading] = useState(false);
  const [animationParent] = useAutoAnimate();

  const [loadingMessage, setLoadingMessage] = useState("");
  const [infoMessage, setInfoMessage] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [categories, setCategories] = useState([]);
  const [flags, setFlags] = useState([]);

  const [categoriesIsLoading, setCategoriesIsLoading] = useState(false);
  const [flagsIsLoading, setFlagsIsLoading] = useState(false);

  useEffect(() => {
    setCategoriesIsLoading(true);
    TagService.getAllTags("categories")
      .then((res) => {
        const categories = res.map((category) => {
          return {
            label: category.title,
            value: category.id,
          };
        });
        setCategories(categories);
        setCategoriesIsLoading(false);
      })
      .catch((error) => {
        console.log(error);
        setCategoriesIsLoading(false);
      });

    setFlagsIsLoading(true);
    TagService.getAllTags("resource-flags")
      .then((res) => {
        const flags = res.map((category) => {
          return {
            label: category.title,
            value: category.id,
          };
        });

        setFlags(flags);
        setFlagsIsLoading(false);
      })
      .catch((error) => {
        console.log(error);
        setFlagsIsLoading(false);
      });
  }, []);

  return (
    <div className={"mx-2 w-max-[600px]"}>
      <ModalHeader title={"Share Resource"} />
      <div
        className={
          "flex flex-col items-center justify-center gap-4 bg-white dark:bg-dark-component-bg px-6 py-3 rounded-b-md"
        }
      >
        {/*Info Section*/}
        <p className={"text-second-text text-sm dark:text-gray-500"}>
          Found an interesting resource? Do you want to share it with the
          community?
          <br />
          Here you to share it!
        </p>

        <Formik
          initialValues={
            usePublishResourceForm() ?? {
              title: "",
              url: "",
              categoryId: "",
              resourceFlagId: "",
            }
          }
          onSubmit={(values) => {
            setErrorMessage("");
            setInfoMessage("");

            setLoadingMessage("Resource publishing..");
            setIsLoading(true);
            ResourceService.publishResource(values)
              .then((resourceId) => {
                setLoadingMessage("Resource reading..");
                setTimeout(() => {
                  ResourceService.getResource(resourceId)
                    .then((resource) => {
                      setIsLoading(false);
                      successToast("Resource Published!");
                      savePublishResourceForm(null);
                      data(resource);
                      destroyModal();
                    })
                    .catch((error) => {
                      setErrorMessage(error.message);
                    });
                }, 50);
              })
              .catch((error) => {
                console.log(error);
                setIsLoading(false);
                if (error.status === 409)
                  setInfoMessage("Resource already explored!");
              });
          }}
          validationSchema={PublishResourceSchema}
        >
          {({ isValid }) => (
            <Form
              className={"w-full flex flex-col gap-4"}
              ref={animationParent}
            >
              <AutoStateSave />

              <TextInput
                name={"title"}
                placeholder={"Brief description"}
                maxLength={76}
              />
              <UrlInput
                name={"url"}
                placeholder={"Paste Url"}
                autoComplete={"url"}
                icon={<BiLink />}
              />
              <div className={"w-full flex justify-between gap-4"}>
                <SelectInput
                  name={"categoryId"}
                  placeholder={"Select category"}
                  data={categories}
                  isLoading={categoriesIsLoading}
                />

                <SelectInput
                  name={"resourceFlagId"}
                  placeholder={"Select flag"}
                  data={flags}
                  isLoading={flagsIsLoading}
                />
              </div>

              {isLoading && (
                <InlineLoader
                  loader={
                    <Radio
                      visible={true}
                      height="24"
                      width="24"
                      colors={["#3B49DF", "#3B49DF", "#3B49DF"]}
                      ariaLabel="radio-loading"
                      wrapperStyle={{}}
                      wrapperClass="radio-wrapper"
                    />
                  }
                  message={loadingMessage ?? "loading ..."}
                  version={"input-like"}
                />
              )}

              {infoMessage && (
                <InfoView message={infoMessage} version={"input-like"} />
              )}

              {errorMessage && (
                <ErrorView message={errorMessage} version={"input-like"} />
              )}

              <PrimaryAccentButton
                type={"submit"}
                disabled={isLoading || !isValid}
                title={"Share"}
              />
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
}
