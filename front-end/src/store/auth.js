import { createSlice } from "@reduxjs/toolkit";

const loadAuthStateFromLocalStorage = () => {
  try {
    const serializedState = localStorage.getItem("auth-info");
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (error) {
    return undefined;
  }
};

const initialState = loadAuthStateFromLocalStorage() || {
  auth: {
    id: "",
    name: "",
    username: "",
    email: "",
    role: "guest",
    avatar: null,
    connectedAccountType: null,
    isProfileBuilt: null,
    userSignedIn: false,
  },
};

const auth = createSlice({
  name: "auth",
  initialState,
  reducers: {
    signIn: (state, action) => {
      console.log(action.payload.result);
      state.auth.userSignedIn = true;
      state.auth.id = action.payload.result.id;
      state.auth.role = action.payload.result.role;
      state.auth.email = action.payload.result.email;
      state.auth.name = action.payload.result.name;
      state.auth.username = action.payload.result.username;
      state.auth.avatar = action.payload.result.avatar;
      state.auth.isProfileBuilt = action.payload.result.isProfileBuilt;
      state.auth.connectedAccountType =
        action.payload.result.connectedAccountType;

      localStorage.setItem("auth-info", JSON.stringify(state));

      if (action.payload.rememberMe) {
        localStorage.setItem("authToken", action.payload.result.token);
      } else {
        sessionStorage.setItem("authToken", action.payload.result.token);
      }
    },
    logOut: (state) => {
      state.auth.userSignedIn = false;
      state.auth.isProfileBuilt = null;
      state.auth.id = null;
      state.auth.role = null;
      state.auth.email = null;
      state.auth.name = null;
      state.auth.username = null;
      localStorage.removeItem("authToken");
      sessionStorage.removeItem("authToken");
      sessionStorage.removeItem("local-buds-count");
      localStorage.removeItem("auth-info");
      localStorage.removeItem("sb-gfozjaqixgovsxfcnnvv-auth-token");
    },
    build: (state) => {
      state.auth.isProfileBuilt = true;
      localStorage.setItem("auth-info", JSON.stringify(state));
    },
  },
});

export const { signIn, logOut, build } = auth.actions;
export default auth.reducer;
