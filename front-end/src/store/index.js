import { configureStore } from "@reduxjs/toolkit";
import auth from "./auth.js";
import modal from "./modal.js";
import process from "./process.js";
import user from "./user.js";
import appSettings from "./appSettings.js";

const store = configureStore({
  reducer: {
    auth,
    user,
    appSettings,
    modal,
    process,
  },
});

export default store;
