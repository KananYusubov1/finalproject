import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  emailVerification: {
    email: "",
    isVerified: false,
  },
  profileBuild: {
    categories: [],
    formValues: null,
    providerAvatar: null,
    firstAvatar: null,
    resourcePublishFormValues: null,
  },
  reportAbuse: {
    contentUrl: null,
    contentId: null,
    contentType: null,
  },
  loading: {
    label: null,
    isVisible: false,
  },
  drawer: false,
};

const process = createSlice({
  name: "modal",
  initialState,
  reducers: {
    toggleDrawer: (state) => {
      state.drawer = !state.drawer;
    },
    finishDrawer: (state) => {
      state.drawer = false;
    },
    startLoading: (state, action) => {
      state.loading.isVisible = true;
      state.loading.label = action.payload;
    },
    stopLoading: (state) => {
      state.loading.isVisible = false;
      state.loading.label = null;
    },
    startEmailVerification: (state, action) => {
      state.emailVerification = {
        email: action.payload,
        isVerified: false,
      };
    },
    verifyEmail: (state) => {
      state.emailVerification.isVerified = true;
    },
    finishVerification: (state) => {
      state.emailVerification = {
        email: null,
        isVerified: false,
      };
    },
    saveCategories: (state, action) => {
      state.profileBuild.categories = action.payload;
    },
    saveFormValues: (state, action) => {
      state.profileBuild.formValues = action.payload;
    },
    saveProviderAvatar: (state, action) => {
      state.profileBuild.providerAvatar = action.payload;
    },
    saveFirstAvatar: (state, action) => {
      state.profileBuild.firstAvatar = action.payload;
    },
    savePublishResourceFormValues: (state, action) => {
      state.profileBuild.resourcePublishFormValues = action.payload;
    },
    startReportAbuse: (state, action) => {
      state.reportAbuse = {
        contentId: action.payload.contentId,
        contentType: action.payload.contentType,
        contentUrl: action.payload.contentUrl,
      };
    },
    finishReportAbuse: (state) => {
      state.reportAbuse = {
        contentId: null,
        contentType: null,
      };
    },
  },
});

export const {
  toggleDrawer,
  finishDrawer,
  startLoading,
  stopLoading,
  startEmailVerification,
  verifyEmail,
  finishVerification,
  saveCategories,
  saveFormValues,
  saveProviderAvatar,
  saveFirstAvatar,
  savePublishResourceFormValues,
  startReportAbuse,
  finishReportAbuse,
} = process.actions;

export default process.reducer;
