import { motion } from "framer-motion";
import PageHeader from "../../components/pages/common/page-header/index.js";
import PrimaryAccentButton from "../../components/buttons/primary-accent-button/index.js";
import PageGuidelines from "../../components/pages/common/page-guidelines/index.js";
import PageStats from "../../components/pages/common/page-stats/index.js";
import { useEffect, useState } from "react";
import TitleDivider from "../../components/title-dividers/title-divider/index.js";
import { createModal } from "../../utils/modal.js";
import { ResourceService } from "../../services";
import Resource from "../../components/pages/explore-page/resource/index.js";
import ResourceSkeleton from "../../components/skeleton-loaders/resource-skeleton/index.js";
import NoResultFound from "../../components/common/no-result-found/index.js";
import { useUserSignedIn } from "../../utils/auth.js";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import { BiSort } from "react-icons/bi";
import PageSearchBar from "../../components/common/page-search-bar/index.js";
import { useSearchParams } from "react-router-dom";
import { TbCategory2 } from "react-icons/tb";
import { HiFlag } from "react-icons/hi";
import { Helmet } from "react-helmet";
import PageFilterContainer from "../../components/pages/common/page-filter-container/index.js";
import { getContentCount } from "../../services/contentCommon.js";

const ExplorePage = () => {
  const [animationParent] = useAutoAnimate();
  const [searchParams, setSearchParams] = useSearchParams();

  const [isLoading, setIsLoading] = useState(false);
  const [resources, setResources] = useState([]);
  const [count, setCount] = useState(0);
  const isSignedIn = useUserSignedIn();

  const loadResources = () => {
    setIsLoading(true);

    getContentCount("explore").then((res) => setCount(res));

    const savedFilter = searchParams.get("filter") === "saved";
    if (!savedFilter)
      ResourceService.getAllResources(
        searchParams.get("q"),
        searchParams.get("category"),
        searchParams.get("flag"),
        searchParams.get("sort")
      )
        .then((response) => {
          setResources(response.items);
        })
        .catch((error) => {
          setResources(error);
        })
        .finally(() => {
          setIsLoading(false);
        });
    else
      ResourceService.getAllSavedResources(
        searchParams.get("q"),
        searchParams.get("category"),
        searchParams.get("flag"),
        searchParams.get("sort")
      )
        .then((response) => {
          setResources(response.items);
        })
        .catch((error) => {
          setResources(error);
        })
        .finally(() => {
          setIsLoading(false);
        });
  };

  useEffect(() => {
    loadResources();
  }, []);

  const handleDeleteResource = (id) => {
    setResources(resources.filter((resource) => resource.id !== id));
    setCount(count - 1);
  };

  const handlePublishResource = (resource) => {
    setResources([resource, ...resources]);
    setCount(count + 1);
  };

  const handleSearch = () => {
    loadResources();
  };

  const handleGetSearchParam = (param) => {
    return searchParams.get(param);
  };

  const pageFilters = [
    {
      label: "Saved",
      paramName: "saved",
    },
  ];

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={"w-full flex flex-col justify-between  gap-6 py-4"}
      ref={animationParent}
    >
      <Helmet>
        <meta charSet="utf-8" />
        <title>Explore - Clubrick Community</title>
      </Helmet>

      <PageHeader
        iconName={"explore"}
        title={"Explore"}
        description={
          "Let's revive forgotten treasures by collecting and sharing valuable resources we discover during our internet journeys.\nAre you ready for the adventure!"
        }
        accentColor={"#47d5e6"}
        actionSide={
          <div className={"w-26"}>
            <PrimaryAccentButton
              title={"Share Resource"}
              click={() => {
                if (isSignedIn) {
                  createModal("publishResource", handlePublishResource);
                } else {
                  createModal("enter");
                }
              }}
            />
          </div>
        }
      />
      <div className={"flex justify-between"}>
        <aside className={"w-60 px-4 flex flex-col gap-5"}>
          {isSignedIn && (
            <>
              <TitleDivider title={"Sub Pages"} />
              <PageFilterContainer
                pageFilters={pageFilters}
                paramName={"filter"}
                onSearch={handleSearch}
                setSearchParam={setSearchParams}
                searchParams={searchParams}
                isRemovable={true}
              />
            </>
          )}

          <PageGuidelines
            title={"share guidelines"}
            guidelines={
              "Share valuable resources.\nInclude a brief description\nAvoid self-promotion.\nEnjoy the sharing experience!"
            }
          />
          <PageStats stat={count} title={"Resources Shared"} />
        </aside>
        <main className={"w-full px-1 flex flex-col gap-3"}>
          {/*Content Header*/}
          {/*<div className={" "}>*/}
          <PageSearchBar
            options={[
              {
                label: "Category",
                type: "category",
                paramName: "category",
                icon: <TbCategory2 />,
              },
              {
                label: "Flag",
                type: "resource-flag",
                paramName: "flag",
                icon: <HiFlag />,
              },
              {
                label: "Sort",
                type: "sort",
                paramName: "sort",
                icon: <BiSort />,
              },
            ]}
            onSearch={handleSearch}
            placeholder={"Search resource"}
            getSearchParam={handleGetSearchParam}
            setSearchParam={setSearchParams}
          />
          {/*</div>*/}

          <div className={"flex flex-col  gap-4"} ref={animationParent}>
            {isLoading ? (
              <div className={"flex flex-col gap-12"} ref={animationParent}>
                <ResourceSkeleton />
                <ResourceSkeleton />
              </div>
            ) : resources.length ? (
              <>
                {resources.map((resource) => (
                  <Resource
                    key={resource.id}
                    resource={resource}
                    onDeleteResource={handleDeleteResource}
                  />
                ))}
              </>
            ) : (
              <NoResultFound />
            )}
          </div>
        </main>
      </div>
    </motion.div>
  );
};

export default ExplorePage;
