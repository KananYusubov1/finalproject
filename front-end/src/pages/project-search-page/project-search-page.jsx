import { motion } from "framer-motion";
import { Helmet } from "react-helmet";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import { useSearchParams } from "react-router-dom";
import PageFilterContainer from "../../components/pages/common/page-filter-container/index.js";
import { useEffect, useState } from "react";
import { MagnifyingGlass } from "react-loader-spinner";
import NoResultFound from "../../components/common/no-result-found/index.js";
import { getAllResources } from "../../services/resources.js";
import Resource from "../../components/pages/explore-page/resource/index.js";
import UserSearchCard from "../../components/pages/common/user-search-card/index.js";
import Question from "../../components/pages/form-page/question/index.js";
import Article from "../../components/pages/articles-page/article/index.js";
import { getAllQuestion } from "../../services/question.js";
import { getAllArticle } from "../../services/article.js";
import { searchUser } from "../../services/user.js";

const ProjectSearchPage = () => {
  const { animationParent } = useAutoAnimate();

  const [searchParams, setSearchParams] = useSearchParams();
  const query = searchParams.get("q");

  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState([]);

  const pageFilters = [
    {
      label: "Articles",
      paramName: "articles",
    },
    {
      label: "Questions",
      paramName: "questions",
    },
    {
      label: "Resources",
      paramName: "resources",
    },
    {
      label: "People",
      paramName: "people",
    },
  ];

  const handleOnSearch = () => {
    setIsLoading(true);
    setData([]);
    const filter = searchParams.get("filter");
    switch (filter) {
      case "articles":
        getAllArticle(query)
          .then((res) => {
            setData(res.items);
          })
          .catch((error) => {
            console.log(error);
          })
          .finally(() => {
            setIsLoading(false);
          });
        break;
      case "questions":
        getAllQuestion(query)
          .then((res) => {
            setData(res.items);
          })
          .catch((error) => {
            console.log(error);
          })
          .finally(() => {
            setIsLoading(false);
          });

        break;
      case "resources":
        getAllResources(query)
          .then((res) => {
            setData(res.items);
          })
          .catch((error) => {
            console.log(error);
          })
          .finally(() => {
            setIsLoading(false);
          });
        break;
      case "people":
        searchUser(query)
          .then((res) => {
            setData(res);
          })
          .catch((error) => {
            console.log(error);
          })
          .finally(() => {
            setIsLoading(false);
          });

        break;
      default:
        setIsLoading(false);

        break;
    }
  };

  useEffect(() => {
    handleOnSearch();
  }, [query]);

  const renderItem = (item) => {
    const filter = searchParams.get("filter");

    switch (filter) {
      case "resources":
        return <Resource resource={item} mode={"search"} />;
      case "people":
        return <UserSearchCard user={item} />;
      case "questions":
        return <Question question={item} mode={"search"} />;
      case "articles":
        return <Article article={item} mode={"search"} />;
    }
  };

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={"w-full flex flex-col items-center mt-3 px-[10%] py-8  gap-4"}
      ref={animationParent}
    >
      <Helmet>
        <meta charSet="utf-8" />
        <title>Search - Clubrick Community</title>
      </Helmet>

      <div className={"w-full"}>
        <span className={"text-3xl font-bold dark:text-gray-300 select-none"}>
          Search results for <span className={"font-modulus"}>{query}</span>
        </span>
      </div>

      <div className={"w-full flex gap-4"}>
        <div className={"w-56"}>
          <PageFilterContainer
            pageFilters={pageFilters}
            paramName={"filter"}
            defaultParam={"articles"}
            onSearch={handleOnSearch}
            setSearchParam={setSearchParams}
            searchParams={searchParams}
          />
        </div>
        <div className={"w-full"}>
          {isLoading ? (
            <div className={"w-full h-full flex items-center justify-center"}>
              <MagnifyingGlass
                visible={true}
                height="100"
                width="100"
                ariaLabel="MagnifyingGlass-loading"
                wrapperStyle={{}}
                wrapperClass="MagnifyingGlass-wrapper"
                glassColor="#c0efff"
                color="#4a86e1"
              />
            </div>
          ) : data.length > 0 ? (
            <div className={"w-full flex flex-col gap-4"}>
              {data.map((item) => renderItem(item))}
            </div>
          ) : (
            <div>
              <NoResultFound />
            </div>
          )}
        </div>
      </div>
    </motion.div>
  );
};

export default ProjectSearchPage;
