import { motion } from "framer-motion";

const ModeratorPage = () => {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={""}
    >
      Moderators
      <aside></aside>
      <main></main>
    </motion.div>
  );
};

export default ModeratorPage;
