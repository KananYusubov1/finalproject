import ProductViewPageHeader from "../../components/pages/shop-page/product-view-page-header/index.js";
import { motion } from "framer-motion";
import { Helmet } from "react-helmet";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import classNames from "classnames";
import ProductViewPagePhotos from "../../components/pages/shop-page/product-view-page-photos/index.js";
import ProductViewPageInfo from "../../components/pages/shop-page/product-view-page-info/index.js";
import { Tab } from "@headlessui/react";
import TabHead from "../../components/tabs/tab-head/index.js";
import MarkdownViewer from "../../components/markdown-viewer/index.js";
import ReviewArea from "../../components/pages/shop-page/review-area/index.js";
import { useEffect, useState } from "react";
import {
  startLoadingProcess,
  stopLoadingProcess,
} from "../../utils/process.js";
import { errorToast } from "../../utils/toast.js";
import { getProduct } from "../../services/shop.js";
import { useParams } from "react-router-dom";
import ContentNotFound from "../../components/common/content-not-found/index.js";

const ProductViewPage = () => {
  const { animationParent } = useAutoAnimate();
  const [product, setProduct] = useState(null);
  const [isNotFound, setIsNotFound] = useState(false);

  const { id } = useParams();

  useEffect(() => {
    startLoadingProcess("Product Loading");
    getProduct(id)
      .then((res) => {
        setProduct(res);
      })
      .catch((error) => {
        console.log(error);
        if (error.status === 404) {
          setIsNotFound(true);
        } else errorToast("A problem accrued, please try again later");
      })
      .finally(() => {
        stopLoadingProcess();
      });
  }, []);

  if (isNotFound) return <ContentNotFound />;

  if (!product) return null;

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={classNames({
        "w-full flex flex-col items-center  gap-4 py-4": true,
      })}
      ref={animationParent}
    >
      <Helmet>
        <meta charSet="utf-8" />
        <title>{product?.name ?? ""} - Clubrick Shop</title>
      </Helmet>
      <ProductViewPageHeader product={product} />
      <div className={"w-full grid grid-cols-[1fr,500px] gap-4"}>
        <ProductViewPagePhotos product={product} />
        <ProductViewPageInfo product={product} />
      </div>
      <Tab.Group>
        <Tab.List className="w-full flex gap-1 rounded-md bg-body-bg dark:bg-dark-body-bg border-[1px] border-border-clr dark:border-dark-border-clr p-1">
          <TabHead key={"description"} label={"Description"} />
          <TabHead key={"reviews"} label={"Reviews"} />
        </Tab.List>
        <Tab.Panels className="w-full mt-2">
          <Tab.Panel
            key={"description"}
            className={"w-full h-full p-3 card-color-schema"}
          >
            <div
              className={
                "card-color-schema card-border-color-schema card-size-schema w-full h-full"
              }
            >
              <MarkdownViewer source={product?.description} />
            </div>
          </Tab.Panel>
          <Tab.Panel
            key={"reviews"}
            className={"w-full h-full p-3 card-color-schema"}
          >
            <div
              className={
                "card-color-schema card-border-color-schema card-size-schema w-full h-full"
              }
            >
              <ReviewArea productId={product.id} />
            </div>
          </Tab.Panel>
        </Tab.Panels>
      </Tab.Group>
    </motion.div>
  );
};

export default ProductViewPage;
