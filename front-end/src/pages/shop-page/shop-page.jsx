import classNames from "classnames";
import { motion } from "framer-motion";
import { Helmet } from "react-helmet";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import PageHeader from "../../components/pages/common/page-header/index.js";
import ProductCard from "../../components/pages/shop-page/product-card/index.js";
import PageSearchBar from "../../components/common/page-search-bar/index.js";
import { BiSort } from "react-icons/bi";
import NoResultFound from "../../components/common/no-result-found/index.js";
import { useEffect, useState } from "react";
import { getFavProducts, getProducts } from "../../services/shop.js";
import { useSearchParams } from "react-router-dom";
import PageFilterContainer from "../../components/pages/common/page-filter-container/index.js";
import { useUserSignedIn } from "../../utils/auth.js";
import ProductSkeletonLoader from "../../components/skeleton-loaders/product-skeleton-loader/index.js";
import PriceIndicator from "../../components/common/price-indicator/index.js";
import { getUsableBuds } from "../../services/buds.js";

const ShopPage = () => {
  const { animationParent } = useAutoAnimate();
  const isSignedIn = useUserSignedIn();
  const [products, setProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const [useAbleBuds, setUseAbleBuds] = useState(0);

  const [searchParams, setSearchParams] = useSearchParams();

  const handleSearch = () => {
    loadProducts();
  };

  const loadProducts = () => {
    setIsLoading(true);

    const favFilter = searchParams.get("filter") === "fav";

    if (favFilter) {
      getFavProducts(searchParams.get("q"), searchParams.get("sort"))
        .then((data) => {
          setProducts(data.items);
        })
        .finally(() => {
          setIsLoading(false);
        });
    } else {
      getProducts(searchParams.get("q"), searchParams.get("sort"))
        .then((data) => {
          setProducts(data.items);
        })
        .finally(() => {
          setIsLoading(false);
        });
    }
  };

  useEffect(() => {
    loadProducts();
  }, []);

  useEffect(() => {
    getUsableBuds().then((res) => setUseAbleBuds(res));
  });

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={classNames({
        "w-full flex flex-col items-center  gap-4 py-4": true,
      })}
      ref={animationParent}
    >
      <Helmet>
        <meta charSet="utf-8" />
        <title>Clubrick Shop</title>
      </Helmet>

      <PageHeader
        iconName={"shop"}
        title={"Shop"}
        description={"Explore our products"}
        accentColor={"#3B49DF"}
        actionSide={<PriceIndicator price={useAbleBuds} isBig={true} />}
      />

      <div
        className={
          "w-full px-1 grid grid-cols-[1fr,100px] gap-5 sticky top-[55px] z-10"
        }
      >
        <PageSearchBar
          options={[
            {
              label: "Sort",
              type: "productSort",
              paramName: "sort",
              icon: <BiSort />,
            },
          ]}
          onSearch={handleSearch}
          setSearchParam={setSearchParams}
          placeholder={"Search product"}
          defaultSearchValue={searchParams.get("q")}
          getSearchParam={(param) => searchParams.get(param)}
        />
        {isSignedIn && (
          <div className={"w-fit ml-[-8px]"}>
            <PageFilterContainer
              pageFilters={[
                {
                  label: "Favorites",
                  paramName: "fav",
                },
              ]}
              onSearch={handleSearch}
              searchParams={searchParams}
              setSearchParam={setSearchParams}
              paramName={"filter"}
              isRemovable={true}
            />
          </div>
        )}
      </div>

      <div className={"grid grid-cols-4 gap-2"} ref={animationParent}>
        {isLoading ? (
          <>
            <ProductSkeletonLoader />
            <ProductSkeletonLoader />
            <ProductSkeletonLoader />
            <ProductSkeletonLoader />
          </>
        ) : products.length ? (
          <>
            {products.map((product) => (
              <ProductCard key={product.id} product={product} />
            ))}
          </>
        ) : (
          <NoResultFound label={"No products found"} />
        )}
      </div>
    </motion.div>
  );
};

export default ShopPage;
