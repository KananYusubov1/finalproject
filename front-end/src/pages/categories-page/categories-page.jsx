import { motion } from "framer-motion";
import CategoriesPageHeader from "../../components/pages/categories-page/categories-page-header/index.js";
import CategoriesContainer from "../../components/pages/categories-page/categories-container/index.js";
import { useEffect, useState } from "react";
import { TagService } from "../../services/";
import CategorySkeleton from "../../components/skeleton-loaders/category-skeleton/index.js";
import { useSearchParams } from "react-router-dom";
import { Helmet } from "react-helmet";

const CategoriesPage = () => {
  const [isLoading, setIsLoading] = useState(false);

  const [categories, setCategories] = useState([]);

  const [searchParams, setSearchParams] = useSearchParams();

  const loadCategories = () => {
    setIsLoading(true);

    const followedFilter = searchParams.get("filter") === "followed";
    if (followedFilter)
      TagService.getAllFollowedCategory(searchParams.get("q"))
        .then((res) => {
          console.log(res);
          setIsLoading(false);
          setCategories(res);
        })
        .catch((error) => {
          console.log(error);
          setIsLoading(false);
        });
    else
      TagService.getAllCategory(searchParams.get("q"))
        .then((res) => {
          console.log(res);
          setIsLoading(false);
          setCategories(res);
        })
        .catch((error) => {
          console.log(error);
          if (error.status === 404) setCategories([]);
          setIsLoading(false);
        });
  };

  useEffect(() => {
    loadCategories();
  }, []);

  const handleChangeSearch = (search) => {
    setSearchParams((prev) => {
      prev.set("q", search);
      return prev;
    });
  };

  const handleSearch = () => {
    loadCategories();
  };

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={"w-full flex flex-col  gap-4 px-2 py-4"}
    >
      <Helmet>
        <meta charSet="utf-8" />
        <title>Categories - Clubrick Community</title>
      </Helmet>

      <CategoriesPageHeader
        searchParams={searchParams}
        setSearchParam={setSearchParams}
        defaultSearchValue={searchParams.get("q")}
        onSearchChange={handleChangeSearch}
        onSearch={handleSearch}
      />

      {isLoading ? (
        <div className={"flex items-center justify-around flex-wrap"}>
          <CategorySkeleton />
          <CategorySkeleton />
          <CategorySkeleton />
          <CategorySkeleton />
        </div>
      ) : (
        <CategoriesContainer categoriesData={categories} />
      )}
    </motion.div>
  );
};

export default CategoriesPage;
