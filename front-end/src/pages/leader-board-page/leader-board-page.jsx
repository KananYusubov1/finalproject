import { motion } from "framer-motion";
import { Helmet } from "react-helmet";
import LastArticles from "../../components/pages/home-page/last-articles/index.js";
import LastQuestions from "../../components/pages/home-page/last-questions/index.js";
import LastResources from "../../components/pages/home-page/last-resources/index.js";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import { useEffect, useState } from "react";
import { getLeaders } from "../../services/buds.js";
import Leader from "../../components/common/leader/index.js";
import PageHeader from "../../components/pages/common/page-header/index.js";
import { TailSpin } from "react-loader-spinner";

const LeaderBoardPage = () => {
  const { animationParent } = useAutoAnimate();

  const [leaders, setLeaders] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    getLeaders()
      .then((res) => setLeaders(res))
      .catch((err) => console.log(err))
      .finally(() => setIsLoading(false));
  }, []);

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={"w-full h-full flex justify-between  gap-4 px-2 py-4"}
      ref={animationParent}
    >
      <Helmet>
        <meta charSet="utf-8" />
        <title>Leader Board - Clubrick Community</title>
      </Helmet>

      <div className={"w-full flex flex-col gap-6"}>
        <PageHeader title={"Leaders!"} accentColor={"#4caf50"} />
        <div className={"w-full flex flex-col gap-5"}>
          {isLoading ? (
            <div>
              <div className={"w-full h-full flex items-center justify-center"}>
                <TailSpin
                  height="60"
                  width="60"
                  color={"#3B49DF"}
                  wrapperClass="radio-wrapper"
                  radius="2"
                  ariaLabel="three-dots-loading"
                  wrapperStyle={{}}
                  wrapperClassName=""
                  visible={true}
                />
              </div>
            </div>
          ) : (
            leaders.map((leader) => <Leader key={leader.id} leader={leader} />)
          )}
        </div>
      </div>

      <div className={"flex flex-col gap-5"}>
        <LastArticles />
        <LastQuestions />
        <LastResources />
      </div>
    </motion.div>
  );
};

export default LeaderBoardPage;
