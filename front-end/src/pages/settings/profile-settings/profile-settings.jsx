import { Form, Formik } from "formik";
import InputIsland from "../../../components/form/input-island/index.js";
import SubmitIsland from "../../../components/form/submit-island/index.js";
import { useEffect, useState } from "react";
import {
  startLoadingProcess,
  stopLoadingProcess,
} from "../../../utils/process.js";
import ErrorView from "../../../components/common/error-view/index.js";
import { SettingsService } from "../../../services";
import { successToast } from "../../../utils/toast.js";
import { updateUser } from "../../../utils/user.js";
import ConnectGithubAccount from "../../../components/form/connect-github-account/index.js";
import { ProfileSettingsSchema } from "../../../validation/Schemas/ProfileSettingsSchema.js";

const ProfileSettings = () => {
  const inputIslands = [
    {
      name: "User",
      inputs: [
        {
          title: "Name",
          type: "text",
          stateName: "name",
        },
        {
          title: "Username",
          type: "username",
          stateName: "username",
        },
        {
          title: "Avatar",
          type: "avatar",
          stateName: "avatar",
        },
      ],
    },
    {
      name: "Basic",
      inputs: [
        {
          title: "Website URL",
          type: "url",
          placeholder: "https://youresite.com",
          maxLength: 100,
          stateName: "websiteUrl",
        },
        {
          title: "Location",
          type: "text",
          placeholder: "Shaki, Azerbaijan",
          maxLength: 100,
          stateName: "location",
        },
        {
          title: "Bio",
          type: "text-area",
          maxLength: 200,
          placeholder: "A short bio",
          stateName: "bio",
        },
      ],
    },
    {
      name: "Coding",
      inputs: [
        {
          title: "Currently learning",
          type: "text-area",
          maxLength: 200,
          hint: "What are you learning right now? What are the new tools and languages you're picking up right now?",
          stateName: "currentlyLearning",
        },
        {
          title: "Available for",
          type: "text-area",
          maxLength: 200,
          hint: "What kinds of collaborations or discussions are you available for? What's a good reason to say Hey! to you these days?",
          stateName: "availableFor",
        },
        {
          title: "Currently hacking on",
          type: "text-area",
          maxLength: 200,
          hint: "What projects are currently occupying most of your time?",
          stateName: "currentlyWorking",
        },
      ],
    },
    {
      name: "Work",
      inputs: [
        {
          title: "Work",
          type: "text",
          maxLength: 100,
          placeholder: "What do you do? Example: CEO at WolfWare Inc.",
          stateName: "work",
        },
        {
          title: "Education",
          type: "text",
          maxLength: 100,
          placeholder:
            "Where did you go to school? Example: Student at Step It Academy.",
          stateName: "education",
        },
      ],
    },
  ];

  const [settings, setSettings] = useState(null);
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
    startLoadingProcess("Settings Loading");
    SettingsService.getProfileSettings()
      .then((res) => {
        stopLoadingProcess();
        setSettings(res);
        console.log(res);
      })
      .catch((error) => {
        stopLoadingProcess();
        console.log(error);
      });
  }, []);

  return (
    <div className={""}>
      {settings && (
        <Formik
          enableReinitialize={true}
          initialValues={{
            ...settings,
          }}
          onSubmit={(values) => {
            setErrorMessage("");
            startLoadingProcess("Saving Settings");
            SettingsService.updateProfileSettings(values)
              .then((res) => {
                stopLoadingProcess();
                successToast("Profile Settings Updated!");
                updateUser(res);
                setSettings(res);
              })
              .catch((error) => {
                stopLoadingProcess();
                console.log(error);
                setErrorMessage("A problem occured.Please try again later .");
              });
          }}
          validationSchema={ProfileSettingsSchema}
        >
          {({ dirty, isValid }) => (
            <Form className={"flex flex-col gap-5"}>
              {errorMessage && (
                <ErrorView message={errorMessage} version={"input-like"} />
              )}

              <ConnectGithubAccount />

              {inputIslands.map((island) => {
                return <InputIsland key={island.name} island={island} />;
              })}

              <SubmitIsland formIsDirty={dirty && isValid} />
            </Form>
          )}
        </Formik>
      )}
    </div>
  );
};

export default ProfileSettings;
