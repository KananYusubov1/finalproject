import { Form, Formik } from "formik";
import InputIsland from "../../../components/form/input-island/index.js";
import SubmitIsland from "../../../components/form/submit-island/index.js";
import { useEffect, useState } from "react";
import {
  startLoadingProcess,
  stopLoadingProcess,
} from "../../../utils/process.js";
import { SettingsService } from "../../../services";
import { successToast } from "../../../utils/toast.js";
import ErrorView from "../../../components/common/error-view/index.js";
import { updateSettings } from "../../../utils/appSettings.js";
import { useUserSignedIn } from "../../../utils/auth.js";
import { useNavigate } from "react-router-dom";

const CustomizationSettings = () => {
  const signedIn = useUserSignedIn();

  const navigate = useNavigate();

  const inputIslands = [
    {
      name: "Appearance",
      inputs: [
        {
          title: "Theme",
          type: "theme",
          stateName: "theme",
        },
      ],
    },
    {
      name: "Content",
      inputs: [
        {
          type: "contentPerPage",
          stateName: "contentPerPage",
        },
      ],
    },
    {
      name: "Branding",
      inputs: [
        {
          title: "Brand Color",
          bigTitle: true,
          stateName: "accentColor",
          type: "color",
        },
      ],
    },
  ];
  const [settings, setSettings] = useState(null);
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
    if (!signedIn) {
      navigate("/enter");
    } else {
      startLoadingProcess("Settings Loading");
      SettingsService.getCustomizationSettings()
        .then((res) => {
          stopLoadingProcess();
          setSettings(res);
          console.log(res);
        })
        .catch((error) => {
          stopLoadingProcess();
          console.log(error);
        });
    }
  }, []);

  return (
    <div className={"flex gap-2"}>
      {settings && (
        <Formik
          enableReinitialize={true}
          initialValues={{
            ...settings,
          }}
          onSubmit={(values) => {
            setErrorMessage("");
            startLoadingProcess("Saving Settings");
            SettingsService.updateCustomizationSettings(values)
              .then((res) => {
                stopLoadingProcess();
                successToast("Customization Settings Updated!");
                console.log(res);
                updateSettings(res);
                setSettings(res);
              })
              .catch((error) => {
                stopLoadingProcess();
                console.log(error);
                setErrorMessage("A problem occured.Please try again later .");
              });
          }}
        >
          {({ dirty }) => (
            <Form className={"flex flex-col gap-5"}>
              {errorMessage && (
                <ErrorView message={errorMessage} version={"input-like"} />
              )}
              {inputIslands.map((island) => {
                return <InputIsland key={island.name} island={island} />;
              })}

              <SubmitIsland formIsDirty={dirty} />
            </Form>
          )}
        </Formik>
      )}
    </div>
  );
};

export default CustomizationSettings;
