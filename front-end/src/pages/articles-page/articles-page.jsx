import { motion } from "framer-motion";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import { useEffect, useState } from "react";
import { useUserSignedIn } from "../../utils/auth.js";
import PageHeader from "../../components/pages/common/page-header/index.js";
import PrimaryAccentButton from "../../components/buttons/primary-accent-button/index.js";
import { createModal } from "../../utils/modal.js";
import TitleDivider from "../../components/title-dividers/title-divider/index.js";
import PageGuidelines from "../../components/pages/common/page-guidelines/index.js";
import PageStats from "../../components/pages/common/page-stats/index.js";
import NoResultFound from "../../components/common/no-result-found/index.js";
import { Helmet } from "react-helmet";
import Article from "../../components/pages/articles-page/article/index.js";
import PageSearchBar from "../../components/common/page-search-bar/index.js";
import { TbCategory2 } from "react-icons/tb";
import { HiFlag } from "react-icons/hi";
import { BiSort } from "react-icons/bi";
import { useNavigate, useSearchParams } from "react-router-dom";
import PageFilterContainer from "../../components/pages/common/page-filter-container/index.js";
import { getAllArticle, getAllSavedArticle } from "../../services/article.js";
import { getContentCount } from "../../services/contentCommon.js";
import ArticleSkeleton from "../../components/skeleton-loaders/article-skeleton/index.js";

const ArticlesPage = () => {
  const [searchParams, setSearchParams] = useSearchParams();

  const [animationParent] = useAutoAnimate();

  const navigate = useNavigate();

  const [isLoading, setIsLoading] = useState(false);
  const [articles, setArticles] = useState([]);
  const [count, setCount] = useState(0);

  const isSignedIn = useUserSignedIn();

  const handleSearch = () => {
    setIsLoading(true);

    getContentCount("articles").then((res) => setCount(res));

    const savedFilter = searchParams.get("filter") === "saved";
    if (savedFilter)
      getAllSavedArticle(
        searchParams.get("q"),
        searchParams.get("category"),
        searchParams.get("flag"),
        searchParams.get("sort")
      )
        .then((res) => {
          console.log(res);
          setArticles(res.items ?? []);
        })
        .catch((error) => {
          console.log(error.message);
        })
        .finally(() => {
          setIsLoading(false);
        });
    else
      getAllArticle(
        searchParams.get("q"),
        searchParams.get("category"),
        searchParams.get("flag"),
        searchParams.get("sort")
      )
        .then((res) => {
          console.log(res);
          setArticles(res.items);
        })
        .catch((error) => {
          console.log(error.message);
        })
        .finally(() => {
          setIsLoading(false);
        });
  };

  const handleGetSearchParam = (param) => {
    return searchParams.get(param);
  };

  const pageFilters = [
    {
      label: "Saved",
      paramName: "saved",
    },
  ];

  useEffect(() => {
    handleSearch();
  }, []);

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={"w-full flex flex-col justify-between  gap-6 py-4"}
      ref={animationParent}
    >
      <Helmet>
        <meta charSet="utf-8" />
        <title>Articles - Clubrick Community</title>
      </Helmet>

      <PageHeader
        iconName={"articles"}
        title={"Articles"}
        description={
          "Remember, knowledge increases with sharing.Share your knowledge with other people with the right title and categorization.\nEveryone's waiting for you!"
        }
        accentColor={"#ffe07d"}
        actionSide={
          <div className={"w-26"}>
            <PrimaryAccentButton
              title={"Publish Article"}
              click={() => {
                if (isSignedIn) {
                  navigate("/publish/article");
                } else {
                  createModal("enter");
                }
              }}
            />
          </div>
        }
      />
      <div className={"flex justify-between"}>
        <aside className={"w-60 px-4 flex flex-col gap-5"}>
          {isSignedIn && (
            <>
              <TitleDivider title={"Sub Pages"} />
              <PageFilterContainer
                pageFilters={pageFilters}
                paramName={"filter"}
                onSearch={handleSearch}
                setSearchParam={setSearchParams}
                searchParams={searchParams}
                isRemovable={true}
              />
            </>
          )}

          <PageGuidelines
            title={"submission guidelines"}
            guidelines={
              "Write in a simple way.\nSolution-oriented approach\nBe open to criticism.\nReady to share information!"
            }
          />
          <PageStats stat={count} title={"Article published"} />
        </aside>
        <main className={"w-full px-1 flex flex-col gap-3"}>
          <div className={"w-full px-1 flex flex-col gap-3 "}>
            <PageSearchBar
              options={[
                {
                  label: "Category",
                  type: "category",
                  paramName: "category",
                  icon: <TbCategory2 />,
                },
                {
                  label: "Flag",
                  type: "article-flag",
                  paramName: "flag",
                  icon: <HiFlag />,
                },
                {
                  label: "Sort",
                  type: "sort",
                  paramName: "sort",
                  icon: <BiSort />,
                },
              ]}
              onSearch={handleSearch}
              getSearchParam={handleGetSearchParam}
              setSearchParam={setSearchParams}
              placeholder={"Search article"}
            />
          </div>

          <div className={"flex flex-col  gap-5"} ref={animationParent}>
            {isLoading ? (
              <div className={"flex flex-col gap-12"} ref={animationParent}>
                <ArticleSkeleton />
                <ArticleSkeleton />
              </div>
            ) : articles.length ? (
              <>
                {articles.map((article) => (
                  <Article key={article.id} article={article} />
                ))}
              </>
            ) : (
              <NoResultFound />
            )}
          </div>
        </main>
      </div>
    </motion.div>
  );
};

export default ArticlesPage;
