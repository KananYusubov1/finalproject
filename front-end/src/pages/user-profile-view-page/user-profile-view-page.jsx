import { motion } from "framer-motion";
import UserProfileViewPageHeader from "../../components/pages/user-profile-view/user-profile-view-page-header/index.js";
import UserProfileViewPageAside from "../../components/pages/user-profile-view/user-profile-view-page-aside/index.js";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { UserService } from "../../services";
import UserNotfound from "../../components/common/user-notfound/index.js";
import {
  startLoadingProcess,
  stopLoadingProcess,
} from "../../utils/process.js";
import { Helmet } from "react-helmet";
import Article from "../../components/pages/articles-page/article/index.js";
import NoResultFound from "../../components/common/no-result-found/index.js";
import { FaBabyCarriage } from "react-icons/fa";
import { getUserArticles } from "../../services/article.js";
import { ThreeDots } from "react-loader-spinner";

const UserProfileViewPage = () => {
  const [user, setUser] = useState({});

  const navigate = useNavigate();
  const { username } = useParams();

  const [notFoundMode, setNotFoundMode] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [articlesLoading, setArticlesLoading] = useState(true);
  const [articles, setArticles] = useState([]);

  useEffect(() => {
    console.log(username);
    startLoadingProcess("User searching");
    UserService.userExistsByUsername(username)
      .then(() => {
        startLoadingProcess("User interrogation");
        UserService.getViewUserByUsername(username)
          .then((res) => {
            stopLoadingProcess(false);
            setUser(res);

            setIsLoading(false);
            setNotFoundMode(false);

            setArticlesLoading(true);
            getUserArticles(res.id)
              .then((result) => {
                setArticles(result.items);
              })
              .catch((error) => {
                console.log(error);
              })
              .finally(() => {
                setArticlesLoading(false);
              });
          })
          .catch((error) => {
            setIsLoading(false);
            stopLoadingProcess(false);
            console.log(error);
          });
      })
      .catch(() => {
        stopLoadingProcess(false);
        setIsLoading(false);
        setNotFoundMode(true);
      });
  }, [username]);

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={
        "w-full flex flex-col items-center justify-center  gap-4 py-4 px-32"
      }
    >
      <Helmet>
        <meta charSet="utf-8" />
        <title>{username} - Clubrick Community</title>
      </Helmet>

      {isLoading ? (
        <></>
      ) : notFoundMode ? (
        <UserNotfound username={username} />
      ) : (
        <>
          <UserProfileViewPageHeader user={user} />
          {/*Main Content Container*/}
          <div className={"w-full grid  grid-cols-3 gap-4 shrink-0 grow-0"}>
            {/*Aside Container*/}
            <div className={"w-full h-52"}>
              <UserProfileViewPageAside user={user} />
            </div>

            <div className={"w-full col-span-2"}>
              {articlesLoading ? (
                <div className={"w-full flex items-center justify-center"}>
                  <ThreeDots
                    height="24"
                    width="24"
                    color={"#3B49DF"}
                    wrapperClass="radio-wrapper"
                    radius="9"
                    ariaLabel="three-dots-loading"
                    wrapperStyle={{}}
                    wrapperClassName=""
                    visible={true}
                  />
                </div>
              ) : articles && articles.length > 0 ? (
                <div className={"w-full flex flex-col gap-4"}>
                  {articles.map((article) => (
                    <Article key={article.id} article={article} mode={"view"} />
                  ))}
                </div>
              ) : (
                <div className={"w-full"}>
                  <NoResultFound
                    icon={<FaBabyCarriage />}
                    label={"Newborn Baby"}
                  />
                </div>
              )}
            </div>
          </div>
        </>
      )}
    </motion.div>
  );
};

export default UserProfileViewPage;
