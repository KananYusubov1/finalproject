import { motion } from "framer-motion";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import { Helmet } from "react-helmet";
import { useUserSignedIn } from "../../utils/auth.js";
import { useNavigate, useSearchParams } from "react-router-dom";
import DashboardStat from "../../components/pages/dashboard-page/dashboard-stat/index.js";
import { useEffect, useState } from "react";
import { getUserArticles } from "../../services/article.js";
import { getFollowers, getFollowings } from "../../services/user.js";
import { useUser } from "../../utils/user.js";
import { ThreeDots } from "react-loader-spinner";
import PageFilterContainer from "../../components/pages/common/page-filter-container/index.js";
import { getAllUserQuestion } from "../../services/question.js";
import Question from "../../components/pages/form-page/question/index.js";
import Article from "../../components/pages/articles-page/article/index.js";
import NoResultFound from "../../components/common/no-result-found/index.js";
import { AiFillTags } from "react-icons/ai";
import { RiMenuSearchLine, RiQuestionAnswerLine } from "react-icons/ri";
import { TbMessage2Question } from "react-icons/tb";
import { HiOutlineNewspaper } from "react-icons/hi";
import UserRelationCard from "../../components/pages/dashboard-page/user-relation-card/index.js";
import classNames from "classnames";
import { getBudsHistory } from "../../services/buds.js";
import { getAllUserResources } from "../../services/resources.js";
import { errorToast } from "../../utils/toast.js";
import Resource from "../../components/pages/explore-page/resource/index.js";
import BudHistoryView from "../../components/pages/dashboard-page/bud-history-view/index.js";

const DashboardPage = () => {
  const { animationParent } = useAutoAnimate();
  const navigate = useNavigate();

  const [searchParams, setSearchParams] = useSearchParams();

  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const userSigned = useUserSignedIn();
  const { id } = useUser();

  if (!userSigned) navigate("/enter");

  const getModeTitle = () => {
    const mode = searchParams.get("mode");
    switch (mode) {
      case "articles":
        return "Articles";
      case "questions":
        return "Questions";
      case "followers":
        return "Followers";
      case "following":
        return "Following users";
      case "resources":
        return "Resources";
    }
  };

  const pageFilters = [
    {
      label: "Articles",
      paramName: "articles",
    },
    {
      label: "Questions",
      paramName: "questions",
    },
    {
      label: "Resources",
      paramName: "resources",
    },
    {
      label: "Followers",
      paramName: "followers",
    },
    {
      label: "Following users",
      paramName: "following",
    },
    {
      label: "Buds History",
      paramName: "budsHistory",
    },
  ];

  const handleSearch = () => {
    setIsLoading(true);

    const mode = searchParams.get("mode");

    switch (mode) {
      case "resources":
        getAllUserResources()
          .then((result) => {
            setData(result);
          })
          .catch((error) => {
            setData([]);
            errorToast("Failed to get resources");
            console.log(error);
          })
          .finally(() => {
            setIsLoading(false);
          });
        break;
      case "articles":
        getUserArticles(id)
          .then((result) => {
            setData(result.items);
          })
          .catch((error) => {
            setData([]);
            errorToast("Failed to get articles");
            console.log(error);
          })
          .finally(() => {
            setIsLoading(false);
          });
        break;
      case "questions":
        getAllUserQuestion()
          .then((result) => {
            setData(result.items);
          })
          .catch((error) => {
            setData([]);
            errorToast("Failed to get questions");
            console.log(error);
          })
          .finally(() => {
            setIsLoading(false);
          });
        break;
      case "followers":
        getFollowers()
          .then((result) => {
            setData(result);
          })
          .catch((error) => {
            setData([]);
            errorToast("Failed to get followers");
            console.log(error);
          })
          .finally(() => {
            setIsLoading(false);
          });
        break;
      case "following":
        getFollowings()
          .then((result) => {
            setData(result);
          })
          .catch((error) => {
            setData([]);
            errorToast("Failed to get following users");
            console.log(error);
          })
          .finally(() => {
            setIsLoading(false);
          });
        break;
      case "budsHistory":
        getBudsHistory()
          .then((result) => {
            setData(result);
          })
          .catch((error) => {
            setData([]);
            errorToast("Failed to get buds history");
            console.log(error);
          })
          .finally(() => {
            setIsLoading(false);
          });
    }
  };

  const renderItem = (item) => {
    const mode = searchParams.get("mode");

    switch (mode) {
      case "following":
      case "followers":
        return <UserRelationCard user={item} />;
      case "questions":
        return <Question question={item} mode={"search"} />;
      case "articles":
        return <Article article={item} mode={"search"} />;
      case "resources":
        return <Resource resource={item} mode={"search"} />;
      case "budsHistory":
        return <BudHistoryView data={item} />;
    }
  };

  useEffect(() => {
    handleSearch();
  }, []);

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={"w-full flex flex-col items-center justify-center  gap-4 py-4"}
      ref={animationParent}
    >
      <Helmet>
        <meta charSet="utf-8" />
        <title>Dashboard - Clubrick Community</title>
      </Helmet>

      <span
        className={
          "w-full flex justify-start dark:text-gray-300 text-3xl font-bold"
        }
      >
        Dashbord » {getModeTitle()}
      </span>

      <div className={"w-full h-32  flex items-center gap-6"}>
        <DashboardStat
          color={"#f1c40f"}
          icon={<AiFillTags />}
          endpoint={"GetUserAnswersCount"}
          label={"Tags followed"}
        />

        <DashboardStat
          color={"#2ecc71"}
          icon={<HiOutlineNewspaper />}
          endpoint={"GetUserPostsCount"}
          label={"Posts published"}
        />

        <DashboardStat
          color={"#2980b9"}
          icon={<RiMenuSearchLine />}
          endpoint={"GetUserResourcesCount"}
          label={"Resource Published"}
        />

        <DashboardStat
          color={"#c0392b"}
          icon={<TbMessage2Question />}
          endpoint={"GetUserQuestionsCount"}
          label={"Question asked"}
        />

        <DashboardStat
          color={"#2980b9"}
          icon={<RiQuestionAnswerLine />}
          endpoint={"GetUserAnswersCount"}
          label={"Question answered"}
        />
      </div>

      <div className={"w-full flex gap-4"}>
        <div className={"w-60"}>
          <PageFilterContainer
            pageFilters={pageFilters}
            paramName={"mode"}
            onSearch={handleSearch}
            setSearchParam={setSearchParams}
            searchParams={searchParams}
            defaultParam={"articles"}
          />
        </div>
        {isLoading ? (
          <div className={"w-full flex items-center justify-center"}>
            <ThreeDots
              height="24"
              width="24"
              color={"#3B49DF"}
              wrapperClass="radio-wrapper"
              radius="9"
              ariaLabel="three-dots-loading"
              wrapperStyle={{}}
              wrapperClassName=""
              visible={true}
            />
          </div>
        ) : (
          <div className={"w-full"}>
            {data && data.length > 0 ? (
              <div
                className={classNames({
                  "w-full flex flex-col gap-4": true,
                  "!flex-row  flex-wrap ":
                    searchParams.get("mode") === "following" ||
                    searchParams.get("mode") === "followers",
                })}
              >
                {data.map((item) => renderItem(item))}
              </div>
            ) : (
              <NoResultFound />
            )}
          </div>
        )}
      </div>
    </motion.div>
  );
};

export default DashboardPage;
