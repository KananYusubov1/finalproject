import { motion } from "framer-motion";
import { Helmet } from "react-helmet";

const ContactPage = () => {
  return (
    <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }}>
      <Helmet>
        <meta charSet="utf-8" />
        <title>Code of Conduct - Clubrick Community</title>
      </Helmet>

      <h1 className="primary-header">Contacts</h1>
      <p className="text">Clubrick Community would love to hear from you!</p>
      <p className={"text"}>
        Email:{" "}
        <a
          href="mailto:support@clubrick.com"
          className="underline-animation dark:text-dark-text-accent text-accent"
        >
          support@clubrick.com
        </a>{" "}
        😎
      </p>
    </motion.div>
  );
};

export default ContactPage;
