import { motion } from "framer-motion";
import { useParams } from "react-router-dom";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import { Helmet } from "react-helmet";
import ContentViewActionBar from "../../components/pages/content-view-page/content-view-action-bar/index.js";
import ContentViewSideBar from "../../components/pages/content-view-page/content-view-side-bar/index.js";
import { useEffect, useState } from "react";
import { errorToast } from "../../utils/toast.js";
import {
  startLoadingProcess,
  stopLoadingProcess,
} from "../../utils/process.js";
import ContentViewUserHeader from "../../components/pages/content-view-page/content-view-user-header/index.js";
import MarkdownViewer from "../../components/markdown-viewer/index.js";
import ContentNotFound from "../../components/common/content-not-found/index.js";
import { getContent } from "../../services/contentCommon.js";
import TagPreview from "../../components/common/tag-preview/index.js";
import AnswerArea from "../../components/pages/content-view-page/answer-area/index.js";
import classNames from "classnames";
import CommentArea from "../../components/pages/content-view-page/comment-area/index.js";
import FooterActionBar from "../../components/pages/content-view-page/footer-action-bar/index.js";

const ContentViewPage = () => {
  const { animationParent } = useAutoAnimate();
  const [content, setContent] = useState();
  const { type, id } = useParams();

  const [isNotFound, setIsNotFound] = useState(false);

  useEffect(() => {
    startLoadingProcess("Content Loading");
    getContent(type, id)
      .then((res) => {
        console.log(res);
        setContent(res);
        stopLoadingProcess();
      })
      .catch((error) => {
        console.log(error);
        if (error.status === 404) {
          setIsNotFound(true);
        } else errorToast("A problem accrued, please try again later");
        stopLoadingProcess();
      });
  }, []);

  if (isNotFound) return <ContentNotFound />;

  if (!content) return null;

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={"w-full h-full flex justify-between  gap-6 md:pt-4 pb-6"}
      ref={animationParent}
    >
      <Helmet>
        <meta charSet="utf-8" />
        <title>{content.header.title ?? ""} - Clubrick Community</title>
      </Helmet>

      <ContentViewActionBar content={content} type={type} id={id} />

      {/*Middle Area | Content area*/}
      <div className={"w-full flex flex-col gap-6 card-color-schema"}>
        {content.header.image && (
          <img
            src={content.header.image}
            alt="Cover image"
            className={"w-full max-h-[400px] object-cover rounded-t-md"}
          />
        )}

        <div
          className={classNames({
            "w-full h-fit flex flex-col gap-4  !px-10 !rounded-none !rounded-t-md !border-t-transparent !border-x-transparent card-size-schema card-color-schema": true,
            "!rounded-t-none": content.header.image,
          })}
        >
          <ContentViewUserHeader
            user={content.userPreview}
            updateTime={content.updateTime}
          />

          <span className={"font-bold text-4xl dark:text-gray-300"}>
            {content.header.title}
          </span>

          <div className={"flex items-center gap-8"}>
            {content.categories.map((category) => (
              <TagPreview
                key={category.id}
                title={category.title}
                colorId={category.accentColor}
              />
            ))}
            {content.articleFlag && (
              <TagPreview
                key={content.articleFlag.id}
                title={content.articleFlag.title}
                colorId={content.articleFlag.accentColor}
              />
            )}
          </div>

          <div className={"max-w-[780px] w-full overflow-clip"}>
            <MarkdownViewer source={content.body} />
          </div>
          <div id={"conversation-area"}></div>
        </div>
        {type === "questions" ? (
          <AnswerArea
            questionId={content.id}
            answers={content.answers}
            correctAnswerId={content.correctAnswerId}
            authorId={content.userPreview.id}
          />
        ) : (
          <CommentArea
            articleId={content.id}
            comments={content.comments}
            authorId={content.userPreview.id}
          />
        )}

        <FooterActionBar content={content} type={type} id={id} />
      </div>

      <ContentViewSideBar
        userId={content.userPreview.id}
        userData={content.userPreview}
      />
    </motion.div>
  );
};

export default ContentViewPage;
