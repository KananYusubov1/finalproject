import { motion } from "framer-motion";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
const AboutPage = () => {
  return (
    <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }}>
      <Helmet>
        <meta charSet="utf-8" />
        <title>About Clubrick - Clubrick Community</title>
      </Helmet>

      <h1 className="primary-header">About CLubrick</h1>
      <p className="text">
        Clubrick is a community of software developers getting together to help
        one another out. The software industry relies on collaboration and
        networked learning. We provide a place for that to happen.
      </p>

      <p className="text">
        We believe in transparency and adding value to the ecosystem. We hope
        you enjoy poking around and participating!
      </p>

      <h1 className="primary-header">Leadership</h1>
      <p className="text">
        Clubrick is led by co-founders (alphabetically by first name){" "}
        <Link
          to={"/CoraEpiro"}
          className="underline-animation text-accent dark:text-dark-text-accent"
        >
          Ali Quliyev
        </Link>{" "}
        ,
        <Link
          to={"/kananysbv"}
          className="underline-animation text-accent dark:text-dark-text-accent"
        >
          Kanan Yusubov
        </Link>{" "}
        ,
        <Link
          to={"/Natiq"}
          className="underline-animation text-accent dark:text-dark-text-accent"
        >
          Natiq Asgarov
        </Link>{" "}
      </p>
    </motion.div>
  );
};

export default AboutPage;
