import { motion } from "framer-motion";
import { Helmet } from "react-helmet";
import ManageProductsHeader from "../../components/pages/manage-products/manage-products-header/index.js";
import { useSearchParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import { getProducts } from "../../services/shop.js";
import ArticleSkeleton from "../../components/skeleton-loaders/article-skeleton/index.js";
import ProductCard from "../../components/pages/shop-page/product-card/index.js";
import NoResultFound from "../../components/common/no-result-found/index.js";
import ProductSkeletonLoader from "../../components/skeleton-loaders/product-skeleton-loader/index.js";

const ManageProducts = () => {
  const { animationParent } = useAutoAnimate();
  const [isLoading, setIsLoading] = useState(true);
  const [products, setProducts] = useState([]);

  const [searchParams, setSearchParams] = useSearchParams();

  const handleChangeSearch = (search) => {
    setSearchParams((prev) => {
      prev.set("q", search);
      return prev;
    });
  };

  const handleSearch = () => {
    loadProducts();
  };

  const loadProducts = () => {
    getProducts()
      .then((data) => {
        setProducts(data.items);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  useEffect(() => {
    loadProducts();
  }, []);

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={"w-full"}
    >
      <Helmet>
        <meta charSet="utf-8" />
        <title>Manage Products - Clubrick Community</title>
      </Helmet>
      <ManageProductsHeader
        onSearchChange={handleChangeSearch}
        onSearch={handleSearch}
        defaultSearchValue={searchParams.get("q")}
      />

      <div className={"grid grid-cols-3 gap-2 mt-1"} ref={animationParent}>
        {isLoading ? (
          <>
            <ProductSkeletonLoader />
            <ProductSkeletonLoader />
            <ProductSkeletonLoader />
          </>
        ) : products.length ? (
          <>
            {products.map((product) => (
              <ProductCard key={product.id} product={product} isManage={true} />
            ))}
          </>
        ) : (
          <NoResultFound label={"No products found"} />
        )}
      </div>
    </motion.div>
  );
};

export default ManageProducts;
