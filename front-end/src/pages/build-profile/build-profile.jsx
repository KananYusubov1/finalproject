import { motion } from "framer-motion";
import "./build-profile.css";
import { useEffect, useState } from "react";
import WelcomeCard from "../../components/pages/build-profile/welcome-card/index.js";
import CategoryFollowStep from "../../components/pages/build-profile/category-follow-step/index.js";
import BuildProfileStep from "../../components/pages/build-profile/build-profile-step/index.js";
import DoneCard from "../../components/cards/done-card/index.js";
import {
  buildUser,
  useUserProfileBuilt,
  useUserSignedIn,
} from "../../utils/auth.js";
import { useNavigate } from "react-router-dom";
import {
  startLoadingProcess,
  stopLoadingProcess,
  useFollowedCategories,
} from "../../utils/process.js";
import { AuthService } from "../../services";
import { saveUser } from "../../utils/user.js";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import { getAppSettings } from "../../services/user.js";
import { updateSettings } from "../../utils/appSettings.js";
import hexagon from "/Hexagon.svg";

const BuildProfile = () => {
  const [animationParent] = useAutoAnimate();
  const navigate = useNavigate();

  const signedIn = useUserSignedIn();
  const profileBuilt = useUserProfileBuilt();
  const followedCategories = useFollowedCategories();

  const [step, setStep] = useState(1);

  const handleOnStepForward = () => {
    setStep(step + 1);
  };

  const handleOnStepBack = () => {
    setStep(step - 1);
  };

  useEffect(() => {
    if (!signedIn || profileBuilt) {
      navigate("/");
    }
  });

  const handleBuildUser = (formValues) => {
    startLoadingProcess("Profile Building");
    let req = {
      ...formValues,
      categoryFollowedList: followedCategories.map((c) => c.id),
    };

    AuthService.userBuild(req)
      .then((res) => {
        buildUser();
        saveUser(res);
        startLoadingProcess("Reading App Settings");
        getAppSettings()
          .then((res) => {
            updateSettings(res);
          })
          .catch(() => {})
          .finally(() => {
            stopLoadingProcess();
            setStep(step + 1);
          });
      })
      .catch((err) => {
        stopLoadingProcess();
        console.log(err);
      });
  };

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={
        "w-[100dvw] h-[100dvh] build-profile-bg flex items-center justify-center "
      }
      ref={animationParent}
    >
      {step === 1 && <WelcomeCard onStepForward={handleOnStepForward} />}

      {step === 2 && <CategoryFollowStep onStepForward={handleOnStepForward} />}

      {step === 3 && (
        <BuildProfileStep
          onStepBack={handleOnStepBack}
          onStepForward={handleBuildUser}
        />
      )}

      {step === 4 && (
        <div className={"bg-white rounded-lg p-4 min-w-[400px]"}>
          <DoneCard
            title={"All done!"}
            description={"Your profile is built!"}
            redirectUrl={"/"}
          />
        </div>
      )}
    </motion.div>
  );
};

export default BuildProfile;
