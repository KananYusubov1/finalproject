import Yup from "../validation-localization.js";

export const PublishQuestionSchema = Yup.object().shape({
  title: Yup.string().required(),
  categoriesIdList: Yup.array().min(1).required(),
  body: Yup.string().required(),
});
