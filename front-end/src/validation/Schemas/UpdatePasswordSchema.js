import Yup from "../validation-localization.js";
export const UpdatePasswordSchema = Yup.object().shape({
  currentPassword: Yup.string()
    .required()
    .matches(
      /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[!@#\$%^&*()\-_=+{};:'",<.>/?\\\[\]|`~])[A-Za-z\d!@#\$%^&*()\-_=+{};:'",<.>/?\\\[\]|`~]{8,}$/,
      "Please write the password according to the requirements."
    ),
  password: Yup.string()
    .required()
    .matches(
      /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[!@#\$%^&*()\-_=+{};:'",<.>/?\\\[\]|`~])[A-Za-z\d!@#\$%^&*()\-_=+{};:'",<.>/?\\\[\]|`~]{8,}$/,
      "Please write the password according to the requirements."
    ),
  confirmPassword: Yup.string()
    .required()
    .matches(
      /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[!@#\$%^&*()\-_=+{};:'",<.>/?\\\[\]|`~])[A-Za-z\d!@#\$%^&*()\-_=+{};:'",<.>/?\\\[\]|`~]{8,}$/,
      "Please write the password according to the requirements."
    ),
});
