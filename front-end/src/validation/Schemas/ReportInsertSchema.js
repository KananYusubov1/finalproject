import Yup from "../validation-localization.js";

export const ReportInsertSchema = Yup.object().shape({
  reportType: Yup.string().required(),
  url: Yup.string().required(),
});
