import Yup from "../validation-localization.js";

export const ProfileSettingsSchema = Yup.object().shape({
  name: Yup.string().required(),
  username: Yup.string().required(),
  websiteUrl: Yup.string().matches(
    /[(http(s)?):\\//\\//(www\\.)?a-zA-Z0-9@:%._\\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)/,
    "Invalid URL format"
  ),
});
