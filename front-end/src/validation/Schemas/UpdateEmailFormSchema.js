import Yup from "../validation-localization.js";
export const UpdateEmailFormSchema = Yup.object().shape({
  usePrimaryEmail: Yup.boolean(),
  removeDisplayEmail: Yup.boolean(),
  email: Yup.string().test("", "", function (item) {
    return this.parent.usePrimaryEmail || this.parent.removeDisplayEmail
      ? true
      : Yup.string().email().isValid(item);
  }),
});
