import Yup from "../validation-localization.js";

export const PublishArticleSchema = Yup.object().shape({
  title: Yup.string().required(),
  categoriesIdList: Yup.array().min(1).required(),
  flagId: Yup.string().required(),
  body: Yup.string().required(),
});
