import Yup from "../validation-localization.js";

export const ResourceEditorSchema = Yup.object().shape({
  title: Yup.string().required(),
  categoryId: Yup.string().required(),
  resourceFlagId: Yup.string().required(),
});
