import Yup from "../validation-localization.js";

export const PublishResourceSchema = Yup.object().shape({
  title: Yup.string().required(),
  categoryId: Yup.string().required(),
  resourceFlagId: Yup.string().required(),
  url: Yup.string()
    .required()
    .matches(
      /^(https?|ftp):\/\/[^\s/$.?#].[^\s]*$/i,
      "Please write the valid URL ."
    ),
});
