import Yup from "../validation-localization.js";
export const UserBuildProfileSchema = Yup.object().shape({
  userName: Yup.string()
    .required()
    .matches(/^[a-zA-Z0-9]+$/, "Only letters and numbers are allowed"),
  profilePhoto: Yup.string().required(),
});
