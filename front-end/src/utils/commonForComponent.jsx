import { BiLink } from "react-icons/bi";
import { writeClipboard } from "./clipboard.js";
import { successToast } from "./toast.js";
import { PiShareFat } from "react-icons/pi";
import { FiEdit } from "react-icons/fi";
import { MdOutlineDelete } from "react-icons/md";
import { createModal } from "./modal.js";
import { GoReport } from "react-icons/go";
import { startReportAbuseProcess } from "./process.js";
import { share } from "./common.js";

export const generateOptionsForContentViewAction = (
  content,
  type,
  id,
  currentId,
  userId,
  navigate
) => {
  const title = content.header.title;

  const options = [
    {
      label: `Copy Content Link`,
      icon: <BiLink />,
      onClick: () => {
        writeClipboard(window.location).then(() =>
          successToast("Content Link Copied")
        );
      },
      type: "item",
    },
    {
      label: "Share Content",
      icon: <PiShareFat />,
      onClick: () => {
        share(title, window.location);
      },
      type: "item",
    },
  ];

  if (currentId && userId === currentId) {
    options.push(
      {
        type: "separator",
      },
      {
        label: "Edit Content",
        icon: <FiEdit size={22} />,
        onClick: () => {
          navigate(`/edit/${type}/${id}`);
        },
        type: "warning-item",
      },
      {
        label: "Delete Content",
        icon: <MdOutlineDelete />,
        onClick: () => {
          createModal("contentDelete", {
            contentId: id,
            contentType: type,
            onDelete: () => {
              navigate(`/${type === "questions" ? "form" : type}`);
            },
          });
        },
        type: "danger-item",
      }
    );
  } else {
    options.push(
      {
        type: "separator",
      },
      {
        label: "Report Content",
        icon: <GoReport />,
        onClick: () => {
          const pathName = window.location.pathname;

          navigate(`/report-abuse?url=${pathName}`);
        },
        type: "danger-item",
      }
    );
  }

  return options;
};
