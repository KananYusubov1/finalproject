import moment from "moment";
import defaultTagColors from "../defaultTagColors.js";

export const getReportDtoFromUrl = (url) => {
  const path = url.replace(/^(?:\/\/|[^\/]+)*\//, "");

  const patterns = [
    { contentType: "User", pattern: /^\/([^\/]+)/ },
    { contentType: "Article", pattern: /^articles\/([^\/]+)/ },
    { contentType: "Answer", pattern: /^answers\/([^\/]+)/ },
    { contentType: "Comment", pattern: /^comments\/([^\/]+)/ },
    { contentType: "Question", pattern: /^questions\/([^\/]+)/ },
    { contentType: "Resource", pattern: /^explore\/([^\/]+)/ },
    { contentType: "Product", pattern: /^shop\/([^\/]+)/ },
    // { contentType: "User", pattern: /^\/([^\/]+)/ },
  ];

  const matchedPattern = patterns.find(({ pattern }) => pattern.test(url));

  if (matchedPattern) {
    const id = url.match(matchedPattern.pattern)[1];
    return {
      ContentType: matchedPattern.contentType,
      ContentId: id,
    };
  } else {
    return null;
  }
};

export const getBudHistoryTypeTitle = (forWhat) => {
  switch (forWhat) {
    case "ViewArticleToUser":
      return "View Article";
    case "ViewArticleToOwner":
      return "View Article By User";
    case "VisitResourceToOwner":
      return "Visit Resource By User";
    case "PublishArticleToOwner":
      return "Publish Article";
    case "PublishQuestionToOwner":
      return "Publish Question";
    case "PublishResourceToOwner":
      return "Publish Resource";
    case "ProductPurchase":
      return "Product Purchase";
    default:
      return "Bud";
  }
};

export const checkUrlValidation = (url) =>
  /[(http(s)?):\\//\\//(www\\.)?a-zA-Z0-9@:%._\\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)/gi.test(
    url
  );

export const getTagColor = (id) => {
  return id
    ? defaultTagColors.find((tag) => tag.id.toString() === id.toString()) ??
        defaultTagColors[0]
    : defaultTagColors[0];
};

export const getFieldsForContentViewAction = (content, type) => {
  const vote = content.vote;
  const isSaved = content.isSaved;
  const actionCount =
    type === "questions" ? content.answers.length : content.commentCount;
  const watchCount = content.watchCount;

  const userId = content.userPreview.id;
  const saveCount = content.saveCount;
  const isWatched = content.isWatched;

  return {
    vote,
    isSaved,
    actionCount,
    watchCount,
    userId,
    saveCount,
    isWatched,
  };
};

export const generateAllUrl = (baseUrl, params) => {
  let query = baseUrl;

  const { q, sort, category, flag, page } = params;

  if (q && q.length > 0) query += `SearchQuery=${q}`;

  if (category && category.length > 0)
    query += `&CategoryIdList=${category.trim()}`;

  if (sort && sort.length > 0) query += `&Sort=${sort.trim()}`;

  if (flag && flag.length > 0) query += `&FlagId=${flag.trim()}`;

  if (page) query += `&Page=${page}`;

  return query;
};
export const getUtcNowDataFormat = () => {
  const now = new Date();

  const year = now.getFullYear();
  const month = (now.getMonth() + 1).toString().padStart(2, "0");
  const day = now.getDate().toString().padStart(2, "0");
  const hours = now.getHours().toString().padStart(2, "0");
  const minutes = now.getMinutes().toString().padStart(2, "0");
  const seconds = now.getSeconds().toString().padStart(2, "0");
  const milliseconds = now.getMilliseconds().toString().padStart(3, "0");

  return (
    year +
    "-" +
    month +
    "-" +
    day +
    "T" +
    hours +
    ":" +
    minutes +
    ":" +
    seconds +
    "." +
    milliseconds +
    "Z"
  );
};

export const share = (title, url) => {
  if (navigator.share) {
    navigator
      .share({
        // Title that occurs over
        // web share dialog
        title,

        // URL to share
        url,
      })
      .then(() => {
        console.log("Thanks for sharing!");
      })
      .catch((err) => {
        console.log("Error while using Web share API:");
        console.log(err);
      });
  } else {
    console.log("Browser doesn't support this API !");
  }
};

export const divideNumber = (number) => {
  const str = number.toString();
  const parts = [];

  for (let i = str.length - 1; i >= 0; i -= 3) {
    const part = str.substring(Math.max(0, i - 2), i + 1);
    parts.unshift(part);
  }

  return parts.join(",");
};

export const formatDate = (inputDate) => {
  const currentDate = moment();
  const date = moment(inputDate, "YYYY-MM-DD HH:mm:ss.SSS Z");

  const isSameDay = date.isSame(currentDate, "day");
  const isYesterday = date.isSame(
    currentDate.clone().subtract(1, "days"),
    "day"
  );

  if (isSameDay) {
    const hoursDifference = currentDate.diff(date, "hours");
    if (hoursDifference <= 0) {
      return "Today";
    } else if (hoursDifference < 24) {
      return `${hoursDifference} hours ago`;
    }
  } else if (isYesterday) {
    return "Yesterday";
  }

  return date.format("MMM D, YYYY");
};

export const convertToBase64 = async (images) => {
  const imageDataArray = [];

  for (const blobUrl of images) {
    try {
      const response = await fetch(blobUrl);
      const blob = await response.blob();

      const reader = new FileReader();

      reader.onloadend = () => {
        const base64data = reader.result;
        imageDataArray.push(base64data);
      };

      reader.readAsDataURL(blob);
    } catch (error) {
      imageDataArray.push(blobUrl);
      console.log(error);
    }
  }

  console.log(imageDataArray.length);
  return imageDataArray;
};

/**
 * Returns an object containing the values that have changed between two objects.
 *
 * @param {object} values - The updated values object.
 * @param {object} initialValues - The initial values object.
 * @return {object} An object containing the changed values.
 */
export const getChangedValues = (values, initialValues) => {
  const changedValues = {};
  Object.keys(values).forEach((key) => {
    if (!(values[key] === initialValues[key])) {
      changedValues[key] = values[key];
    }
  });
  return changedValues;
};

export const getTagTypeTitle = (tagType) => {
  switch (tagType) {
    case "categories":
      return "Category";
    case "resource-flags":
      return "Resource Flag";
    case "article-flags":
      return "Article Flag";
    default:
      return "Tag";
  }
};

export function arraysEqual(a, b) {
  if (a.length !== b.length) {
    return false;
  }

  // console.log("a", a);
  // console.log("b", b);

  for (let i = 0; i < a.length; i++) {
    if (JSON.stringify(a[i]) !== JSON.stringify(b[i])) {
      return false;
    }
  }
  return true;
}
