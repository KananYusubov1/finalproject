/**
 * Writes the given data to the clipboard.
 *
 * @param {any} data - The data to be written to the clipboard.
 * @return {Promise} A promise that resolves when the data has been written to the clipboard.
 */
export const writeClipboard = (data) => navigator.clipboard.writeText(data);

/**
 * Reads the URL from the clipboard.
 *
 * @return {Promise<string|null>} The URL from the clipboard, or null if it is not a valid URL.
 */
export const readClipboardUrl = () =>
  navigator.clipboard.readText().then((clipboardText) => {
    const urlPattern = /^(https?|ftp):\/\/[^\s/$.?#].[^\s]*$/i;

    if (!urlPattern.test(clipboardText)) {
      return null;
    }

    return clipboardText;
  });
