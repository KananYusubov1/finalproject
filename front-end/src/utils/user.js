import store from "../store";
import { useSelector } from "react-redux";
import { save, remove, update } from "../store/user.js";

export const useUser = () => useSelector((state) => state.user.user);

export const saveUser = (user) => store.dispatch(save(user));

export const updateUser = (user) => store.dispatch(update(user));

export const removeUser = () => store.dispatch(remove());
