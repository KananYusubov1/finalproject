import store from "../store";
import { useSelector } from "react-redux";
import { signOut, toggleTheme, update } from "../store/appSettings.js";

export const useThemeSetting = () =>
  useSelector((state) => state.appSettings.theme);

export const toggleThemeSetting = () => store.dispatch(toggleTheme());

export const signOutSetting = () => store.dispatch(signOut());

export const updateSettings = (settings) => store.dispatch(update(settings));
