import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./components/app/index.js";
import { Provider } from "react-redux";
import store from "../src/store";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-toastify";
import { Analytics } from "@vercel/analytics/react";

ReactDOM.createRoot(document.getElementById("root")).render(
  <Provider store={store}>
    <App />
    <ToastContainer />
    <Analytics />
  </Provider>
);
