﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDomain.Exceptions.UserExceptions
{
    public class UserReviewIsExistException : Exception
    {
        public UserReviewIsExistException() : base("This user's review is already exist") { }
    }
}
