﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDomain.Exceptions.UserExceptions
{
    public class DoesNotBelongException : Exception
    {
        public DoesNotBelongException(string content) : base($"This {content} does not belong to this user") { }
    }
}
