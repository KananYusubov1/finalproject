﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDomain.Exceptions.UserExceptions
{
    public class HaveNotEnoughtBudsException : Exception
    {
        public HaveNotEnoughtBudsException() : base("You don't have enough buds") { }
    }
}
