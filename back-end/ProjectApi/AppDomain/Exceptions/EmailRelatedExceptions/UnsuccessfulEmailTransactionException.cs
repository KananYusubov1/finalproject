﻿namespace AppDomain.Exceptions.OtpExceptions;

public class UnsuccessfulEmailTransactionException : Exception
{
    public UnsuccessfulEmailTransactionException(string toEmail, string what)
        : base($"An error occured while sending {what} to {toEmail}.") { }
}