﻿namespace AppDomain.Exceptions.Common;

public class EntityExistException : Exception
{
    public EntityExistException(string selector)
        : base($"Entity already exist for - {selector}") { }
}
