﻿using System.Text.Json.Serialization;

namespace AppDomain.Enums;

[JsonConverter(typeof(JsonStringEnumConverter))]
public enum ContentType
{
    Article,
    Answer,
    Comment,
    Lesson,
    Question,
    Resource,
    Product,
    User
}