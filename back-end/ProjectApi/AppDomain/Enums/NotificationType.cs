﻿using System.Text.Json.Serialization;

namespace AppDomain.Enums;

[JsonConverter(typeof(JsonStringEnumConverter))]
public enum NotificationType
{
    All,
    Answer,
    CorrectAnswer,
    Article,
    Comment,
    Follow,
    BudsRelated,
    ProductPurchase
}