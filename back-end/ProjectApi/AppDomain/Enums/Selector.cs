﻿using System.Text.Json.Serialization;

namespace AppDomain.Enums;

[JsonConverter(typeof(JsonStringEnumConverter))]
public enum Selector
{
    ById,
    ByEmail,
    ByUsername,
    ByUsersecret,
}