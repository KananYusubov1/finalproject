﻿using System.Text.Json.Serialization;

namespace AppDomain.Enums;

[JsonConverter(typeof(JsonStringEnumConverter))]
public enum ReportStatus
{
    All,
    Active,
    Declined,
    ResultedWithContentFreeze
}