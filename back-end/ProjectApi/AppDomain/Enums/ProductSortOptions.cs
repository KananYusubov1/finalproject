﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace AppDomain.Enums;

[JsonConverter(typeof(JsonStringEnumConverter))]
public enum ProductSortOptions
{
    Ranking,
    LowestPrice,
    HighestPrice,
    Newest,
    BestSellers,
    MostReviewed,

    /// <summary>
    /// Yəni axtarışa ən çox favourite də olanlar gələcək
    /// </summary>
    TopRated
}
