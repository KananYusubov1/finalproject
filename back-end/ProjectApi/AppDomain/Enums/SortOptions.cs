﻿using System.Text.Json.Serialization;

namespace AppDomain.Enums;

/// <summary>
/// Sort Options
/// </summary>
[JsonConverter(typeof(JsonStringEnumConverter))]
public enum SortOptions
{
    /// <summary>
    /// Yəni axtarışa ən uyğun olan content-lər ən üstdə sıralanacaq görünəcək
    /// </summary>
    Relevant,

    /// <summary>
    /// Tarixcə yeni olaraq sıralanacaq
    /// </summary>
    Newest,

    /// <summary>
    /// UpVote ən çox olanlar ilk sıralanacaq - Günlük
    /// </summary>
    TopDay,

    /// <summary>
    /// UpVote ən çox olanlar ilk sıralanacaq - Həftəlik
    /// </summary>
    TopWeek,

    /// <summary>
    /// UpVote ən çox olanlar ilk sıralanacaq - Aylıq
    /// </summary>
    TopMonth,

    /// <summary>
    /// UpVote ən çox olanlar ilk sıralanacaq - İllik
    /// </summary>
    TopYear,

    /// <summary>
    /// UpVote ən çox olanlar ilk sıralanacaq - Bütün tarixlər üzrə
    /// </summary>
    TopInfinity,
}
