﻿namespace AppDomain.Enums;

public enum ModeratorActions
{
    Reported,
    Deleted,
    Updated,
    Finished,
    Taken
}