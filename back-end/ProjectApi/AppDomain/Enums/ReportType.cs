﻿using System.Text.Json.Serialization;

namespace AppDomain.Enums;

[JsonConverter(typeof(JsonStringEnumConverter))]
public enum ReportType
{
    Rude_Or_Vulgar,
    Harassment_Or_Hate,
    Spam_Or_Copyright,
    Inappropriate_Listing,
    Other
}