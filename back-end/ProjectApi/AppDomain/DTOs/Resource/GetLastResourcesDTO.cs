﻿namespace AppDomain.DTOs.Resource;

public class GetLastResourcesDTO
{
    public string Id { get; set; }
    public string Title { get; set; }
    public string Url { get; set; }
}