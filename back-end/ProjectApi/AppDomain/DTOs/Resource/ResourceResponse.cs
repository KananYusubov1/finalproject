﻿using AppDomain.DTO;
using AppDomain.DTOs.Tags;
using AppDomain.Responses.UserResponses;

namespace AppDomain.DTOs.Resource;

public class ResourceResponse
{
    public string Id { get; set; }

    public UserContentTitleResponse User { get; set; }

    public string? Title { get; set; }

    public string? Url { get; set; }

    public TagPreviewResponseDto Category { get; set; }

    public TagPreviewResponseDto Flag { get; set; }

    public VoteDTO Vote { get; set; }

    public int SaveCount { get; set; }

    public int VisitCount { get; set; }

    public bool IsSaved { get; set; }

    public DateTime UpdateTime { get; set; }
}