﻿namespace AppDomain.DTOs.Resource;

public class ResourceDTO
{
    public string? Title { get; set; }
    public string? Url { get; set; }
    public string CategoryId { get; set; }
    public string ResourceFlagId { get; set; }
}