﻿namespace AppDomain.DTOs.Resource;

public class EditResourceDTO
{
    public string ResourceId { get; set; }
    public string? Title { get; set; }
    public string? CategoryId { get; set; }
    public string? ResourceFlagId { get; set; }
}