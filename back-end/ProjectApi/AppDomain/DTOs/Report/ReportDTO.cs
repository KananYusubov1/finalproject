﻿using AppDomain.Enums;

namespace AppDomain.DTOs.Report;

public class ReportDTO
{
    public string Id { get; set; }
    public string ContentId { get; set; }
    public ContentType ContentType { get; set; }
    public ReportType ReportType { get; set; }
    public string ReportMessage { get; set; }
}
