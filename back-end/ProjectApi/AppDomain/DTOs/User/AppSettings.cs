﻿using AppDomain.Enums;

namespace AppDomain.DTOs.User;

public class AppSettings
{
    public UiTheme Theme { get; set; } = UiTheme.System;
}