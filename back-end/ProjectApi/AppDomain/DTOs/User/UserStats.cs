﻿namespace AppDomain.DTOs.User;

public class UserStats
{
    public double VoteRate { get; set; } = 0;

    public int FollowerCount { get; set; } = 0;

    public int ArticleViews { get; set; } = 0;

    public int ResourceVisits { get; set; } = 0;
}
