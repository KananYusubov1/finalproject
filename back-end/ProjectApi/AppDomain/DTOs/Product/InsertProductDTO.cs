﻿using AppDomain.Entities.ContentRelated;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDomain.DTOs.Product
{
    public class InsertProductDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<string> PhotoPaths { get; set; }
        public double Price { get; set; }
        public double? Discount { get; set; }
        public int Stock { get; set; }

    }
}
