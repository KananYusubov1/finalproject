﻿using AppDomain.Entities.ContentRelated;

namespace AppDomain.DTOs.Product;

public class GetAllProductDTO
{
    public string Id { get; set; }
    public string Name { get; set; }
    public List<Photo> Photos { get; set; } = new();
    public double Rank { get; set; }
    public int SellCount { get; set; }
    public double Price { get; set; }
    public double Discount { get; set; }
    public double DiscountedPrice { get; set; }
    public bool IsFavourited { get; set; } = false;

}