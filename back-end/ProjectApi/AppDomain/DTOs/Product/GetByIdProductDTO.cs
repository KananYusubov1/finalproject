﻿using AppDomain.Entities.ContentRelated;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDomain.DTOs.Product
{
    public class GetByIdProductDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<Photo> Photos { get; set; } = new();
        public double Price { get; set; }
        public double Discount { get; set; }
        public double DiscountedPrice { get; set; }
        public int Quantity { get; set; }
        public int SellCount { get; set; }
        public int ReviewsCount { get; set; }
        public double Rank { get; set; }
        public bool IsFavourited { get; set; } = false;
    }
}
