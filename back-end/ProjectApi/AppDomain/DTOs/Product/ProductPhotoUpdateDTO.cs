﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDomain.DTOs.Product
{
    public class ProductPhotoUpdateDTO
    {
        public List<string> Photos { get; set; }
    }
}
