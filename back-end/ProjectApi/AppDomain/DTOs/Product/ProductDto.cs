﻿namespace AppDomain.DTOs.Product;

public class ProductDto
{
    public string Title { get; set; }
    public string Description { get; set; }
    public int BudsValue { get; set; }
    public int Stock { get; set; }
    public string ImageUrl { get; set; }
}