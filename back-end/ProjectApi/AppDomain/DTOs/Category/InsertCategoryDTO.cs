﻿namespace AppDomain.DTOs.Category;

public class InsertCategoryDTO
{
    public string Title { get; set; } = String.Empty;
    public string? Description { get; set; } = String.Empty;
    public string? IconLink { get; set; } = String.Empty;
    public string AccentColor { get; set; } = String.Empty;
}
