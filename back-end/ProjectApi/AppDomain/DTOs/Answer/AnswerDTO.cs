﻿namespace AppDomain.DTO;

public class AnswerDTO
{
    public string Id { get; set; }
    public string UserId { get; set; }
    public string Body { get; set; } // Markdown
    public int VoteCount { get; set; }
    public DateTime UpdateTime { get; set; }
}