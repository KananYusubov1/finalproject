﻿using AppDomain.DTO;
using AppDomain.Responses.UserResponses;

namespace AppDomain.DTOs.Answer;

public class GetByIdAnswerDTO
{
    public string Id { get; set; }
    public string Body { get; set; }
    public UserContentTitleResponse UserContent { get; set; }
    public VoteDTO VoteDTO { get; set; }
    public DateTime UpdateTime { get; set; }
}