﻿namespace AppDomain.DTOs.Admin;

public class BannerDTO
{
    public string Photo { get; set; }
    public string Url { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime ValidUntil { get; set; }
}