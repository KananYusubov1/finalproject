﻿namespace AppDomain.DTOs.Tags;

public class TagFullResponseDto
{
    public string Id { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public string IconLink { get; set; }
    public string AccentColor { get; set; }
    public int UseCount { get; set; }
    public bool IsFollowed { get; set; } = false;
}