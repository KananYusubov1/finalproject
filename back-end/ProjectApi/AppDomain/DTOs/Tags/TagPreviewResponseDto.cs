﻿namespace AppDomain.DTOs.Tags;

public class TagPreviewResponseDto
{
    public string Id { get; set; }
    public string Title { get; set; }
    public string AccentColor { get; set; }
}