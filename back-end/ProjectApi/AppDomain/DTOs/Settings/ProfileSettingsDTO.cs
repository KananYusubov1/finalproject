﻿namespace AppDomain.DTOs.Settings;

public class ProfileSettingsDTO
{
    public string? Name { get; set; }

    public string? Username { get; set; }

    public string? Avatar { get; set; }

    public string? WebsiteUrl { get; set; }

    public string? Location { get; set; }

    public string? Bio { get; set; }

    public string? CurrentlyLearning { get; set; }

    public string? AvailableFor { get; set; }

    public string? CurrentlyWorking { get; set; }

    public string? Work { get; set; }

    public string? Education { get; set; }
}
