﻿using AppDomain.Responses.UserResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDomain.DTOs.Review
{
    public class GetAllReviewDTO
    {
        public string Id { get; set; }
        public UserContentTitleResponse UserContent { get; set; }
        public string Body { get; set; }
        public int Rank { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
