﻿namespace AppDomain.DTOs.ArticleFlag;

public class InsertArticleFlagDTO
{
    public string Title { get; set; } = String.Empty;
    public string? Description { get; set; } = String.Empty;
    public string? IconLink { get; set; } = String.Empty;
    public string AccentColor { get; set; } = String.Empty;
}
