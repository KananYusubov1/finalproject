﻿using AppDomain.Enums;

namespace AppDomain.DTOs.Buds;

public class BudsDTO
{
    public DateTime CreatedTime { get; set; }
    public int BudsAmount { get; set; }
    public BudsActionType ForWhat { get; set; }
    public ContentType BudsEntryType { get; set; }
}