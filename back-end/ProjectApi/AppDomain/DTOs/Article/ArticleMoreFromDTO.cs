﻿using AppDomain.DTOs.Category;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDomain.DTOs.Article
{
    public class ArticleMoreFromDTO
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public MoreFromTagDTO Flag { get; set; }
        public List<MoreFromTagDTO> Categories { get; set; } = new();
    }
}
