﻿using AppDomain.ValueObjects;

namespace AppDomain.DTOs.Article;

public class UpdateArticleDTO
{
    public string Id { get; set; }
    public string? Body { get; set; }
    public Header? Header { get; set; }
    public int ReadingTime { get; set; }
    public string? FlagId { get; set; }
    public List<string>? CategoryIdList { get; set; }
}