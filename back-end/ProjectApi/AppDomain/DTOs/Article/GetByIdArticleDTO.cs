﻿using AppDomain.DTO;
using AppDomain.DTOs.Category;
using AppDomain.DTOs.Comment;
using AppDomain.Responses.UserResponses;
using AppDomain.ValueObjects;

namespace AppDomain.DTOs.Article;

public class GetByIdArticleDTO
{
    public string Id { get; set; }
    public UserPreviewResponse UserPreview { get; set; }
    public Header Header { get; set; }
    public string Body { get; set; }
    public List<TagDTO> Categories { get; set; } = new();
    public TagDTO ArticleFlag { get; set; }
    public DateTime UpdateTime { get; set; }
    public int ReadingTime { get; set; }
    public int SaveCount { get; set; }
    public int CommentCount { get; set; }
    public VoteDTO Vote { get; set; }
    public List<GetArticleCommentDTO> Comments { get; set; }
    public bool IsSaved { get; set; }
    public bool IsViewed { get; set; }
}