﻿namespace AppDomain.DTOs.Article;

public class GetLastArticlesDTO
{
    public string Id { get; set; }
    public string Title { get; set; }
    public List<string> Tags { get; set; }
}
