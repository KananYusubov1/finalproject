﻿using AppDomain.Enums;

namespace AppDomain.DTOs.Notification;

public class NotificationDTO
{
    public NotificationType Type { get; set; }
    public string? AdditionalDetails { get; set; } // Additional Details cover some particular details about users or contents which are different than our general notifications
}