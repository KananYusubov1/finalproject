﻿namespace AppDomain.DTOs.Auth;

public class InsertPendingUserDTO
{
    public string Name { get; set; } = string.Empty;
    public string Email { get; set; } = string.Empty;
    public string Password { get; set; } = string.Empty;
}
