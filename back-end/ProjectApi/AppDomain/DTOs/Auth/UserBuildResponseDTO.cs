﻿namespace AppDomain.DTOs.Auth;

public class UserBuildResponseDTO
{
    public string Id { get; set; }

    public string Username { get; set; } = string.Empty;

    public string Name { get; set; } = string.Empty;

    public string Email { get; set; } = string.Empty;

    public string Avatar { get; set; } = string.Empty;
}
