﻿using AppDomain.Enums;

namespace AppDomain.DTOs.Auth;

public class UserAuthDto
{
    public UserAuthDto() { }

    public UserAuthDto(
        string id,
        string token,
        UserRole role,
        string email,
        bool profileIsBuild,
        string name,
        string username = "",
        string avatar = ""
    )
    {
        Id = id;
        Token = token;
        Role = role;
        Email = email;
        IsProfileBuilt = profileIsBuild;
        Name = name;
        Username = username;
        Avatar = avatar;
    }

    public string Id { get; set; }

    public string Username { get; set; } = string.Empty;

    public string Name { get; set; } = string.Empty;

    public string Token { get; set; } = string.Empty;

    public UserRole Role { get; set; } = UserRole.Sapling;

    public string Email { get; set; } = string.Empty;

    public string Avatar { get; set; } = string.Empty;

    public ConnectedAccountType? ConnectedAccountType { get; set; }

    public bool IsProfileBuilt { get; set; }

    public bool IsTwoFactorActivated { get; set; } = false;
}
