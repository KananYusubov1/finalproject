﻿using AppDomain.Enums;

namespace AppDomain.DTOs.Auth;

public class EnterWithProviderDTO
{
    public string Secret { get; set; } // Currently equal to ProviderId

    public string AccountUsername { get; set; }

    public string Email { get; set; }

    public string AvatarUrl { get; set; }

    public ConnectedAccountType Type { get; set; }
}
