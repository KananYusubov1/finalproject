﻿using AppDomain.DTO;
using AppDomain.DTOs.Category;
using AppDomain.Enums;
using AppDomain.Responses.UserResponses;
using AppDomain.ValueObjects;

namespace AppDomain.DTOs.Question;

public class GetAllQuestionDTO
{
    public string Id { get; set; }
    public UserContentTitleResponse UserContent { get; set; }
    public QuestionStatus Status { get; set; }
    public Header Header { get; set; }
    public List<TagDTO> QuestionCategories { get; set; } = new();
    public DateTime UpdateTime { get; set; }
    public int? AnswerCount { get; set; }
    public int ReadingTime { get; set; }
    public VoteDTO VoteDTO { get; set; }
    public bool IsSaved { get; set; }
    public bool IsViewed { get; set; }
    public bool IsWatched { get; set; }
}