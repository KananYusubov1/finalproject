﻿using AppDomain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDomain.DTOs.Question
{
    public class UpdateQuestionDTO
    {
        public string Id { get; set; }
        public Header? Header { get; set; }
        public string? Body { get; set; }
    }
}
