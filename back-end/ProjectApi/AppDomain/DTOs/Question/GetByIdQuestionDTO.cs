﻿using AppDomain.DTO;
using AppDomain.DTOs.Answer;
using AppDomain.DTOs.Category;
using AppDomain.Responses.UserResponses;
using AppDomain.ValueObjects;

namespace AppDomain.DTOs.Question;

public class GetByIdQuestionDTO
{
    public string Id { get; set; }
    public UserPreviewResponse UserPreview { get; set; }
    public Header Header { get; set; }
    public string Body { get; set; }
    public List<TagDTO> Categories { get; set; }
    public int ReadingTime { get; set; }
    public DateTime UpdateTime { get; set; }
    public int ViewCount { get; set; }
    public VoteDTO Vote { get; set; }
    public int WatchCount { get; set; }
    public int SaveCount { get; set; }
    public int AnswerCount { get; set; }
    public List<GetByIdAnswerDTO> Answers { get; set; }
    public string CorrectAnswerId { get; set; }
    public bool IsWatched { get; set; }
    public bool IsSaved { get; set; }
}