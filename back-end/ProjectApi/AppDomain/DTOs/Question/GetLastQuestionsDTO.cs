﻿namespace AppDomain.DTOs.Question;

public class GetLastQuestionsDTO
{
    public string Id { get; set; }
    public string Title { get; set; }
    public int AnswerCount { get; set; }
}