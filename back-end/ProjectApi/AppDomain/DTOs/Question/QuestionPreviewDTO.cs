﻿using AppDomain.DTO;
using AppDomain.DTOs.Category;
using AppDomain.Entities;
using AppDomain.Entities.TagBaseRelated;
using AppDomain.Enums;
using AppDomain.Responses.UserResponses;
using AppDomain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDomain.DTOs.Question
{
    public class QuestionPreviewDTO
    {
        public string Id { get; set; }
        public UserContentTitleResponse UserContent {  get; set; }
        public QuestionStatus Status { get; set; }
        public string Body { get; set; }
        public VoteDTO Vote { get; set; }
        public int AnswerCount { get; set; }
        public int ReadingTime { get; set; }
        public int WatchCount { get; set; }
        public bool IsWatched { get; set; }
        public bool IsSaved { get; set; }
        public bool IsViewed { get; set; }
        public DateTime UpdateTime { get; set; }
        public Header Header { get; set; }
        public List<TagDTO> Categories { get; set; } = new();
    }
}
