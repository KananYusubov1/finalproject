﻿using AppDomain.DTO;
using AppDomain.Responses.UserResponses;

namespace AppDomain.DTOs.Comment;

public class GetArticleCommentDTO
{
    public string Id { get; set; }
    public string Body { get; set; } // Markdown
    public UserContentTitleResponse UserContent { get; set; }
    public VoteDTO VoteDTO { get; set; }
    public DateTime UpdateTime { get; set; }
}