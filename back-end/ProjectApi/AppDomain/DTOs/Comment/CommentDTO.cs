﻿namespace AppDomain.DTO;

public class CommentDTO
{
    public string Id { get; set; }
    public string Body { get; set; } // Markdown
    public string UserId { get; set; }
    public int VoteCount { get; set; }
    public DateTime UpdateTime { get; set; }
    public IEnumerable<CommentDTO> ChildComments { get; set; }
}