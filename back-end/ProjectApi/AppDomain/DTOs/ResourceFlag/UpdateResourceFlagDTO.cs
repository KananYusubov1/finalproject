﻿namespace AppDomain.DTOs.ResourceFlag;

public class UpdateResourceFlagDTO
{
    public string Id { get; set; }
    public string Title { get; set; }
    public string? Description { get; set; }
    public string? IconLink { get; set; }
    public string AccentColor { get; set; }
}
