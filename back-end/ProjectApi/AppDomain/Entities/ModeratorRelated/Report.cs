﻿using AppDomain.Common.Entities;
using AppDomain.Enums;

namespace AppDomain.Entities.AdminRelated;

public class Report : EntityBase
{
    public string UserId { get; set; }
    public string ContentId { get; set; }
    public ContentType ContentType { get; set; }
    public ReportType ReportType { get; set; }
    public string ReportMessage { get; set; }
    public DateTime CreatedDate { get; set; }
    public DateTime? CaseClosedDate { get; set; } = null;
    public ReportStatus Status { get; set; }
    public string? ModeratorMessage { get; set; } = string.Empty;
    public string? ResponsibleModeratorId { get; set; } = string.Empty;
    public bool isDeleted { get; set; }
}