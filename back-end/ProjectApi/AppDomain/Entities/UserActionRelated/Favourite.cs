﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDomain.Entities.UserActionRelated
{
    public class Favourite
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string ProductId { get; set; }
    }
}
