﻿using AppDomain.Common.Entities;
using AppDomain.Enums;

namespace AppDomain.Entities.UserActionRelated;

public class Vote : UserActionBase
{
    public VoteStatus Status { get; set; }
}
