﻿using AppDomain.Enums;

namespace AppDomain.Entities.NotificationRelated.AdditionalDetails;

public class BudsDetails
{
    public string BudsEntryId { get; set; }
    public ContentType BudsEntryType { get; set; }
    public string BudsEarnerId { get; set; }
    public int BudsAmount { get; set; }
    public BudsActionType ForWhat { get; set; }
}