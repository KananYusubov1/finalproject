﻿namespace AppDomain.Entities.NotificationRelated.AdditionalDetails;

public class ArticleDetails
{
    public string ArticleId { get; set; }
    public string ArticleOwnerId { get; set; }
}