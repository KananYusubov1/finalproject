﻿namespace AppDomain.Entities.NotificationRelated.AdditionalDetails;

public class CommentDetails
{
    public string EntryId { get; set; }
    public string CommentId { get; set; }
}