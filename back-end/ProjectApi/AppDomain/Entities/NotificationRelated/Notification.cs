﻿using AppDomain.Enums;

namespace AppDomain.Entities.NotificationRelated;

public class Notification
{
    public NotificationType Type { get; set; }
    public string? Body { get; set; }
    public object? Content { get; set; }
    public DateTime CreatedAt { get; set; }
    public bool IsViewed { get; set; } = false;
}