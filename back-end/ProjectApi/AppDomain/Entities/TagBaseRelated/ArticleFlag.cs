﻿namespace AppDomain.Entities.TagBaseRelated;

public class ArticleFlag : TagBase
{
    public ArticleFlag() { }

    public ArticleFlag(TagBase tag)
    {
        Id = tag.Id;
        Title = tag.Title;
        Description = tag.Description;
        UseCount = tag.UseCount;
        IconLink = tag.IconLink;
        AccentColor = tag.AccentColor;
        IsDeleted = tag.IsDeleted;
    }
}