﻿using AppDomain.Entities.ContentRelated;

namespace AppDomain.Entities.TagBaseRelated;

public class Category : TagBase
{
    public List<Article> Articles { get; set; }
    public List<Question> Questions { get; set; }
    public List<Lesson> Lessons { get; set; }

    public Category() { }

    public Category(TagBase tag)
    {
        Id = tag.Id;
        Title = tag.Title;
        Description = tag.Description;
        UseCount = tag.UseCount;
        IconLink = tag.IconLink;
        AccentColor = tag.AccentColor;
        IsDeleted = tag.IsDeleted;
    }
}