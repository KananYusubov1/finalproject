﻿using AppDomain.Common.Entities;

namespace AppDomain.Entities.AdminRelated;

public class Banner : EntityBase
{
    public string Photo { get; set; }
    public string Url { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime ValidUntil { get; set; }
}