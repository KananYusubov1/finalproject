﻿using AppDomain.Common.Entities;

namespace AppDomain.Entities.ProductRelated;

public class Product : EntityBase
{
    public string Title { get; set; }
    public string Description { get; set; }
    public int BudsValue { get; set; }
    public int Stock { get; set; }
    public string ImageUrl { get; set; }
    public DateTimeOffset CreatedDate { get; set; }
    public DateTimeOffset? DeletedDate { get; set; } = null;
}