﻿namespace AppDomain.Entities.ProductRelated;

public class Order
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public string UserId { get; set; }
    public string ProductId { get; set; }
    public int Quantity { get; set; }
    public DateTimeOffset OrderDate { get; set; }
    public DateTimeOffset? ReturnDate { get; set; } = null;
    public bool IsDone { get; set; } = false;
}