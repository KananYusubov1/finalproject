﻿namespace AppDomain.Entities.UserRelated;

public class AuthToken
{
    public string Token { get; set; }
    public DateTime ExpirationDate { get; set; }
}
