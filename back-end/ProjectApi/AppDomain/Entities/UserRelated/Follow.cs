﻿using AppDomain.Common.Entities;

namespace AppDomain.Entities.UserRelated;

public class Follow : EntityBase
{
    public string FollowerId { get; set; } // Follow edən
    public string FollowingId { get; set; } // Follow edilən
    public bool IsDeleted { get; set; }
}