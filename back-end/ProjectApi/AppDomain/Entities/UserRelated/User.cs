﻿using System.ComponentModel.DataAnnotations.Schema;
using AppDomain.Entities.NotificationRelated;
using AppDomain.ValueObjects;
using AppDomain.Entities.UserActionRelated;
using AppDomain.DTOs.Auth;
using System.ComponentModel;

namespace AppDomain.Entities.UserRelated;

[Serializable]
public class User : PendingUser
{
    // Name is in PendingUser
    // Email is in PendingUser
    // Password is in PendingUser
    // UserSecret is in PendingUser
    // JoinedTime is in PendingUser
    public User() { }

    public User(PendingUser pendingUser)
    {
        Id = pendingUser.Id;
        Name = pendingUser.Name;
        Email = pendingUser.Email;
        Password = pendingUser.Password;
        UserSecret = pendingUser.UserSecret;
        JoinedTime = pendingUser.JoinedTime;
        RefreshToken = pendingUser.RefreshToken;
        ConnectedAccountList = pendingUser.ConnectedAccountList;
    }

    public void BuildUser(BuildUserDTO buildUser)
    {
        Username = buildUser.Username;
        PersonalInfo = new PersonalInfo();
        PersonalInfo.Bio = buildUser.Bio;
        PersonalInfo.WebsiteUrl = buildUser.WebsiteURL;
        PersonalInfo.Work = buildUser.Work;
        PersonalInfo.Education = buildUser.Education;
        CategoryFollowedList = buildUser.CategoryFollowedList;
    }

    [Column(TypeName = "jsonb")]
    public PersonalInfo? PersonalInfo { get; set; } = new();

    [Column(TypeName = "jsonb")]
    public List<string>? CategoryFollowedList { get; set; } = new();

    [Column(TypeName = "jsonb")]
    public Brand Brand { get; set; } = new();

    [Column(TypeName = "jsonb")]
    public Settings Settings { get; set; } = new();

    [Column(TypeName = "jsonb")]
    public List<Badge>? BadgeList { get; set; } = new();

    [Column(TypeName = "jsonb")]
    public List<Notification>? NotificationList { get; set; } = new();

    public List<Vote> VoteList { get; set; } = new();

    public List<View> ViewList { get; set; } = new();

    public List<Save> SaveList { get; set; } = new();

    public List<Watch> WatchList { get; set; } = new();

    [DefaultValue(false)]
    public bool IsFrozen { get; set; } = false;

    public DateTime? FrozenUntil { get; set; } = null;

    public int AllTimeBudsPoints { get; set; } = 0;
    public int UsableBudsPoints { get; set; } = 0;
}