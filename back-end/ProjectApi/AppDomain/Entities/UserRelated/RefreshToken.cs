﻿using AppDomain.Common.Entities;

namespace AppDomain.Entities.UserRelated;

public class RefreshToken
{
    public string Token { get; set; }
    public DateTime ExpirationDate { get; set; }
}