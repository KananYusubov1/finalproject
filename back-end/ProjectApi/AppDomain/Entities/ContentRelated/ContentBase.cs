﻿using AppDomain.Common.Entities;

namespace AppDomain.Entities.ContentRelated;

public class ContentBase : EntityBase
{
    public string UserId { get; set; } = String.Empty;
    public DateTime UpdateTime { get; set; }
    public int VoteCount { get; set; }
    public bool IsDeleted { get; set; } = false;
}