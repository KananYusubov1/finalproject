﻿using AppDomain.Common.Entities;
using AppDomain.Entities.UserActionRelated;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDomain.Entities.ContentRelated
{
    public class Product : EntityBase
    {
        public string Name { get; set; } = String.Empty;
        public string Description { get; set; } = String.Empty;

        [Column(TypeName = "jsonb")]
        public List<Photo> Photos { get; set; } = new();
        public List<Review>? Reviews { get; set; }
        public double Rank { get; set; }
        public double Price { get; set; }
        public double Discount { get; set; }
        public int Quantity { get; set; }
        public int Stock { get; set; }
        public int SellCount { get; set; }
        public List<Favourite> Favourites { get; set; } = new();
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public bool IsDeleted { get; set; }
    }
}
