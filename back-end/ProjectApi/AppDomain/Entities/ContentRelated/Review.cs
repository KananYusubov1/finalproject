﻿using AppDomain.Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDomain.Entities.ContentRelated;

public class Review : EntityBase
{
    public string UserId { get; set; }
    public string ProductId { get; set; }
    public string Body { get; set; }
    public int Rank { get; set; }
    public DateTime CreatedAt { get; set; }  
    public DateTime UpdatedAt { get; set; } 

}
