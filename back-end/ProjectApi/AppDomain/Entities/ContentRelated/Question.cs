﻿namespace AppDomain.Entities.ContentRelated;

public class Question : Post
{
    public int WatchCount { get; set; }
    public string? CorrectAnswerId { get; set; }
    public List<Answer> Answers { get; set; }
}