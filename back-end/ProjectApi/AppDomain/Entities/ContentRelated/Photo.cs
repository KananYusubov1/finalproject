﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDomain.Entities.ContentRelated
{
    public class Photo
    {
        public string Id { get; set; } = String.Empty;
        public string Path { get; set; } = String.Empty;
    }
}
