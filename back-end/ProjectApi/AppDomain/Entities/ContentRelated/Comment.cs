﻿using AppDomain.DTO;

namespace AppDomain.Entities.ContentRelated;

public class Comment : Entry
{
    public string? ParentCommentId { get; set; }
    public string ArticleId { get; set; }
    public Comment? ParentComment { get; set; }
    public IEnumerable<CommentDTO>? ChildComments { get; set;}
}