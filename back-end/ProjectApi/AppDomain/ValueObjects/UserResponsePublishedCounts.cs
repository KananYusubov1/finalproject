﻿namespace AppDomain.ValueObjects;

public class UserViewResponsePublishedCounts
{
    public int PublishedAnswerCount { get; set; }
    public int PublishedArticlesCount { get; set; }
    public int PublishedQuestionCount { get; set; }
}