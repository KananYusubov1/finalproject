﻿namespace AppDomain.ValueObjects;

public class Brand
{
    public string AccentColor { get; set; } = "#222222";
    public string? BannerPhoto { get; set; } = string.Empty;
}
