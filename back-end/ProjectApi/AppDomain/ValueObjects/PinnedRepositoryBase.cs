﻿namespace AppDomain.ValueObjects;

public class PinnedRepository
{
    public string RepoId { get; set; } // id
    public string Name { get; set; } // name
    public bool Fork { get; set; } // fork
    public string? Description { get; set; } // description
    public int Stars { get; set; } // stargazers_count
    public string Url { get; set; } // html_url
    public string TopProgrammingLanguage { get; set; } // langauge
}
