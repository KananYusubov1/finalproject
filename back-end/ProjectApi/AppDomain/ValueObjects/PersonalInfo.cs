﻿using System.ComponentModel.DataAnnotations.Schema;

namespace AppDomain.ValueObjects;

public class PersonalInfo
{
    public string? Bio { get; set; } = string.Empty;
    public string? Work { get; set; } = string.Empty;
    public string? Education { get; set; } = string.Empty;
    public string? Location { get; set; } = string.Empty;
    public string? DisplayEmail { get; set; } = string.Empty;
    public string? WebsiteUrl { get; set; } = string.Empty;

    [Column(TypeName = "jsonb")]
    public List<PinnedRepository>? PinnedRepositories { get; set; } = new();

    [Column(TypeName = "jsonb")]
    public List<string>? PinnedArticles { get; set; } = new();
    public string? CurrentlyWorking { get; set; } = string.Empty;
    public string? AvailableFor { get; set; } = string.Empty;
    public string? CurrentlyLearning { get; set; } = string.Empty;
}