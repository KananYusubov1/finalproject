﻿using AppDomain.Enums;

namespace AppDomain.ValueObjects;

public class ContentView
{
    public string ContentId { get; set; }
    public string UserId { get; set; }
    public string? Title { get; set; }
    public ContentType ContentType { get; set; }
    public DateTime UpdateTime { get; set; }
}