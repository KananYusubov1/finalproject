﻿using AppDomain.DTOs.Review;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDomain.Interfaces
{
    public interface IReviewRepository
    {
        Task<string> InsertReview(string productId, string body, int rank);
        Task<List<GetAllReviewDTO>> GetAllReviews(string productId);
        Task UpdateReview(string reviewId, string? body, int? rank);
        Task DeleteReview(string reviewId);
    }
}
