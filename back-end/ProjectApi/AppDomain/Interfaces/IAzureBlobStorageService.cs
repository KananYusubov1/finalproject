﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDomain.Interfaces;

public interface IAzureBlobStorageService
{
    /// <summary>
    /// Upload User Avatar to Azure Cloud
    /// </summary>
    /// <param name="imageData">Image data Base64</param>
    /// <param name="userId">User Id</param>
    /// <returns>Url of uploaded image</returns>
    public Task<string> UploadAvatarAsync(string imageData, string userId);

    /// <summary>
    ///
    /// </summary>
    /// <param name="imageData"></param>
    /// <param name="userId"></param>
    /// <returns></returns>
    public Task<string> UpdateAvatarAsync(string imageData, string blobUrl, string userId);

    /// <summary>
    /// Upload Tag's Icon to Azure Cloud
    /// </summary>
    /// <param name="imageData">Image data Base64</param>
    /// <param name="userId">User Id</param>
    /// <returns>Url of uploaded image</returns>
    public Task<string> UploadTagIconAsync(string iconData, string tagId);

    /// <summary>
    ///
    /// </summary>
    /// <param name="iconData"></param>
    /// <param name="tagId"></param>
    /// <returns></returns>
    public Task<string> UpdateTagIconAsync(string iconData, string tagUrl, string tagId);

    /// <summary>
    ///
    /// </summary>
    /// <param name="tagUrl"></param>
    /// <returns></returns>
    public Task DeleteTagIconAsync(string tagUrl);

    public Task<string> UploadCoverImage(string imageData, string articleId);
    public Task<string> UpdateCoverImage(string imageData, string blobUrl, string articleId);

    public Task<List<string>> UploadProductPhotos(List<string> imagesData, string productId);

    public Task<string> UploadProductPhoto(string imageData, string productId);

    public Task DeleteProductPhoto(string imageUrl);
}
