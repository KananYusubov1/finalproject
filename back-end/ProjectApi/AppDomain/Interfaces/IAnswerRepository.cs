﻿using AppDomain.DTOs.Answer;
using AppDomain.Entities.ContentRelated;
using AppDomain.Enums;

namespace AppDomain.Interfaces;

public interface IAnswerRepository
{
    Task<List<Answer>> GetAnswerById(string id);
    Task<string> InsertAnswer(Answer answer);
    Task<string> UpdateAnswer(string answerId,string body);
    Task<int> DeleteAnswer(string answerId);
    Task<Task> DeleteAnswers(string questionId);
    Task<int> VoteAnswer(VoteStatus status, string answerId, string tokenId);
    Task<GetByIdAnswerDTO> GetById(string answerId, string tokenId);
    
}