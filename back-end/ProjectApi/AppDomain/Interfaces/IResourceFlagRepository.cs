﻿using AppDomain.Entities.TagBaseRelated;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDomain.Interfaces
{
    public interface IResourceFlagRepository
    {
        Task<List<ResourceFlag>> GetAllResourceFlag(string keyword = "");
        Task<ResourceFlag> GetResourceFlagById(string id);
        Task<string> InsertResourceFlag(ResourceFlag resourceFlag);
        Task<string> UpdateResourceFlag(ResourceFlag resourceFlag);
        Task<string> DeleteResourceFlag(string id);
        Task<int> IncrementUseCounts(List<string> resourceFlagIdList);
        Task<int> DecrementUseCounts(List<string> resourceFlagIdList);
    }
}
