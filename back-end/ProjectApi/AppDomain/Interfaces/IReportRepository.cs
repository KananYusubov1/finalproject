﻿using AppDomain.DTOs.Report;
using AppDomain.Entities.AdminRelated;
using AppDomain.Enums;

namespace AppDomain.Interfaces;

public interface IReportRepository
{
    Task<Task> ReportContent(ReportDTO reportDTO);
    Task<IEnumerable<Report>> GetAllReports();
    Task<IEnumerable<Report>> GetAllReportsByStatus(ReportStatus status);
    Task<IEnumerable<Report>> GetAllReportsByModeratorId(string moderatorId, ReportStatus reportStatus = ReportStatus.All);
    Task<Report> GetReportById(string reportId);
    //IEnumerable<Report> ReadReportsByContentType(ContentType contentType);
    //Task<Report> UpdateReportStatusAsync(ReportStatus reportStatus, string reportId, string moderatorId);
    Task<Report> SubmitReportAsync(string reportId, ReportStatus reportStatus, string moderatorMessage);
    Task<Task> DeleteReportAsync(string reportId);
}