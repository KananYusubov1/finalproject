﻿using AppDomain.Entities.ContentRelated;
using AppDomain.DTOs.Pagination;
using AppDomain.DTOs.Article;
using AppDomain.Enums;

namespace AppDomain.Interfaces;

public interface IArticleRepository
{
    Task<List<GetAllArticleDTO>> GetArticlesForFeedAsync();
    Task<int> GetAllArticlesCount();
    Task<PaginatedListDto<GetAllArticleDTO>> GetAllArticle(
        string? searchQuery,
        List<string>? categoryIdList,
        string flagId,
        SortOptions sort,
        int page = 1
    );

    Task<PaginatedListDto<GetAllArticleDTO>> GetAllSavedArticle(
        string? searchQuery,
        List<string>? categoryIdList,
        string flagId,
        SortOptions sort,
        int page = 1
    );

    Task<IEnumerable<GetLastArticlesDTO>> GetLastArticlesAsync();
    Task<string> InsertArticle(Article article, List<string> categoryIdList, string flagId);
    Task<int> UpdateArticle(UpdateArticleDTO updateArticleDTO, string userId);
    Task<int> DeleteArticle(string articleId, string userId);
    Task<GetByIdArticleDTO> GetByIdArticle(string articleId, string userId);
    Task<int> SaveArticle(string articleId, string tokenId);
    Task<int> VoteArticle(VoteStatus status, string articleId, string tokenId);

    Task<PaginatedListDto<GetAllArticleDTO>> GetAllSavedArticleForUser(
        string? searchQuery,
        List<string>? categoryIdList,
        string flagId,
        SortOptions sort,
        int page = 1);

    Task<PaginatedListDto<GetAllArticleDTO>> GetAllUserArticle(
        string tokenId,
        string? searchQuery,
        List<string>? categoryIdList,
        string flagId,
        SortOptions sort,
        int page = 1);

    Task<List<GetAllArticleDTO>> GetFlow();
    Task<List<ArticleMoreFromDTO>> GetArticleMoreFrom(string tokenId);
}
