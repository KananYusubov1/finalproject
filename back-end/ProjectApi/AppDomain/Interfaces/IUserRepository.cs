﻿using AppDomain.Exceptions.Common;
using AppDomain.Exceptions.UserExceptions;
using AppDomain.Entities.UserRelated;
using AppDomain.Responses;
using AppDomain.ValueObjects;
using AppDomain.Enums;
using AppDomain.DTOs.Settings;
using AppDomain.Entities.ContentRelated;
using AppDomain.DTOs.Article;
using AppDomain.DTOs.User;
using AppDomain.Responses.UserResponses;

namespace AppDomain.Interfaces;

public interface IUserRepository
{
    /// <summary>
    /// Get current user's app settings
    /// </summary>
    /// <returns></returns>
    Task<AppSettings> GetAppSettingsAsync();

    Task<int> GetAllTimeBudsAsync();
    Task<int> GetUsableBudsAsync();

    /// <summary>
    /// Retrieves the leaderboard data asynchronously.
    /// </summary>
    /// <returns>An IEnumerable of LeaderboardResponse containing leaderboard data.</returns>
    IEnumerable<LeaderboardResponse> GetLeaderboard();

    /// <summary>
    /// Adds buds to the specified content asynchronously.
    /// </summary>
    /// <param name="contentType">The type of content to add buds to.</param>
    /// <param name="contentId">The unique identifier of the content to add buds to.</param>
    /// <param name="buds">The number of buds to add.</param>
    /// <param name="userId">Optional. The user identifier if provided; otherwise, it's assumed to be the current user.</param>
    /// <returns>A task representing the asynchronous operation.</returns>
    Task AddBudsAsync(
        ContentType contentType,
        string contentId,
        BudsActionType budsAction,
        string userId = null
    );

    /// <summary>
    /// Checks if the user has set a password.
    /// </summary>
    /// <returns>A task that completes with a boolean value indicating if the user has set a password.</returns>
    Task<bool> PasswordIsSet();

    /// <summary>
    /// Checks if the user is followed by another user.
    /// </summary>
    /// <param name="userId">The unique identifier of the user to check if they are followed.</param>
    /// <returns>A task that completes with a boolean value indicating if the user is followed.</returns>
    Task<bool> UserIsFollowed(string userId);

    /// <summary>
    /// Retrieves a user based on a unique selector and identifier asynchronously.
    /// </summary>
    /// <param name="selector">The selector used to uniquely identify the user.</param>
    /// <param name="identifier">The identifier associated with the user.</param>
    /// <returns>
    /// A task representing the asynchronous operation to retrieve the user.
    /// If successful, it returns the user; otherwise, it returns null.
    /// </returns>
    /// <remarks>
    /// This method retrieves a user from the data source based on a unique selector and identifier.
    /// It performs this operation asynchronously.
    /// If the retrieval is successful, it returns the user as a result of the task.
    /// If no user is found, it returns null.
    /// </remarks>
    Task<User> GetUserBySelectorAsync(Selector selector, string identifier);

    /// <summary>
    /// Retrieves a user by their ID asynchronously.
    /// </summary>
    Task<User> GetUserByIdAsync(string? id);

    /// <summary>
    /// Retrieves a user by their email asynchronously.
    /// </summary>
    Task<User> GetUserByEmailAsync(string? email);

    /// <summary>
    /// Retrieves a user by their username asynchronously.
    /// </summary>
    Task<User> GetUserByUsernameAsync(string? userName);

    /// <summary>
    /// Retrieves a user by their user secret asynchronously.
    /// </summary>
    Task<User> GetUserByUserSecretAsync(string? userSecret);

    /// <summary>
    /// Retrieves the number of content items to display per page asynchronously.
    /// </summary>
    /// <returns>
    /// A task representing the asynchronous operation to retrieve the content items per page.
    /// If successful, it returns the number of content items per page as an integer.
    /// </returns>
    /// <remarks>
    /// This method retrieves the number of content items to display per page from the application settings.
    /// It performs this operation asynchronously.
    /// If the retrieval is successful, it returns the content items per page as an integer result of the task.
    /// </remarks>
    Task<int> GetContentPerPage();

    /// <summary>
    /// Retrieves the user's profile asynchronously.
    /// </summary>
    /// <returns>A task representing the asynchronous operation. The user's profile information.</returns>
    Task<UserProfileResponse> GetUserProfileAsync();

    /// <summary>
    /// Retrieves customization information asynchronously.
    /// </summary>
    /// <returns>A task representing the asynchronous operation. The customization information.</returns>
    Task<CustomizationSettings> GetCustomizationAsync();

    /// <summary>
    /// Retrieves personal information asynchronously.
    /// </summary>
    /// <returns>A task representing the asynchronous operation. Personal information response.</returns>
    Task<PersonalInfoResponse> GetPersonalInfoAsync();

    /// <summary>
    /// Get user's profile settings
    /// </summary>
    /// <returns></returns>
    Task<ProfileSettingsDTO> GetProfileSettingsAsync();

    /// <summary>
    /// Retrieves the published counts associated with a user asynchronously.
    /// </summary>
    /// <param name="userId">The user's ID.</param>
    /// <returns>A task representing the asynchronous operation. The published counts for the user.</returns>
    Task<UserViewResponsePublishedCounts> GetUserViewResponsePublishedCountsAsync(string userId);

    /// <summary>
    /// Retrieves a collection of pinned repositories for a user asynchronously.
    /// </summary>
    /// <param name="userId">The unique identifier of the user.</param>
    /// <returns> A task that represents the asynchronous operation. The task result is an enumerable collection of
    /// <see cref="PinnedRepository"/> objects.
    /// </returns>
    Task<IEnumerable<PinnedRepository>> GetPinnedRepositoriesAsync(string userId);

    /// <summary>
    /// Retrieves a collection of all repositories associated with a user asynchronously.
    /// </summary>
    /// <returns> A task that represents the asynchronous operation. The task result is an enumerable collection of
    /// <see cref="RepositoryResponse"/> objects.
    /// </returns>
    Task<IEnumerable<RepositoryResponse>> GetAllRepositoriesAsync();

    /// <summary>
    /// Updates personal information asynchronously.
    /// </summary>
    /// <param name="response">The updated personal information response.</param>
    /// <returns>A task representing the asynchronous operation. The updated personal information response.</returns>
    Task<ProfileSettingsDTO> UpdateProfileSettingsAsync(ProfileSettingsDTO response);

    /// <summary>
    /// Updates the password of user asynchronously.
    /// <exception cref="EntityNotFoundException"/>
    /// <exception cref="UserInvalidPasswordException"/>
    Task<Task> UpdatePasswordAsync(string currentPassword, string newPassword);

    /// <summary>
    /// Update user's primary email
    /// </summary>
    /// <param name="email"></param>
    /// <returns></returns>
    Task<Task> UpdatePrimaryEmail(string email);

    /// <summary>
    /// Updates customization information asynchronously.
    /// </summary>
    /// <param name="customization">The updated customization information.</param>
    /// <returns>A task representing the asynchronous operation. The updated customization information.</returns>
    Task<CustomizationSettings> UpdateCustomizationAsync(CustomizationSettings customization);

    /// <summary>
    /// Asynchronously updates the list of pinned repositories for a user.
    /// </summary>
    /// <param name="repositoryIds">An array of repository IDs to pin.</param>
    /// <returns>
    /// A task that represents the asynchronous operation. The task result is an enumerable collection
    /// of <see cref="PinnedRepository"/> objects representing the updated list of pinned repositories.
    /// </returns>
    Task<Task> UpdatePinnedRepositoriesAsync(string repositoryId);

    /// <summary>
    /// Updates pinned articles asynchronously for a specified user.
    /// </summary>
    /// <param name="articleId">The ID of the article to be updated as pinned.</param>
    /// <returns>A task representing the asynchronous operation.</returns>
    Task<Task> UpdatePinnedArticlesAsync(string articleId);

    /// <summary>
    /// Retrieves a list of pinned articles for a user.
    /// </summary>
    /// <param name="userId">The ID of the user for whom to retrieve pinned articles.</param>
    /// <returns>A collection of DTOs representing pinned articles for the specified user.</returns>
    Task<IEnumerable<GetAllArticleDTO>> GetPinnedArticlesAsync(string userId);

    /// <summary>
    /// Retrieves an article by its ID.
    /// </summary>
    /// <param name="articleId">The ID of the article to retrieve.</param>
    /// <returns>The article with the specified ID, or null if not found.</returns>
    Task<Article> GetArticle(string articleId);

    /// <summary>
    /// Updates the user's display email address asynchronously.
    /// </summary>
    /// <param name="updateCommand">The command containing the new email address.</param>
    /// <returns>
    /// An action result representing the result of the update operation. If successful, it returns
    /// the updated display email address; otherwise, it returns a problem details response.
    /// </returns>
    Task<Task> UpdateDisplayEmailAsync(string email, bool usePrimaryEmail, bool removeDisplayEmail);

    /// <summary>
    /// </summary>
    /// Changes the role of a user asynchronously.
    /// </summary>
    /// <param name="role">The new user role to assign.</param>
    /// <param name="userId">The ID of the user whose role is to be changed.</param>
    /// <returns>
    /// An asynchronous task representing the HTTP action result.
    /// If successful, it returns an OK result. If there is an error, it returns a problem result with an error message.
    /// </returns>
    /// <remarks>
    /// This method allows a moderator to change the role of a user by sending a command.
    /// It performs this operation asynchronously and returns an HTTP action result.
    /// If the operation is successful, it returns an OK result. If there is an error, it returns a problem result with an error message.
    /// </remarks>
    Task<Task> ChangeRoleAsync(UserRole role, string userId);

    /// <summary>
    /// Follows a user asynchronously based on the provided user ID.
    /// </summary>
    /// <param name="followId">The user ID of the user to follow.</param>
    /// <returns>
    /// A task that represents the asynchronous operation. The task indicates the success or failure of the follow operation.
    /// </returns>
    /// <remarks>
    /// This method allows the currently authenticated user to follow another user identified by their user ID.
    /// It asynchronously performs the follow operation and returns a task that represents the outcome.
    /// </remarks>
    Task<Task> FollowUserAsync(string followId);

    /// <summary>
    /// Retrieves a list of user profiles representing the users being followed by the currently authenticated user asynchronously.
    /// </summary>
    /// <returns>
    /// A task that represents the asynchronous operation. The task result is an enumerable collection of user profiles
    /// representing the users being followed by the currently authenticated user.
    /// </returns>
    /// <remarks>
    /// This asynchronous method fetches a list of user profiles for users being followed by the currently authenticated user.
    /// It first retrieves the IDs of the followed users and then asynchronously retrieves their user profiles.
    /// The resulting collection contains user profiles for each followed user.
    /// </remarks>
    Task<IEnumerable<UserProfileResponse>> GetFollowingsAsync();

    /// <summary>
    /// Retrieves a list of user profiles representing the users who are following the currently authenticated user asynchronously.
    /// </summary>
    /// <returns>
    /// A task that represents the asynchronous operation. The task result is an enumerable collection of user profiles
    /// representing the users who are following the currently authenticated user.
    /// </returns>
    /// <remarks>
    /// This asynchronous method fetches a list of user profiles for users who are following the currently authenticated user.
    /// It retrieves the IDs of followers and then asynchronously retrieves their user profiles.
    /// The resulting collection contains user profiles for each follower.
    /// </remarks>
    Task<IEnumerable<UserProfileResponse>> GetFollowersAsync();

    /// <summary>
    /// Retrieves a collection of connected email addresses asynchronously.
    /// </summary>
    /// <returns>
    /// A task representing the asynchronous operation to fetch a collection of connected email addresses.
    /// </returns>
    /// <remarks>
    /// This method allows fetching a collection of email addresses that are connected to the user's account.
    /// It performs this operation asynchronously and returns a task that represents the outcome.
    /// </remarks>
    Task<IEnumerable<EmailResponse>> GetConnectedEmailsAsync();

    /// <summary>
    /// Reset the password of user asynchronously.
    /// </summary>
    /// <exception cref="EntityNotFoundException"/>
    Task<Task> ResetPasswordAsync(string email, string newPassword);

    /// <summary>
    /// Deletes a user asynchronously and returns a task.
    /// </summary>
    Task<User> DeleteUserAsync(string id);

    /// <summary>
    /// Delete user actin relations
    /// </summary>
    /// <param name="contentId"></param>
    /// <param name="contentType"></param>
    /// <returns></returns>
    Task<Task> DeleteUserActionRelationsAsync(string contentId, ContentType contentType);

    /// <summary>
    /// Deletes all relations associated with the current user asynchronously.
    /// </summary>
    /// <returns>A task representing the asynchronous operation.</returns>
    Task DeleteAllUserRelationsAsync();

    /// <summary>
    /// Restores user relations for the specified user asynchronously.
    /// </summary>
    /// <param name="userId">The ID of the user for whom relations should be restored.</param>
    /// <returns>A task representing the asynchronous operation.</returns>
    Task<Task> RestoreUserRelationsAsync(string userId);

    /// <summary>
    /// Checks if a username exists asynchronously.
    /// </summary>
    Task<bool> IsUsernameExistAsync(string username);

    /// <summary>
    /// Checks if an email exists asynchronously.
    /// </summary>
    Task<bool> IsEmailExistAsync(string email);

    /// <summary>
    /// Checks if the current user is following a user with the specified email.
    /// </summary>
    /// <param name="targetEmail">The email of the user to check for followers.</param>
    /// <returns>
    /// A task representing the asynchronous operation.
    /// If the current user is following the target user, it returns true; otherwise, it returns false.
    /// </returns>
    /// <remarks>
    /// This method checks if the current user is following another user based on the provided target email.
    /// It performs this check asynchronously and returns true if the current user is following the target user;
    /// otherwise, it returns false.
    /// </remarks>
    Task<bool> IsCurrentUserFollowAsync(string targetEmail);

    /// <summary>
    /// Checks if a connected account type exists in the database.
    /// </summary>
    /// <param name="newType">The connected account type to check for existence.</param>
    /// <returns>
    /// A task representing the asynchronous operation.
    /// If the connected account type exists, it returns true; otherwise, it returns false.
    /// </returns>
    /// <remarks>
    /// This method checks if a connected account type, represented by <paramref name="newType"/>,
    /// already exists in the database. It performs this check asynchronously and returns true
    /// if the connected account type exists; otherwise, it returns false.
    /// </remarks>
    Task<bool> IsConnectedAccountTypeExistAsync(ConnectedAccountType newType);

    /// <summary>
    /// Retrieves a collection of user profiles based on a search query.
    /// </summary>
    /// <param name="search">The search query to filter user profiles.</param>
    /// <returns>
    /// A task representing the asynchronous operation.
    /// The task result contains an enumerable collection of user profile responses.
    /// </returns>
    /// <remarks>
    /// This method searches for user profiles based on the provided search query and returns them as profile responses.
    /// The search query is used to filter and find user profiles matching specific criteria.
    /// </remarks>
    Task<IEnumerable<UserProfileResponse>> GetAllUsersAsync(string search);

    /// <summary>
    /// Adds a connected account asynchronously.
    /// </summary>
    /// <param name="type">The type of the connected account to add.</param>
    /// <param name="providerId">The provider ID associated with the connected account.</param>
    /// <param name="username">The username of the connected account.</param>
    /// <param name="email">The email address associated with the connected account.</param>
    /// <returns>
    /// An asynchronous task representing the addition of the connected account.
    /// </returns>
    /// <remarks>
    /// This method allows the repository to add a connected account by providing the type, provider ID, username, and email address.
    /// It performs this operation asynchronously.
    /// </remarks>
    Task<Task> AddConnectedAccountAsync(
        ConnectedAccountType type,
        string ProviderId,
        string Username,
        string email
    );

    /// <summary>
    /// Retrieves the currently authenticated user based on their user ID claim.
    /// </summary>
    /// <returns>
    /// A task that represents the asynchronous operation. The task result is the currently authenticated
    /// user if found, or null if an exception occurs or the user is not authenticated.
    /// </returns>
    /// <remarks>
    /// This method fetches the user with associated data (e.g., saved items, votes, views) based on the user's ID claim.
    /// If the user is authenticated and found in the database, the method returns the user; otherwise, it returns null.
    /// </remarks>
    Task<User> GetCurrentUser();
    Task<UserPreviewResponse> GetMe();
    Task<string> GetCurrentUserId();

    /// <summary>
    /// Retrieves a list of user profiles representing the users being followed by the currently authenticated user asynchronously.
    /// </summary>
    /// <returns>
    /// A task that represents the asynchronous operation. The task result is an enumerable collection of user profiles
    /// representing the users being followed by the currently authenticated user.
    /// </returns>
    /// <remarks>
    /// This asynchronous method fetches a list of user profiles for users being followed by the currently authenticated user.
    /// It first retrieves the IDs of the followed users and then asynchronously retrieves their user profiles.
    /// The resulting collection contains user profiles for each followed user.
    /// </remarks>
    string GetClaimValue(string claimType);

    Task<int> GetCategoryFollowedsCount();
    Task<int> GetUserArticlesCount();
    Task<int> GetUserQuestionsCount();
    Task<int> GetUserAnswersCount();
    Task<int> GetUserResourcesCount();

}
