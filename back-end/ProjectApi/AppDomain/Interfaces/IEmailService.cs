﻿using AppDomain.Entities.UserRelated;

namespace AppDomain.Interfaces;

/// <summary>
/// Represents an interface for sending and verifying emails with OTP codes.
/// </summary>
public interface IEmailService
{
    /// <summary>
    /// Sends an email synchronously to the specified recipient with an OTP code.
    /// </summary>
    /// <param name="toEmail">The email address of the recipient.</param>
    /// <param name="toName">The name of the recipient.</param>
    /// <param name="otpCode">The OTP (One-Time Password) code to include in the email.</param>
    void SendEmail(string toEmail, string toName, string otpCode);

    /// <summary>
    /// Asynchronously verifies an email address by comparing the provided OTP code.
    /// </summary>
    /// <param name="email">Email address to be verified.</param>
    /// <param name="otpCode">OTP code to be checked for verification.</param>
    /// <returns>Task containing the result of the email verification.</returns>
    Task<EmailVerification> VerifyEmailAsync(string email, string otpCode);
}