﻿using AppDomain.Entities.UserRelated;
using AppDomain.Enums;
using Microsoft.AspNetCore.Http;

namespace AppDomain.Interfaces;

/// <summary>
/// Interface for a JWT service responsible for geneRank a security token for a given email address.
/// </summary>
public interface ITokenService
{
    /// <summary>
    /// Generates a JWT security token for a user with the specified ID and email.
    /// </summary>
    /// <param name="id">The user's ID.</param>
    /// <param name="email">The user's email address.</param>
    /// <param name="role">The user's  role.</param>
    /// <returns>The JWT security token as a string.</returns>
    AuthToken GenerateSecurityToken(string id, string email, UserRole role);

    /// <summary>
    /// Retrieves a specific claim from a given security token.
    /// </summary>
    /// <param name="token">The security token from which to retrieve the claim.</param>
    /// <param name="claimType">The type of claim to retrieve.</param>
    /// <returns>The value of the specified claim if found; otherwise, an empty string.</returns>
    /// <remarks>
    /// This method allows you to extract a specific claim, identified by its type, from a security token.
    /// Security tokens commonly contain claims related to user identity and authorization.
    /// </remarks>
    string GetClaimsFromToken(string token, string claimType);

    /// <summary>
    /// Generates a new refresh token.
    /// </summary>
    /// <returns>A new refresh token.</returns>
    /// <remarks>
    /// This method generates a new refresh token, typically used for authentication purposes.
    /// Refresh tokens are used to obtain new access tokens without requiring the user to re-enter their credentials.
    /// </remarks>
    RefreshToken GenerateRefreshToken();

    /// <summary>
    /// Sets a refresh token in the provided HttpContext.
    /// </summary>
    /// <param name="token">The refresh token to set in the HttpContext.</param>
    /// <param name="context">The HttpContext to set the refresh token in.</param>
    /// <returns>The HttpContext with the refresh token set.</returns>
    /// <remarks>
    /// This method sets a refresh token in the provided HttpContext, making it available for later use during authentication.
    /// Refresh tokens are often stored in cookies or other forms of secure storage for future token renewal.
    /// </remarks>
    HttpContext SetRefreshToken(RefreshToken token, HttpContext context);

    HttpContext SetAuthToken(AuthToken token, HttpContext context);
}
