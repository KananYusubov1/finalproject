﻿using AppDomain.DTOs.Tags;
using AppDomain.Entities.TagBaseRelated;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDomain.Interfaces
{
    public interface ICategoryRepository
    {
        Task<List<TagFullResponseDto>> GetAllCategory(string? tokenId,string keyword = "");
        Task<Category> GetCategoryById(string id);
        Task<string> InsertCategory(Category category);
        Task<string> UpdateCategory(Category category);
        Task<string> DeleteCategory(string categoryId);
        Task<int> IncrementUseCounts(List<string> categoryIdList);
        Task<int> DecrementUseCounts(List<string> categoryIdList);
        Task<int> Follow(string categoryId,string tokenId);
        Task<List<TagFullResponseDto>> GetAllFollowedCategory(string tokenId, string keyword = "");
    }
}
