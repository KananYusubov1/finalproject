﻿using AppDomain.DTOs.Pagination;
using AppDomain.DTOs.Product;
using AppDomain.Enums;

namespace AppDomain.Interfaces;

public interface IProductRepository
{
    Task<PaginatedListDto<GetAllProductDTO>> GetAllProducts(
        string? searchQuery, ProductSortOptions sort, int page = 1);

    Task<GetByIdProductDTO> GetByIdProduct(string productId);

    Task InsertProduct(InsertProductDTO insertProductDTO);

    Task FavouritedProduct(string productId);
    Task<PaginatedListDto<GetAllProductDTO>> GetAllFavouriteProducts(
        string? searchQuery, ProductSortOptions sort, int page = 1);

    Task DeleteProduct(string productId);

    Task UpdateProduct(UpdateProductDTO updateProductDTO);

    Task BuyProduct(string productId,int quantity);

    Task<Dictionary<string,int>> GetAllRankForProduct(string productId);
}