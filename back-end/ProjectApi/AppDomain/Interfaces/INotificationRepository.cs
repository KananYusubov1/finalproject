﻿using AppDomain.DTOs.Notification;
using AppDomain.Entities.NotificationRelated;
using AppDomain.Enums;
using AppDomain.Responses;

namespace AppDomain.Interfaces;

public interface INotificationRepository
{
    /// <summary>
    /// Posts a notification asynchronously.
    /// </summary>
    /// <param name="notificationDTO">The data transfer object containing notification details.</param>
    /// <returns>An asynchronous task representing the operation completion.</returns>
    Task<Task> PostNotificationAsync(NotificationDTO notificationDTO);

    /// <summary>
    /// Gets the count of unread notifications for the current user asynchronously.
    /// </summary>
    /// <returns>The number of unread notifications.</returns>
    Task<int> GetUnreadNotificationsCountAsync();
    Task<IEnumerable<BudsNotificationResponse>> GetAllBudsRelatedNotificationsAsync();
    Task<Task> ReadNotificationAsync();

    /// <summary>
    /// Gets all notifications for the current user asynchronously.
    /// </summary>
    /// <returns>A collection of notifications.</returns>
    Task<IEnumerable<Notification>> GetAllNotificationsAsync();

    /// <summary>
    /// Gets notifications of a specific type for the current user asynchronously.
    /// </summary>
    /// <param name="type">The type of notifications to retrieve.</param>
    /// <returns>A collection of notifications of the specified type.</returns>
    Task<IEnumerable<Notification>> GetNotificationsByTypeAsync(NotificationType type);
}