﻿using AppDomain.ValueObjects;

namespace AppDomain.Responses;

public class PersonalInfoResponse
{
    public PersonalInfo PersonalInfo { get; set; }
}
