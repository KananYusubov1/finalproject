﻿using AppDomain.DTOs.Article;

namespace AppDomain.Responses;

public class ArticleNotificationResponse
{
    public DateTime CreatedAt { get; set; }
    public GetAllArticleDTO ArticleDTO { get; set; }
}