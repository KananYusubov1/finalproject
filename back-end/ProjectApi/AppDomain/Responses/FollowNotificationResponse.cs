﻿using AppDomain.Responses.UserResponses;

namespace AppDomain.Responses;

public class FollowNotificationResponse
{
    public UserProfileResponse FollowerUserProfile { get; set; }
    public DateTime CreatedAt { get; set; }
}