﻿using AppDomain.Enums;

namespace AppDomain.Responses;

public class EmailResponse
{
    public string Email { get; set; }
    public string Title { get; set; }
}
