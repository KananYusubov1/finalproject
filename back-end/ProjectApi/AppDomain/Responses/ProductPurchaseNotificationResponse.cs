﻿using AppDomain.DTOs.Product;

namespace AppDomain.Responses;

public class ProductPurchaseNotificationResponse
{
    public GetAllProductDTO Product { get; set; }
    public int PurchaseQuantity { get; set; }
    public int TotalSum { get; set; }
}
