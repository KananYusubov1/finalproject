﻿using AppDomain.DTOs.Buds;

namespace AppDomain.Responses;

public class BudsNotificationResponse
{
    public BudsDTO BudsDTO { get; set; }
    public object EntryDTO { get; set; }
}