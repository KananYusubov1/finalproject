﻿namespace AppDomain.Responses.UserResponses;

public class UserContentTitleResponse
{
    public string UserId { get; set; }
    public string ProfilePhoto { get; set; }
    public string Name { get; set; }
    public string Username { get; set; }
}