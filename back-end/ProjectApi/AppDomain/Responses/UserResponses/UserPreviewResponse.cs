﻿using AppDomain.ValueObjects;

namespace AppDomain.Responses.UserResponses;

public class UserPreviewResponse
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string Username { get; set; }
    public string DisplayEmail { get; set; }
    public string ProfilePhoto { get; set; }
    public Brand Brand { get; set; }
    public string? Bio { get; set; }
    public string? Location { get; set; }
    public string? Education { get; set; }
    public string? Work { get; set; }
    public DateTime Joined { get; set; }
    public bool IsUserFollowed { get; set; } = false;
}