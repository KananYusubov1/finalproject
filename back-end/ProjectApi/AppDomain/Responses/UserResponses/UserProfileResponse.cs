﻿namespace AppDomain.Responses.UserResponses;

public class UserProfileResponse
{
    public string UserId { get; set; }
    public string Name { get; set; }
    public string Username { get; set; }
    public string Email { get; set; }
    public string ProfilePhoto { get; set; }
    public bool IsUserFollowed { get; set; } = false;
}