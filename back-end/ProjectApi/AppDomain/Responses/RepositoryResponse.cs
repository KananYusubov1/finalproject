﻿namespace AppDomain.Responses;

public class RepositoryResponse
{
    public string RepoId { get; set; }
    public string RepoName { get; set; }
    public bool Fork { get; set; }
    public bool IsSelected { get; set; }

    public RepositoryResponse(string repoId, string repoName, bool fork, bool isSelected)
    {
        RepoId = repoId;
        RepoName = repoName;
        Fork = fork;
        IsSelected = isSelected;
    }
}
