﻿using AppDomain.DTO;
using AppDomain.DTOs.Article;
using AppDomain.DTOs.Comment;

namespace AppDomain.Responses;

public class CommentNotificationResponse
{
    public DateTime CreatedAt { get; set; }
    public GetAllArticleDTO ArticleDTO { get; set; }
    public GetArticleCommentDTO CommentDTO { get; set; }
}
