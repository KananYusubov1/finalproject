﻿using AppDomain.DTOs.Answer;
using AppDomain.DTOs.Article;
using AppDomain.DTOs.Notification;
using AppDomain.DTOs.Question;
using AppDomain.DTOs.Resource;
using AppDomain.Entities.NotificationRelated;
using AppDomain.Entities.NotificationRelated.AdditionalDetails;
using AppDomain.Enums;
using AppDomain.Interfaces;
using AppDomain.Responses;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;

namespace Infrastructure.Persistence;

/// <summary>
/// A repository for managing notifications.
/// </summary>
public class NotificationRepository : INotificationRepository
{
    private readonly ClubrickDbContext _context;
    private readonly IHttpContextAccessor _httpContextAccessor;

    /// <summary>
    /// Initializes a new instance of the <see cref="NotificationRepository"/> class.
    /// </summary>
    /// <param name="context">The database context for accessing notification-related data.</param>
    /// <param name="httpContextAccessor">The HTTP context accessor for accessing HTTP-related information.</param>
    public NotificationRepository(
        ClubrickDbContext context,
        IHttpContextAccessor httpContextAccessor
    )
    {
        _context = context;
        _httpContextAccessor = httpContextAccessor;
    }

    /// <inheritdoc/>
    public async Task<Task> PostNotificationAsync(NotificationDTO notificationDTO)
    {
        var notification = new Notification
        {
            Type = notificationDTO.Type,
            CreatedAt = DateTime.UtcNow,
            IsViewed = false,
        };

        switch (notification.Type)
        {
            case NotificationType.Follow:
                var followDetails = JsonConvert.DeserializeObject<FollowDetails>(
                    notificationDTO.AdditionalDetails
                );

                var followInstance = await _context.Follows.FindAsync(
                    followDetails.FollowInstanceId
                );
                var follower = await _context.Users.FindAsync(followInstance.FollowerId);

                var followResponse = follower.ToFollowNotificationResponse();

                var followingUser = await _context.Users.FindAsync(followInstance.FollowingId);

                notification.Body = JsonConvert.SerializeObject(followResponse);

                followingUser.NotificationList.Add(notification);

                _context.Users.Update(followingUser);

                break;
            case NotificationType.CorrectAnswer:
                var answerDetails = JsonConvert.DeserializeObject<AnswerDetails>(
                    notificationDTO.AdditionalDetails
                );
                var answerer = _context.Users.FirstOrDefault(
                    user => user.Id == answerDetails.AnswererId
                );

                var answerResponse = await ModelConvertors.ToAnswerNotificationResponse(
                    answerDetails.AnswerId,
                    _context
                );
                notification.Body = JsonConvert.SerializeObject(answerResponse);
                notification.Content = JsonConvert.SerializeObject(answerResponse);

                answerer.NotificationList.Add(notification);

                _context.Users.Update(answerer);

                break;
            case NotificationType.Answer:
                answerDetails = JsonConvert.DeserializeObject<AnswerDetails>(
                    notificationDTO.AdditionalDetails
                );

                var userIds = await _context.Watches
                    .Where(
                        x =>
                            x.ContentId == answerDetails.QuestionId
                            && x.UserId != answerDetails.AnswererId
                    )
                    .Select(x => x.UserId)
                    .ToListAsync();

                var questionOwner = _context.Users.FirstOrDefault(
                    user =>
                        user.Id
                        == _context.Questions
                            .FirstOrDefault(question => question.Id == answerDetails.QuestionId)
                            .UserId
                );

                var users = await _context.Users
                    .Where(user => userIds.Contains(user.Id))
                    .ToListAsync();

                users.Add(questionOwner);

                answerResponse = await ModelConvertors.ToAnswerNotificationResponse(
                    answerDetails.AnswerId,
                    _context
                );
                notification.Body = JsonConvert.SerializeObject(answerResponse);

                foreach (var user in users)
                {
                    user.NotificationList.Add(notification);

                    _context.Users.Update(user);
                }
                break;
            case NotificationType.Article:
                var articleDetails = JsonConvert.DeserializeObject<ArticleDetails>(
                    notificationDTO.AdditionalDetails
                );

                userIds = await _context.Follows
                    .Where(follow => follow.FollowingId == articleDetails.ArticleOwnerId)
                    .Select(follow => follow.FollowerId)
                    .ToListAsync();

                var followingUsers = await _context.Users
                    .Where(user => userIds.Contains(user.Id))
                    .ToListAsync();

                var articleResponse = await ModelConvertors.ToArticleNotificationResponse(
                    articleDetails.ArticleId,
                    _context
                );

                notification.Body = JsonConvert.SerializeObject(articleResponse);

                foreach (var user in followingUsers)
                {
                    user.NotificationList.Add(notification);

                    _context.Users.Update(user);
                }

                break;
            case NotificationType.Comment: // Comments belong to articles
                var commenterDetails = JsonConvert.DeserializeObject<CommentDetails>(
                    notificationDTO.AdditionalDetails
                );

                var commentResponse = await ModelConvertors.ToCommentNotificationResponse(
                    commenterDetails.CommentId,
                    _context
                );

                notification.Body = JsonConvert.SerializeObject(commentResponse);

                var articleOwner = _context.Users.FirstOrDefault(
                    user => user.Id == commentResponse.ArticleDTO.UserContent.UserId
                );

                articleOwner.NotificationList.Add(notification);

                _context.Users.Update(articleOwner);

                break;
            case NotificationType.BudsRelated:
                var budsDetails = JsonConvert.DeserializeObject<BudsDetails>(
                    notificationDTO.AdditionalDetails
                );      

                var budsResponse = await budsDetails.ToBudsNotificationResponse(_context);

                notification.Body = JsonConvert.SerializeObject(budsResponse);

                var budsEarner = await _context.Users.FindAsync(budsDetails.BudsEarnerId);

                budsEarner.NotificationList.Add(notification);

                _context.Users.Update(budsEarner);

                break;
            case NotificationType.ProductPurchase:
                var productDetails = JsonConvert.DeserializeObject<ProductPurchaseDetails>(
                    notificationDTO.AdditionalDetails
                );

                var productResponse = await productDetails.ToProductPurchaseNotificationResponse(
                    _context
                );

                notification.Body = JsonConvert.SerializeObject(productResponse);

                var buyer = await _context.Users.FindAsync(productDetails.BuyerId);

                buyer.NotificationList.Add(notification);

                _context.Users.Update(buyer);

                break;
            default:
                break;
        }

        await _context.SaveChangesAsync();

        return Task.CompletedTask;
    }

    /// <inheritdoc/>
    public async Task<int> GetUnreadNotificationsCountAsync()
    {
        var user = await GetCurrentUser();

        if (user is null)
            throw new UnauthorizedAccessException("There is no such user in system.");

        var unreadNotificationsCount = user.NotificationList
            .Where(n => !n.IsViewed && n.Type != NotificationType.BudsRelated)
            .Count();

        return unreadNotificationsCount;
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<BudsNotificationResponse>> GetAllBudsRelatedNotificationsAsync()
    {
        var user = await GetCurrentUser();

        var usersNotifications = user.NotificationList;

        var response = new List<BudsNotificationResponse>();

        var budsResponse = new BudsNotificationResponse();

        usersNotifications.ForEach(notification =>
        {
            if (notification.Type != NotificationType.BudsRelated)
                return;

            budsResponse = JsonConvert.DeserializeObject<BudsNotificationResponse>(
                notification.Body
            );

            response.Add(budsResponse);
        });

        response = response.OrderByDescending(x => x.BudsDTO.CreatedTime).ToList();

        return response;
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<Notification>> GetAllNotificationsAsync()
    {
        var user = await GetCurrentUser();

        var usersNotifications = user.NotificationList;

        var response = new List<Notification>();

        usersNotifications.ForEach(notification =>
        {
            if (notification.Type == NotificationType.BudsRelated)
                return;

            switch (notification.Type)
            {
                case NotificationType.Follow:
                    notification.Content =
                        JsonConvert.DeserializeObject<FollowNotificationResponse>(
                            notification.Body
                        );
                    break;
                case NotificationType.Comment:
                    notification.Content =
                        JsonConvert.DeserializeObject<CommentNotificationResponse>(
                            notification.Body
                        );
                    break;
                case NotificationType.Article:
                    notification.Content =
                        JsonConvert.DeserializeObject<ArticleNotificationResponse>(
                            notification.Body
                        );
                    break;
                case NotificationType.CorrectAnswer:
                case NotificationType.Answer:
                    notification.Content =
                        JsonConvert.DeserializeObject<AnswerNotificationResponse>(
                            notification.Body
                        );
                    break;

                case NotificationType.ProductPurchase:
                    notification.Content =
                        JsonConvert.DeserializeObject<ProductPurchaseNotificationResponse>(
                            notification.Body
                        );
                    break;
                default:
                    break;
            }

            response.Add(notification);
        });

        response = response.OrderByDescending(x => x.CreatedAt).ToList();

        return response;
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<Notification>> GetNotificationsByTypeAsync(NotificationType type)
    {
        var user = await GetCurrentUser();

        var usersNotifications = user.NotificationList
            .Where(n => type == NotificationType.All ? true : n.Type == type)
            .ToList();

        var notificationList = new List<Notification>();

        usersNotifications.ForEach(notification =>
        {
            if (notification.Type == NotificationType.Follow)
                notification.Content = JsonConvert.DeserializeObject<FollowNotificationResponse>(
                    notification.Body
                );
            else if (notification.Type == NotificationType.Comment)
                notification.Content = JsonConvert.DeserializeObject<CommentNotificationResponse>(
                    notification.Body
                );
            else if (notification.Type == NotificationType.Article)
                notification.Content = JsonConvert.DeserializeObject<ArticleNotificationResponse>(
                    notification.Body
                );
            else if (notification.Type == NotificationType.Answer)
                notification.Content = JsonConvert.DeserializeObject<AnswerNotificationResponse>(
                    notification.Body
                );

            notification.IsViewed = true;

            notificationList.Add(notification);
        });

        notificationList = notificationList.OrderByDescending(x => x.CreatedAt).ToList();

        return notificationList;
    }

    /// <inheritdoc/>
    public async Task<Task> ReadNotificationAsync()
    {
        var user = await GetCurrentUser();

        user.NotificationList.ForEach(n =>
        {
            n.IsViewed = true;
        });

        _context.Update(user);

        await _context.SaveChangesAsync();

        return Task.CompletedTask;
    }

    private async Task<AppDomain.Entities.UserRelated.User> GetCurrentUser()
    {
        try
        {
            var id = GetClaimValue("userId");

            if (id is null)
                throw new UnauthorizedAccessException("There is no such user in system.");

            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id.Equals(id));

            return user;
        }
        catch (Exception)
        {
            return null;
        }
    }

    private string GetClaimValue(string claimType)
    {
        var token = GetTokenFromRequest();

        if (token is null)
            return null;

        var claim = token.Claims.FirstOrDefault(c => c.Type == claimType);

        return claim?.Value;
    }

    private JwtSecurityToken GetTokenFromRequest()
    {
        var authHeader = _httpContextAccessor.HttpContext.Request.Headers[
            "Authorization"
        ].FirstOrDefault();

        if (!string.IsNullOrEmpty(authHeader) && authHeader.StartsWith("Bearer "))
        {
            var token = authHeader.Substring("Bearer ".Length);
            var tokenHandler = new JwtSecurityTokenHandler();

            if (tokenHandler.CanReadToken(token))
                return tokenHandler.ReadJwtToken(token);
        }

        return null;
    }
}
