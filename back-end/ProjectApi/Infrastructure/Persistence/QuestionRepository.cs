﻿using AppDomain.DTO;
using AppDomain.DTOs.Answer;
using AppDomain.DTOs.Category;
using AppDomain.DTOs.Pagination;
using AppDomain.DTOs.Question;
using AppDomain.Entities.ContentRelated;
using AppDomain.Entities.NotificationRelated.AdditionalDetails;
using AppDomain.Entities.TagBaseRelated;
using AppDomain.Entities.UserActionRelated;
using AppDomain.Enums;
using AppDomain.Exceptions.UserExceptions;
using AppDomain.Interfaces;
using AppDomain.Responses.UserResponses;
using Application.Services;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Infrastructure.Persistence
{
    public class QuestionRepository : IQuestionRepository
    {
        private ClubrickDbContext _context;
        private DapperContext _dapperContext;
        private readonly IMapper _mapper;
        private readonly IUserRepository _userRepository;
        private readonly INotificationRepository _notificationRepository;

        public QuestionRepository(
            ClubrickDbContext context,
            DapperContext dapperContext,
            IMapper mapper,
            IUserRepository userRepository,
            INotificationRepository notificationRepository)
        {
            _context = context;
            _dapperContext = dapperContext;
            _mapper = mapper;
            _userRepository = userRepository;
            _notificationRepository = notificationRepository;
        }

        public async Task<IEnumerable<GetLastQuestionsDTO>> GetLastQuestionsAsync()
        {
            var questions = _context.Questions
                .Include(q => q.Answers)
                .OrderByDescending(x => x.UpdateTime)
                .Take(5)
                .Select(x => x.ToLastQuestionDTO())
                .ToList();

            return questions;
        }

        public async Task<string> InsertQuestion(
            AppDomain.Entities.ContentRelated.Question question,
            List<string> CategoriesIdList
        )
        {
            var user = await _userRepository.GetCurrentUser();

            if (user.IsFrozen)
                throw new UserForbiddenException(
                    user.Id,
                    "insert question",
                    user.FrozenUntil ?? DateTime.Now
                );

            try
            {
                bool temp = false;

                List<Category> categories = new List<Category>();

                foreach (var item in CategoriesIdList)
                {
                    var findCategory = _context.Categories.FirstOrDefault(x => x.Id == item);
                    if (findCategory is null)
                    {
                        temp = true;
                        break;
                    }

                    findCategory.UseCount++;
                    _context.Update(findCategory);

                    categories.Add(findCategory);
                }

                if (temp)
                    return "0";

                _context.Questions.Add(question);

                await _context.SaveChangesAsync();

                question.Categories.AddRange(categories);

                _context.Questions.Update(question);

                await _context.SaveChangesAsync();

                await _userRepository.AddBudsAsync(
                    ContentType.Question,
                    question.Id,
                    BudsActionType.PublishQuestionToOwner
                );

                return question.Id;
            }
            catch (Exception)
            {
                return "-1";
            }
        }

        public async Task<string> UpdateQuestion(UpdateQuestionDTO question, string tokenId)
        {
            var user = await _userRepository.GetCurrentUser();

            if (user.IsFrozen)
                throw new UserForbiddenException(
                    user.Id,
                    "update question",
                    user.FrozenUntil ?? DateTime.Now
                );

            try
            {
                var findQuestion = _context.Questions.FirstOrDefault(x => x.Id == question.Id);

                if (findQuestion is null)
                    return "0";

                if (findQuestion.UserId != tokenId)
                    return "0";

                if (question.Body is not null)
                    findQuestion.Body = question.Body;

                if (question.Header is not null)
                    findQuestion.Header = question.Header;

                await _context.SaveChangesAsync();

                return findQuestion.Id;
            }
            catch (Exception)
            {
                return "-1";
            }
        }

        public async Task<int> DeleteQuestion(string questionId, string tokenId)
        {
            var user = await _userRepository.GetCurrentUser();

            if (user.IsFrozen)
                throw new UserForbiddenException(
                    user.Id,
                    "delete question",
                    user.FrozenUntil ?? DateTime.Now
                );

            try
            {
                var findQuestion = _context.Questions.FirstOrDefault(x => x.Id == questionId);

                if (findQuestion is null)
                    return 0;

                if (findQuestion.UserId != tokenId)
                    return 0;

                var questionAnswerList = _context.Answers
                    .Where(x => x.QuestionId == questionId)
                    .ToList();

                questionAnswerList.ForEach(x =>
                {
                    x.IsDeleted = true;
                    x.UpdateTime = DateTime.UtcNow;
                });

                findQuestion.IsDeleted = true;

                findQuestion.UpdateTime = DateTime.UtcNow;

                await _context.SaveChangesAsync();

                return 1;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public async Task<string> SetCorrectAnswer(
            string questionId,
            string answerId,
            string userId
        )
        {
            try
            {
                var findQuestion = _context.Questions.FirstOrDefault(x => x.Id == questionId);

                if (findQuestion is null)
                    return "0";

                if (findQuestion.UserId != userId)
                    return "0";

                var findAnswer = _context.Answers.FirstOrDefault(x => x.Id == answerId);

                var user = _context.Users.FirstOrDefault(x => x.Id == findAnswer.UserId);

                if (findAnswer is null)
                    return "1";

                if (findAnswer.QuestionId != questionId)
                    return "1";

                if (findQuestion.CorrectAnswerId is null)
                {
                    findQuestion.CorrectAnswerId = answerId;

                    findQuestion.UpdateTime = DateTime.UtcNow;

                    var answerDetails = new AnswerDetails
                    {
                        AnswerId = answerId,
                        AnswererId = findAnswer.UserId,
                        QuestionId = questionId,
                    };

                    if (findQuestion.UserId != findAnswer.UserId)
                    {
                        await _notificationRepository.PostNotificationAsync(
                            new()
                            {
                                Type = NotificationType.CorrectAnswer,
                                AdditionalDetails = JsonConvert.SerializeObject(answerDetails)
                            }
                        );

                        await _userRepository.AddBudsAsync(
                            ContentType.Answer,
                            answerId,
                            BudsActionType.CorrectAnswerToUser,
                            user.Id
                        );
                    }
                }
                else
                {
                    if (findQuestion.CorrectAnswerId == answerId)
                    {
                        findQuestion.CorrectAnswerId = null;

                        findQuestion.UpdateTime = DateTime.UtcNow;
                    }
                    else
                    {
                        findQuestion.CorrectAnswerId = answerId;

                        findQuestion.UpdateTime = DateTime.UtcNow;

                        await _userRepository.AddBudsAsync(
                            ContentType.Answer,
                            answerId,
                            BudsActionType.CorrectAnswerToUser,
                            user.Id
                        );
                    }
                }

                _context.Users.Update(user);

                await _context.SaveChangesAsync();

                return questionId;
            }
            catch (Exception)
            {
                return "-1";
            }
        }

        public async Task<QuestionPreviewDTO> GetQuestionPreview(string questionId, string tokenId)
        {
            try
            {
                var findQuestion = _context.Questions
                    .Include(x => x.Categories)
                    .FirstOrDefault(x => x.Id == questionId);

                var tokenUser = _context.Users
                    .Include(x => x.SaveList)
                    .Include(x => x.ViewList)
                    .Include(x => x.WatchList)
                    .Include(x => x.VoteList)
                    .FirstOrDefault(x => x.Id == tokenId);

                var questionUser = _context.Users.FirstOrDefault(x => x.Id == findQuestion.UserId);

                if (findQuestion is null)
                    return null;

                bool saved =
                    tokenUser != null
                        ? tokenUser.SaveList.Exists(
                            x => x.ContentId == questionId && x.ContentType == ContentType.Question
                        )
                        : false;

                bool viewed =
                    tokenUser != null
                        ? tokenUser.ViewList.Exists(
                            x => x.ContentId == questionId && x.ContentType == ContentType.Question
                        )
                        : false;

                bool watched =
                    tokenUser != null
                        ? tokenUser.WatchList.Exists(
                            x => x.ContentId == questionId && x.ContentType == ContentType.Question
                        )
                        : false;

                int watchCount = _context.Watches
                    .Where(x => x.ContentId == questionId && x.ContentType == ContentType.Question)
                    .Count();

                int answerCount = _context.Answers.Where(x => x.QuestionId == questionId).Count();

                VoteStatus voteStatus;

                if (tokenUser != null)
                {
                    Vote vote = tokenUser.VoteList.FirstOrDefault(
                        x => x.ContentId == questionId && x.ContentType == ContentType.Question
                    );

                    if (vote is null)
                        voteStatus = VoteStatus.UnVote;
                    else
                        voteStatus = vote.Status;
                }
                else
                    voteStatus = VoteStatus.UnVote;

                QuestionStatus status;

                if (answerCount is not 0)
                {
                    if (findQuestion.CorrectAnswerId is null)
                        status = QuestionStatus.UnSolved;
                    else
                        status = QuestionStatus.Solved;
                }
                else
                    status = QuestionStatus.UnAnswered;

                UserContentTitleResponse userContentTitleResponse =
                    new()
                    {
                        Name = questionUser.Name,
                        Username = questionUser.Username,
                        ProfilePhoto = questionUser.ProfilePhoto,
                        UserId = questionUser.Id
                    };

                QuestionPreviewDTO questionPreviewDTO = _mapper.Map<QuestionPreviewDTO>(
                    findQuestion
                );

                questionPreviewDTO.UserContent = userContentTitleResponse;

                questionPreviewDTO.IsSaved = saved;

                questionPreviewDTO.IsViewed = viewed;

                questionPreviewDTO.IsWatched = watched;

                questionPreviewDTO.WatchCount = watchCount;

                questionPreviewDTO.AnswerCount = answerCount;

                questionPreviewDTO.Status = status;

                questionPreviewDTO.Vote = new VoteDTO()
                {
                    VoteCount = findQuestion.VoteCount,
                    Status = voteStatus
                };

                foreach (var item in findQuestion.Categories)
                {
                    TagDTO questionCategory = _mapper.Map<TagDTO>(item);
                    questionPreviewDTO.Categories.Add(questionCategory);
                }

                return questionPreviewDTO;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<GetByIdQuestionDTO> GetQuestionById(string questionId, string userId)
        {
            try
            {
                var findQuestion = _context.Questions
                    .Include(x => x.Categories)
                    .FirstOrDefault(x => x.Id == questionId);

                var user = _context.Users
                    .Include(x => x.VoteList)
                    .Include(x => x.ViewList)
                    .Include(x => x.SaveList)
                    .Include(x => x.WatchList)
                    .FirstOrDefault(x => x.Id == userId);

                var questionUser = _context.Users.FirstOrDefault(x => x.Id == findQuestion.UserId);

                if (findQuestion is null)
                    return null;

                bool saved =
                    user != null
                        ? user.SaveList.Exists(
                            x => x.ContentId == questionId && x.ContentType == ContentType.Question
                        )
                        : false;

                bool watched =
                    user != null
                        ? user.WatchList.Exists(
                            x => x.ContentId == questionId && x.ContentType == ContentType.Question
                        )
                        : false;

                int watchCount = _context.Watches
                    .Where(x => x.ContentId == questionId && x.ContentType == ContentType.Question)
                    .Count();

                int viewCount = _context.Views
                    .Where(x => x.ContentId == questionId && x.ContentType == ContentType.Question)
                    .Count();

                int saveCount = _context.Saves
                    .Where(x => x.ContentId == questionId && x.ContentType == ContentType.Question)
                    .Count();

                var answerList = _context.Answers.Where(x => x.QuestionId == questionId).ToList();

                bool isFollow =
                    user != null
                        ? _context.Follows.Any(
                            x => x.FollowerId == user.Id && x.FollowingId == questionUser.Id
                        )
                        : false;

                UserPreviewResponse userPreviewResponse =
                    new()
                    {
                        Name = questionUser.Name,
                        DisplayEmail = questionUser.Email,
                        ProfilePhoto = questionUser.ProfilePhoto,
                        Brand = questionUser.Brand,
                        Bio = questionUser.PersonalInfo?.Bio,
                        Location = questionUser.PersonalInfo?.Location,
                        Education = questionUser.PersonalInfo?.Education,
                        Work = questionUser.PersonalInfo?.Work,
                        Joined = questionUser.JoinedTime,
                        Id = questionUser.Id,
                        IsUserFollowed = isFollow,
                        Username = questionUser.Username,
                    };

                VoteStatus voteStatus;

                if (user != null)
                {
                    Vote vote = user.VoteList.FirstOrDefault(
                        x => x.ContentId == questionId && x.ContentType == ContentType.Question
                    );

                    if (vote is null)
                        voteStatus = VoteStatus.UnVote;
                    else
                        voteStatus = vote.Status;
                }
                else
                    voteStatus = VoteStatus.UnVote;

                List<GetByIdAnswerDTO> getByIdAnswerDTOs = new List<GetByIdAnswerDTO>();

                var answersList = _context.Answers.OrderByDescending(x => x.UpdateTime).ToList();

                foreach (var answer in answersList)
                {
                    if (answer.QuestionId == questionId)
                    {
                        var answerUser = _context.Users
                            .Include(x => x.VoteList)
                            .FirstOrDefault(x => x.Id == answer.UserId);

                        VoteStatus voteAnswerStatus;

                        Vote answerVote =
                            user != null
                                ? user.VoteList.FirstOrDefault(
                                    x =>
                                        x.ContentId == answer.Id
                                        && x.ContentType == ContentType.Answer
                                )
                                : null;

                        if (answerVote is null)
                            voteAnswerStatus = VoteStatus.UnVote;
                        else
                            voteAnswerStatus = answerVote.Status;

                        getByIdAnswerDTOs.Add(
                            new GetByIdAnswerDTO()
                            {
                                Body = answer.Body,
                                Id = answer.Id,
                                UpdateTime = answer.UpdateTime,
                                UserContent = new UserContentTitleResponse()
                                {
                                    UserId = answerUser.Id,
                                    Name = answerUser.Name,
                                    Username = answerUser.Username,
                                    ProfilePhoto = answerUser.ProfilePhoto,
                                },
                                VoteDTO = new VoteDTO()
                                {
                                    VoteCount = answer.VoteCount,
                                    Status = voteAnswerStatus
                                }
                            }
                        );
                    }
                }

                GetByIdQuestionDTO getByIdQuestionDTO = _mapper.Map<GetByIdQuestionDTO>(
                    findQuestion
                );

                getByIdQuestionDTO.IsSaved = saved;

                getByIdQuestionDTO.IsWatched = watched;

                getByIdQuestionDTO.WatchCount = watchCount;

                getByIdQuestionDTO.ViewCount = viewCount;

                getByIdQuestionDTO.SaveCount = saveCount;

                getByIdQuestionDTO.AnswerCount = answerList.Count;

                getByIdQuestionDTO.Answers = getByIdAnswerDTOs;

                getByIdQuestionDTO.UserPreview = userPreviewResponse;

                getByIdQuestionDTO.CorrectAnswerId = findQuestion.CorrectAnswerId;

                getByIdQuestionDTO.Vote = new VoteDTO()
                {
                    VoteCount = findQuestion.VoteCount,
                    Status = voteStatus
                };

                if (user != null)
                {
                    if (
                        !user.ViewList.Any(
                            x => x.ContentId == questionId && x.ContentType == ContentType.Question
                        )
                    )
                    {
                        findQuestion.ViewCount++;
                        user.ViewList.Add(
                            new View()
                            {
                                Id = IDGeneratorService.GetShortUniqueId(),
                                UserId = userId,
                                ContentId = questionId,
                                ContentType = ContentType.Question
                            }
                        );
                    }
                }

                if (user != null)
                    _context.Users.Update(user);

                _context.Questions.Update(findQuestion);

                await _context.SaveChangesAsync();

                return getByIdQuestionDTO;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<int> SaveQuestion(string questionId, string userId)
        {
            var user = _context.Users.Include(x => x.SaveList).FirstOrDefault(x => x.Id == userId);

            if (user is null)
                return -1;

            if (user.IsFrozen)
                throw new UserForbiddenException(
                    user.Id,
                    "save question",
                    user.FrozenUntil ?? DateTime.Now
                );

            try
            {
                var findQuestion = _context.Questions.FirstOrDefault(x => x.Id == questionId);

                if (findQuestion is null)
                    return 0;

                var save = new Save()
                {
                    Id = IDGeneratorService.GetShortUniqueId(),
                    UserId = userId,
                    ContentId = questionId,
                    ContentType = ContentType.Question
                };

                if (user.SaveList.Count is 0)
                    user.SaveList.Add(save);
                else
                {
                    var findSave = _context.Saves.FirstOrDefault(
                        x => x.ContentId == questionId && x.ContentType == ContentType.Question
                    );

                    if (findSave != null)
                        _context.Saves.Remove(findSave);
                    else
                        user.SaveList.Add(save);
                }

                _context.Users.Update(user);

                await _context.SaveChangesAsync();

                return 1;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public async Task<int> WatchQuestion(string questionId, string userId)
        {
            var user = _context.Users
                .Include(x => x.SaveList)
                .Include(x => x.WatchList)
                .FirstOrDefault(x => x.Id == userId);

            if (user is null)
                return -1;

            if (user.IsFrozen)
                throw new UserForbiddenException(
                    user.Id,
                    "watch question",
                    user.FrozenUntil ?? DateTime.Now
                );

            try
            {
                var findQuestion = _context.Questions.FirstOrDefault(x => x.Id == questionId);

                if (findQuestion is null)
                    return 0;

                var watch = new Watch()
                {
                    Id = IDGeneratorService.GetShortUniqueId(),
                    UserId = userId,
                    ContentId = questionId,
                    ContentType = ContentType.Question
                };

                if (user.WatchList.Count is 0)
                {
                    user.WatchList.Add(watch);
                    findQuestion.WatchCount++;
                }
                else
                {
                    var findWatch = _context.Watches.FirstOrDefault(
                        x => x.ContentId == questionId && x.ContentType == ContentType.Question
                    );

                    if (findWatch != null)
                    {
                        _context.Watches.Remove(findWatch);
                        findQuestion.WatchCount--;
                    }
                    else
                    {
                        user.WatchList.Add(watch);
                        findQuestion.WatchCount++;
                    }
                }

                _context.Users.Update(user);

                _context.Questions.Update(findQuestion);

                await _context.SaveChangesAsync();

                return 1;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public async Task<int> VoteQuestion(VoteStatus status, string questionId, string userId)
        {
            var user = _context.Users.Include(x => x.VoteList).FirstOrDefault(x => x.Id == userId);

            if (user is null)
                return -1;

            if (user.IsFrozen)
                throw new UserForbiddenException(
                    user.Id,
                    "vote question",
                    user.FrozenUntil ?? DateTime.Now
                );

            try
            {
                var findQuestion = _context.Questions.FirstOrDefault(x => x.Id == questionId);

                if (user is null || findQuestion is null)
                    return 0;

                var voteEntity = user.VoteList.FirstOrDefault(
                    s => s.ContentType == ContentType.Question && s.ContentId.Equals(questionId)
                );

                if (voteEntity is null)
                {
                    Vote vote =
                        new()
                        {
                            Id = IDGeneratorService.GetShortUniqueId(),
                            ContentId = questionId,
                            UserId = userId,
                            ContentType = ContentType.Question,
                            Status = status
                        };

                    findQuestion.VoteCount += status == VoteStatus.UpVote ? 1 : -1;
                    user.VoteList.Add(vote);
                }
                else
                {
                    if (status != voteEntity.Status)
                    {
                        if (voteEntity.Status == VoteStatus.DownVote)
                            findQuestion.VoteCount += 1;
                        else if (voteEntity.Status == VoteStatus.UpVote)
                            findQuestion.VoteCount -= 1;
                        voteEntity.Status = status;
                        findQuestion.VoteCount += status == VoteStatus.DownVote ? -1 : 1;
                        _context.Update(voteEntity);
                    }
                    else
                    {
                        findQuestion.VoteCount += status == VoteStatus.DownVote ? 1 : -1;
                        user.VoteList.Remove(voteEntity);
                    }
                }

                _context.Users.Update(user);

                _context.Questions.Update(findQuestion);

                await _context.SaveChangesAsync();

                return 1;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public async Task<PaginatedListDto<GetAllQuestionDTO>> GetAllQuestion(
            string? searchQuery,
            List<string>? categoryIdList,
            SortOptions sort,
            int page = 1
        )
        {
            IQueryable<AppDomain.Entities.ContentRelated.Question> query = _context.Questions
                .Where(c => !c.IsDeleted)
                .Include(x => x.Categories)
                .Include(x => x.Answers);

            int pageSize = await _userRepository.GetContentPerPage();

            query = query.Skip((page - 1) * pageSize).Take(pageSize);

            if (categoryIdList != null)
            {
                foreach (var category in categoryIdList)
                {
                    if (!string.IsNullOrWhiteSpace(category))
                        query = query.Where(x => x.Categories.Any(c => c.Id.Equals(category)));
                }
            }

            if (!string.IsNullOrWhiteSpace(searchQuery))
                query = query.Where(r => r.Header.Title.ToLower().Contains(searchQuery.ToLower()));

            query = sort switch
            {
                SortOptions.Newest => query.OrderByDescending(r => r.UpdateTime),
                SortOptions.TopWeek
                    => query
                        .OrderByDescending(r => r.VoteCount)
                        .Where(r => r.UpdateTime >= DateTime.UtcNow.AddDays(-7)),
                SortOptions.TopMonth
                    => query
                        .OrderByDescending(r => r.VoteCount)
                        .Where(r => r.UpdateTime >= DateTime.UtcNow.AddMonths(-1)),
                SortOptions.TopYear
                    => query
                        .OrderByDescending(r => r.VoteCount)
                        .Where(r => r.UpdateTime >= DateTime.UtcNow.AddYears(-1)),
                SortOptions.TopInfinity => query.OrderByDescending(r => r.VoteCount),
                _
                    => query.OrderByDescending(
                        r => _context.Saves.Count(r => r.ContentId.Equals(r.Id))
                    )
            };

            List<GetAllQuestionDTO> getAllQuestionDTOList = new();

            var tokenId = _userRepository.GetClaimValue("userId");

            var findUser = _context.Users
                .Include(x => x.SaveList)
                .Include(x => x.ViewList)
                .Include(x => x.VoteList)
                .Include(x => x.WatchList)
                .FirstOrDefault(x => x.Id == tokenId);

            foreach (var item in query)
            {
                GetAllQuestionDTO getAllQuestionDTO = _mapper.Map<GetAllQuestionDTO>(item);

                var answerCount = item.Answers.Count;

                bool saved =
                    findUser != null
                        ? findUser.SaveList.Exists(
                            x => x.ContentId == item.Id && x.ContentType == ContentType.Question
                        )
                        : false;

                bool viewed =
                    findUser != null
                        ? findUser.ViewList.Exists(
                            x => x.ContentId == item.Id && x.ContentType == ContentType.Question
                        )
                        : false;

                bool watched =
                    findUser != null
                        ? findUser.WatchList.Exists(
                            x => x.ContentId == item.Id && x.ContentType == ContentType.Question
                        )
                        : false;

                VoteStatus voteStatus;

                if (findUser != null)
                {
                    Vote vote = findUser.VoteList.FirstOrDefault(
                        x => x.ContentId == item.Id && x.ContentType == ContentType.Question
                    );

                    if (vote is null)
                        voteStatus = VoteStatus.UnVote;
                    else
                        voteStatus = vote.Status;
                }
                else
                {
                    voteStatus = VoteStatus.UnVote;
                }

                QuestionStatus status;

                if (answerCount is not 0)
                {
                    if (item.CorrectAnswerId is null)
                        status = QuestionStatus.UnSolved;
                    else
                        status = QuestionStatus.Solved;
                }
                else
                    status = QuestionStatus.UnAnswered;

                UserContentTitleResponse userContentTitleResponse = new() { UserId = item.UserId };

                getAllQuestionDTO.AnswerCount = answerCount;

                getAllQuestionDTO.IsSaved = saved;

                getAllQuestionDTO.IsViewed = viewed;

                getAllQuestionDTO.IsWatched = watched;

                getAllQuestionDTO.VoteDTO = new VoteDTO()
                {
                    Status = voteStatus,
                    VoteCount = item.VoteCount
                };

                getAllQuestionDTO.Status = status;

                getAllQuestionDTO.UserContent = userContentTitleResponse;

                foreach (var category in item.Categories)
                {
                    TagDTO questionCategory = _mapper.Map<TagDTO>(category);
                    getAllQuestionDTO.QuestionCategories.Add(questionCategory);
                }

                getAllQuestionDTOList.Add(getAllQuestionDTO);
            }

            foreach (var item in getAllQuestionDTOList)
            {
                var questionUser = _context.Users.FirstOrDefault(
                    x => x.Id == item.UserContent.UserId
                );

                item.UserContent = new UserContentTitleResponse()
                {
                    UserId = questionUser.Id,
                    Name = questionUser.Name,
                    Username = questionUser.Username,
                    ProfilePhoto = questionUser.ProfilePhoto,
                };
            }

            int totalCount = await query.CountAsync();

            var paginationQuestions = getAllQuestionDTOList
                .Select(
                    r =>
                        new GetAllQuestionDTO()
                        {
                            Id = r.Id,
                            UserContent = r.UserContent,
                            Status = r.Status,
                            AnswerCount = r.AnswerCount,
                            IsSaved = r.IsSaved,
                            IsViewed = r.IsViewed,
                            IsWatched = r.IsWatched,
                            VoteDTO = r.VoteDTO,
                            QuestionCategories = r.QuestionCategories,
                            Header = r.Header,
                            ReadingTime = r.ReadingTime,
                            UpdateTime = r.UpdateTime,
                        }
                )
                .ToList();

            return new PaginatedListDto<GetAllQuestionDTO>(
                paginationQuestions,
                new PaginationMeta(page, pageSize, totalCount)
            );
        }

        public async Task<int> GetAllQuestionCount()
        {
            var questionsCount = _context.Questions.CountAsync();

            return await questionsCount;
        }

        public async Task<PaginatedListDto<GetAllQuestionDTO>> GetAllSavedQuestion(
            string searchQuery,
            List<string> categoryIdList,
            SortOptions sort,
            int page = 1
        )
        {
            var tokenId = _userRepository.GetClaimValue("userId");

            var findUser = _context.Users
                .Include(x => x.SaveList)
                .Include(x => x.ViewList)
                .Include(x => x.VoteList)
                .Include(x => x.WatchList)
                .FirstOrDefault(x => x.Id == tokenId);

            if (findUser != null || findUser.IsFrozen != true)
            {
                if (findUser.SaveList.Count != 0)
                {
                    if (!findUser.SaveList.Any(x => x.ContentType == ContentType.Question))
                        return null;
                }
                else
                    return null;
            }
            else
                return null;

            IQueryable<Question> query = _context.Questions
                .Where(c => !c.IsDeleted)
                .Include(x => x.Categories)
                .Include(x => x.Answers);

            int pageSize = await _userRepository.GetContentPerPage();

            query = query.Skip((page - 1) * pageSize).Take(pageSize);

            var savedQuestion = findUser.SaveList
                .Where(x => x.ContentType == ContentType.Question)
                .ToList();

            var savedQuestionIds = savedQuestion.Select(item => item.ContentId).ToList();

            query = query.Where(x => savedQuestionIds.Contains(x.Id));

            if (categoryIdList != null)
            {
                foreach (var category in categoryIdList)
                {
                    if (!string.IsNullOrWhiteSpace(category))
                        query = query.Where(x => x.Categories.Any(c => c.Id.Equals(category)));
                }
            }

            if (!string.IsNullOrWhiteSpace(searchQuery))
                query = query.Where(r => r.Header.Title.ToLower().Contains(searchQuery.ToLower()));

            query = sort switch
            {
                SortOptions.Newest => query.OrderByDescending(r => r.UpdateTime),
                SortOptions.TopWeek
                    => query
                        .OrderByDescending(r => r.VoteCount)
                        .Where(r => r.UpdateTime >= DateTime.UtcNow.AddDays(-7)),
                SortOptions.TopMonth
                    => query
                        .OrderByDescending(r => r.VoteCount)
                        .Where(r => r.UpdateTime >= DateTime.UtcNow.AddMonths(-1)),
                SortOptions.TopYear
                    => query
                        .OrderByDescending(r => r.VoteCount)
                        .Where(r => r.UpdateTime >= DateTime.UtcNow.AddYears(-1)),
                SortOptions.TopInfinity => query.OrderByDescending(r => r.VoteCount),
                _
                    => query.OrderByDescending(
                        r => _context.Saves.Count(r => r.ContentId.Equals(r.Id))
                    )
            };

            List<GetAllQuestionDTO> getAllQuestionDTOList = new();

            query = query.OrderByDescending(x => x.VoteCount);

            foreach (var item in query)
            {
                GetAllQuestionDTO getAllQuestionDTO = _mapper.Map<GetAllQuestionDTO>(item);

                var answerCount = item.Answers.Count;

                bool saved =
                    findUser != null
                        ? findUser.SaveList.Exists(
                            x => x.ContentId == item.Id && x.ContentType == ContentType.Question
                        )
                        : false;

                bool viewed =
                    findUser != null
                        ? findUser.ViewList.Exists(
                            x => x.ContentId == item.Id && x.ContentType == ContentType.Question
                        )
                        : false;

                bool watched =
                    findUser != null
                        ? findUser.WatchList.Exists(
                            x => x.ContentId == item.Id && x.ContentType == ContentType.Question
                        )
                        : false;

                VoteStatus voteStatus;

                if (findUser != null)
                {
                    Vote vote = findUser.VoteList.FirstOrDefault(
                        x => x.ContentId == item.Id && x.ContentType == ContentType.Question
                    );

                    if (vote is null)
                        voteStatus = VoteStatus.UnVote;
                    else
                        voteStatus = vote.Status;
                }
                else
                {
                    voteStatus = VoteStatus.UnVote;
                }

                QuestionStatus status;

                if (answerCount is not 0)
                {
                    if (item.CorrectAnswerId is null)
                        status = QuestionStatus.UnSolved;
                    else
                        status = QuestionStatus.Solved;
                }
                else
                    status = QuestionStatus.UnAnswered;

                UserContentTitleResponse userContentTitleResponse = new() { UserId = item.UserId };

                getAllQuestionDTO.AnswerCount = answerCount;

                getAllQuestionDTO.IsSaved = saved;

                getAllQuestionDTO.IsViewed = viewed;

                getAllQuestionDTO.IsWatched = watched;

                getAllQuestionDTO.VoteDTO = new VoteDTO()
                {
                    Status = voteStatus,
                    VoteCount = item.VoteCount
                };

                getAllQuestionDTO.Status = status;

                getAllQuestionDTO.UserContent = userContentTitleResponse;

                foreach (var category in item.Categories)
                {
                    TagDTO questionCategory = _mapper.Map<TagDTO>(category);
                    getAllQuestionDTO.QuestionCategories.Add(questionCategory);
                }

                getAllQuestionDTOList.Add(getAllQuestionDTO);
            }

            foreach (var item in getAllQuestionDTOList)
            {
                var questionUser = _context.Users.FirstOrDefault(
                    x => x.Id == item.UserContent.UserId
                );

                item.UserContent = new UserContentTitleResponse()
                {
                    UserId = questionUser.Id,
                    Name = questionUser.Name,
                    Username = questionUser.Username,
                    ProfilePhoto = questionUser.ProfilePhoto,
                };
            }

            int totalCount = await query.CountAsync();

            var paginationQuestions = getAllQuestionDTOList
                .Select(
                    r =>
                        new GetAllQuestionDTO()
                        {
                            Id = r.Id,
                            UserContent = r.UserContent,
                            Status = r.Status,
                            AnswerCount = r.AnswerCount,
                            IsSaved = r.IsSaved,
                            IsViewed = r.IsViewed,
                            IsWatched = r.IsWatched,
                            VoteDTO = r.VoteDTO,
                            QuestionCategories = r.QuestionCategories,
                            Header = r.Header,
                            ReadingTime = r.ReadingTime,
                            UpdateTime = r.UpdateTime,
                        }
                )
                .ToList();

            return new PaginatedListDto<GetAllQuestionDTO>(
                paginationQuestions,
                new PaginationMeta(page, pageSize, totalCount)
            );
        }

        public async Task<PaginatedListDto<GetAllQuestionDTO>> GetAllWatchedQuestion(
            string searchQuery,
            List<string> categoryIdList,
            SortOptions sort,
            int page = 1
        )
        {
            var tokenId = _userRepository.GetClaimValue("userId");

            var findUser = _context.Users
                .Include(x => x.SaveList)
                .Include(x => x.ViewList)
                .Include(x => x.VoteList)
                .Include(x => x.WatchList)
                .FirstOrDefault(x => x.Id == tokenId);

            if (findUser != null || findUser.IsFrozen != true)
            {
                if (findUser.WatchList.Count != 0)
                {
                    if (!findUser.WatchList.Any(x => x.ContentType == ContentType.Question))
                        return null;
                }
                else
                    return null;
            }
            else
                return null;

            IQueryable<Question> query = _context.Questions
                .Where(c => !c.IsDeleted)
                .Include(x => x.Categories)
                .Include(x => x.Answers);

            int pageSize = await _userRepository.GetContentPerPage();

            query = query.Skip((page - 1) * pageSize).Take(pageSize);

            var watchedQuestion = findUser.WatchList
                .Where(x => x.ContentType == ContentType.Question)
                .ToList();

            var watchedQuestionIds = watchedQuestion.Select(item => item.ContentId).ToList();

            query = query.Where(x => watchedQuestionIds.Contains(x.Id));

            if (categoryIdList != null)
            {
                foreach (var category in categoryIdList)
                {
                    if (!string.IsNullOrWhiteSpace(category))
                        query = query.Where(x => x.Categories.Any(c => c.Id.Equals(category)));
                }
            }

            if (!string.IsNullOrWhiteSpace(searchQuery))
                query = query.Where(r => r.Header.Title.ToLower().Contains(searchQuery.ToLower()));

            query = sort switch
            {
                SortOptions.Newest => query.OrderByDescending(r => r.UpdateTime),
                SortOptions.TopWeek
                    => query
                        .OrderByDescending(r => r.VoteCount)
                        .Where(r => r.UpdateTime >= DateTime.UtcNow.AddDays(-7)),
                SortOptions.TopMonth
                    => query
                        .OrderByDescending(r => r.VoteCount)
                        .Where(r => r.UpdateTime >= DateTime.UtcNow.AddMonths(-1)),
                SortOptions.TopYear
                    => query
                        .OrderByDescending(r => r.VoteCount)
                        .Where(r => r.UpdateTime >= DateTime.UtcNow.AddYears(-1)),
                SortOptions.TopInfinity => query.OrderByDescending(r => r.VoteCount),
                _
                    => query.OrderByDescending(
                        r => _context.Saves.Count(r => r.ContentId.Equals(r.Id))
                    )
            };

            List<GetAllQuestionDTO> getAllQuestionDTOList = new();

            query = query.OrderByDescending(x => x.VoteCount);

            foreach (var item in query)
            {
                GetAllQuestionDTO getAllQuestionDTO = _mapper.Map<GetAllQuestionDTO>(item);

                var answerCount = item.Answers.Count;

                bool saved =
                    findUser != null
                        ? findUser.SaveList.Exists(
                            x => x.ContentId == item.Id && x.ContentType == ContentType.Question
                        )
                        : false;

                bool viewed =
                    findUser != null
                        ? findUser.ViewList.Exists(
                            x => x.ContentId == item.Id && x.ContentType == ContentType.Question
                        )
                        : false;

                bool watched =
                    findUser != null
                        ? findUser.WatchList.Exists(
                            x => x.ContentId == item.Id && x.ContentType == ContentType.Question
                        )
                        : false;

                VoteStatus voteStatus;

                if (findUser != null)
                {
                    Vote vote = findUser.VoteList.FirstOrDefault(
                        x => x.ContentId == item.Id && x.ContentType == ContentType.Question
                    );

                    if (vote is null)
                        voteStatus = VoteStatus.UnVote;
                    else
                        voteStatus = vote.Status;
                }
                else
                {
                    voteStatus = VoteStatus.UnVote;
                }

                QuestionStatus status;

                if (answerCount is not 0)
                {
                    if (item.CorrectAnswerId is null)
                        status = QuestionStatus.UnSolved;
                    else
                        status = QuestionStatus.Solved;
                }
                else
                    status = QuestionStatus.UnAnswered;

                UserContentTitleResponse userContentTitleResponse = new() { UserId = item.UserId };

                getAllQuestionDTO.AnswerCount = answerCount;

                getAllQuestionDTO.IsSaved = saved;

                getAllQuestionDTO.IsViewed = viewed;

                getAllQuestionDTO.IsWatched = watched;

                getAllQuestionDTO.VoteDTO = new VoteDTO()
                {
                    Status = voteStatus,
                    VoteCount = item.VoteCount
                };

                getAllQuestionDTO.Status = status;

                getAllQuestionDTO.UserContent = userContentTitleResponse;

                foreach (var category in item.Categories)
                {
                    TagDTO questionCategory = _mapper.Map<TagDTO>(category);
                    getAllQuestionDTO.QuestionCategories.Add(questionCategory);
                }

                getAllQuestionDTOList.Add(getAllQuestionDTO);
            }

            foreach (var item in getAllQuestionDTOList)
            {
                var questionUser = _context.Users.FirstOrDefault(
                    x => x.Id == item.UserContent.UserId
                );

                item.UserContent = new UserContentTitleResponse()
                {
                    UserId = questionUser.Id,
                    Name = questionUser.Name,
                    Username = questionUser.Username,
                    ProfilePhoto = questionUser.ProfilePhoto,
                };
            }

            int totalCount = await query.CountAsync();

            var paginationQuestions = getAllQuestionDTOList
                .Select(
                    r =>
                        new GetAllQuestionDTO()
                        {
                            Id = r.Id,
                            UserContent = r.UserContent,
                            Status = r.Status,
                            AnswerCount = r.AnswerCount,
                            IsSaved = r.IsSaved,
                            IsViewed = r.IsViewed,
                            IsWatched = r.IsWatched,
                            VoteDTO = r.VoteDTO,
                            QuestionCategories = r.QuestionCategories,
                            Header = r.Header,
                            ReadingTime = r.ReadingTime,
                            UpdateTime = r.UpdateTime,
                        }
                )
                .ToList();

            return new PaginatedListDto<GetAllQuestionDTO>(
                paginationQuestions,
                new PaginationMeta(page, pageSize, totalCount)
            );
        }

        public async Task<PaginatedListDto<GetAllQuestionDTO>> GetAllUserQuestion(
            string searchQuery,
            List<string> categoryIdList,
            SortOptions sort,
            int page = 1
        )
        {
            var tokenId = _userRepository.GetClaimValue("userId");

            var findUser = _context.Users
                .Include(x => x.SaveList)
                .Include(x => x.ViewList)
                .Include(x => x.VoteList)
                .Include(x => x.WatchList)
                .FirstOrDefault(x => x.Id == tokenId);

            if (findUser is null)
                return null;
            else
            {
                if (findUser.IsFrozen is true)
                    return null;
            }

            IQueryable<Question> query = _context.Questions
                .Where(c => !c.IsDeleted && c.UserId == tokenId)
                .Include(x => x.Categories)
                .Include(x => x.Answers);

            int pageSize = await _userRepository.GetContentPerPage();

            query = query.Skip((page - 1) * pageSize).Take(pageSize);

            if (categoryIdList != null)
            {
                foreach (var category in categoryIdList)
                {
                    if (!string.IsNullOrWhiteSpace(category))
                        query = query.Where(x => x.Categories.Any(c => c.Id.Equals(category)));
                }
            }

            if (!string.IsNullOrWhiteSpace(searchQuery))
                query = query.Where(r => r.Header.Title.ToLower().Contains(searchQuery.ToLower()));

            query = sort switch
            {
                SortOptions.Newest => query.OrderByDescending(r => r.UpdateTime),
                SortOptions.TopWeek
                    => query
                        .OrderByDescending(r => r.VoteCount)
                        .Where(r => r.UpdateTime >= DateTime.UtcNow.AddDays(-7)),
                SortOptions.TopMonth
                    => query
                        .OrderByDescending(r => r.VoteCount)
                        .Where(r => r.UpdateTime >= DateTime.UtcNow.AddMonths(-1)),
                SortOptions.TopYear
                    => query
                        .OrderByDescending(r => r.VoteCount)
                        .Where(r => r.UpdateTime >= DateTime.UtcNow.AddYears(-1)),
                SortOptions.TopInfinity => query.OrderByDescending(r => r.VoteCount),
                _
                    => query.OrderByDescending(
                        r => _context.Saves.Count(r => r.ContentId.Equals(r.Id))
                    )
            };

            List<GetAllQuestionDTO> getAllQuestionDTOList = new();

            query = query.OrderByDescending(x => x.VoteCount);

            foreach (var item in query)
            {
                GetAllQuestionDTO getAllQuestionDTO = _mapper.Map<GetAllQuestionDTO>(item);

                var answerCount = item.Answers.Count;

                bool saved =
                    findUser != null
                        ? findUser.SaveList.Exists(
                            x => x.ContentId == item.Id && x.ContentType == ContentType.Question
                        )
                        : false;

                bool viewed =
                    findUser != null
                        ? findUser.ViewList.Exists(
                            x => x.ContentId == item.Id && x.ContentType == ContentType.Question
                        )
                        : false;

                bool watched =
                    findUser != null
                        ? findUser.WatchList.Exists(
                            x => x.ContentId == item.Id && x.ContentType == ContentType.Question
                        )
                        : false;

                VoteStatus voteStatus;

                if (findUser != null)
                {
                    Vote vote = findUser.VoteList.FirstOrDefault(
                        x => x.ContentId == item.Id && x.ContentType == ContentType.Question
                    );

                    if (vote is null)
                        voteStatus = VoteStatus.UnVote;
                    else
                        voteStatus = vote.Status;
                }
                else
                {
                    voteStatus = VoteStatus.UnVote;
                }

                QuestionStatus status;

                if (answerCount is not 0)
                {
                    if (item.CorrectAnswerId is null)
                        status = QuestionStatus.UnSolved;
                    else
                        status = QuestionStatus.Solved;
                }
                else
                    status = QuestionStatus.UnAnswered;

                UserContentTitleResponse userContentTitleResponse = new() { UserId = item.UserId };

                getAllQuestionDTO.AnswerCount = answerCount;

                getAllQuestionDTO.IsSaved = saved;

                getAllQuestionDTO.IsViewed = viewed;

                getAllQuestionDTO.IsWatched = watched;

                getAllQuestionDTO.VoteDTO = new VoteDTO()
                {
                    Status = voteStatus,
                    VoteCount = item.VoteCount
                };

                getAllQuestionDTO.Status = status;

                getAllQuestionDTO.UserContent = userContentTitleResponse;

                foreach (var category in item.Categories)
                {
                    TagDTO questionCategory = _mapper.Map<TagDTO>(category);
                    getAllQuestionDTO.QuestionCategories.Add(questionCategory);
                }

                getAllQuestionDTOList.Add(getAllQuestionDTO);
            }

            foreach (var item in getAllQuestionDTOList)
            {
                var questionUser = _context.Users.FirstOrDefault(
                    x => x.Id == item.UserContent.UserId
                );

                item.UserContent = new UserContentTitleResponse()
                {
                    UserId = questionUser.Id,
                    Name = questionUser.Name,
                    Username = questionUser.Username,
                    ProfilePhoto = questionUser.ProfilePhoto,
                };
            }

            int totalCount = await query.CountAsync();

            var paginationQuestions = getAllQuestionDTOList
                .Select(
                    r =>
                        new GetAllQuestionDTO()
                        {
                            Id = r.Id,
                            UserContent = r.UserContent,
                            Status = r.Status,
                            AnswerCount = r.AnswerCount,
                            IsSaved = r.IsSaved,
                            IsViewed = r.IsViewed,
                            IsWatched = r.IsWatched,
                            VoteDTO = r.VoteDTO,
                            QuestionCategories = r.QuestionCategories,
                            Header = r.Header,
                            ReadingTime = r.ReadingTime,
                            UpdateTime = r.UpdateTime,
                        }
                )
                .ToList();

            return new PaginatedListDto<GetAllQuestionDTO>(
                paginationQuestions,
                new PaginationMeta(page, pageSize, totalCount)
            );
        }
    }
}
