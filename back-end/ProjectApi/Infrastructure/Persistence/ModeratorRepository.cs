﻿using AppDomain.Enums;
using AppDomain.Interfaces;
using Infrastructure.Services;

namespace Infrastructure.Persistence;

public class ModeratorRepository: IModeratorRepository
{
    private readonly ClubrickDbContext _context;
    private readonly IUserRepository _userRepository;
    private readonly IAnswerRepository _answerRepository;
    private readonly ICommentRepository _commentRepository;

    public ModeratorRepository(ClubrickDbContext context, IUserRepository userRepository,
        ICommentRepository commentRepository, IAnswerRepository answerRepository)
    {
        _context = context;
        _userRepository = userRepository;
        _answerRepository = answerRepository;
        _commentRepository = commentRepository;
    }

    /// <inheritdoc/>
    public async Task<Task> FreezeContentAsync(string contentId, ContentType contentType, string responsibleModeratorId)
    {
        switch (contentType)
        {
            case ContentType.Article:
                var article = _context.Articles.Find(contentId);
                article.IsDeleted = true;
                _context.Articles.Update(article);

                await _commentRepository.DeleteArticleCommentsAsync(article.Id);

                break;
            case ContentType.Question:
                var question = _context.Questions.Find(contentId); //
                question.IsDeleted = true;
                _context.Questions.Update(question);

                await _answerRepository.DeleteAnswers(question.Id);

                break;
            case ContentType.Comment:
                var comment = _context.Comments.Find(contentId);
                comment.IsDeleted = true;
                _context.Comments.Update(comment);
                break;
            case ContentType.Answer:
                var answer = _context.Answers.Find(contentId);
                answer.IsDeleted = true;
                _context.Answers.Update(answer);
                break;
            case ContentType.Lesson:
                var lesson = _context.Lessons.Find(contentId);
                lesson.IsDeleted = true;
                _context.Lessons.Update(lesson);
                break;
            case ContentType.User:
                await FreezeUserAsync(contentId, DateTime.Now.AddDays(7));
                break;
            default:
                throw new Exception("Invalid content type");
        }

        await _context.SaveChangesAsync();

        var moderator = await _userRepository.GetCurrentUser();

        LogService.LogModeratorAction
            (ModeratorActions.Deleted, moderator.Id, $"content tpye: {contentType.ToString().ToLower()} with ID: {contentId}");

        return Task.CompletedTask;
    }

    /// <inheritdoc/>
    public async Task<Task> FreezeUserAsync(string userId, DateTime frozenUntil)
    {
        var user = await _userRepository.GetUserByIdAsync(userId);

        user.IsFrozen = true;
        user.FrozenUntil = frozenUntil;

        _context.Users.Update(user);

        await _context.SaveChangesAsync();

        LogService.LogUserAction(UserActions.Frozen, userId, $"until {frozenUntil.ToString()}");

        return Task.CompletedTask;
    }

    /// <inheritdoc/>
    public async Task<Task> UnfreezeUserAsync(string userId)
    {
        var user = await _userRepository.GetUserByIdAsync(userId);

        user.IsFrozen = false;
        user.FrozenUntil = null;

        _context.Users.Update(user);

        await _context.SaveChangesAsync();

        LogService.LogUserAction(UserActions.Unfrozen, userId);

        return Task.CompletedTask;
    }
}