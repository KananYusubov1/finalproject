﻿using AppDomain.DTO;
using AppDomain.DTOs.Pagination;
using AppDomain.DTOs.Resource;
using AppDomain.Entities.ContentRelated;
using AppDomain.Entities.UserActionRelated;
using AppDomain.Entities.TagBaseRelated;
using AppDomain.Enums;
using AppDomain.Exceptions.Common;
using AppDomain.Interfaces;
using Application.Services;
using Microsoft.EntityFrameworkCore;
using AppDomain.Exceptions.UserExceptions;
using AppDomain.Responses.UserResponses;
using System.Drawing.Printing;

namespace Infrastructure.Persistence;

public class ResourceRepository : IResourceRepository
{
    private readonly ClubrickDbContext _context;
    private readonly DapperContext _dapperContext;
    private readonly IUserRepository _userRepository;

    public ResourceRepository(
        ClubrickDbContext context,
        DapperContext dapperContext,
        IUserRepository userRepository
    )
    {
        _context = context;
        _dapperContext = dapperContext;
        _userRepository = userRepository;
    }

    public async Task<IEnumerable<GetLastResourcesDTO>> GetLastResourcesAsync()
    {
        var resources = _context.Resources
            .OrderByDescending(x => x.UpdateTime)
            .Take(5)
            .Select(x => x.ToLastResourceDTO())
            .ToList();

        return resources;
    }

    public async Task<string> InsertResource(Resource resource)
    {
        var user = await _userRepository.GetCurrentUser();

        if (user.IsFrozen)
            throw new UserForbiddenException(
                user.Id,
                "insert resource",
                user.FrozenUntil ?? DateTime.Now
            );

        try
        {
            //using var con = _dapperContext.CreateConnection();

            //var insertedId = con.Query<string>(
            //        "SELECT public.insert_resource(@r_id, @title, @url, @category_id, @resource_flag_id, @user_id)",
            //        new
            //        {
            //            @r_id = resource.Id,
            //            @title = resource.Title,
            //            @url = resource.Url,
            //            @category_id = resource.Category.Id,
            //            @resource_flag_id = resource.Flag.Id,
            //            @user_id = resource.UserId
            //        }
            //    )
            //    .FirstOrDefault();

            var resourceCategory = _context.Categories
                .Where(x => x.Id == resource.Category.Id)
                .FirstOrDefault();

            var resourceResourceFlag = _context.ResourceFlags
                .Where(x => x.Id == resource.Flag.Id)
                .FirstOrDefault();

            resourceCategory.UseCount++;

            resourceResourceFlag.UseCount++;

            resource.Flag = resourceResourceFlag;

            resource.Category = resourceCategory;

            var inserted = _context.Resources.Add(resource).Entity.Id;

            await _context.SaveChangesAsync();

            _context.Update(resourceResourceFlag);

            _context.Update(resourceCategory);

            await _userRepository.AddBudsAsync(
                ContentType.Resource,
                resource.Id,
                BudsActionType.PublishResourceToOwner
            );

            return inserted;
        }
        catch (Exception)
        {
            return "-1";
        }
    }

    public async Task<string> DeleteResource(string id)
    {
        try
        {
            var userId = _userRepository.GetClaimValue("userId");

            var user = await _context.Users
                .Include(r => r.SaveList)
                .Include(r => r.VoteList)
                .FirstOrDefaultAsync(u => u.Id == userId);

            if (user.IsFrozen)
                throw new UserForbiddenException(
                    user.Id,
                    "delete resource",
                    user.FrozenUntil ?? DateTime.Now
                );

            var resource = _context.Resources
                .Include(x => x.Category)
                .Include(x => x.Flag)
                .FirstOrDefault(x => x.Id == id);

            if (!resource.UserId.Equals(userId))
                return "0";

            var resourceCategory = _context.Categories
                .Where(x => x.Id == resource.Category.Id)
                .FirstOrDefault();

            var resourceResourceFlag = _context.ResourceFlags
                .Where(x => x.Id == resource.Flag.Id)
                .FirstOrDefault();

            resourceCategory.UseCount--;

            resourceResourceFlag.UseCount--;

            var saved = user.SaveList
                .Where(x => x.ContentId == resource.Id && x.ContentType == ContentType.Resource)
                .FirstOrDefault();

            var voted = user.VoteList
                .Where(x => x.ContentId == resource.Id && x.ContentType == ContentType.Resource)
                .FirstOrDefault();

            if (voted != null)
                user.VoteList.Remove(voted);

            if (saved != null)
                user.SaveList.Remove(saved);

            _context.Update(resourceCategory);

            _context.Update(resourceResourceFlag);

            _context.Update(user);

            var deleted = _context.Resources.Remove(resource).Entity.Id;

            await _context.SaveChangesAsync();

            return deleted;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async Task<int> SaveResource(string resourceId, string userId)
    {
        var user = await _context.Users
            .Include(r => r.SaveList)
            .FirstOrDefaultAsync(u => u.Id == userId);

        if (user.IsFrozen)
            throw new UserForbiddenException(
                user.Id,
                "save resource",
                user.FrozenUntil ?? DateTime.Now
            );

        var resource = await _context.Resources.FirstOrDefaultAsync(x => x.Id == resourceId);

        if (user is null || resource is null)
            return -2;

        var saveEntity = user.SaveList.FirstOrDefault(
            s => s.ContentType == ContentType.Resource && s.ContentId.Equals(resourceId)
        );

        if (saveEntity is not null)
            user.SaveList.Remove(saveEntity);
        else
        {
            Save save =
                new()
                {
                    Id = IDGeneratorService.GetShortUniqueId(),
                    ContentId = resourceId,
                    UserId = userId,
                    ContentType = ContentType.Resource
                };
            user.SaveList.Add(save);
        }

        _context.Update(user);

        try
        {
            await _context.SaveChangesAsync();
            return 0;
        }
        catch (Exception)
        {
            return -1;
        }
    }

    public async Task<int> VoteResource(VoteStatus status, string resourceId, string userId)
    {
        var user = _context.Users.Include(u => u.VoteList).FirstOrDefault(x => x.Id == userId);

        if (user.IsFrozen)
            throw new UserForbiddenException(
                user.Id,
                "vote resource",
                user.FrozenUntil ?? DateTime.Now
            );

        try
        {
            var resource = _context.Resources.FirstOrDefault(y => y.Id == resourceId);

            if (user is null || resource is null)
                return 0;

            var voteEntity = user.VoteList.FirstOrDefault(
                s => s.ContentType == ContentType.Resource && s.ContentId.Equals(resourceId)
            );

            if (voteEntity is null)
            {
                Vote vote =
                    new()
                    {
                        Id = IDGeneratorService.GetShortUniqueId(),
                        ContentId = resourceId,
                        UserId = userId,
                        ContentType = ContentType.Resource,
                        Status = status
                    };

                resource.VoteCount += status == VoteStatus.UpVote ? 1 : -1;
                user.VoteList.Add(vote);
            }
            else
            {
                if (status != voteEntity.Status)
                {
                    if (voteEntity.Status == VoteStatus.DownVote)
                        resource.VoteCount += 1;
                    else if (voteEntity.Status == VoteStatus.UpVote)
                        resource.VoteCount -= 1;
                    voteEntity.Status = status;
                    resource.VoteCount += status == VoteStatus.DownVote ? -1 : 1;
                    _context.Update(voteEntity);
                }
                else
                {
                    resource.VoteCount += status == VoteStatus.DownVote ? 1 : -1;
                    user.VoteList.Remove(voteEntity);
                }
            }

            _context.Users.Update(user);

            _context.Resources.Update(resource);

            await _context.SaveChangesAsync();

            return 1;
        }
        catch (Exception)
        {
            return -1;
        }
    }

    public async Task<ResourceResponse> GetResource(string resourceId)
    {
        var user = await _userRepository.GetCurrentUser();

        var resource =
            await _context.Resources
                .Include(r => r.Flag)
                .Include(r => r.Category)
                .FirstOrDefaultAsync(r => r.Id.Equals(resourceId))
            ?? throw new EntityNotFoundException(resourceId);

        var savedList = user?.SaveList ?? new();
        var voteList = user?.VoteList ?? new();

        return new()
        {
            Id = resource.Id,
            User = GetUserResponse(resource.UserId),
            Title = resource.Title,
            Url = resource.Url,
            SaveCount = _context.Saves.Where(sr => sr.ContentId.Equals(resource.Id)).Count(),
            IsSaved = savedList.FirstOrDefault(s => s.ContentId.Equals(resource.Id)) is not null,
            VisitCount = resource.VisitCount,
            Vote = ResourceVote(resource.Id, resource.VoteCount, voteList),
            Flag = new()
            {
                Id = resource.Flag.Id,
                AccentColor = resource.Flag.AccentColor,
                Title = resource.Flag.Title
            },
            Category = new()
            {
                Id = resource.Category.Id,
                AccentColor = resource.Category.AccentColor,
                Title = resource.Category.Title
            },
            UpdateTime = resource.UpdateTime
        };
    }

    public async Task<PaginatedListDto<ResourceResponse>> GetAllResources(
        string? searchQuery,
        string? categoryId,
        string? flagId,
        SortOptions sort,
        int page = 1
    )
    {
        IQueryable<Resource> query = _context.Resources
            .Where(c => !c.IsDeleted)
            .Include(r => r.Flag)
            .Include(r => r.Category);

        if (!string.IsNullOrWhiteSpace(categoryId))
            query = query.Where(r => r.Category.Id.Equals(categoryId));

        if (!string.IsNullOrWhiteSpace(flagId))
            query = query.Where(r => r.Flag.Id.Equals(flagId));

        if (!string.IsNullOrWhiteSpace(searchQuery))
            query = query.Where(
                r =>
                    r.Title.ToLower().Contains(searchQuery.ToLower())
                    || r.Url.ToLower().Contains(searchQuery.ToLower())
            );

        query = sort switch
        {
            SortOptions.Newest => query.OrderByDescending(r => r.UpdateTime),
            SortOptions.TopWeek
                => query
                    .OrderByDescending(r => r.VoteCount)
                    .Where(r => r.UpdateTime >= DateTime.UtcNow.AddDays(-7)),
            SortOptions.TopMonth
                => query
                    .OrderByDescending(r => r.VoteCount)
                    .Where(r => r.UpdateTime >= DateTime.UtcNow.AddMonths(-1)),
            SortOptions.TopYear
                => query
                    .OrderByDescending(r => r.VoteCount)
                    .Where(r => r.UpdateTime >= DateTime.UtcNow.AddYears(-1)),
            SortOptions.TopInfinity => query.OrderByDescending(r => r.VoteCount),
            SortOptions.Relevant => query.OrderByDescending(r => r.VisitCount),
            _
                => query
                    .OrderByDescending(r => _context.Saves.Count(r => r.ContentId.Equals(r.Id)))
                    .ThenByDescending(r => r.VisitCount)
        };

        var user = await _userRepository.GetCurrentUser();

        var savedList = user?.SaveList ?? new();
        var voteList = user?.VoteList ?? new();

        int pageSize = await _userRepository.GetContentPerPage();
        int totalCount = await query.CountAsync();

        var count = _context.Saves.Count();

        var paginationResources = await query
            .Skip((page - 1) * pageSize)
            .Take(pageSize)
            .Select(
                r =>
                    new ResourceResponse
                    {
                        Id = r.Id,
                        User = new() { UserId = r.UserId },
                        Title = r.Title,
                        Url = r.Url,
                        SaveCount = _context.Saves.Where(sr => sr.ContentId.Equals(r.Id)).Count(),
                        IsSaved = false,
                        VisitCount = r.VisitCount,
                        Vote = ResourceVote(r.Id, r.VoteCount, voteList),
                        Flag = new()
                        {
                            Id = r.Flag.Id,
                            AccentColor = r.Flag.AccentColor,
                            Title = r.Flag.Title
                        },
                        Category = new()
                        {
                            Id = r.Category.Id,
                            AccentColor = r.Category.AccentColor,
                            Title = r.Category.Title
                        },
                        UpdateTime = r.UpdateTime
                    }
            )
            .ToListAsync();

        paginationResources.ForEach(r =>
        {
            r.IsSaved = savedList.FirstOrDefault(s => s.ContentId.Equals(r.Id)) is not null;
            r.User = GetUserResponse(r.User.UserId);
        });

        return new PaginatedListDto<ResourceResponse>(
            paginationResources,
            new PaginationMeta(page, pageSize, totalCount)
        );
    }

    public async Task<PaginatedListDto<ResourceResponse>> GetAllSavedResources(
        string? searchQuery,
        string? categoryId,
        string? flagId,
        SortOptions sort,
        int page = 1
    )
    {
        IQueryable<Resource> query = _context.Resources
            .Where(c => !c.IsDeleted)
            .Include(r => r.Flag)
            .Include(r => r.Category);

        var user = await _userRepository.GetCurrentUser();

        var savedList = user?.SaveList ?? new();
        var voteList = user?.VoteList ?? new();

        if (!string.IsNullOrWhiteSpace(categoryId))
            query = query.Where(r => r.Category.Id.Equals(categoryId));

        if (!string.IsNullOrWhiteSpace(flagId))
            query = query.Where(r => r.Flag.Id.Equals(flagId));

        if (!string.IsNullOrWhiteSpace(searchQuery))
            query = query.Where(
                r =>
                    r.Title.ToLower().ToLower().Contains(searchQuery.ToLower())
                    || r.Url.ToLower().Contains(searchQuery.ToLower())
            );

        query = sort switch
        {
            SortOptions.Relevant => query.OrderByDescending(r => r.VisitCount),
            SortOptions.Newest => query.OrderByDescending(r => r.UpdateTime),
            SortOptions.TopWeek
                => query
                    .OrderByDescending(r => r.VoteCount)
                    .Where(r => r.UpdateTime >= DateTime.UtcNow.AddDays(-7)),
            SortOptions.TopMonth
                => query
                    .OrderByDescending(r => r.VoteCount)
                    .Where(r => r.UpdateTime >= DateTime.UtcNow.AddMonths(-1)),
            SortOptions.TopYear
                => query
                    .OrderByDescending(r => r.VoteCount)
                    .Where(r => r.UpdateTime >= DateTime.UtcNow.AddYears(-1)),
            SortOptions.TopInfinity => query.OrderByDescending(r => r.VoteCount),
            _
                => query
                    .OrderByDescending(r => _context.Saves.Count(r => r.ContentId == r.Id))
                    .ThenByDescending(r => r.VisitCount)
        };

        int pageSize = await _userRepository.GetContentPerPage();
        int totalCount = await query.CountAsync();

        var resources = await query.ToListAsync();
        var filteredResources = resources.Where(r => savedList.Any(s => s.ContentId == r.Id));

        var paginationResources = filteredResources
            .Skip((page - 1) * pageSize)
            .Take(pageSize)
            .Select(
                r =>
                    new ResourceResponse
                    {
                        Id = r.Id,
                        Title = r.Title,
                        User = new() { UserId = r.UserId },
                        Url = r.Url,
                        SaveCount = _context.Saves.Where(sr => sr.ContentId.Equals(r.Id)).Count(),
                        VisitCount = r.VisitCount,
                        IsSaved = true,
                        Vote = ResourceVote(r.Id, r.VoteCount, voteList),
                        Flag = new()
                        {
                            Id = r.Flag.Id,
                            AccentColor = r.Flag.AccentColor,
                            Title = r.Flag.Title
                        },
                        Category = new()
                        {
                            Id = r.Category.Id,
                            AccentColor = r.Category.AccentColor,
                            Title = r.Category.Title
                        },
                        UpdateTime = r.UpdateTime
                    }
            )
            .ToList();

        paginationResources.ForEach(r =>
        {
            r.User = GetUserResponse(r.User.UserId);
        });

        return new PaginatedListDto<ResourceResponse>(
            paginationResources,
            new PaginationMeta(page, pageSize, totalCount)
        );
    }

    private UserContentTitleResponse GetUserResponse(string userId)
    {
        var user = _context.Users.First(u => u.Id.Equals(userId));
        return new()
        {
            UserId = userId,
            Name = user.Name,
            Username = user.Username,
            ProfilePhoto = user.ProfilePhoto,
        };
    }

    private int GetSaveCount(string resourceId)
    {
        var count = _context.Saves.Count(r => r.ContentId.Equals(resourceId));

        return count;
    }

    public async Task<Task> VisitedUrl(string resourceId)
    {
        var resource =
            await _context.Resources.FirstOrDefaultAsync(y => y.Id == resourceId)
            ?? throw new EntityNotFoundException(resourceId);

        resource.VisitCount++;

        _context.Update(resource);

        await _context.SaveChangesAsync();

        await _userRepository.AddBudsAsync(
            ContentType.Resource,
            resource.Id,
            BudsActionType.VisitResourceToOwner,
            resource.UserId
        );

        return Task.CompletedTask;
    }

    private static VoteDTO ResourceVote(string resourceId, int voteCount, List<Vote> votedList)
    {
        return new VoteDTO
        {
            VoteCount = voteCount,
            Status =
                votedList.FirstOrDefault(v => v.ContentId.Equals(resourceId))?.Status
                ?? VoteStatus.UnVote
        };
    }

    public async Task<string> EditResource(EditResourceDTO editResourceDTO)
    {
        var user = await _userRepository.GetCurrentUser();

        if (user.IsFrozen)
            throw new UserForbiddenException(
                user.Id,
                "edit resource",
                user.FrozenUntil ?? DateTime.Now
            );

        try
        {
            var findResource = _context.Resources
                .Include(x => x.Category)
                .Include(x => x.Flag)
                .FirstOrDefault(x => x.Id == editResourceDTO.ResourceId && x.UserId == user.Id);

            if (findResource is null)
                return "0";

            if (
                editResourceDTO.CategoryId is null
                && editResourceDTO.ResourceFlagId is null
                && editResourceDTO.Title is null
            )
                return editResourceDTO.ResourceId;

            Category category = null;

            ResourceFlag resourceFlag = null;

            if (editResourceDTO.CategoryId != null)
                category = _context.Categories.FirstOrDefault(
                    x => x.Id == editResourceDTO.CategoryId
                );

            if (editResourceDTO.ResourceFlagId != null)
                resourceFlag = _context.ResourceFlags.FirstOrDefault(
                    x => x.Id == editResourceDTO.ResourceFlagId
                );

            if (editResourceDTO.Title != null)
                findResource.Title = editResourceDTO.Title;

            if (editResourceDTO.CategoryId != null)
            {
                if (category is null)
                    return "0";
                findResource.Category = category;
            }

            if (editResourceDTO.ResourceFlagId != null)
            {
                if (resourceFlag is null)
                    return "0";
                findResource.Flag = resourceFlag;
            }

            findResource.UpdateTime = DateTime.UtcNow;

            _context.Resources.Update(findResource);

            await _context.SaveChangesAsync();

            return findResource.Id;
        }
        catch (Exception)
        {
            return "-1";
        }
    }

    public Task<int> GetResourcesCount() => _context.Resources.CountAsync();

    public async Task<List<ResourceResponse>> GetAllUserResources()
    {
        try
        {
            var user =
                await _userRepository.GetCurrentUser()
                ?? throw new UserNotFoundException("GetAllUserResources");

            IQueryable<Resource> query = _context.Resources.Where(
                c => !c.IsDeleted && c.UserId == user.Id
            );

            var savedList = user?.SaveList ?? new();

            var voteList = user?.VoteList ?? new();

            var count = _context.Saves.Count();

            var resources = await query
                .Select(
                    r =>
                        new ResourceResponse
                        {
                            Id = r.Id,
                            User = new()
                            {
                                UserId = r.UserId,
                                Name = user.Name,
                                ProfilePhoto = user.ProfilePhoto,
                                Username = user.Username
                            },
                            Title = r.Title,
                            Url = r.Url,
                            SaveCount = _context.Saves
                                .Where(sr => sr.ContentId.Equals(r.Id))
                                .Count(),
                            IsSaved = false,
                            VisitCount = r.VisitCount,
                            Vote = ResourceVote(r.Id, r.VoteCount, voteList),
                            Flag = new()
                            {
                                Id = r.Flag.Id,
                                AccentColor = r.Flag.AccentColor,
                                Title = r.Flag.Title
                            },
                            Category = new()
                            {
                                Id = r.Category.Id,
                                AccentColor = r.Category.AccentColor,
                                Title = r.Category.Title
                            },
                            UpdateTime = r.UpdateTime
                        }
                )
                .OrderByDescending(x => x.UpdateTime)
                .ToListAsync();

            return resources;
        }
        catch (Exception)
        {
            throw;
        }
    }
}
