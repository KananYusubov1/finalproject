﻿using AppDomain.Common.Config;
using AppDomain.DTOs.Article;
using AppDomain.DTOs.Notification;
using AppDomain.DTOs.Settings;
using AppDomain.DTOs.User;
using AppDomain.Entities.ContentRelated;
using AppDomain.Entities.NotificationRelated.AdditionalDetails;
using AppDomain.Entities.UserActionRelated;
using AppDomain.Entities.UserRelated;
using AppDomain.Enums;
using AppDomain.Exceptions.Common;
using AppDomain.Exceptions.UserExceptions;
using AppDomain.Interfaces;
using AppDomain.Responses;
using AppDomain.Responses.UserResponses;
using AppDomain.ValueObjects;
using Application.Services;
using Infrastructure.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;

namespace Infrastructure.Persistence;

/// <summary>
/// Repository for managing user-related operations.
/// </summary>
public class UserRepository : IUserRepository
{
    private readonly ClubrickDbContext _context;
    private readonly ICryptService _cryptService;
    private readonly IAzureBlobStorageService _azureBlobStorageService;
    private readonly IEmailService _emailService;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly INotificationRepository _notificationRepository;
    private readonly UserDefaults _userDefaults;

    /// <summary>
    /// Initializes a new instance of the <see cref="UserRepository"/> class.
    /// </summary>
    /// <param name="context">The database context for accessing user-related data.</param>
    /// <param name="userDefaults">The user defaults configuration.</param>
    /// <param name="cryptService">The service for cryptographic operations.</param>
    /// <param name="httpContextAccessor">The HTTP context accessor for accessing HTTP-related information.</param>
    /// <param name="notificationRepository">The repository for notification-related operations.</param>
    /// <param name="azureBlobStorageService">The service for Azure Blob Storage operations.</param>
    public UserRepository(
        ClubrickDbContext context,
        UserDefaults userDefaults,
        ICryptService cryptService,
        IHttpContextAccessor httpContextAccessor,
        INotificationRepository notificationRepository,
        IAzureBlobStorageService azureBlobStorageService
    )
    {
        _context = context;
        _cryptService = cryptService;
        _userDefaults = userDefaults;
        _httpContextAccessor = httpContextAccessor;
        _notificationRepository = notificationRepository;
        _azureBlobStorageService = azureBlobStorageService;
    }

    public async Task<int> GetAllTimeBudsAsync()
    {
        var user = await GetCurrentUser();

        return user.AllTimeBudsPoints;
    }

    public async Task<int> GetUsableBudsAsync()
    {
        var user = await GetCurrentUser();

        return user.UsableBudsPoints;
    }

    /// <inheritdoc/>
    public IEnumerable<LeaderboardResponse> GetLeaderboard()
    {
        var wholeboard = _context.Users
            .OrderByDescending(user => user.AllTimeBudsPoints)
            .ThenByDescending(user => user.JoinedTime);

        var leaderboard = wholeboard.Take(10).Select(user => user.ToLeaderboardResponse()).ToList();

        int rank = 1;
        foreach (var user in leaderboard)
            user.Rank = rank++;

        return leaderboard;
    }

    /// <inheritdoc/>
    public async Task AddBudsAsync(
        ContentType contentType,
        string contentId,
        BudsActionType budsAction,
        string userId = null
    )
    {
        var user = userId is null ? await GetCurrentUser() : await _context.Users.FindAsync(userId);

        bool viewed = false;

        if (budsAction == BudsActionType.WriteAnswerToUser)
        {
            var answers = _context.Answers;

            int countOfAnswersOfUserToGivenQuestion = answers
                .Where(
                    ans =>
                        ans.UserId == user.Id
                        && ans.QuestionId
                            == _context.Questions
                                .FirstOrDefault(
                                    que =>
                                        que.Id
                                        == answers
                                            .FirstOrDefault(ans => ans.Id == contentId)
                                            .QuestionId
                                )
                                .Id
                )
                .Count();

            if (countOfAnswersOfUserToGivenQuestion > 3)
                viewed = true;
        }
        else
        {
            viewed = _context.Views.Any(
                view =>
                    view.UserId == user.Id
                    && view.ContentId == contentId
                    && view.ContentType == contentType
            );
        }

        if (
            !viewed
            || budsAction == BudsActionType.CorrectAnswerToUser
            || budsAction == BudsActionType.ProductPurchase
        )
        {
            var view = new View()
            {
                Id = IDGeneratorService.GetShortUniqueId(),
                UserId = user.Id,
                ContentId = contentId,
                ContentType = contentType
            };

            _context.Views.Add(view);

            var buds = budsAction switch
            {
                BudsActionType.ViewArticleToUser => 4,
                BudsActionType.ViewArticleToOwner => 3,
                BudsActionType.PublishArticleToOwner => 10,
                BudsActionType.VisitResourceToOwner => 2,
                BudsActionType.WriteAnswerToUser => 4,
                BudsActionType.CorrectAnswerToUser => 3,
                BudsActionType.PublishQuestionToOwner => 5,
                BudsActionType.PublishResourceToOwner => 3,
                BudsActionType.ProductPurchase => ProcessProductPurchase(contentId, user), // test it
                _ => 0,
            };

            if (budsAction != BudsActionType.ProductPurchase)
            {
                user.AllTimeBudsPoints += buds;
                user.UsableBudsPoints += buds;
            }

            await _notificationRepository.PostNotificationAsync(
                new NotificationDTO
                {
                    Type = NotificationType.BudsRelated,
                    AdditionalDetails = JsonConvert.SerializeObject(
                        new BudsDetails
                        {
                            BudsEntryId = contentId,
                            BudsEntryType = contentType,
                            BudsEarnerId = user.Id,
                            BudsAmount = buds,
                            ForWhat = budsAction
                        }
                    )
                }
            );

            _context.Users.Update(user);

            await _context.SaveChangesAsync();
        }
    }

    public int ProcessProductPurchase(string productId, User user)
    {
        var productNotification = user.NotificationList
            .Where(not => not.Type == NotificationType.ProductPurchase)
            .Last();

        var productResponse = JsonConvert.DeserializeObject<ProductPurchaseNotificationResponse>(
            productNotification.Body
        );

        var product = productResponse.Product;

        if (productResponse.Product.Id != productId)
            throw new Exception("Product is not exist.");

        int subtractedBuds = (int)(productResponse.PurchaseQuantity * product.Price);

        return -subtractedBuds;
    }

    /// <inheritdoc/>
    public async Task<Task> AddConnectedAccountAsync(
        ConnectedAccountType type,
        string ProviderId,
        string Username,
        string email
    )
    {
        var user = await GetCurrentUser();

        var account = new ConnectedAccount
        {
            Id = IDGeneratorService.GetShortUniqueId(),
            ProviderId = ProviderId,
            Username = Username,
            Email = email,
            AccountType = type
        };

        user.ConnectedAccountList.Add(account);

        _context.Users.Update(user);

        await _context.SaveChangesAsync();

        LogService.LogUserAction(
            UserActions.Added,
            user.Id,
            $"{type.ToString().ToLower()} account"
        );

        return Task.CompletedTask;
    }

    /// <inheritdoc/>
    public async Task<Task> ChangeRoleAsync(UserRole role, string userId)
    {
        var user = await GetUserByIdAsync(userId);

        user.Role = role;

        _context.Users.Update(user);

        await _context.SaveChangesAsync();

        LogService.LogUserAction(
            UserActions.Changed,
            userId,
            $"to {role.ToString().ToLower()} role"
        );

        return Task.CompletedTask;
    }

    /// <inheritdoc/>
    public async Task<Task> DeleteUserActionRelationsAsync(
        string contentId,
        ContentType contentType
    )
    {
        var userId = GetClaimValue("userId");

        var user = await _context.Users
            .Include(r => r.SaveList)
            .Include(r => r.VoteList)
            .Include(r => r.ViewList)
            .FirstOrDefaultAsync(u => u.Id == userId);

        // Save
        var saveEntity = user.SaveList.FirstOrDefault(
            s => s.ContentType == contentType && s.ContentId.Equals(contentId)
        );
        if (saveEntity is not null)
            user.SaveList.Remove(saveEntity);

        // Vote
        var voteEntity = user.VoteList.FirstOrDefault(
            s => s.ContentType == contentType && s.ContentId.Equals(contentId)
        );
        if (voteEntity is not null)
            user.VoteList.Remove(voteEntity);

        // View
        var viewEntity = user.ViewList.FirstOrDefault(
            s => s.ContentType == contentType && s.ContentId.Equals(contentId)
        );
        if (viewEntity is not null)
            user.ViewList.Remove(viewEntity);

        _context.Update(user);

        await _context.SaveChangesAsync();

        LogService.LogUserAction(
            UserActions.Deleted,
            user.Id,
            $"actions related to {contentType.ToString().ToLower()}"
        );

        return Task.CompletedTask;
    }

    //public async Task<IEnumerable<string>> GetDeletedContentsAsync()

    /// <inheritdoc/>
    public async Task<User> DeleteUserAsync(string id)
    {
        var user = await GetUserByIdAsync(id);

        user.IsDeleted = true;

        await _context.SaveChangesAsync();

        await DeleteAllUserRelationsAsync();

        LogService.LogUserAction(UserActions.Deleted, user.Id, "account");

        return user;
    }

    /// <inheritdoc/>
    public async Task DeleteAllUserRelationsAsync()
    {
        var userId = GetClaimValue("userId");

        await _context.Comments
            .Where(u => u.UserId == userId)
            .ForEachAsync(comment =>
            {
                comment.IsDeleted = true;
                _context.Comments.Update(comment);
            });

        await _context.Articles
            .Where(u => u.UserId == userId)
            .ForEachAsync(article =>
            {
                article.IsDeleted = true;
                _context.Articles.Update(article);
            });

        await _context.Questions
            .Where(u => u.UserId == userId)
            .ForEachAsync(question =>
            {
                question.IsDeleted = true;
                _context.Questions.Update(question);
            });

        await _context.Answers
            .Where(u => u.UserId == userId)
            .ForEachAsync(answer =>
            {
                answer.IsDeleted = true;
                _context.Answers.Update(answer);
            });

        await _context.Follows
            .Where(u => u.FollowerId == userId || u.FollowingId == userId)
            .ForEachAsync(follow =>
            {
                follow.IsDeleted = true;
                _context.Follows.Update(follow);
            });

        await _context.Resources
            .Where(u => u.UserId == userId)
            .ForEachAsync(resource =>
            {
                resource.IsDeleted = true;
                _context.Resources.Update(resource);
            });

        await _context.Saves
            .Where(u => u.UserId == userId)
            .ForEachAsync(save =>
            {
                save.IsDeleted = true;
                _context.Saves.Update(save);
            });

        await _context.Views
            .Where(u => u.UserId == userId)
            .ForEachAsync(view =>
            {
                view.IsDeleted = true;
                _context.Views.Update(view);
            });

        await _context.Votes
            .Where(u => u.UserId == userId)
            .ForEachAsync(vote =>
            {
                vote.IsDeleted = true;
                _context.Votes.Update(vote);
            });

        await _context.Watches
            .Where(u => u.UserId == userId)
            .ForEachAsync(watch =>
            {
                watch.IsDeleted = true;
                _context.Watches.Update(watch);
            });

        await _context.Comments
            .Where(u => u.UserId == userId)
            .ForEachAsync(comment =>
            {
                comment.IsDeleted = true;
                _context.Comments.Update(comment);
            });

        await _context.SaveChangesAsync();
    }

    /// <inheritdoc/>
    public async Task<Task> RestoreUserRelationsAsync(string userId)
    {
        var user = await GetUserByIdAsync(userId);

        if (user is null)
            throw new Exception("User not Found");

        await _context.Comments
            .Where(u => u.UserId == userId)
            .ForEachAsync(comment =>
            {
                comment.IsDeleted = false;
                _context.Comments.Update(comment);
            });

        await _context.Articles
            .Where(u => u.UserId == userId)
            .ForEachAsync(article =>
            {
                article.IsDeleted = false;
                _context.Articles.Update(article);
            });

        await _context.Questions
            .Where(u => u.UserId == userId)
            .ForEachAsync(question =>
            {
                question.IsDeleted = false;
                _context.Questions.Update(question);
            });

        await _context.Answers
            .Where(u => u.UserId == userId)
            .ForEachAsync(answer =>
            {
                answer.IsDeleted = false;
                _context.Answers.Update(answer);
            });

        await _context.Follows
            .Where(u => u.FollowerId == userId || u.FollowingId == userId)
            .ForEachAsync(follow =>
            {
                follow.IsDeleted = false;
                _context.Follows.Update(follow);
            });

        await _context.Resources
            .Where(u => u.UserId == userId)
            .ForEachAsync(resource =>
            {
                resource.IsDeleted = false;
                _context.Resources.Update(resource);
            });

        await _context.Saves
            .Where(u => u.UserId == userId)
            .ForEachAsync(save =>
            {
                save.IsDeleted = false;
                _context.Saves.Update(save);
            });

        await _context.Views
            .Where(u => u.UserId == userId)
            .ForEachAsync(view =>
            {
                view.IsDeleted = false;
                _context.Views.Update(view);
            });

        await _context.Votes
            .Where(u => u.UserId == userId)
            .ForEachAsync(vote =>
            {
                vote.IsDeleted = false;
                _context.Votes.Update(vote);
            });

        await _context.Watches
            .Where(u => u.UserId == userId)
            .ForEachAsync(watch =>
            {
                watch.IsDeleted = false;
                _context.Watches.Update(watch);
            });

        await _context.SaveChangesAsync();

        return Task.CompletedTask;
    }

    /// <inheritdoc/>
    public async Task<Task> FollowUserAsync(string whoIsFollowingId) // Kim təqib edilir yəni qarşı tərəf
    {
        var user = await GetCurrentUser();

        var whoIsFollowing = await GetUserByIdAsync(whoIsFollowingId);

        if (user.IsFrozen)
            throw new UserForbiddenException(
                user.Id,
                "follow user",
                user.FrozenUntil ?? DateTime.Now
            );

        var followCheck = _context.Follows.FirstOrDefault(
            x => x.FollowerId == user.Id && x.FollowingId == whoIsFollowingId
        );

        if (followCheck is not null)
        {
            _context.Follows.Remove(followCheck);

            LogService.LogUserAction(
                UserActions.UnFollows,
                user.Id,
                $"user with ID: {whoIsFollowingId}"
            );
        }
        else
        {
            var newFollowInstance = new Follow
            {
                Id = IDGeneratorService.GetUniqueId(),
                FollowerId = user.Id, // Kim təqib edir yəni biz
                FollowingId = whoIsFollowingId
            };

            _context.Follows.Add(newFollowInstance);

            var followInstanceDetails = new FollowDetails
            {
                FollowInstanceId = newFollowInstance.Id,
            };

            await _notificationRepository.PostNotificationAsync(
                new()
                {
                    Type = NotificationType.Follow,
                    AdditionalDetails = JsonConvert.SerializeObject(followInstanceDetails),
                }
            );

            LogService.LogUserAction(
                UserActions.Follows,
                user.Id,
                $"user with ID: {whoIsFollowingId}"
            );
        }

        await _context.SaveChangesAsync();

        return Task.CompletedTask;
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<RepositoryResponse>> GetAllRepositoriesAsync()
    {
        var user = await GetCurrentUser();

        var username = user.ConnectedAccountList
            .FirstOrDefault(x => x.AccountType == ConnectedAccountType.GitHub)
            .Username;

        //var username = "CoraEpiro";

        HttpClient client = new();

        client.DefaultRequestHeaders.Add("User-Agent", "request");

        HttpResponseMessage response = client
            .GetAsync($"https://api.github.com/users/{username}/repos")
            .Result;

        var repos = new List<RepositoryResponse>();

        if (response.StatusCode is HttpStatusCode.OK)
        {
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var repositories = JsonConvert.DeserializeObject<dynamic>(responseBody);

            foreach (var repository in repositories)
            {
                var repoPrivate = (bool)repository["private"];

                if (repoPrivate)
                    continue;

                var repoId = repository["id"].ToString();
                var repoName = repository["name"].ToString();
                var repoFork = (bool)repository["fork"];
                var isSelected =
                    user.PersonalInfo?.PinnedRepositories?.Any(r => r.RepoId.Equals(repoId))
                    ?? false;

                var repo = new RepositoryResponse(repoId, repoName, repoFork, isSelected);

                repos.Add(repo);
            }
        }
        else
            throw new EntityNotFoundException(username);

        return repos;
    }

    public async Task<Task> UpdatePinnedArticlesAsync(string articleId)
    {
        var user = await GetCurrentUser();

        var pinnedArticles = user.PersonalInfo.PinnedArticles ?? new();

        try
        {
            var id = pinnedArticles.First(x => x == articleId);

            pinnedArticles.Remove(id);

            LogService.LogUserAction(
                UserActions.Deleted,
                user.Id,
                $"pinned article with ID: {articleId}"
            );
        }
        catch (Exception)
        {
            await IsArticleExistAsync(articleId);

            pinnedArticles.Add(articleId);

            LogService.LogUserAction(
                UserActions.Added,
                user.Id,
                $"pinned article with ID: {articleId}"
            );
        }

        user.PersonalInfo.PinnedArticles = pinnedArticles;

        _context.Users.Update(user);

        await _context.SaveChangesAsync();

        return Task.CompletedTask;
    }

    /// <inheritdoc/>
    public async Task<Task> UpdatePinnedRepositoriesAsync(string repositoryId)
    {
        var user = await GetCurrentUser();

        var pinnedRepositories = user.PersonalInfo.PinnedRepositories ?? new();

        try
        {
            var repo = pinnedRepositories.First(x => x.RepoId == repositoryId);

            pinnedRepositories.Remove(repo);

            LogService.LogUserAction(
                UserActions.Deleted,
                user.Id,
                $"pinned repository with ID: {repositoryId}"
            );
        }
        catch (Exception)
        {
            var newPinnedRepo = await GetRepositoryAsyncById(repositoryId);

            pinnedRepositories.Add(newPinnedRepo);

            LogService.LogUserAction(
                UserActions.Added,
                user.Id,
                $"pinned repository with ID: {repositoryId}"
            );
        }

        user.PersonalInfo.PinnedRepositories = pinnedRepositories;

        _context.Users.Update(user);

        await _context.SaveChangesAsync();

        return Task.CompletedTask;
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<UserProfileResponse>> GetAllUsersAsync(string search)
    {
        var users = _context.Users
            .Where(
                user =>
                    user.Username.ToLower().Contains(search.ToLower())
                    || user.Name.ToLower().Contains(search.ToLower())
            )
            .ToList();

        var userProfiles = new List<UserProfileResponse>();

        foreach (var user in users)
        {
            var userProfile = ModelConvertors.ToUserProfileResponse(user);

            userProfile.IsUserFollowed = await IsCurrentUserFollowAsync(user.Email);

            userProfiles.Add(userProfile);
        }

        return userProfiles;
    }

    /// <inheritdoc/>
    public string GetClaimValue(string claimType)
    {
        var token = GetTokenFromRequest();

        if (token is null)
            return null;

        var claim = token.Claims.FirstOrDefault(c => c.Type == claimType);

        return claim?.Value;
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<EmailResponse>> GetConnectedEmailsAsync()
    {
        var user = await GetCurrentUser();

        var emails = new List<EmailResponse>
        {
            new EmailResponse { Email = user.Email, Title = "Primary" }
        };

        if (!string.IsNullOrWhiteSpace(user.PersonalInfo.DisplayEmail))
            emails.Add(
                new EmailResponse { Email = user.PersonalInfo.DisplayEmail, Title = "Display" }
            );

        foreach (var account in user.ConnectedAccountList)
            emails.Add(
                new EmailResponse { Email = account.Email, Title = account.AccountType.ToString() }
            );

        return emails;
    }

    /// <inheritdoc/>
    public async Task<int> GetContentPerPage()
    {
        var user = await GetCurrentUser();

        int pageSize = user?.Settings?.ContentPerPage ?? _userDefaults.ContentPerPage;

        return pageSize;
    }

    /// <inheritdoc/>
    public async Task<UserPreviewResponse> GetMe()
    {
        var user = await GetCurrentUser();

        return user.ToUserPreviewResponse();
    }

    /// <inheritdoc/>
    public async Task<User> GetCurrentUser()
    {
        var id = GetClaimValue("userId");

        if (id is null)
            return null;

        var user = _context.Users
            .Include(r => r.SaveList)
            .Include(r => r.VoteList)
            .Include(r => r.ViewList)
            .SingleOrDefault(u => u.Id.Equals(id));

        if (user is null)
            return null;

        return user;
    }

    /// <inheritdoc/>
    public async Task<string> GetCurrentUserId()
    {
        var userId = GetClaimValue("userId");

        return userId;
    }

    /// <inheritdoc/>
    public async Task<CustomizationSettings> GetCustomizationAsync()
    {
        var user = await GetCurrentUser();

        var customization = new CustomizationSettings
        {
            ContentPerPage = user.Settings.ContentPerPage,
            Theme = user.Settings.Theme,
            AccentColor = user.Brand.AccentColor,
            BannerPhoto = user.Brand.BannerPhoto
        };

        return customization;
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<UserProfileResponse>> GetFollowersAsync()
    {
        var user = await GetCurrentUser();

        var followersId = _context.Follows
            .Where(x => x.FollowingId == user.Id)
            .Select(x => x.FollowerId)
            .ToList();

        var followers = new List<UserProfileResponse>();

        foreach (var id in followersId)
        {
            var userProfile = await GetUserProfileAsync(id);

            followers.Add(userProfile);
        }

        return followers;
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<UserProfileResponse>> GetFollowingsAsync()
    {
        var user = await GetCurrentUser();

        var followingsId = _context.Follows
            .Where(x => x.FollowerId == user.Id)
            .Select(x => x.FollowingId)
            .ToList();

        var followings = new List<UserProfileResponse>();

        foreach (var id in followingsId)
        {
            var userProfile = await GetUserProfileAsync(id);

            followings.Add(userProfile);
        }

        return followings;
    }

    /// <inheritdoc/>
    public async Task<PersonalInfoResponse> GetPersonalInfoAsync()
    {
        var user = await GetCurrentUser();

        var personalInfo = new PersonalInfoResponse();
        personalInfo.PersonalInfo = user.PersonalInfo;

        return personalInfo;
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<PinnedRepository>> GetPinnedRepositoriesAsync(string userId)
    {
        var user = await GetUserByIdAsync(userId);

        var pinnedRepositories = user.PersonalInfo.PinnedRepositories;

        return pinnedRepositories;
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<GetAllArticleDTO>> GetPinnedArticlesAsync(string userId)
    {
        var user = await GetUserByIdAsync(userId);

        var pinnedRepositories = new List<GetAllArticleDTO>();

        foreach (var articleId in user.PersonalInfo.PinnedArticles)
        {
            var article = await GetArticle(articleId);
            var dto = await article.ToArticleDTO(_context);
            pinnedRepositories.Add(dto);
        }

        return pinnedRepositories;
    }

    /// <inheritdoc/>
    public async Task<Article> GetArticle(string articleId)
    {
        var article = await _context.Articles.FindAsync(articleId);

        return article;
    }

    /// <inheritdoc/>
    public async Task<ProfileSettingsDTO> GetProfileSettingsAsync()
    {
        var user = await GetCurrentUser();

        return new ProfileSettingsDTO()
        {
            Name = user.Name,
            Username = user.Username,
            Avatar = user.ProfilePhoto,
            AvailableFor = user.PersonalInfo.AvailableFor,
            Bio = user.PersonalInfo.Bio,
            Location = user.PersonalInfo.Location,
            WebsiteUrl = user.PersonalInfo.WebsiteUrl,
            CurrentlyLearning = user.PersonalInfo.CurrentlyLearning,
            CurrentlyWorking = user.PersonalInfo.CurrentlyWorking,
            Education = user.PersonalInfo.Education,
            Work = user.PersonalInfo.Work,
        };
    }

    /// <inheritdoc/>
    public async Task<User> GetUserByEmailAsync(string email)
    {
        var user =
            await _context.Users.FirstOrDefaultAsync(x => x.Email == email)
            ?? throw new EntityNotFoundException(email);

        return user;
    }

    /// <inheritdoc/>
    public async Task<User> GetUserByIdAsync(string id)
    {
        var user =
            _context.Users.FirstOrDefault(x => x.Id.Equals(id))
            ?? throw new EntityNotFoundException(id);

        return user;
    }

    /// <inheritdoc/>
    public async Task<User> GetUserBySelectorAsync(Selector selector, string identifier)
    {
        var user = new User();

        switch (selector)
        {
            case Selector.ById:
                user = await GetUserByIdAsync(identifier);
                break;
            case Selector.ByEmail:
                user = await GetUserByEmailAsync(identifier);
                break;
            case Selector.ByUsername:
                user = await GetUserByUsernameAsync(identifier);
                break;
            case Selector.ByUsersecret:
                user = await GetUserByUserSecretAsync(identifier);
                break;
            default:
                throw new EntityNotFoundException(
                    "Selector: " + selector + " - " + "Identifier: " + identifier
                );
        }

        return user;
    }

    /// <inheritdoc/>
    public async Task<User> GetUserByUsernameAsync(string username)
    {
        var user = await _context.Users.FirstOrDefaultAsync(x => x.Username == username);

        return user;
    }

    /// <inheritdoc/>
    public async Task<User> GetUserByUserSecretAsync(string usersecret)
    {
        var user = await _context.Users.FirstOrDefaultAsync(x => x.UserSecret == usersecret);

        return user!;
    }

    /// <inheritdoc/>
    public async Task<UserProfileResponse> GetUserProfileAsync()
    {
        var user = await GetCurrentUser();

        var profile = ModelConvertors.ToUserProfileResponse(user);

        return profile;
    }

    /// <inheritdoc/>
    public async Task<UserProfileResponse> GetUserProfileAsync(string userId)
    {
        var user = await GetUserByIdAsync(userId);

        var profile = ModelConvertors.ToUserProfileResponse(user);

        return profile;
    }

    /// <inheritdoc/>
    public async Task<UserViewResponsePublishedCounts> GetUserViewResponsePublishedCountsAsync(
        string userId
    )
    {
        var counts = new UserViewResponsePublishedCounts();

        counts.PublishedAnswerCount = await _context.Answers.CountAsync(x => x.UserId == userId);
        counts.PublishedArticlesCount = await _context.Articles.CountAsync(x => x.UserId == userId);
        counts.PublishedQuestionCount = await _context.Questions.CountAsync(
            x => x.UserId == userId
        );

        return counts;
    }

    /// <inheritdoc/>
    public async Task<bool> IsConnectedAccountTypeExistAsync(ConnectedAccountType newType)
    {
        var user = await GetCurrentUser();

        return user.ConnectedAccountList.Any(a => a.AccountType.Equals(newType));
    }

    /// <inheritdoc/>
    public async Task<bool> IsCurrentUserFollowAsync(string targetEmail)
    {
        var user = await GetCurrentUser();

        if (user is null)
            return false;

        var targetUser = await GetUserByEmailAsync(targetEmail);

        var isFollow = _context.Follows.Any(
            x => x.FollowerId == user.Id && x.FollowingId == targetUser.Id
        );

        return isFollow;
    }

    /// <inheritdoc/>
    public async Task<bool> IsEmailExistAsync(string email)
    {
        try
        {
            var user =
                await _context.Users.FirstOrDefaultAsync(x => x.Email == email)
                ?? await _context.PendingUsers.FirstOrDefaultAsync(x => x.Email == email);

            return user is not null;
        }
        catch (EntityNotFoundException)
        {
            return false;
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <inheritdoc/>
    public async Task<bool> IsUsernameExistAsync(string username)
    {
        var user = await _context.Users.FirstOrDefaultAsync(x => x.Username == username);

        return user is not null;
    }

    /// <inheritdoc/>
    public async Task<bool> PasswordIsSet()
    {
        var user = await GetCurrentUser();

        return !string.IsNullOrWhiteSpace(user.Password);
    }

    /// <inheritdoc/>
    public async Task<Task> ResetPasswordAsync(string email, string newPassword)
    {
        var user = await GetUserByEmailAsync(email) ?? throw new EntityNotFoundException(email);

        user.Password = _cryptService.CryptPassword(newPassword);

        _context.Update(user);

        await _context.SaveChangesAsync();

        LogService.LogUserAction(UserActions.Updated, user.Id, "password");

        return Task.CompletedTask;
    }

    /// <inheritdoc/>
    public async Task<CustomizationSettings> UpdateCustomizationAsync(
        CustomizationSettings customization
    )
    {
        var userId = GetClaimValue("userId");

        var user = await GetUserByIdAsync(userId);

        user.Settings = new()
        {
            Theme = customization.Theme ?? user.Settings.Theme,
            ContentPerPage = customization.ContentPerPage ?? user.Settings.ContentPerPage,
        };

        user.Brand = new()
        {
            BannerPhoto = customization.BannerPhoto ?? user.Brand.BannerPhoto,
            AccentColor = customization.AccentColor ?? user.Brand.AccentColor
        };

        var savedUser = _context.Update(user).Entity;

        await _context.SaveChangesAsync();

        if (savedUser is null)
            throw new DbUpdateException(
                "Something went wrong during UpdateCustomizationAsync process."
            );

        LogService.LogUserAction(UserActions.Updated, user.Id, "customization");

        var newCustomization = new CustomizationSettings()
        {
            Theme = savedUser.Settings.Theme,
            ContentPerPage = savedUser.Settings.ContentPerPage,
            AccentColor = savedUser.Brand.AccentColor,
            BannerPhoto = savedUser.Brand.BannerPhoto
        };

        return newCustomization;
    }

    /// <inheritdoc/>
    public async Task<Task> UpdateDisplayEmailAsync(
        string email,
        bool usePrimaryEmail,
        bool removeDisplayEmail
    )
    {
        var user = await GetCurrentUser();
        string newDisplayEmail = null;

        if (removeDisplayEmail)
            newDisplayEmail = null;
        else if (usePrimaryEmail)
            newDisplayEmail = user.Email;
        else
        {
            if (
                _context.Users.Any(
                    u => u.Email.Equals(email) || (u.PersonalInfo.DisplayEmail ?? "").Equals(email)
                )
            )
                throw new EntityExistException(email);

            newDisplayEmail = email;
        }

        var beforeEmail = user.PersonalInfo.DisplayEmail;
        user.PersonalInfo.DisplayEmail = newDisplayEmail;

        _context.Update(user);

        await _context.SaveChangesAsync();

        LogService.LogUserAction(
            UserActions.Updated,
            user.Id,
            $"display email: {beforeEmail} to {email}"
        );

        return Task.CompletedTask;
    }

    /// <inheritdoc/>
    public async Task<Task> UpdatePasswordAsync(string currentPassword, string newPassword)
    {
        var userId = GetClaimValue("userId");
        var user = await GetUserByIdAsync(userId) ?? throw new EntityNotFoundException(userId);

        if (!_cryptService.CheckPassword(currentPassword, user.Password))
            throw new UserInvalidPasswordException(userId);

        user.Password = _cryptService.CryptPassword(newPassword);

        await _context.SaveChangesAsync();

        LogService.LogUserAction(UserActions.Updated, user.Id, "password");

        return Task.CompletedTask;
    }

    /// <inheritdoc/>
    public async Task<Task> UpdatePrimaryEmail(string email)
    {
        var user = await GetCurrentUser();

        if (await _context.Users.AnyAsync(u => u.Email.Equals(email)))
            throw new EntityExistException(email);

        var beforeEmail = user.Email;
        user.Email = email;

        _context.Update(user);
        await _context.SaveChangesAsync();

        LogService.LogUserAction(
            UserActions.Updated,
            user.Id,
            $"primary email: {beforeEmail} to {email}"
        );

        return Task.CompletedTask;
    }

    /// <inheritdoc/>
    public async Task<ProfileSettingsDTO> UpdateProfileSettingsAsync(ProfileSettingsDTO settings)
    {
        var user = await GetCurrentUser();

        user.Name = settings.Name ?? user.Name;
        user.Username = settings.Username ?? user.Username;

        if (!user.ProfilePhoto.Equals(settings.Avatar))
            user.ProfilePhoto = await _azureBlobStorageService.UpdateAvatarAsync(
                settings.Avatar,
                user.ProfilePhoto,
                user.Id
            );

        user.PersonalInfo.Location = settings.Location ?? user.PersonalInfo.Location;
        user.PersonalInfo.Bio = settings.Bio ?? user.PersonalInfo.Bio;
        user.PersonalInfo.WebsiteUrl = settings.WebsiteUrl ?? user.PersonalInfo.WebsiteUrl;
        user.PersonalInfo.CurrentlyLearning =
            settings.CurrentlyLearning ?? user.PersonalInfo.CurrentlyLearning;
        user.PersonalInfo.AvailableFor = settings.AvailableFor ?? user.PersonalInfo.AvailableFor;
        user.PersonalInfo.CurrentlyWorking =
            settings.CurrentlyWorking ?? user.PersonalInfo.CurrentlyWorking;
        user.PersonalInfo.Work = settings.Work ?? user.PersonalInfo.Work;
        user.PersonalInfo.Education = settings.Education ?? user.PersonalInfo.Education;

        var savedUser = _context.Update(user).Entity;

        await _context.SaveChangesAsync();

        if (savedUser is null)
            throw new DbUpdateException(
                "Something went wrong during UpdateCustomizationAsync process."
            );

        LogService.LogUserAction(UserActions.Updated, user.Id, "profile");

        var newProfileSettings = new ProfileSettingsDTO()
        {
            Name = user.Name,
            Username = user.Username,
            Avatar = user.ProfilePhoto,
            AvailableFor = user.PersonalInfo.AvailableFor,
            Bio = user.PersonalInfo.Bio,
            Location = user.PersonalInfo.Location,
            CurrentlyLearning = user.PersonalInfo.CurrentlyLearning,
            CurrentlyWorking = user.PersonalInfo.CurrentlyWorking,
            Education = user.PersonalInfo.Education,
            Work = user.PersonalInfo.Work,
        };

        return newProfileSettings;
    }

    /// <inheritdoc/>
    public async Task<bool> UserIsFollowed(string userId)
    {
        var user = await GetCurrentUser();

        return await _context.Follows.AnyAsync(
            f => f.FollowerId.Equals(user.Id) && f.FollowingId.Equals(userId)
        );
    }

    private async Task IsArticleExistAsync(string articleId)
    {
        var user = await GetCurrentUser();

        var article = _context.Articles.First(a => a.Id == articleId);

        if (article is null)
            throw new EntityNotFoundException("ArticleId: " + articleId);
    }

    private async Task<PinnedRepository> GetRepositoryAsyncById(string repoId)
    {
        var user = await GetCurrentUser();

        var username = user.ConnectedAccountList
            .FirstOrDefault(x => x.AccountType == ConnectedAccountType.GitHub)
            .Username;

        HttpClient client = new HttpClient();

        client.DefaultRequestHeaders.Add("User-Agent", "request");

        HttpResponseMessage response = client
            .GetAsync($"https://api.github.com/users/{username}/repos")
            .Result;

        var pinnedRepo = new PinnedRepository();

        if (response.StatusCode is HttpStatusCode.OK)
        {
            string responseBody = response.Content.ReadAsStringAsync().Result;

            var repositories = JsonConvert.DeserializeObject<dynamic>(responseBody);

            dynamic repository = null;

            foreach (var repo in repositories)
            {
                if (repo["id"].ToString() != repoId)
                    continue;

                repository = repo;
                break;
            }

            pinnedRepo.RepoId = repository["id"].ToString();
            pinnedRepo.Name = repository["name"].ToString();
            pinnedRepo.Fork = (bool)repository["fork"];
            pinnedRepo.Description = repository["description"].ToString();
            pinnedRepo.Stars = (int)repository["stargazers_count"];
            pinnedRepo.Url = repository["html_url"].ToString();
            pinnedRepo.TopProgrammingLanguage = repository["language"].ToString();
        }
        else
            throw new EntityNotFoundException(username + " RepoId: " + repoId);

        return pinnedRepo;
    }

    private JwtSecurityToken GetTokenFromRequest()
    {
        var authHeader = _httpContextAccessor.HttpContext.Request.Headers[
            "Authorization"
        ].FirstOrDefault();

        if (!string.IsNullOrEmpty(authHeader) && authHeader.StartsWith("Bearer "))
        {
            var token = authHeader.Substring("Bearer ".Length);
            var tokenHandler = new JwtSecurityTokenHandler();

            if (tokenHandler.CanReadToken(token))
                return tokenHandler.ReadJwtToken(token);
        }

        return null;
    }

    private async Task<User> RemoveUserAsync(string id)
    {
        var pendingUser = await _context.PendingUsers.FindAsync(id);

        if (pendingUser is not null)
        {
            pendingUser = _context.PendingUsers.Remove(pendingUser).Entity;

            await _context.SaveChangesAsync();

            LogService.LogUserAction(UserActions.Deleted, id, "permanently");

            return pendingUser is null ? null : new User(pendingUser);
        }
        else
        {
            var user = await _context.Users.FindAsync(id);

            user = _context.Users.Remove(user).Entity;

            await _context.SaveChangesAsync();

            LogService.LogUserAction(UserActions.Deleted, id, "permanently");

            return user is null ? null : user;
        }
    }

    /// <inheritdoc/>
    public async Task<AppSettings> GetAppSettingsAsync()
    {
        var user = await GetCurrentUser();

        return new() { Theme = user.Settings.Theme };
    }

    public async Task<int> GetCategoryFollowedsCount()
    {
        try
        {
            return GetCurrentUser().Result.CategoryFollowedList.Count;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async Task<int> GetUserArticlesCount()
    {
        try
        {
            return _context.Articles
                .Where(x => x.UserId == GetCurrentUser().Result.Id)
                .ToList()
                .Count;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async Task<int> GetUserQuestionsCount()
    {
        try
        {
            return _context.Questions
                .Where(x => x.UserId == GetCurrentUser().Result.Id)
                .ToList()
                .Count;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async Task<int> GetUserAnswersCount()
    {
        try
        {
            return _context.Answers
                .Where(x => x.UserId == GetCurrentUser().Result.Id)
                .ToList()
                .Count;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async Task<int> GetUserResourcesCount()
    {
        try
        {
            return _context.Resources
                .Where(x => x.UserId == GetCurrentUser().Result.Id)
                .ToList()
                .Count;
        }
        catch (Exception)
        {
            throw;
        }
    }
}
