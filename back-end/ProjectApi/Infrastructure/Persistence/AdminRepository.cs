﻿using AppDomain.DTOs.Admin;
using AppDomain.Entities.AdminRelated;
using AppDomain.Interfaces;
using Application.Services;

namespace Infrastructure.Persistence;

/// <summary>
/// Repository for handling admin-related operations.
/// </summary>
public class AdminRepository : IAdminRepository
{
    private readonly ClubrickDbContext _context;

    /// <summary>
    /// Initializes a new instance of the <see cref="AdminRepository"/> class.
    /// </summary>
    /// <param name="context">The database context for accessing data.</param>
    public AdminRepository(ClubrickDbContext context)
    {
        _context = context;
    }

    /// <inheritdoc/>
    public async Task<Task> PostBannerAsync(BannerDTO bannerDTO)
    {
        var banner = new Banner
        {
            Id = IDGeneratorService.GetShortUniqueId(),
            Photo = bannerDTO.Photo,
            Url = bannerDTO.Url,
            Title = bannerDTO.Title,
            Description = bannerDTO.Description,
            CreatedAt = bannerDTO.CreatedAt,
            ValidUntil = bannerDTO.ValidUntil,
        };

        _context.Banners.Add(banner);

        await _context.SaveChangesAsync();

        return Task.CompletedTask;
    }

    /// <inheritdoc/>
    public async Task<Task> DeleteBannerAsync(string bannerId)
    {
        var banner = _context.Banners.Find(bannerId);

        if (banner is null)
            throw new Exception("This banner is not exist.");

        _context.Banners.Remove(banner);

        await _context.SaveChangesAsync();

        return Task.CompletedTask;
    }

    /// <inheritdoc/>
    public async Task<Task> UpdateBannerAsync(
        string bannerId, string? photo, string? url,
        string? title, string? description, DateTime? validUntil)
    {
        var banner = _context.Banners.Find(bannerId);

        if (banner is null)
            throw new Exception("This banner is not exist.");

        banner.Photo = photo ?? banner.Photo;
        banner.Url = url ?? banner.Url;
        banner.Title = title ?? banner.Title;
        banner.Description = description ?? banner.Description;
        banner.ValidUntil = validUntil ?? banner.ValidUntil;

        _context.Banners.Update(banner);

        await _context.SaveChangesAsync();

        return Task.CompletedTask;
    }
}