﻿using AppDomain.Entities.TagBaseRelated;
using AppDomain.Interfaces;

namespace Infrastructure.Persistence
{
    public class ArticleFlagRepository : IArticleFlagRepository
    {
        private readonly ClubrickDbContext _context;
        private readonly IAzureBlobStorageService _azureBlobStorage;

        public ArticleFlagRepository(
            ClubrickDbContext context,
            IAzureBlobStorageService azureBlobStorage
        )
        {
            _context = context;
            _azureBlobStorage = azureBlobStorage;
        }

        public async Task<int> DecrementUseCounts(List<string> articleFlagIdList)
        {
            try
            {
                bool temp = false;
                foreach (var item in articleFlagIdList)
                {
                    var existingArticleFlag = await _context.ArticleFlags.FindAsync(item);

                    if (existingArticleFlag is null)
                        continue;

                    if (existingArticleFlag.UseCount > 0)
                        existingArticleFlag.UseCount--;

                    temp = true;

                    _context.Update(existingArticleFlag);
                }

                await _context.SaveChangesAsync();
                if (temp is false)
                    return 0;
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public async Task<string> DeleteArticleFlag(string id)
        {
            var existingArticleFLag = await _context.ArticleFlags.FindAsync(id);

            if (existingArticleFLag is null)
                return null;

            existingArticleFLag.IsDeleted = true;

            _context.ArticleFlags.Update(existingArticleFLag);

            await _context.SaveChangesAsync();

            return id;
        }

        public async Task<List<ArticleFlag>> GetAllArticleFlag(string keyword)
        {
            var result = _context.ArticleFlags.ToList();

            if (keyword is not null)
                result = result.Where(x => x.Title.ToLower().Contains(keyword.ToLower())).ToList();

            return result;
        }

        public async Task<ArticleFlag> GetArticleFlagById(string id)
        {
            var existingArticleFlag = await _context.ArticleFlags.FindAsync(id);

            return existingArticleFlag;
        }

        public async Task<int> IncrementUseCounts(List<string> articleFlagIdList)
        {
            try
            {
                bool temp = false;
                foreach (var item in articleFlagIdList)
                {
                    var existingArticleFlag = await _context.ArticleFlags.FindAsync(item);

                    if (existingArticleFlag is null)
                        continue;

                    existingArticleFlag.UseCount++;

                    temp = true;

                    _context.Update(existingArticleFlag);
                }

                await _context.SaveChangesAsync();

                if (temp is false)
                    return 0;
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public async Task<string> InsertArticleFlag(ArticleFlag articleFlag)
        {
            try
            {
                var find = _context.ArticleFlags.FirstOrDefault(
                    x => x.Title.ToLower().Equals(articleFlag.Title.ToLower())
                );

                if (find is not null)
                    return "0";

                if (!string.IsNullOrWhiteSpace(articleFlag.IconLink))
                {
                    var iconLink = await _azureBlobStorage.UploadTagIconAsync(
                        articleFlag.IconLink,
                        articleFlag.Id
                    );

                    articleFlag.IconLink = iconLink;
                }

                var inserted = _context.ArticleFlags.Add(articleFlag);

                await _context.SaveChangesAsync();

                return inserted.Entity.Id;
            }
            catch (Exception)
            {
                return "-1";
            }
        }

        public async Task<string> UpdateArticleFlag(ArticleFlag articleFlag)
        {
            try
            {
                var existingArticleFlag = await _context.ArticleFlags.FindAsync(articleFlag.Id);

                if (existingArticleFlag is null)
                    return "0";

                if (
                    !existingArticleFlag.Title.Equals(
                        articleFlag.Title,
                        StringComparison.OrdinalIgnoreCase
                    )
                )
                {
                    var conflict = _context.Categories.FirstOrDefault(
                        x => x.Title.ToLower().Equals(articleFlag.Title.ToLower())
                    );

                    if (conflict is not null)
                        return "-2";
                }

                if (
                    !string.IsNullOrWhiteSpace(articleFlag.IconLink)
                    && !articleFlag.IconLink.StartsWith("https://")
                )
                    if (string.IsNullOrWhiteSpace(existingArticleFlag.IconLink))
                    {
                        var iconLink = await _azureBlobStorage.UploadTagIconAsync(
                            articleFlag.IconLink,
                            articleFlag.Id
                        );

                        articleFlag.IconLink = iconLink;
                    }
                    else
                    {
                        articleFlag.IconLink = await _azureBlobStorage.UpdateTagIconAsync(
                            articleFlag.IconLink,
                            existingArticleFlag.IconLink,
                            existingArticleFlag.Id
                        );
                    }

                articleFlag.UseCount = existingArticleFlag.UseCount;

                _context.Entry(existingArticleFlag).CurrentValues.SetValues(articleFlag);

                await _context.SaveChangesAsync();

                return articleFlag.Id;
            }
            catch (Exception)
            {
                return "-1";
            }
        }
    }
}
