﻿using AppDomain.DTOs.Notification;
using AppDomain.DTOs.Pagination;
using AppDomain.DTOs.Product;
using AppDomain.Entities.ContentRelated;
using AppDomain.Entities.NotificationRelated.AdditionalDetails;
using AppDomain.Entities.UserActionRelated;
using AppDomain.Enums;
using AppDomain.Exceptions.UserExceptions;
using AppDomain.Interfaces;
using Application.Services;
using AutoMapper;
using Infrastructure.Services;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Quartz.Util;

namespace Infrastructure.Persistence;

public class ProductRepository : IProductRepository
{
    private ClubrickDbContext _context;

    private IAzureBlobStorageService _azureBlobStorage;

    private readonly IMapper _mapper;

    private readonly IUserRepository _userRepository;
    private readonly INotificationRepository _notificationRepository;

    public ProductRepository(
        ClubrickDbContext context,
        IMapper mapper,
        IUserRepository userRepository,
        INotificationRepository notificationRepository,
        IAzureBlobStorageService azureBlobStorage
    )
    {
        _context = context;
        _mapper = mapper;
        _userRepository = userRepository;
        _notificationRepository = notificationRepository;
        _azureBlobStorage = azureBlobStorage;
    }

    public async Task<PaginatedListDto<GetAllProductDTO>> GetAllProducts(
        string searchQuery,
        ProductSortOptions sort,
        int page = 1
    )
    {
        IQueryable<Product> query = _context.Products.Include(x => x.Favourites);

        int pageSize = await _userRepository.GetContentPerPage();

        query = query.Skip((page - 1) * pageSize).Take(pageSize);

        if (!string.IsNullOrWhiteSpace(searchQuery))
            query = query.Where(x => x.Name.ToLower().Contains(searchQuery.ToLower()));

        query = sort switch
        {
            ProductSortOptions.TopRated => query.OrderByDescending(x => x.Favourites.Count),
            ProductSortOptions.BestSellers => query.OrderByDescending(x => x.SellCount),
            ProductSortOptions.Ranking => query.OrderByDescending(x => x.Rank),
            ProductSortOptions.HighestPrice
                => query.OrderByDescending(x => x.Price - (x.Price * (x.Discount / 100))),
            ProductSortOptions.LowestPrice
                => query.OrderBy(x => x.Price - (x.Price * (x.Discount / 100))),
            ProductSortOptions.Newest => query.OrderByDescending(x => x.CreatedAt),
            _ => query.OrderByDescending(x => x.Price),
        };

        var user = await _userRepository.GetCurrentUser();

        List<GetAllProductDTO> result = new List<GetAllProductDTO>();

        foreach (var item in query)
        {
            GetAllProductDTO getAllProductDTO = _mapper.Map<GetAllProductDTO>(item);

            getAllProductDTO.DiscountedPrice =
                item.Discount != 0
                    ? CalculateDiscountedPrice(item.Price, item.Discount)
                    : item.Price;

            if (user != null)
            {
                if (item.Favourites.Exists(x => x.UserId == user.Id))
                {
                    getAllProductDTO.IsFavourited = true;
                }
            }

            result.Add(getAllProductDTO);
        }

        int totalCount = await query.CountAsync();

        return new PaginatedListDto<GetAllProductDTO>(
            result,
            new PaginationMeta(page, pageSize, totalCount)
        );
    }

    public double CalculateDiscountedPrice(double price, double discount)
    {
        return Math.Floor(price - (price * (discount / 100)));
    }

    public async Task<GetByIdProductDTO> GetByIdProduct(string productId)
    {
        try
        {
            var user = await _userRepository.GetCurrentUser();

            var product = _context.Products
                .Where(x => x.Id == productId)
                .Include(x => x.Favourites)
                .Include(x => x.Reviews)
                .FirstOrDefault();

            GetByIdProductDTO productDTO = _mapper.Map<GetByIdProductDTO>(product);

            productDTO.DiscountedPrice = CalculateDiscountedPrice(
                productDTO.Price,
                productDTO.Discount
            );

            productDTO.ReviewsCount = product.Reviews.Count;

            if (user != null)
                productDTO.IsFavourited = product.Favourites.Exists(x => x.UserId == user.Id);

            return productDTO;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async Task InsertProduct(InsertProductDTO insertProductDTO)
    {
        try
        {
            if (_userRepository.GetCurrentUser().Result.Role != UserRole.Admin)
                throw new UserNotFoundException("InsertProduct");

            Product product = _mapper.Map<Product>(insertProductDTO);

            product.Id = IDGeneratorService.GetShortUniqueId();

            var photos = await _azureBlobStorage.UploadProductPhotos(
                insertProductDTO.PhotoPaths,
                product.Id
            );

            for (int i = 0; i < photos.Count; i++)
            {
                product.Photos.Add(new Photo() { Id = i.ToString(), Path = photos[i] });
            }

            product.Quantity = product.Stock;

            product.CreatedAt = DateTime.UtcNow;
            product.UpdatedAt = DateTime.UtcNow;

            _context.Products.Add(product);

            await _context.SaveChangesAsync();
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async Task FavouritedProduct(string productId)
    {
        try
        {
            var user = await _userRepository.GetCurrentUser();

            if (user is null)
                throw new UserNotFoundException("FavouritedProduct");

            var product = _context.Products
                .Where(x => x.Id == productId)
                .Include(x => x.Favourites)
                .FirstOrDefault();

            bool temp = true;

            if (product is not null)
            {
                foreach (var item in product.Favourites)
                {
                    if (item.UserId == user.Id)
                    {
                        product.Favourites.Remove(item);
                        temp = false;
                        break;
                    }
                }
            }
            if (temp)
            {
                product.Favourites.Add(
                    new Favourite()
                    {
                        Id = IDGeneratorService.GetShortUniqueId(),
                        ProductId = productId,
                        UserId = user.Id
                    }
                );
            }

            await _context.SaveChangesAsync();

            return;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async Task<PaginatedListDto<GetAllProductDTO>> GetAllFavouriteProducts(
        string searchQuery,
        ProductSortOptions sort,
        int page = 1
    )
    {
        try
        {
            List<GetAllProductDTO> result = new List<GetAllProductDTO>();

            var user = await _userRepository.GetCurrentUser();

            int pageSize = await _userRepository.GetContentPerPage();

            if (user is null)
                return new PaginatedListDto<GetAllProductDTO>(
                    result,
                    new PaginationMeta(page, pageSize, 0)
                );

            IQueryable<Product> query = _context.Products
                .Include(x => x.Favourites)
                .Where(x => x.Favourites.Any(x => x.UserId == user.Id));

            int totalCount = await query.CountAsync();

            query = query.Skip((page - 1) * pageSize).Take(pageSize);

            if (!string.IsNullOrWhiteSpace(searchQuery))
                query = query.Where(x => x.Name.ToLower().Contains(searchQuery.ToLower()));

            query = sort switch
            {
                ProductSortOptions.TopRated => query.OrderByDescending(x => x.Favourites.Count),
                ProductSortOptions.BestSellers => query.OrderByDescending(x => x.SellCount),
                ProductSortOptions.Ranking => query.OrderByDescending(x => x.Rank),
                ProductSortOptions.HighestPrice
                    => query.OrderByDescending(x => x.Price - (x.Price * (x.Discount / 100))),
                ProductSortOptions.LowestPrice
                    => query.OrderBy(x => x.Price - (x.Price * (x.Discount / 100))),
                ProductSortOptions.Newest => query.OrderByDescending(x => x.CreatedAt),
                _ => query.OrderByDescending(x => x.Price),
            };

            foreach (var item in query)
            {
                GetAllProductDTO getAllProductDTO = _mapper.Map<GetAllProductDTO>(item);

                getAllProductDTO.DiscountedPrice =
                    item.Discount != 0
                        ? CalculateDiscountedPrice(item.Price, item.Discount)
                        : item.Price;

                if (user != null)
                {
                    if (item.Favourites.Exists(x => x.UserId == user.Id))
                    {
                        getAllProductDTO.IsFavourited = true;
                    }
                }

                result.Add(getAllProductDTO);
            }

            return new PaginatedListDto<GetAllProductDTO>(
                result,
                new PaginationMeta(page, pageSize, totalCount)
            );
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async Task DeleteProduct(string productId)
    {
        try
        {
            if (_userRepository.GetCurrentUser().Result.Role != UserRole.Admin)
                throw new UserNotFoundException("DeleteProduct");

            var product = await _context.Products
                .Where(x => x.Id == productId)
                .Include(x => x.Favourites)
                .FirstOrDefaultAsync();

            product.IsDeleted = true;

            foreach (var item in product.Favourites)
            {
                if (item.ProductId == productId)
                    _context.Favourites.Remove(item);
            }

            _context.Products.Update(product);

            await _context.SaveChangesAsync();
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async Task UpdateProduct(UpdateProductDTO updateProductDTO)
    {
        try
        {
            if (_userRepository.GetCurrentUser().Result.Role != UserRole.Admin)
                throw new UserNotFoundException("UpdateProduct");

            var product =
                _context.Products.Where(x => x.Id == updateProductDTO.ProductId).FirstOrDefault()
                ?? throw new Exception("Product not found");

            if (!updateProductDTO.Name.IsNullOrWhiteSpace())
                product.Name = updateProductDTO.Name;

            if (!updateProductDTO.Description.IsNullOrWhiteSpace())
                product.Description = updateProductDTO.Description;

            if (updateProductDTO.Price != null)
                product.Price = Convert.ToInt32(updateProductDTO.Price);

            if (updateProductDTO.Discount != null)
                product.Discount = Convert.ToDouble(updateProductDTO.Discount);

            if (updateProductDTO.Quantity != null)
                product.Stock = Convert.ToInt32(updateProductDTO.Quantity);

            var newPhotos = await UpdateProductPhotos(
                updateProductDTO.PhotoUpdateDTO.Photos,
                product.Photos.Select(x => x.Path).ToList(),
                product.Id
            );

            var photoDto = new List<Photo>();

            for (int i = 0; i < newPhotos.Count; i++)
            {
                photoDto.Add(new Photo() { Id = i.ToString(), Path = newPhotos[i] });
            }

            product.Photos = photoDto;

            product.UpdatedAt = DateTime.UtcNow;

            _context.Products.Update(product);

            await _context.SaveChangesAsync();
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async Task BuyProduct(string productId, int quantity)
    {
        try
        {
            var user =
                await _userRepository.GetCurrentUser()
                ?? throw new UserNotFoundException("BuyProduct");
            var product = _context.Products.Where(x => x.Id == productId).FirstOrDefault();

            if (product is null || product.Stock < quantity)
                throw new Exception("Product not found");

            var total = Convert.ToInt32(
                CalculateDiscountedPrice(product.Price, product.Discount) * quantity
            );

            if (total > user.UsableBudsPoints)
                throw new HaveNotEnoughtBudsException();

            user.UsableBudsPoints -= total;

            product.SellCount += quantity;

            product.Quantity -= quantity;

            product.Stock = product.Quantity;

            _context.Users.Update(user);

            _context.Products.Update(product);

            await _context.SaveChangesAsync();

            await _notificationRepository.PostNotificationAsync(
                new NotificationDTO
                {
                    Type = NotificationType.ProductPurchase,
                    AdditionalDetails = JsonConvert.SerializeObject(
                        new ProductPurchaseDetails
                        {
                            BuyerId = user.Id,
                            ProductId = product.Id,
                            PurchaseQuantity = quantity,
                            TotalSum = total
                        }
                    ),
                }
            );

            await _userRepository.AddBudsAsync(
                contentType: ContentType.Product,
                contentId: product.Id,
                budsAction: BudsActionType.ProductPurchase,
                userId: user.Id
            );
        }
        catch (Exception)
        {
            throw;
        }
    }

    private async Task<List<string>> UpdateProductPhotos(
        List<string> updatePhotos,
        List<string> productPhotos,
        string productId
    )
    {
        List<string> newProductPhotos = new(updatePhotos.Capacity);

        while (newProductPhotos.Count < updatePhotos.Count)
        {
            newProductPhotos.Add(null);
        }

        for (int i = 0; i < updatePhotos.Count; i++)
        {
            string currentPhoto = updatePhotos[i];

            if (currentPhoto.StartsWith("data"))
            {
                newProductPhotos[i] = await _azureBlobStorage.UploadProductPhoto(
                    currentPhoto,
                    productId
                );
            }
            else if (productPhotos.Contains(currentPhoto))
            {
                int productIndex = productPhotos.IndexOf(currentPhoto);
                newProductPhotos[i] = currentPhoto;
            }
            else
            {
                await _azureBlobStorage.DeleteProductPhoto(currentPhoto);
            }
        }

        newProductPhotos.RemoveAll(item => item == null);

        return newProductPhotos;
    }

    public async Task<Dictionary<string, int>> GetAllRankForProduct(string productId)
    {
        try
        {
            Dictionary<string, int> ranks = new();

            var reviewList = _context.Reviews.Where(x => x.ProductId == productId).ToList();

            for (int i = 1; i <= 5; i++)
            {
                if (reviewList.Count is 0)
                    ranks.Add($"{i}", 0);
                else
                {
                    var value = reviewList.Where(x => x.Rank == i).ToList().Count;
                    ranks.Add($"{i}", value);
                }
            }

            return ranks;
        }
        catch (Exception)
        {
            throw;
        }
    }
}
