﻿using AppDomain.Entities.TagBaseRelated;
using AppDomain.Interfaces;

namespace Infrastructure.Persistence
{
    public class ResourceFlagRepository : IResourceFlagRepository
    {
        private ClubrickDbContext _context;
        private readonly IAzureBlobStorageService _azureBlobStorage;

        public ResourceFlagRepository(
            ClubrickDbContext context,
            IAzureBlobStorageService azureBlobStorage
        )
        {
            _context = context;
            _azureBlobStorage = azureBlobStorage;
        }

        public async Task<int> DecrementUseCounts(List<string> resourceFlagIdList)
        {
            try
            {
                bool temp = false;
                foreach (var item in resourceFlagIdList)
                {
                    var existingResourceFlag = await _context.ResourceFlags.FindAsync(item);

                    if (existingResourceFlag is null)
                        continue;

                    if (existingResourceFlag.UseCount > 0)
                        existingResourceFlag.UseCount--;

                    temp = true;

                    _context.Update(existingResourceFlag);
                }

                await _context.SaveChangesAsync();
                if (temp is false)
                    return 0;
                return 1;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public async Task<int> IncrementUseCounts(List<string> resourceFlagIdList)
        {
            try
            {
                bool temp = false;
                foreach (var item in resourceFlagIdList)
                {
                    var existingResourceFlag = await _context.ResourceFlags.FindAsync(item);

                    if (existingResourceFlag is null)
                        continue;

                    existingResourceFlag.UseCount++;

                    temp = true;

                    _context.Update(existingResourceFlag);
                }

                await _context.SaveChangesAsync();

                if (temp is false)
                    return 0;
                return 1;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public async Task<List<ResourceFlag>> GetAllResourceFlag(string keyword = "")
        {
            var result = _context.ResourceFlags.ToList();
            if (keyword is not null)
                result = result.Where(x => x.Title.ToLower().Contains(keyword.ToLower())).ToList();

            return result;
        }

        public async Task<ResourceFlag> GetResourceFlagById(string id)
        {
            var existingResourceFlag = await _context.ResourceFlags.FindAsync(id);

            return existingResourceFlag;
        }

        public async Task<string> InsertResourceFlag(ResourceFlag resourceFlag)
        {
            try
            {
                var find = _context.ResourceFlags.FirstOrDefault(
                    x => x.Title.ToLower().Equals(resourceFlag.Title.ToLower())
                );

                if (find is not null)
                    return "0";

                if (!string.IsNullOrWhiteSpace(resourceFlag.IconLink))
                {
                    var iconLink = await _azureBlobStorage.UploadTagIconAsync(
                        resourceFlag.IconLink,
                        resourceFlag.Id
                    );

                    resourceFlag.IconLink = iconLink;
                }

                _context.ResourceFlags.Add(resourceFlag);

                await _context.SaveChangesAsync();

                

                return resourceFlag.Id;
            }
            catch (Exception)
            {
                return "-1";
            }
        }

        public async Task<string> UpdateResourceFlag(ResourceFlag resourceFlag)
        {
            try
            {
                var existingResourceFlag = await _context.ResourceFlags.FindAsync(resourceFlag.Id);

                if (existingResourceFlag is null)
                    return "0";

                if (
                    !existingResourceFlag.Title.Equals(
                        resourceFlag.Title,
                        StringComparison.OrdinalIgnoreCase
                    )
                )
                {
                    var conflict = _context.Categories.FirstOrDefault(
                        x => x.Title.ToLower().Equals(resourceFlag.Title.ToLower())
                    );

                    if (conflict is not null)
                        return "-2";
                }

                if (
                    !string.IsNullOrWhiteSpace(resourceFlag.IconLink)
                    && !resourceFlag.IconLink.StartsWith("https://")
                )
                    if (string.IsNullOrWhiteSpace(existingResourceFlag.IconLink))
                    {
                        var iconLink = await _azureBlobStorage.UploadTagIconAsync(
                            resourceFlag.IconLink,
                            resourceFlag.Id
                        );

                        resourceFlag.IconLink = iconLink;
                    }
                    else
                    {
                        resourceFlag.IconLink = await _azureBlobStorage.UpdateTagIconAsync(
                            resourceFlag.IconLink,
                            existingResourceFlag.IconLink,
                            existingResourceFlag.Id
                        );
                    }

                resourceFlag.UseCount = existingResourceFlag.UseCount;

                _context.Entry(existingResourceFlag).CurrentValues.SetValues(resourceFlag);

                await _context.SaveChangesAsync();

                return resourceFlag.Id;
            }
            catch (Exception)
            {
                return "-1";
            }
        }

        public async Task<string> DeleteResourceFlag(string id)
        {
            try
            {
                var existingResourceFlag = await _context.ResourceFlags.FindAsync(id);

                if (existingResourceFlag is null)
                    return "0";

                existingResourceFlag.IsDeleted = true;

                _context.ResourceFlags.Update(existingResourceFlag);

                await _context.SaveChangesAsync();

                return id;
            }
            catch (Exception)
            {
                return "-1";
            }
        }
    }
}
