﻿using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Data;

namespace Infrastructure.Persistence
{
    public class DapperContext
    {
        private readonly IConfiguration _configuration;

        private readonly string connectionString;
        public DapperContext(IConfiguration configuration)
        {
            _configuration = configuration;
            connectionString = _configuration.GetConnectionString("DefaultConnection");
        }
        public IDbConnection CreateConnection() => new NpgsqlConnection(connectionString);
    }
}