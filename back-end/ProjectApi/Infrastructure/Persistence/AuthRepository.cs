﻿using AppDomain.DTOs.Auth;
using AppDomain.Entities.UserRelated;
using AppDomain.Enums;
using AppDomain.Exceptions.Common;
using AppDomain.Exceptions.OtpExceptions;
using AppDomain.Exceptions.UserExceptions;
using AppDomain.Interfaces;
using Application.Services;
using Infrastructure.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System.Data;
using System.IdentityModel.Tokens.Jwt;

namespace Infrastructure.Persistence;

/// <inheritdoc/>
public class AuthRepository : IAuthRepository
{
    private readonly IAzureBlobStorageService _azureBlobStorage;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly IUserRepository _userRepository;
    private readonly ICryptService _cryptService;
    private readonly ITokenService _tokenService;
    private readonly IEmailService _emailService;
    private readonly IMemoryCache _cache;
    private readonly ClubrickDbContext _context;
    private static UserAuthDto _authDto = new();

    /// <summary>
    /// Initializes a new instance of the AuthRepository class with required dependencies.
    /// </summary>
    /// <param name="context">The database context for interacting with authentication data.</param>
    /// <param name="cryptService">A service for cryptographic operations.</param>
    /// <param name="tokenService">A service for handling JWT tokens.</param>
    /// <param name="emailService">A service for sending email notifications.</param>
    /// <param name="httpContextAccessor">An accessor for working with the current HTTP context.</param>
    /// <param name="azureBlobStorage">A service for interacting with Azure Blob Storage.</param>
    /// <param name="userRepository">A repository for user-related operations.</param>
    public AuthRepository(
        ClubrickDbContext context,
        ICryptService cryptService,
        ITokenService tokenService,
        IEmailService emailService,
        IHttpContextAccessor httpContextAccessor,
        IAzureBlobStorageService azureBlobStorage,
        IUserRepository userRepository,
        IMemoryCache cache
    )
    {
        _context = context;
        _cryptService = cryptService;
        _tokenService = tokenService;
        _azureBlobStorage = azureBlobStorage;
        _httpContextAccessor = httpContextAccessor;
        _emailService = emailService;
        _userRepository = userRepository;
        _cache = cache;
    }

    /// <inheritdoc/>
    public async Task<UserAuthDto> LogInAsync(string email, string password)
    {
        var user =
            await _context.Users.FirstOrDefaultAsync(x => x.Email == email)
            ?? await _context.PendingUsers.FirstOrDefaultAsync(x => x.Email == email)
            ?? throw new EntityNotFoundException(email);

        if (string.IsNullOrWhiteSpace(user.Password))
            throw new EntityNotFoundException(email);

        var isPasswordValid = _cryptService.CheckPassword(password, user.Password);

        if (!isPasswordValid)
            throw new UserInvalidPasswordException(email);

        var authToken = _tokenService.GenerateSecurityToken(user.Id, user.Email, user.Role);
        var token = authToken.Token;

        var refreshToken = _tokenService.GenerateRefreshToken();

        _httpContextAccessor.HttpContext = _tokenService.SetAuthToken(
            authToken,
            _httpContextAccessor.HttpContext
        );

        //_httpContextAccessor.HttpContext = _tokenService.SetRefreshToken(
        //    refreshToken,
        //    _httpContextAccessor.HttpContext
        //);

        user.RefreshToken = token;

        _context.Update(user);
        await _context.SaveChangesAsync();

        _authDto = new UserAuthDto(
            user.Id,
            token,
            user.Role,
            user.Email,
            user.IsProfileBuilt,
            user.Name,
            user.GetType() == typeof(User) ? ((User)user).Username : string.Empty,
            user.GetType() == typeof(User) ? ((User)user).ProfilePhoto : string.Empty
        );

        return _authDto;
    }

    /// <inheritdoc/>
    public async Task<UserAuthDto> RegisterUserAsync(InsertPendingUserDTO insertUser)
    {
        if (await _userRepository.IsEmailExistAsync(insertUser.Email))
            throw new EntityExistException(insertUser.Email);

        var id = IDGeneratorService.GetUniqueId();

        var authToken = _tokenService.GenerateSecurityToken(id, insertUser.Email, UserRole.Sapling);

        var token = authToken.Token;
        var refreshToken = _tokenService.GenerateRefreshToken();

        _httpContextAccessor.HttpContext = _tokenService.SetAuthToken(
            authToken,
            _httpContextAccessor.HttpContext
        );
        //_httpContextAccessor.HttpContext = _tokenService.SetRefreshToken(
        //    refreshToken,
        //    _httpContextAccessor.HttpContext
        //);

        var user = _context.PendingUsers
            .Add(
                new PendingUser
                {
                    Id = id,
                    Email = insertUser.Email,
                    Name = insertUser.Name,
                    Password = _cryptService.CryptPassword(insertUser.Password),
                    JoinedTime = DateTime.UtcNow,
                    Role = UserRole.Sapling,
                    RefreshToken = refreshToken.Token,
                    ExpirationDate = refreshToken.ExpirationDate
                }
            )
            .Entity;

        await _context.SaveChangesAsync();

        var autDto = new UserAuthDto(
            user.Id,
            token,
            user.Role,
            user.Email,
            user.IsProfileBuilt,
            user.Name
        );

        LogService.LogUserAction(UserActions.Registered, user.Id);

        return autDto;
    }

    /// <inheritdoc/>
    public async Task<UserBuildResponseDTO> BuildUserAsync(BuildUserDTO buildUser)
    {
        var userEmail = GetClaimValue("userEmail");

        PendingUser pendingUser =
            _context.PendingUsers.FirstOrDefault(x => x.Email == userEmail)
            ?? throw new EntityNotFoundException(userEmail);

        string avatarUrl = await _azureBlobStorage.UploadAvatarAsync(
            buildUser.ProfilePhoto,
            pendingUser.Id
        );

        User user = new User(pendingUser);

        user.BuildUser(buildUser);

        user.IsProfileBuilt = true;
        user.ProfilePhoto = avatarUrl;

        _context.PendingUsers.Remove(pendingUser);

        _context.Users.Add(user);

        await _context.SaveChangesAsync();

        var dto = new UserBuildResponseDTO()
        {
            Id = user.Id,
            Username = user.Username,
            Name = user.Name,
            Email = user.Email,
            Avatar = user.ProfilePhoto
        };

        LogService.LogUserAction(UserActions.Built, user.Id);

        return dto;
    }

    /// <inheritdoc/>
    public async Task<bool> VerifyEmailAsync(string email, string otpCode)
    {
        var verification =
            _context.EmailVerifications.FirstOrDefault(x => x.Email == email)
            ?? throw new EntityNotFoundException(email);

        if (verification.ExpireUntil < DateTime.UtcNow)
            throw new OtpCodeExpiredException(email);

        if (verification.OTP.Equals(otpCode))
        {
            _context.EmailVerifications.Remove(verification);
            await _context.SaveChangesAsync();

            return true;
        }
        else
        {
            return false;
        }
    }

    /// <inheritdoc/>
    public async Task<Task> SendOTPCodeAsync(string toEmail, string toName)
    {
        var verification = await _context.EmailVerifications.FirstOrDefaultAsync(
            v => v.Email.Equals(toEmail)
        );

        if (verification is not null)
        {
            _context.EmailVerifications.Remove(verification);
            await _context.SaveChangesAsync();
        }

        var otpCode = OTPCodeGenerator.GenerateOTPCode();

        _context.EmailVerifications.Add(
            new EmailVerification
            {
                Id = IDGeneratorService.GetShortUniqueId(),
                Email = toEmail,
                OTP = otpCode,
                ExpireUntil = DateTime.UtcNow.AddMinutes(2)
            }
        );

        await _context.SaveChangesAsync();

        _emailService.SendEmail(toEmail, otpCode, toName);

        return Task.CompletedTask;
    }

    /// <inheritdoc/>
    public async Task<string> UpdateTokenAsync(string email, string refreshToken)
    {
        var user = await _userRepository.GetUserByEmailAsync(email);

        if (!user.RefreshToken.Equals(refreshToken))
            throw new BadHttpRequestException("Invalid Token");

        var authToken = _tokenService.GenerateSecurityToken(user.Id, user.Email, user.Role);
        var token = authToken.Token;

        user.RefreshToken = token;

        _context.Update(user);

        await _context.SaveChangesAsync();

        return token;
    }

    /// <inheritdoc/>
    public async Task<UserAuthDto> EnterWitProvider(EnterWithProviderDTO userDto)
    {
        var pendingUser = await _context.PendingUsers.FirstOrDefaultAsync(
            u => u.Email.Equals(userDto.Email)
        );

        var user =
            pendingUser
            ?? await _context.Users.FirstOrDefaultAsync(u => u.Email.Equals(userDto.Email));

        if (user is null)
        {
            user = new PendingUser
            {
                Id = IDGeneratorService.GetUniqueId(),
                Name = userDto.AccountUsername,
                Email = userDto.Email,
                UserSecret = userDto.Secret,
                ProfilePhoto = userDto.AvatarUrl,
                IsProfileBuilt = false,
                JoinedTime = DateTime.UtcNow,
            };

            _context.Add(user);

            await _context.SaveChangesAsync();
        }

        user.ConnectedAccountList ??= new();

        if (!user.ConnectedAccountList.Any(account => account.AccountType.Equals(userDto.Type)))
            user.ConnectedAccountList.Add(
                new()
                {
                    Id = IDGeneratorService.GetShortUniqueId(),
                    ProviderId = userDto.Secret,
                    Username = userDto.AccountUsername,
                    Email = userDto.Email,
                    AccountType = userDto.Type,
                }
            );

        var authToken = _tokenService.GenerateSecurityToken(user.Id, userDto.Email, user.Role);
        var token = authToken.Token;
        var refreshToken = _tokenService.GenerateRefreshToken();

        _httpContextAccessor.HttpContext = _tokenService.SetAuthToken(
            authToken,
            _httpContextAccessor.HttpContext
        );
        //_httpContextAccessor.HttpContext = _tokenService.SetRefreshToken(
        //    refreshToken,
        //    _httpContextAccessor.HttpContext
        //);

        user.RefreshToken = refreshToken.Token;
        user.ExpirationDate = refreshToken.ExpirationDate;

        _context.Update(user);

        await _context.SaveChangesAsync();

        return new()
        {
            Id = user.Id,
            Avatar = user.ProfilePhoto,
            IsProfileBuilt = user.IsProfileBuilt,
            Email = user.Email,
            Name = user.Name,
            Username = user.Username,
            Role = user.Role,
            Token = token,
            ConnectedAccountType = userDto.Type,
        };
    }

    /// <inheritdoc/>
    public async Task<User> GetCurrentUser()
    {
        try
        {
            var id = GetClaimValue("userId");

            var user = await _context.Users
                .Include(r => r.SaveList)
                .Include(r => r.VoteList)
                .Include(r => r.ViewList)
                .FirstOrDefaultAsync(u => u.Id.Equals(id));

            return user;
        }
        catch (Exception)
        {
            return null;
        }
    }

    /// <inheritdoc/>
    public string GetClaimValue(string claimType)
    {
        var token = GetTokenFromRequest();

        if (token is null)
            return null;

        var claim = token.Claims.FirstOrDefault(c => c.Type == claimType);

        return claim?.Value;
    }

    private JwtSecurityToken GetTokenFromRequest()
    {
        var authHeader = _httpContextAccessor.HttpContext.Request.Headers[
            "Authorization"
        ].FirstOrDefault();

        if (!string.IsNullOrEmpty(authHeader) && authHeader.StartsWith("Bearer "))
        {
            var token = authHeader.Substring("Bearer ".Length);
            var tokenHandler = new JwtSecurityTokenHandler();

            if (tokenHandler.CanReadToken(token))
                return tokenHandler.ReadJwtToken(token);
        }

        return null;
    }
}
