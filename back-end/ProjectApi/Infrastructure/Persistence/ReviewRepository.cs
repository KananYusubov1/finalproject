﻿using AppDomain.DTOs.Review;
using AppDomain.Entities.ContentRelated;
using AppDomain.Exceptions.UserExceptions;
using AppDomain.Interfaces;
using AppDomain.Responses.UserResponses;
using Application.Services;
using AutoMapper;
using Quartz.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Persistence
{
    public class ReviewRepository : IReviewRepository
    {
        private ClubrickDbContext _context;

        private readonly IMapper _mapper;

        private readonly IUserRepository _userRepository;

        public ReviewRepository(
            ClubrickDbContext context,
            IUserRepository userRepository,
            IMapper mapper
        )
        {
            _context = context;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<string> InsertReview(string productId, string body, int rank)
        {
            try
            {
                var user =
                    await _userRepository.GetCurrentUser()
                    ?? throw new UserNotFoundException("InsertReview");

                var checkReview = _context.Reviews
                    .Where(x => x.UserId == user.Id && x.ProductId == productId)
                    .FirstOrDefault();

                if (checkReview != null)
                    throw new UserReviewIsExistException();

                var review = new Review
                {
                    Id = IDGeneratorService.GetShortUniqueId(),
                    ProductId = productId,
                    Body = body,
                    UserId = user.Id,
                    Rank = rank,
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                };

                _context.Reviews.Add(review);

                await _context.SaveChangesAsync();

                await ChangeProductRank(productId);

                return review.Id;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<List<GetAllReviewDTO>> GetAllReviews(string productId)
        {
            try
            {
                List<GetAllReviewDTO> getAllReviewDTOs = new();

                var reviews = _context.Reviews.Where(x => x.ProductId == productId).ToList();

                foreach (var item in reviews)
                {
                    GetAllReviewDTO reviewDTO = _mapper.Map<GetAllReviewDTO>(item);

                    var user = _context.Users.Where(x => x.Id == item.UserId).FirstOrDefault();

                    if (user is null)
                        throw new UserNotFoundException("GetAllReviews");

                    reviewDTO.UserContent = new UserContentTitleResponse
                    {
                        UserId = user.Id,
                        Name = user.Name,
                        ProfilePhoto = user.ProfilePhoto,
                    };

                    getAllReviewDTOs.Add(reviewDTO);
                }

                return getAllReviewDTOs;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task UpdateReview(string reviewId, string? body, int? rank)
        {
            try
            {
                var user = await _userRepository.GetCurrentUser();

                var review = _context.Reviews
                    .Where(x => x.UserId == user.Id && x.Id == reviewId)
                    .FirstOrDefault();

                bool temp = false;

                if (review is null)
                    throw new DoesNotBelongException("Review");

                if (!body.IsNullOrWhiteSpace())
                {
                    review.Body = body;
                    temp = true;
                }

                if (rank != null)
                {
                    review.Rank = Convert.ToInt32(rank);

                    temp = true;

                    await ChangeProductRank(review.ProductId);
                }

                if (temp)
                {
                    review.UpdatedAt = DateTime.UtcNow;

                    _context.Reviews.Update(review);

                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task ChangeProductRank(string productId)
        {
            var reviewsForProduct = _context.Reviews.Where(x => x.ProductId == productId).ToList();

            double reviewsRankSum = reviewsForProduct.Sum(x => x.Rank);

            double newRankForProduct;

            if (reviewsForProduct.Count is 0)
                newRankForProduct = 0;
            else
                newRankForProduct = reviewsRankSum / reviewsForProduct.Count;

            
            var product = _context.Products.Where(x => x.Id == productId).FirstOrDefault();

            if (product != null)
                product.Rank = newRankForProduct;

            _context.Products.Update(product);

            await _context.SaveChangesAsync();
        }

        public async Task DeleteReview(string reviewId)
        {
            try
            {
                var user = await _userRepository.GetCurrentUser();

                var review = _context.Reviews
                    .Where(x => x.UserId == user.Id && x.Id == reviewId)
                    .FirstOrDefault();

                if (review is null)
                    throw new DoesNotBelongException("Review");

                _context.Reviews.Remove(review);

                await _context.SaveChangesAsync();

                await ChangeProductRank(review.ProductId);
            }
            catch (Exception)
            {
                throw;
            }
        }


    }
}
