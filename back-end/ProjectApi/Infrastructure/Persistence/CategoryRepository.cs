﻿using AppDomain.DTOs.Tags;
using AppDomain.Entities.TagBaseRelated;
using AppDomain.Enums;
using AppDomain.Exceptions.UserExceptions;
using AppDomain.Interfaces;
using AutoMapper;

namespace Infrastructure.Persistence;

public class CategoryRepository : ICategoryRepository
{
    private readonly ClubrickDbContext _context;
    private readonly IUserRepository _userRepository;
    private readonly IAzureBlobStorageService _azureBlobStorage;
    private readonly IMapper _mapper;

    public CategoryRepository(
        ClubrickDbContext context,
        IMapper mapper,
        IUserRepository userRepository,
        IAzureBlobStorageService azureBlobStorage
    )
    {
        _context = context;
        _mapper = mapper;
        _userRepository = userRepository;
        _azureBlobStorage = azureBlobStorage;
    }

    public async Task<string> DeleteCategory(string categoryId)
    {
        var existingCategory = await _context.Categories.FindAsync(categoryId);

        if (existingCategory is null)
            return null;

        existingCategory.IsDeleted = true;

        _context.Categories.Update(existingCategory);

        await _context.SaveChangesAsync();

        return categoryId;
    }

    public async Task<List<TagFullResponseDto>> GetAllCategory(string tokenId, string keyword)
    {
        try
        {
            List<TagFullResponseDto> tagFullResponses = new();

            var user = _context.Users.FirstOrDefault(x => x.Id == tokenId);

            var result = _context.Categories.ToList();
            if (keyword is not null)
            {
                result = result.Where(x => x.Title.ToLower().Contains(keyword.ToLower())).ToList();
            }

            if (user is not null)
                user.CategoryFollowedList ??= new();

            foreach (var item in result)
            {
                TagFullResponseDto responseDto = _mapper.Map<TagFullResponseDto>(item);

                if (user is not null)
                    responseDto.IsFollowed = user.CategoryFollowedList.Any(c => item.Id.Equals(c));

                tagFullResponses.Add(responseDto);
            }

            return tagFullResponses;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async Task<Category> GetCategoryById(string id)
    {
        return _context.Categories.Where(c => c.Id == id).FirstOrDefault();
    }

    public async Task<string> InsertCategory(Category category)
    {
        var user = await _userRepository.GetCurrentUser();

        if (user.IsFrozen)
            throw new UserForbiddenException(
                user.Id,
                "insert category",
                user.FrozenUntil ?? DateTime.Now
            );

        try
        {
            var find = _context.Categories.FirstOrDefault(
                x => x.Title.ToLower().Equals(category.Title.ToLower())
            );

            if (find is not null)
                return "0";

            if (!string.IsNullOrWhiteSpace(category.IconLink))
            {
                var iconLink = await _azureBlobStorage.UploadTagIconAsync(
                    category.IconLink,
                    category.Id
                );

                category.IconLink = iconLink;
            }

            var inserted = _context.Categories.Add(category);

            await _context.SaveChangesAsync();

            return inserted.Entity.Id;
        }
        catch (Exception)
        {
            return "-1";
        }
    }

    public async Task<string> UpdateCategory(Category category)
    {
        try
        {
            var existingCategory = await _context.Categories.FindAsync(category.Id);

            if (existingCategory is null)
                return "0";

            if (!existingCategory.Title.Equals(category.Title, StringComparison.OrdinalIgnoreCase))
            {
                var conflict = _context.Categories.FirstOrDefault(
                    x => x.Title.ToLower().Equals(category.Title.ToLower())
                );

                if (conflict is not null)
                    return "-2";
            }

            category.UseCount = existingCategory.UseCount;

            if (
                !string.IsNullOrWhiteSpace(category.IconLink)
                && !category.IconLink.StartsWith("https://")
            )
                if (string.IsNullOrWhiteSpace(existingCategory.IconLink))
                {
                    var iconLink = await _azureBlobStorage.UploadTagIconAsync(
                        category.IconLink,
                        category.Id
                    );

                    category.IconLink = iconLink;
                }
                else
                {
                    category.IconLink = await _azureBlobStorage.UpdateTagIconAsync(
                        category.IconLink,
                        existingCategory.IconLink,
                        existingCategory.Id
                    );
                }

            _context.Entry(existingCategory).CurrentValues.SetValues(category);

            await _context.SaveChangesAsync();

            return category.Id;
        }
        catch (Exception)
        {
            return "-1";
        }
    }

    public async Task<int> IncrementUseCounts(List<string> categoryIdList)
    {
        try
        {
            bool temp = false;
            foreach (var item in categoryIdList)
            {
                var existingCategory = await _context.Categories.FindAsync(item);

                if (existingCategory is null)
                    continue;

                existingCategory.UseCount++;

                temp = true;

                _context.Update(existingCategory);
            }

            await _context.SaveChangesAsync();

            if (temp is false)
                return 0;
            return 1;
        }
        catch (Exception)
        {
            return 0;
        }
    }

    public async Task<int> DecrementUseCounts(List<string> categoryIdList)
    {
        try
        {
            bool temp = false;
            foreach (var item in categoryIdList)
            {
                var existingCategory = await _context.Categories.FindAsync(item);

                if (existingCategory is null)
                    continue;

                if (existingCategory.UseCount > 0)
                    existingCategory.UseCount--;

                temp = true;

                _context.Update(existingCategory);
            }

            await _context.SaveChangesAsync();
            if (temp is false)
                return 0;
            return 1;
        }
        catch (Exception)
        {
            return 0;
        }
    }

    public async Task<int> Follow(string categoryId, string tokenId)
    {
        try
        {
            var findCategory = _context.Categories.FirstOrDefault(c => c.Id == categoryId);

            if (findCategory is null)
                return 0;

            var findUser = _context.Users.FirstOrDefault(x => x.Id == tokenId);

            findUser.CategoryFollowedList ??= new List<string>();

            if (findUser.CategoryFollowedList.Any(c => c.Equals(categoryId)))
            {
                findUser.CategoryFollowedList.Remove(categoryId);
                findCategory.UseCount--;
            }
            else
            {
                findUser.CategoryFollowedList.Add(categoryId);
                findCategory.UseCount++;
            }

            _context.Categories.Update(findCategory);

            _context.Users.Update(findUser);

            await _context.SaveChangesAsync();

            return 1;
        }
        catch (Exception)
        {
            return -1;
        }
    }

    public async Task<List<TagFullResponseDto>> GetAllFollowedCategory(
        string tokenId,
        string keyword
    )
    {
        try
        {
            var findUser = _context.Users.FirstOrDefault(x => x.Id == tokenId);

            if (findUser.CategoryFollowedList is null)
                return null;

            var result = new List<TagFullResponseDto>();

            foreach (var item in findUser.CategoryFollowedList)
            {
                var category = _context.Categories.FirstOrDefault(x => x.Id == item);
                if (category != null)
                {
                    TagFullResponseDto tagFullResponse = _mapper.Map<TagFullResponseDto>(category);
                    tagFullResponse.IsFollowed = true;
                    result.Add(tagFullResponse);
                }
            }

            if (keyword is not null)
                result = result.Where(x => x.Title.Contains(keyword)).ToList();

            return result;
        }
        catch (Exception)
        {
            throw;
        }
    }
}
