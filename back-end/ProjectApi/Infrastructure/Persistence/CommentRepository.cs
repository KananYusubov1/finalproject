﻿using AppDomain.DTO;
using AppDomain.Entities.ContentRelated;
using AppDomain.Entities.NotificationRelated.AdditionalDetails;
using AppDomain.Entities.UserActionRelated;
using AppDomain.Entities.UserRelated;
using AppDomain.Enums;
using AppDomain.Exceptions.Common;
using AppDomain.Exceptions.UserExceptions;
using AppDomain.Interfaces;
using Application.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;

namespace Infrastructure.Persistence;

/// <summary>
/// Repository for managing comments.
/// </summary>
public class CommentRepository : ICommentRepository
{
    private readonly ClubrickDbContext _context;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly INotificationRepository _notificationRepository;

    /// <summary>
    /// Initializes a new instance of the <see cref="CommentRepository"/> class.
    /// </summary>
    /// <param name="context">The database context for accessing comment-related data.</param>
    public CommentRepository(
        ClubrickDbContext context,
        IHttpContextAccessor httpContextAccessor,
        INotificationRepository notificationRepository)
    {
        _context = context;
        _httpContextAccessor = httpContextAccessor;
        _notificationRepository = notificationRepository;
    }

    /// <inheritdoc/>
    public async Task<CommentDTO> InsertCommentAsync(string commentBody, string contentId, string parentCommentId = null)
    {
        var comment = new Comment
        {
            Id = IDGeneratorService.GetShortUniqueId(),
            UserId = GetCurrentUserId(),
            Body = commentBody,
            UpdateTime = DateTime.UtcNow,
            VoteCount = 0,
            IsDeleted = false,
            ParentCommentId = parentCommentId,
            ArticleId = contentId
        };

        _context.Comments.Add(comment);

        var commentDetails = new CommentDetails
        {
            EntryId = contentId,
            CommentId = comment.Id,
        };

        await _context.SaveChangesAsync();

        var article = _context.Articles.FirstOrDefault(art => art.Id == contentId);

        if (article.UserId != comment.UserId)
            await _notificationRepository.PostNotificationAsync(
                new()
                {
                    Type = NotificationType.Comment,
                    AdditionalDetails = JsonConvert.SerializeObject(commentDetails)
                });

        return comment.ToCommentDTO();
    }
    public async Task<Task> DeleteCommentAsync(string commentId)
    {
        var comment = await _context.Comments.FindAsync(commentId);

        comment.IsDeleted = true;

        var childComments  = _context.Comments
            .Where(c => c.ParentCommentId == commentId)
            .ToList();

        DeleteChildComments(commentId, childComments);

        _context.Comments.Update(comment);

        await _context.SaveChangesAsync();

        return Task.CompletedTask;
    }
    private IEnumerable<Comment> DeleteChildComments(string parentCommentId, List<Comment> comments)
    {
        var childComments = comments.Where(comment => comment.ParentCommentId == parentCommentId).ToList();

        comments.RemoveAll(c => childComments.Any(child => child.Id == c.Id));

        foreach (var child in childComments)
        {
            child.IsDeleted = true;
            DeleteChildComments(child.Id, comments);
        }

        return childComments;
    }
    /// <inheritdoc/>
    public async Task<Task> EditCommentAsync(string commentId, string content)
    {
        var user = await GetCurrentUser();

        if (user.IsFrozen)
            throw new UserForbiddenException(
                user.Id,
                "edit comment",
                user.FrozenUntil ?? DateTime.Now
            );

        var comment = _context.Comments.Find(commentId);

        if (comment is null)
            throw new EntityExistException("Comment not found: " + commentId);

        if (comment.UserId != user.Id)
            throw new UnauthorizedAccessException("You are not allowed to edit this comment");

        comment.Body = content;

        _context.Comments.Update(comment);

        await _context.SaveChangesAsync();

        return Task.CompletedTask;
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<CommentDTO>> GetAllCommentsAsync(string articleId)
    {
        var comments = _context.Comments
            .Where(comment => comment.ArticleId == articleId)
            .ToList();

        var mainComments = comments.Where(comment => comment.ParentCommentId is null);
        var childComments = comments.Except(mainComments).ToList();

        var mainCommentsDTO = mainComments
            .Select(comment => comment.ToCommentDTO())
            .OrderByDescending(comment => comment.VoteCount)
            .ToList();

        var hierarchyComments = mainCommentsDTO
            .Select(maincomment =>
            {
                maincomment.ChildComments = GetChildComments(maincomment.Id, childComments);
                return maincomment;
            }).ToList();

        return hierarchyComments;
    }

    private IEnumerable<CommentDTO> GetChildComments(string parentCommentId, List<Comment> comments)
    {
        var childComments = comments.Where(comment => comment.ParentCommentId == parentCommentId).ToList();

        comments.RemoveAll(c => childComments.Any(child => child.Id == c.Id));

        foreach (var child in childComments)
            child.ChildComments = GetChildComments(child.Id, comments);

        return childComments
            .Select(comment => comment.ToCommentDTO())
            .OrderByDescending(comment => comment.VoteCount)
            .ToList();
    }

    public async Task VoteCommentAsync(VoteStatus status, string commentId)
    {
        var user = await GetCurrentUser();

        if (user is null)
            throw new Exception("User not found");
        else if (user.IsFrozen)
            throw new UserForbiddenException(
                user.Id,
                "vote comment",
                user.FrozenUntil ?? DateTime.Now
            );

        var comment = _context.Comments.FirstOrDefault(y => y.Id == commentId);

        if (comment is null)
            throw new Exception("Comment is not exist.");

        var voteEntity = user.VoteList.FirstOrDefault(
            s => s.ContentType == ContentType.Comment && s.ContentId.Equals(commentId)
        );

        if (voteEntity is null)
        {
            Vote vote =
                new()
                {
                    Id = IDGeneratorService.GetShortUniqueId(),
                    ContentId = commentId,
                    UserId = user.Id,
                    ContentType = ContentType.Comment,
                    Status = status
                };

            comment.VoteCount += status == VoteStatus.UpVote ? 1 : -1;

            user.VoteList.Add(vote);
        }
        else
        {
            if (status != voteEntity.Status)
            {
                if (voteEntity.Status == VoteStatus.DownVote)
                    comment.VoteCount += 1;
                else if (voteEntity.Status == VoteStatus.UpVote)
                    comment.VoteCount -= 1;

                voteEntity.Status = status;
                comment.VoteCount += status == VoteStatus.DownVote ? -1 : 1;
                _context.Update(voteEntity);
            }
            else
            {
                comment.VoteCount += status == VoteStatus.DownVote ? 1 : -1;
                user.VoteList.Remove(voteEntity);
            }
        }

        _context.Update(user);

        _context.Comments.Update(comment);

        await _context.SaveChangesAsync();
    }

    private async Task<User> GetCurrentUser()
    {
        var id = GetClaimValue("userId");

        var user = _context.Users
            .Include(r => r.SaveList)
            .Include(r => r.VoteList)
            .Include(r => r.ViewList)
            .SingleOrDefault(u => u.Id.Equals(id));

        return user;
    }

    private string GetClaimValue(string claimType)
    {
        var token = GetTokenFromRequest();

        if (token is null)
            return null;

        var claim = token.Claims.FirstOrDefault(c => c.Type == claimType);

        return claim?.Value;
    }

    private string GetCurrentUserId()
    {
        var userId = GetClaimValue("userId");

        return userId;
    }

    private JwtSecurityToken GetTokenFromRequest()
    {
        var authHeader = _httpContextAccessor.HttpContext.Request.Headers[
            "Authorization"
        ].FirstOrDefault();

        if (!string.IsNullOrEmpty(authHeader) && authHeader.StartsWith("Bearer "))
        {
            var token = authHeader.Substring("Bearer ".Length);
            var tokenHandler = new JwtSecurityTokenHandler();

            if (tokenHandler.CanReadToken(token))
                return tokenHandler.ReadJwtToken(token);
        }

        return null;
    }

    public async Task<Task> DeleteArticleCommentsAsync(string articleId)
    {
        var article = _context.Articles.Find(articleId);

        await _context.Comments
            .Where(comment => comment.ArticleId == articleId && comment.ParentCommentId == null)
            .ForEachAsync(comment =>
            {
                DeleteCommentAsync(comment.Id);
            });

        return Task.CompletedTask;
    }

    /// <inheritdoc/> Nested comment get all
    /*public async Task<IEnumerable<CommentDTO>> GetAllCommentsAsync(string contentId)
    {
        var topLevelComments = _context.Comments
            .Where(comment => comment.ArticleId == contentId && comment.ParentCommentId == null)
            .ToList();

        // Create a list to hold the top-level CommentDTO objects
        var commentDTOs = new List<CommentDTO>();

        // Convert top-level comments to CommentDTOs and build the hierarchy
        foreach (var comment in topLevelComments)
        {
            var commentDTO = ModelConvertors.ToCommentDTO(comment);
            BuildCommentHierarchy(commentDTO);
            commentDTOs.Add(commentDTO);
        }

        return commentDTOs;
    }*/

    /*private void BuildCommentHierarchy(CommentDTO commentDTO)
    {
        // Fetch child comments for the given parent comment
        var childComments = _context.Comments
            .Where(comment => comment.ParentCommentId == commentDTO.Id)
            .ToList();

        // Recursively build child comments
        foreach (var childComment in childComments)
        {
            var childCommentDTO = ModelConvertors.ToCommentDTO(childComment);
            BuildCommentHierarchy(childCommentDTO);
            commentDTO.NestedComments.Add(childCommentDTO);
        }
    }*/
}