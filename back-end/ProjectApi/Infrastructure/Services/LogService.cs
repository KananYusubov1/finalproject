﻿using AppDomain.Enums;
using Serilog;


namespace Infrastructure.Services;

public class LogService
{
    public static void LogUserAction(UserActions action, string userId, string what = "") =>
        Log.Information($"User with ID {userId} {action.ToString().ToLower()} {what}.");

    public static void LogModeratorAction(ModeratorActions action, string moderatorId, string what) =>
        Log.Information($"Moderator with ID {moderatorId} {action.ToString().ToLower()} {what}.");
    public static void LogModeratorActionOnReport(ReportStatus action, string moderatorId, string reportId) =>
        Log.Information($"Moderator with ID {moderatorId} {action.ToString().ToLower()} report with ID {reportId}.");

}