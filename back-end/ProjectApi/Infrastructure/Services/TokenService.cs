﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using AppDomain.Common.Config;
using AppDomain.Entities.UserRelated;
using AppDomain.Enums;
using AppDomain.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;

namespace Infrastructure.Services;

/// <inheritdoc/>
public class TokenService : ITokenService
{
    private readonly JwtConfig _config;

    /// <summary>
    /// Initializes a new instance of the <see cref="TokenService"/> class.
    /// </summary>
    /// <param name="config">The JWT configuration settings.</param>
    /// <exception cref="ArgumentNullException">Thrown when the <paramref name="config"/> is null.</exception>
    public TokenService(JwtConfig config) =>
        _config = config ?? throw new ArgumentNullException(nameof(config));

    /// <inheritdoc/>
    public RefreshToken GenerateRefreshToken()
    {
        var refreshToken = new RefreshToken
        {
            Token = Convert.ToBase64String(RandomNumberGenerator.GetBytes(64)),
            ExpirationDate = DateTime.UtcNow.AddDays(7),
        };

        return refreshToken;
    }

    /// <inheritdoc/>
    public HttpContext SetRefreshToken(RefreshToken token, HttpContext context)
    {
        var cookieOptions = new CookieOptions
        {
            HttpOnly = true,
            Expires = token.ExpirationDate,
            Secure = true,
            SameSite = SameSiteMode.Strict,
        };

        context.Response.Cookies.Append("refreshToken", token.Token, cookieOptions);

        return context;
    }

    /// <inheritdoc/>
    public HttpContext SetAuthToken(AuthToken token, HttpContext context)
    {
        var cookieOptions = new CookieOptions
        {
            HttpOnly = true,
            Expires = token.ExpirationDate,
            Secure = true,
            SameSite = SameSiteMode.Strict,
        };

        context.Response.Cookies.Append("authToken", token.Token, cookieOptions);

        return context;
    }

    /// <inheritdoc/>
    public AuthToken GenerateSecurityToken(string id, string email, UserRole role)
    {
        var claims = new[]
        {
            new Claim("userId", id),
            new Claim("userEmail", email),
            new Claim("userRole", role.ToString())
        };

        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.Secret));
        var signingCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

        var expires = DateTime.UtcNow.AddMinutes(_config.ExpiresInMinutes);

        var token = new JwtSecurityToken(
            issuer: _config.Issuer,
            audience: _config.Audience,
            expires: expires,
            signingCredentials: signingCredentials,
            claims: claims
        );

        var accessToken = new JwtSecurityTokenHandler().WriteToken(token);

        return new AuthToken() { Token = accessToken, ExpirationDate = expires };
    }

    /// <inheritdoc/>
    public string GetClaimsFromToken(string token, string claimType)
    {
        var signingKey = _config.Secret;

        var tokenHandler = new JwtSecurityTokenHandler();
        var validationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(Convert.FromBase64String(signingKey)),
            ValidateIssuer = false,
            ValidateAudience = false
        };

        try
        {
            var principal = tokenHandler.ValidateToken(token, validationParameters, out _);

            var claim = principal.Claims.FirstOrDefault(c => c.Type == claimType);

            return claim?.Value;
        }
        catch (SecurityTokenException)
        {
            return null;
        }
    }
}
