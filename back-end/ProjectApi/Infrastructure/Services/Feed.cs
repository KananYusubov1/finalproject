﻿using AppDomain.Entities.ContentRelated;
using AppDomain.Entities.NotificationRelated;
using AppDomain.Entities.TagBaseRelated;
using AppDomain.Entities.UserRelated;
using AppDomain.Enums;
using AppDomain.Responses;
using AppDomain.ValueObjects;
using Application.Services;
using Bogus;
using Infrastructure.Persistence;
using Newtonsoft.Json;

namespace Infrastructure.Services;

/// <summary>
/// The <c>Feed</c> class provides methods to populate a <c>ClubrickDbContext</c> with test data for various entities.
/// It uses the Bogus library to generate fake data for these entities.
/// </summary>
public class Feed
{
    private readonly ClubrickDbContext _context;
    private readonly List<ResourceFlag> ResourceFlags;
    private readonly List<Category> Categories;
    private readonly List<Question> Questions;
    private readonly List<Comment> Comments;
    private readonly List<Article> Articles;
    private readonly List<Answer> Answers;
    private readonly List<User> Users;
    private readonly Faker faker;

    /// <summary>
    /// Initializes a new instance of the <see cref="Feed"/> class with the provided database context.
    /// </summary>
    /// <param name="context">The <see cref="ClubrickDbContext"/> for accessing database data.</param>
    public Feed(ClubrickDbContext context)
    {
        _context = context;
        ResourceFlags = _context.ResourceFlags.ToList();
        Categories = _context.Categories.ToList();
        Questions = _context.Questions.ToList();
        Comments = _context.Comments.ToList();
        Answers = _context.Answers.ToList();
        Articles = _context.Articles.ToList();
        Users = _context.Users.ToList();
        faker = new Faker();
    }

    /// <summary>
    /// Populates tags of a specified type with fake data.
    /// </summary>
    /// <param name="tagType">The type of tags to populate (e.g., Category, ArticleFlag, ResourceFlag).</param>
    /// <param name="numberOfTags">The number of tags to generate.</param>
    public async Task<IEnumerable<TagBase>> FeedTag(TagType tagType, int numberOfTags)
    {
        var tags = new List<TagBase>();

        foreach (var i in Enumerable.Range(1, numberOfTags))
        {
            var tag = new TagBase
            {
                Id = IDGeneratorService.GetShortUniqueId(),
                Title = faker.Commerce.ProductName(),
                Description = faker.Lorem.Sentence(),
                UseCount = faker.Random.Int(0, 100),
                IconLink = faker.Internet.Url(),
                AccentColor = faker.Commerce.Color(),
                IsDeleted = false,
            };

            switch (tagType)
            {
                case TagType.Category:
                    _context.Categories.Add(new Category(tag));
                    break;
                case TagType.ArticleFlag:
                    _context.ArticleFlags.Add(new ArticleFlag(tag));
                    break;
                case TagType.ResourceFlag:
                    _context.ResourceFlags.Add(new ResourceFlag(tag));
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(tagType), tagType, null);
            }

            tags.Add(tag);
        }

        await _context.SaveChangesAsync();

        return tags;
    }

    /// <summary>
    /// Generates and adds fake resource data to the database.
    /// </summary>
    /// <param name="numberOfResources">The number of fake resources to generate.</param>
    public async Task<IEnumerable<Resource>> FeedResource(int numberOfResources)
    {
        var resources = new List<Resource>();

        foreach (var i in Enumerable.Range(1, numberOfResources))
        {
            var resource = new Resource
            {
                Id = IDGeneratorService.GetShortUniqueId(),
                UserId = faker.Random.ListItem(Users).Id ?? null,
                UpdateTime = faker.Date.Recent(),
                VoteCount = faker.Random.Int(0, 100),
                Title = faker.Commerce.ProductName(),
                Url = faker.Internet.Url(),
                Category = faker.Random.ListItem(Categories) ?? null,
                Flag = faker.Random.ListItem(ResourceFlags) ?? null,
                VisitCount = faker.Random.Int(0, 100),
                IsDeleted = false,
            };

            _context.Resources.Add(resource);

            resources.Add(resource);
        }

        await _context.SaveChangesAsync();

        return resources;
    }

    /// <summary>
    /// Generates and adds fake question data to the database.
    /// </summary>
    /// <param name="numberOfQuestions">The number of fake questions to generate.</param>
    public async Task<IEnumerable<Question>> FeedQuestion(int numberOfQuestions)
    {
        var questions = new List<Question>();

        foreach (var i in Enumerable.Range(1, numberOfQuestions))
        {
            var question = new Question
            {
                Id = IDGeneratorService.GetShortUniqueId(),
                UserId = faker.Random.ListItem(Users).Id ?? null,
                UpdateTime = faker.Date.Recent(),
                VoteCount = faker.Random.Int(0, 100),
                Body = faker.Lorem.Sentence(),
                Categories =
                    new List<Category>(faker.Random.ListItems(Categories, faker.Random.Int(1, 3)))
                    ?? null,
                Header = new Header
                {
                    Title = faker.Commerce.ProductName(),
                    Image = faker.Internet.Url(),
                },
                ViewCount = faker.Random.Int(0, 100),
                ReadingTime = faker.Random.Int(1, 10),
                WatchCount = faker.Random.Int(0, 100),
                CorrectAnswerId = faker.Random.ListItem(Answers).Id ?? null,
                IsDeleted = false,
            };

            _context.Questions.Add(question);

            questions.Add(question);
        }

        await _context.SaveChangesAsync();

        return questions;
    }

    /// <summary>
    /// Generates and adds fake answer data to the database.
    /// </summary>
    /// <param name="numberOfAnswers">The number of fake answers to generate.</param>
    public async Task<IEnumerable<Answer>> FeedAnswer(int numberOfAnswers)
    {
        var answers = new List<Answer>();

        var question = faker.Random.ListItem(Questions);

        foreach (var i in Enumerable.Range(1, numberOfAnswers))
        {
            var answer = new Answer
            {
                Id = IDGeneratorService.GetShortUniqueId(),
                UserId = faker.Random.ListItem(Users).Id ?? null,
                UpdateTime = faker.Date.Recent(),
                VoteCount = faker.Random.Int(0, 100),
                Body = faker.Lorem.Sentence(),
                QuestionId = question.Id ?? null,
                Question = question ?? null,
                IsDeleted = false,
            };

            _context.Answers.Add(answer);

            answers.Add(answer);
        }

        await _context.SaveChangesAsync();

        return answers;
    }

    /// <summary>
    /// Generates and adds fake comment data to the database.
    /// </summary>
    /// <param name="numberOfComments">The number of fake comments to generate.</param>
    public async Task<IEnumerable<Comment>> FeedComment(int numberOfComments)
    {
        var comments = new List<Comment>();

        var contentType = faker.Random.Enum<ContentType>();

        foreach (var i in Enumerable.Range(1, numberOfComments))
        {
            var comment = new Comment
            {
                Id = IDGeneratorService.GetShortUniqueId(),
                UserId = faker.Random.ListItem(Users).Id,
                UpdateTime = faker.Date.Between(DateTime.UtcNow.AddYears(-2), DateTime.UtcNow),
                VoteCount = faker.Random.Int(0, 100),
                Body = faker.Lorem.Sentence(),
                ParentCommentId = Comments.Count is not 0
                    ? faker.Random.ListItem(Comments).Id
                    : null,
                ArticleId = faker.Random.ListItem(Articles).Id,
                IsDeleted = false,
            };

            _context.Comments.Add(comment);

            comments.Add(comment);
        }

        await _context.SaveChangesAsync();

        return comments;
    }

    public async Task<IEnumerable<Notification>> FeedNotification(int numberOfNotifications)
    {
        var notifications = new List<Notification>();

        //foreach (var i in Enumerable.Range(1, numberOfNotifications))
        //{
        //    var type = faker.PickRandom<NotificationType>();

        //    var notification = new Notification
        //    {
        //        TriggerUserId = faker.PickRandom(Users).Id,
        //        Type = type,
        //        ContentId = type switch
        //        {
        //            NotificationType.Follow => faker.PickRandom(Users).Id,
        //            NotificationType.Answer => faker.PickRandom(Answers).Id,
        //            _ => string.Empty,
        //        },
        //        Body = null,
        //        Content = null,
        //        Message = faker.Lorem.Sentence(),
        //        CreatedAt = faker.Date.Between(DateTime.UtcNow.AddYears(-2), DateTime.UtcNow),
        //        IsViewed = false,
        //    };

        //    var user = new User();
        //    user.NotificationList = new List<Notification>();

        //    switch (notification.Type)
        //    {
        //        case NotificationType.Follow:

        //            var triggerUser = await _context.Users
        //                .FindAsync(notification.TriggerUserId);

        //            var triggerUserFollowDTO = triggerUser.ToFollowNotificationResponse();

        //            notification.Body = JsonConvert.SerializeObject(triggerUserFollowDTO);
        //            notification.Content = triggerUserFollowDTO;

        //            user = _context.Users.Find(notification.ContentId);
        //            if (user.NotificationList is null)
        //                user.NotificationList = new List<Notification>();
        //            user.NotificationList.Add(notification);

        //            _context.Users.Update(user);

        //            break;
        //        case NotificationType.Answer:
        //            var userIds = _context.Watches
        //                .Where(x => x.ContentId == notification.ContentId)
        //                .Select(x => x.UserId)
        //                .ToList();

        //            var users = _context.Users.ToList();

        //            foreach (var userId in userIds)
        //            {
        //                user = users.Find(x => x.Id == userId);

        //                var triggerUserAnswerDTO = user.ToAnswerNotificationResponse(,);

        //                notification.Body = JsonConvert.SerializeObject(triggerUserAnswerDTO);
        //                notification.Content = triggerUserAnswerDTO;

        //                user.NotificationList.Add(notification);

        //                _context.Users.Update(user);
        //            }
        //            break;
        //        default:
        //            break;
        //    }

        //    notifications.Add(notification);
        //}

        //await _context.SaveChangesAsync();

        return notifications;
    }
}
