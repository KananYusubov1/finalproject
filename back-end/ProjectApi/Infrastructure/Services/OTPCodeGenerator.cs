﻿namespace Infrastructure.Services;

/// <summary>
/// Service for Generating OTP codes.
/// </summary>
public static class OTPCodeGenerator
{
    /// <summary>
    /// Generate Otp code.
    /// </summary>
    /// <returns></returns>
    public static string GenerateOTPCode()
    {
        Random random = new();
        int code = random.Next(100000, 999999);

        return code.ToString("D6");
    }
}