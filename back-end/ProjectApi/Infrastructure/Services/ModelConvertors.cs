﻿using AppDomain.DTO;
using AppDomain.DTOs.Answer;
using AppDomain.DTOs.Article;
using AppDomain.DTOs.Buds;
using AppDomain.DTOs.Category;
using AppDomain.DTOs.Comment;
using AppDomain.DTOs.Product;
using AppDomain.DTOs.Question;
using AppDomain.DTOs.Report;
using AppDomain.DTOs.Resource;
using AppDomain.DTOs.User;
using AppDomain.Entities.AdminRelated;
using AppDomain.Entities.ContentRelated;
using AppDomain.Entities.NotificationRelated.AdditionalDetails;
using AppDomain.Entities.TagBaseRelated;
using AppDomain.Entities.UserActionRelated;
using AppDomain.Entities.UserRelated;
using AppDomain.Enums;
using AppDomain.Responses;
using AppDomain.Responses.UserResponses;
using AppDomain.ValueObjects;
using Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

/// <summary>
/// Provides extension methods for converting between entity models and Data Transfer Objects (DTOs).
/// </summary>
public static class ModelConvertors
{
    /// <summary>
    /// Converts a <see cref="User"/> entity to a <see cref="UserDTO"/> Data Transfer Object.
    /// </summary>
    /// <param name="user">The <see cref="User"/> entity to convert.</param>
    /// <returns>The converted <see cref="UserDTO"/>.</returns>
    public static UserDTO ToUserDTO(this User user)
    {
        return new UserDTO
        {
            UserName = user.Username,
            Email = user.Email,
            Name = user.Name,
            Password = user.Password,
            ProfilePhoto = user.ProfilePhoto,
            PersonalInfo = user.PersonalInfo,
            CategoryFollowedList = user.CategoryFollowedList,
            Brand = user.Brand,
            Settings = user.Settings,
            JoinedTime = user.JoinedTime,
            ConnectedAccountList = user.ConnectedAccountList,
            BadgeList = user.BadgeList,
            NotificationList = user.NotificationList,
        };
    }

    /// <summary>
    /// Converts a <see cref="PendingUser"/> entity to a <see cref="PendingUserDTO"/> Data Transfer Object.
    /// </summary>
    /// <param name="user">The <see cref="PendingUser"/> entity to convert.</param>
    /// <returns>The converted <see cref="PendingUserDTO"/>.</returns>
    public static PendingUserDTO ToPendingUserDTO(this PendingUser user)
    {
        return new PendingUserDTO
        {
            Name = user.Name,
            Email = user.Email,
            Password = user.Password,
            JoinedTime = user.JoinedTime
        };
    }

    /// <summary>
    /// Converts a <see cref="User"/> entity to a <see cref="UserViewResponse"/> Data Transfer Object,
    /// including additional information like answer counts.
    /// </summary>
    /// <param name="user">The <see cref="User"/> entity to convert.</param>
    /// <param name="answers">A collection of answers associated with the user.</param>
    /// <returns>The converted <see cref="UserViewResponse"/>.</returns>
    public static UserViewResponse ToUserViewResponse(
        this User user,
        UserViewResponsePublishedCounts counts
    )
    {
        return new UserViewResponse
        {
            Id = user.Id,
            Name = user.Name,
            DisplayEmail = user.PersonalInfo?.DisplayEmail,
            ProfilePhoto = user.ProfilePhoto,
            PersonalInfo = user.PersonalInfo,
            FollowedCategoryCount = user.CategoryFollowedList.Count(),
            PublishedArticlesCount = counts.PublishedArticlesCount,
            PublishedAnswerCount = counts.PublishedAnswerCount,
            PublishedQuestionCount = counts.PublishedQuestionCount,
            Brand = user.Brand,
            ConnectedAccountList = user.ConnectedAccountList,
            BadgeList = user.BadgeList,
            Joined = user.JoinedTime
        };
    }

    public static UserPreviewResponse ToUserPreviewResponse(this User user)
    {
        return new UserPreviewResponse
        {
            Id = user.Id,
            Name = user.Name,
            Username = user.Username,
            DisplayEmail = user.PersonalInfo?.DisplayEmail,
            ProfilePhoto = user.ProfilePhoto,
            Brand = user.Brand,
            Bio = user.PersonalInfo?.Bio,
            Location = user.PersonalInfo?.Location,
            Education = user.PersonalInfo?.Education,
            Work = user.PersonalInfo?.Work,
            Joined = user.JoinedTime
        };
    }

    public static UserProfileResponse ToUserProfileResponse(this User user)
    {
        return new UserProfileResponse
        {
            UserId = user.Id,
            Name = user.Name,
            Username = user.Username,
            Email = user.Email,
            ProfilePhoto = user.ProfilePhoto
        };
    }

    public static FollowNotificationResponse ToFollowNotificationResponse(this User user)
    {
        return new FollowNotificationResponse
        {
            CreatedAt = DateTime.UtcNow,
            FollowerUserProfile = user.ToUserProfileResponse(),
        };
    }

    public async static Task<AnswerNotificationResponse> ToAnswerNotificationResponse(
        string answerId,
        ClubrickDbContext _context
    )
    {
        var answer = await _context.Answers.FindAsync(answerId);

        return new AnswerNotificationResponse
        {
            CreatedAt = DateTime.UtcNow,
            AnswerDTO = await answer.ToAnswerDTO(_context),
            QuestionDTO = await answer.Question.ToQuestionDTO(_context),
        };
    }

    public static async Task<ArticleNotificationResponse> ToArticleNotificationResponse(
        string articleId,
        ClubrickDbContext _context
    )
    {
        var article = await _context.Articles.FindAsync(articleId);

        return new ArticleNotificationResponse
        {
            CreatedAt = DateTime.UtcNow,
            ArticleDTO = await article.ToArticleDTO(_context)
        };
    }

    public static async Task<ProductPurchaseNotificationResponse> ToProductPurchaseNotificationResponse(
        this ProductPurchaseDetails productDetails,
        ClubrickDbContext _context
    )
    {
        var product = _context.Products.FirstOrDefault(pro => pro.Id == productDetails.ProductId);

        return new ProductPurchaseNotificationResponse
        {
            Product = product.ToGetAllProductDTO(),
            PurchaseQuantity = productDetails.PurchaseQuantity,
            TotalSum = productDetails.TotalSum
        };
    }

    public static GetAllProductDTO ToGetAllProductDTO(this Product product)
    {
        return new GetAllProductDTO
        {
            Id = product.Id,
            Name = product.Name,
            Photos = product.Photos,
            Rank = product.Rank,
            SellCount = product.SellCount,
            Price = product.Price,
            Discount = product.Discount,
            DiscountedPrice = product.Discount
        };
    }

    public static async Task<BudsNotificationResponse> ToBudsNotificationResponse(
        this BudsDetails budsDetails,
        ClubrickDbContext _context
    )
    {
        var entryDTO = new object();
        object entry;
        switch (budsDetails.BudsEntryType)
        {
            case ContentType.Article:
                entry = await _context.Articles.FindAsync(budsDetails.BudsEntryId);
                var articleDTO = await (entry as Article).ToArticleDTO(_context);
                entryDTO = JsonConvert.SerializeObject(articleDTO);
                break;
            case ContentType.Answer:
                entry = await _context.Answers.FindAsync(budsDetails.BudsEntryId);
                var answerDTO = await (entry as Answer).ToAnswerDTO(_context);
                entryDTO = JsonConvert.SerializeObject(answerDTO);
                break;
            case ContentType.Comment:
                // entry = _context.Comments.Find(budsDetails.BudsEntryId);
                // entryDTO = (entry as Comment).ToCommentDTO();
                break;
            case ContentType.Lesson:
                // coming sooon
                break;
            case ContentType.Question:
                entry = await _context.Questions.FindAsync(budsDetails.BudsEntryId);
                var questionDTO = await (entry as Question).ToQuestionDTO(_context);
                entryDTO = JsonConvert.SerializeObject(questionDTO);
                break;
            case ContentType.Resource:
                entry = await _context.Resources.FindAsync(budsDetails.BudsEntryId);
                var resourceDTO = (entry as Resource).ToResourceDTO();
                entryDTO = JsonConvert.SerializeObject(resourceDTO);
                break;
            case ContentType.Product:
                entry = await _context.Products.FindAsync(budsDetails.BudsEntryId);
                var productDTO = (entry as Product).ToGetAllProductDTO();
                entryDTO = JsonConvert.SerializeObject(productDTO);
                break;
            default:
                break;
        }

        return new BudsNotificationResponse
        {
            BudsDTO = budsDetails.ToBudsDTO(),
            EntryDTO = entryDTO,
        };
    }

    public static BudsDTO ToBudsDTO(this BudsDetails budsDetails)
    {
        return new BudsDTO
        {
            CreatedTime = DateTime.Now,
            BudsAmount = budsDetails.BudsAmount,
            ForWhat = budsDetails.ForWhat,
            BudsEntryType = budsDetails.BudsEntryType,
        };
    }

    public static async Task<CommentNotificationResponse> ToCommentNotificationResponse(
        string commentId,
        ClubrickDbContext _context
    )
    {
        var comment = await _context.Comments.FindAsync(commentId);
        var article = await _context.Articles.FindAsync(comment.ArticleId);
        var user = await _context.Users.FindAsync(comment.UserId);

        return new CommentNotificationResponse
        {
            CreatedAt = DateTime.UtcNow,
            ArticleDTO = await article.ToArticleDTO(_context),
            CommentDTO = comment.ToArticleCommentDTO(user),
        };
    }

    public static UserContentTitleResponse ToUserContentTitleResponse(this User user)
    {
        return new UserContentTitleResponse
        {
            UserId = user.Id,
            Name = user.Name,
            Username = user.Username,
            ProfilePhoto = user.ProfilePhoto
        };
    }

    public async static Task<GetByIdAnswerDTO> ToAnswerDTO(
        this Answer answer,
        ClubrickDbContext _context
    )
    {
        var user = await _context.Users.FindAsync(answer.UserId);

        var vote = await _context.Votes.FirstOrDefaultAsync(
            x => x.UserId == answer.UserId && x.ContentType == ContentType.Answer
        );
        return new GetByIdAnswerDTO
        {
            Id = answer.Id,
            Body = answer.Body,
            UserContent = user.ToUserContentTitleResponse(),
            VoteDTO = vote?.ToVoteDTO(answer.VoteCount),
            UpdateTime = answer.UpdateTime,
        };
    }

    public static CommentDTO ToCommentDTO(this Comment comment)
    {
        return new CommentDTO
        {
            Id = comment.Id,
            UserId = comment.UserId,
            Body = comment.Body,
            UpdateTime = comment.UpdateTime,
            VoteCount = comment.VoteCount,
        };
    }

    public static GetArticleCommentDTO ToArticleCommentDTO(this Comment comment, User user)
    {
        return new GetArticleCommentDTO
        {
            Id = comment.Id,
            Body = comment.Body,
            UpdateTime = comment.UpdateTime,
            UserContent = user.ToUserContentTitleResponse(),
            VoteDTO = null
        };
    }

    public static Report ToReport(this ReportDTO reportDTO)
    {
        return new Report
        {
            Id = reportDTO.Id,
            ContentType = reportDTO.ContentType,
            ContentId = reportDTO.ContentId,
            ReportType = reportDTO.ReportType,
            ReportMessage = reportDTO.ReportMessage,
        };
    }

    public static ReportDTO ToReportDTO(this Report report)
    {
        return new ReportDTO
        {
            Id = report.Id,
            ContentType = report.ContentType,
            ContentId = report.ContentId,
            ReportType = report.ReportType,
            ReportMessage = report.ReportMessage,
        };
    }

    public static ContentView ToContentView(this Question question)
    {
        return new ContentView
        {
            ContentId = question.Id,
            UserId = question.UserId,
            Title = question.Header.Title,
            ContentType = ContentType.Question,
            UpdateTime = question.UpdateTime
        };
    }

    public static ContentView ToContentView(this Answer answer)
    {
        return new ContentView
        {
            ContentId = answer.Id,
            UserId = answer.UserId,
            Title = answer.Question.Header.Title,
            ContentType = ContentType.Answer,
            UpdateTime = answer.UpdateTime
        };
    }

    public static ContentView ToContentView(this Article article)
    {
        return new ContentView
        {
            ContentId = article.Id,
            UserId = article.UserId,
            Title = article.Header.Title,
            ContentType = ContentType.Article,
            UpdateTime = article.UpdateTime
        };
    }

    public static ContentView ToContentView(this Comment comment)
    {
        return new ContentView
        {
            ContentId = comment.Id,
            UserId = comment.UserId,
            Title = comment.Body,
            ContentType = ContentType.Comment,
            UpdateTime = comment.UpdateTime
        };
    }

    public static ContentView ToContentView(this Lesson lesson)
    {
        return new ContentView
        {
            ContentId = lesson.Id,
            UserId = lesson.UserId,
            Title = lesson.Header.Title,
            ContentType = ContentType.Lesson,
            UpdateTime = lesson.UpdateTime
        };
    }

    public static async Task<GetAllArticleDTO> ToArticleDTO(
        this Article article,
        ClubrickDbContext _context
    )
    {
        var user = await _context.Users.FindAsync(article.UserId);

        var vote = await _context.Votes.FirstOrDefaultAsync(
            x => x.UserId == article.UserId && x.ContentType == ContentType.Article
        );

        return new GetAllArticleDTO
        {
            Id = article.Id,
            UserContent = user.ToUserContentTitleResponse(),
            Header = article.Header,
            ArticleCategories = article.Categories.Select(tag => tag.ToTagDTO()).ToList(),
            ArticleFlag = article.Flag?.ToTagDTO(),
            UpdateTime = article.UpdateTime,
            ReadingTime = article.ReadingTime,
            CommentCount = article.Comments?.Count ?? 0,
            VoteDTO = vote?.ToVoteDTO(article.VoteCount),
            IsSaved = _context.Saves.Any(
                x => x.ContentType == ContentType.Article && x.ContentId == article.Id
            ),
            IsViewed = _context.Views.Any(
                x => x.ContentType == ContentType.Article && x.ContentId == article.Id
            ),
        };
    }

    public static GetLastArticlesDTO ToLastArticleDTO(this Article article)
    {
        List<string> tags = article.Categories.Select(c => c.Title).ToList();
        tags.Add(article.Flag.Title);
        return new GetLastArticlesDTO
        {
            Id = article.Id,
            Title = article.Header.Title,
            Tags = tags
        };
    }

    public static async Task<GetAllQuestionDTO> ToQuestionDTO(
        this Question question,
        ClubrickDbContext _context
    )
    {
        var user = await _context.Users.FindAsync(question.UserId);

        var vote = await _context.Votes.FirstOrDefaultAsync(x => x.UserId == question.UserId);

        return new GetAllQuestionDTO
        {
            Id = question.Id,
            UserContent = user.ToUserContentTitleResponse(),
            Status = question.Answers?.Count is 0
                ? QuestionStatus.UnAnswered
                : (
                    question.CorrectAnswerId is null
                        ? QuestionStatus.UnSolved
                        : QuestionStatus.Solved
                ),
            Header = question.Header,
            QuestionCategories = question.Categories.Select(tag => tag.ToTagDTO()).ToList(),
            UpdateTime = question.UpdateTime,
            AnswerCount = question.Answers?.Count ?? 0,
            ReadingTime = question.ReadingTime,
            VoteDTO = vote?.ToVoteDTO(question.VoteCount),
            IsSaved = _context.Saves.Any(
                x => x.ContentType == ContentType.Article && x.ContentId == question.Id
            ),
            IsViewed = _context.Views.Any(
                x => x.ContentType == ContentType.Article && x.ContentId == question.Id
            ),
            IsWatched = _context.Watches.Any(
                x => x.ContentType == ContentType.Article && x.ContentId == question.Id
            ),
        };
    }

    public static GetLastQuestionsDTO ToLastQuestionDTO(this Question question)
    {
        return new GetLastQuestionsDTO
        {
            Id = question.Id,
            Title = question.Header.Title,
            AnswerCount = question.Answers?.Count ?? 0,
        };
    }

    public static ResourceDTO ToResourceDTO(this Resource resource)
    {
        return new ResourceDTO
        {
            Title = resource.Title,
            Url = resource.Url,
            CategoryId = resource.Category.Id,
            ResourceFlagId = resource.Flag.Id,
        };
    }

    public static GetLastResourcesDTO ToLastResourceDTO(this Resource resource)
    {
        return new GetLastResourcesDTO
        {
            Id = resource.Id,
            Title = resource.Title,
            Url = resource.Url
        };
    }

    public static VoteDTO ToVoteDTO(this Vote vote, int voteCount)
    {
        return new VoteDTO { Status = vote.Status, VoteCount = voteCount, };
    }

    public static TagDTO ToTagDTO(this TagBase tagBase)
    {
        return new TagDTO
        {
            Id = tagBase.Id,
            Title = tagBase.Title,
            AccentColor = tagBase.AccentColor,
        };
    }

    public static LeaderboardResponse ToLeaderboardResponse(this User user)
    {
        return new LeaderboardResponse
        {
            UserId = user.Id,
            Name = user.Name,
            Username = user.Username,
            ProfilePhoto = user.ProfilePhoto,
            BudsPoints = user.AllTimeBudsPoints,
        };
    }
}
