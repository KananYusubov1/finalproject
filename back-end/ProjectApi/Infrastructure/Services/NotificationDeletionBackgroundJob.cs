﻿using Infrastructure.Persistence;
using Quartz;

namespace Infrastructure.Services;

public class NotificationDeletionBackgroundJob : IJob
{
    private readonly ClubrickDbContext _context;

    public NotificationDeletionBackgroundJob(ClubrickDbContext context)
    {
        _context = context;
    }

    public Task Execute(IJobExecutionContext context)
    {
        var users = _context.Users.ToList();

        var threshHoldDuration = TimeSpan.FromDays(3);

        foreach (var user in users)
        {
            if (user.NotificationList != null)
            {

                user.NotificationList.RemoveAll(notification =>
                {
                    TimeSpan timeDifference = DateTime.UtcNow - notification.CreatedAt;

                    return timeDifference > threshHoldDuration;
                });

                _context.Users.Update(user);
            }
        }

        _context.SaveChanges();

        return Task.CompletedTask;
    }
}