﻿using AppDomain.Common.Config;
using AppDomain.Entities.UserRelated;
using AppDomain.Interfaces;
using Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;
using sib_api_v3_sdk.Api;
using sib_api_v3_sdk.Client;
using sib_api_v3_sdk.Model;

namespace Infrastructure.Services;

/// <inheritdoc/>
public class EmailService : IEmailService
{
    private readonly ClubrickDbContext _context;
    private readonly EmailConfig _config;

    /// <summary>
    /// Initializes a new instance of the <see cref="EmailService"/> class.
    /// </summary>
    /// <param name="context">The database context for accessing data.</param>
    /// <param name="config">The email configuration for sending emails.</param>
    public EmailService(ClubrickDbContext context, EmailConfig config)
    {
        _context = context;
        _config = config;
    }

    /// <inheritdoc/>
    public void SendEmail(
        string toEmail,
        string otpCode,
        string toName
    )
    {
        if (toName is null)
            toName = "Dear Developer";

        if (!Configuration.Default.ApiKey.ContainsKey("api-key"))
            Configuration.Default.ApiKey.Add("api-key", _config.ApiKey);

        var apiInstance = new TransactionalEmailsApi();
        var emailTo = new List<SendSmtpEmailTo>();
        emailTo.Add(new SendSmtpEmailTo(toEmail, toName));

        var sender = new SendSmtpEmailSender(_config.SenderName, _config.SenderEmail);

        Dictionary<string, object> _parmas = new Dictionary<string, object>();
        _parmas.Add("otp_code", otpCode);
        _parmas.Add("user_name", toName);

        var sendSmtpEmail = new SendSmtpEmail(sender, emailTo, null, null, null, null, _config.Subject, null, null, null, _config.TemplateId, _parmas, null, null, null, null);
        apiInstance.SendTransacEmail(sendSmtpEmail);
    }

    /// <inheritdoc/>
    public async Task<EmailVerification> VerifyEmailAsync(string email, string otpCode)
    {
        var verification = await _context.EmailVerifications.FirstOrDefaultAsync(
            x => x.Email == email && x.OTP == otpCode
        );

        if (verification is null)
            return null;
        else if (verification.ExpireUntil < DateTime.UtcNow)
            return null;

        return verification;
    }
}