﻿using Microsoft.Extensions.Options;
using Quartz;

namespace Infrastructure.Services;

[DisallowConcurrentExecution]
public class QuartzServices : IConfigureOptions<QuartzOptions>
{
    public void Configure(QuartzOptions options)
    {
        var jobKey = JobKey.Create(nameof(NotificationDeletionBackgroundJob));

        options
            .AddJob<NotificationDeletionBackgroundJob>(jobBuilder => jobBuilder.WithIdentity(jobKey))
            .AddTrigger(trigger =>
                trigger
                    .ForJob(jobKey)
                    .WithSimpleSchedule(schedule =>
                        schedule.WithIntervalInHours(12).RepeatForever()));
    }
}