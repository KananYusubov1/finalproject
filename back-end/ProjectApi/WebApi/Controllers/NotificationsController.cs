﻿using AppDomain.DTOs.Notification;
using AppDomain.Entities.NotificationRelated;
using AppDomain.Interfaces;
using Application.Tasks.Commands.Insert.InsertNotification;
using Application.Tasks.Queries.NotificationQueries.GetAllBudsNotifications;
using Application.Tasks.Queries.NotificationQueries.GetAllNotifications;
using Application.Tasks.Queries.NotificationQueries.GetNotificationsByType;
using Application.Tasks.Queries.NotificationQueries.GetUnreadNotificationsCount;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers;

/// <summary>
/// Controller for managing notifications.
/// </summary>
[Route("api/[controller]")]
[ApiController]
public class NotificationsController : ControllerBase
{
    private readonly IMediator _mediator;
    private readonly INotificationRepository _repo;

    /// <summary>
    /// Initializes a new instance of the <see cref="NotificationsController"/> class.
    /// </summary>
    /// <param name="mediator">The mediator for handling queries and commands.</param>
    public NotificationsController(IMediator mediator, INotificationRepository repo)
    {
        _mediator = mediator;
        _repo = repo;
    }

    [HttpPatch("ReadNotifications")]
    public async Task<ActionResult> ReadNotifications()
    {
        try
        {
            await _repo.ReadNotificationAsync();
            return Ok();
        }
        catch (Exception)
        {
            return Problem("A problem accrued please try again later.");
        }
    }

    /// <summary>
    /// Posts a new notification.
    /// </summary>
    /// <param name="insertCommand">The command to insert a new notification.</param>
    /// <returns>
    /// HTTP 200 OK with the created notification data if successful,
    /// or HTTP 500 Internal Server Error if an error occurs.
    /// </returns>
    [HttpPost("PostNotification")]
    public async Task<ActionResult<NotificationDTO>> PostNotification(
        [FromBody] InsertNotificationCommand insertCommand
    )
    {
        try
        {
            var notification = await _mediator.Send(insertCommand);

            return Ok(notification);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Gets the count of unread notifications.
    /// </summary>
    /// <param name="query">The query to retrieve unread notifications count.</param>
    /// <returns>An asynchronous task representing the HTTP action result containing the count of unread notifications.</returns>
    [HttpGet("GetUnreadNotificationsCount")]
    public async Task<ActionResult<int>> GetUnreadNotificationsCount(
        GetUnreadNotificationsCountQuery query
    )
    {
        try
        {
            var count = await _mediator.Send(query);

            return Ok(count);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }


    [HttpGet("GetAllBudsRelatedNotifications")]
    public async Task<ActionResult<IEnumerable<Notification>>> GetAllBudsRelatedNotifications(
        GetAllBudsNotificationsQuery query
    )
    {
        try
        {
            var notifications = await _mediator.Send(query);

            return Ok(notifications);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Gets all notifications.
    /// </summary>
    /// <param name="query">The query to retrieve all notifications.</param>
    /// <returns>An asynchronous task representing the HTTP action result containing a list of notifications.</returns>
    [HttpGet("GetAllNotifications")]
    public async Task<ActionResult<IEnumerable<Notification>>> GetAllNotifications(
        GetAllNotificationsQuery query
    )
    {
        try
        {
            var notifications = await _mediator.Send(query);

            return Ok(notifications);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Gets notifications by type.
    /// </summary>
    /// <param name="query">The query to retrieve notifications by type.</param>
    /// <returns>An asynchronous task representing the HTTP action result containing a list of notifications.</returns>
    [HttpGet("GetNotificationsByType/{Type}")]
    public async Task<ActionResult<IEnumerable<Notification>>> GetNotificationsByType(
        GetNotificationsByTypeQuery query
    )
    {
        try
        {
            var notifications = await _mediator.Send(query);

            return Ok(notifications);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }
}
