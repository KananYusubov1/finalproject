﻿using AppDomain.DTOs.Review;
using AppDomain.Interfaces;
using Application.Tasks.Commands.Delete.DeleteReview;
using Application.Tasks.Commands.Insert.InsertReview;
using Application.Tasks.Commands.Update.UpdateReview;
using Application.Tasks.Queries.ReviewQueries.GetAll;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReviewController : ControllerBase
    {
        private readonly IMediator _mediator;

        private readonly IReviewRepository _reviewRepository;

        public ReviewController(IMediator mediator, IReviewRepository reviewRepository)
        {
            _mediator = mediator;
            _reviewRepository = reviewRepository;
        }

        [HttpGet("GetAllReviews")]
        public async Task<ActionResult<List<GetAllReviewDTO>>> GetAllReviews(
            [FromQuery] GetAllReviewQuery query
        )
        {
            try
            {
                var data = await _mediator.Send(query);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpPost("InsertReview")]
        public async Task<ActionResult> InsertReview([FromBody] InsertReviewCommand command)
        {
            try
            {
                var data = await _mediator.Send(command);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpPut("edit")]
        public async Task<ActionResult> UpdateReview([FromBody] UpdateReviewCommand command)
        {
            try
            {
                await _mediator.Send(command);
                return Ok("Review updated process is successfully");
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpDelete("delete/{ReviewId}")]
        public async Task<ActionResult> DeleteReview(DeleteReviewCommand command)
        {
            try
            {
                await _mediator.Send(command);
                return Ok("Review deleted successfully");
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }
    }
}
