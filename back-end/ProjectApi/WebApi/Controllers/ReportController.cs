﻿using AppDomain.DTOs.Report;
using AppDomain.Exceptions.UserExceptions;
using Application.Tasks.Commands.Delete.DeleteReport;
using Application.Tasks.Commands.Insert.InsertContentReport;
using Application.Tasks.Commands.Update.SubmitReport;
using Application.Tasks.Queries.ReportQueries.GetAllReports;
using Application.Tasks.Queries.ReportQueries.GetAllReportsByModeratorId;
using Application.Tasks.Queries.ReportQueries.GetAllReportsByStatus;
using Application.Tasks.Queries.ReportQueries.GetReportById;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ReportController : ControllerBase
{
    private readonly IMediator _mediator;

    public ReportController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpPost("ReportContent")]
    public async Task<ActionResult> ReportContent([FromBody] InsertReportCommand insertCommand)
    {
        try
        {
            var result = await _mediator.Send(insertCommand);

            return Ok(result);
        }
        catch (UserForbiddenException ex)
        {
            return Unauthorized(ex.Message);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Retrieves all reports asynchronously.
    /// </summary>
    /// <returns>
    /// A synchronous task representing the HTTP action result.
    /// If successful, it returns an OK result with a collection of reports.
    /// If there is an error, it returns a problem result with an error message.
    /// </returns>
    [HttpGet("GetAllReports")]
    public async Task<ActionResult<IEnumerable<ReportDTO>>> GetAllReportsAsync(
        GetAllReportsQuery query
    )
    {
        try
        {
            var reports = await _mediator.Send(query);

            var reportsDTO = reports.Select(report => report.ToReportDTO()).ToList();

            return Ok(reportsDTO);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    [HttpGet("GetAllReportsByStatus")]
    public async Task<ActionResult<IEnumerable<ReportDTO>>> GetAllReportsByStatus(
        GetAllReportsByStatusQuery query
    )
    {
        try
        {
            var reports = await _mediator.Send(query);

            var reportsDTO = reports.Select(report => report.ToReportDTO()).ToList();

            return Ok(reportsDTO);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    [HttpGet("GetAllReportsByModeratorId")]
    public async Task<ActionResult<IEnumerable<ReportDTO>>> GetAllReportsByModeratorId(
        GetAllReportsByModeratorIdQuery query
    )
    {
        try
        {
            var reports = await _mediator.Send(query);

            var reportsDTO = reports.Select(report => report.ToReportDTO()).ToList();

            return Ok(reportsDTO);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    [HttpGet("GetReportById")]
    public async Task<ActionResult<IEnumerable<ReportDTO>>> GetReportById(GetReportByIdQuery query)
    {
        try
        {
            var report = await _mediator.Send(query);

            return Ok(report.ToReportDTO());
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    [HttpPatch("SubmitReport")]
    public async Task<ActionResult<ReportDTO>> SubmitReport(
        [FromBody] SubmitReportCommand updateCommand
    )
    {
        try
        {
            var report = await _mediator.Send(updateCommand);

            return Ok(report.ToReportDTO());
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Deletes a report asynchronously.
    /// </summary>
    /// <param name="deleteCommand">The command to delete a report.</param>
    /// <returns>
    /// An asynchronous task representing the HTTP action result.
    /// If successful, it returns an OK result.
    /// If there is an error, it returns a problem result with an error message.
    /// </returns>
    [HttpDelete("DeleteReport/{ReportId}")]
    public async Task<ActionResult> DeleteReport(DeleteReportCommand deleteCommand)
    {
        try
        {
            var result = await _mediator.Send(deleteCommand);

            return Ok(result);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }
}
