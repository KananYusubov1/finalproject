﻿using AppDomain.Interfaces;
using AppDomain.Responses.UserResponses;
using Application.Tasks.Commands.Delete.DeleteBanner;
using Application.Tasks.Commands.Insert.InsertBanner;
using Application.Tasks.Commands.Update.UpdateUser.UpdateBanner;
using Application.Tasks.Commands.Update.UpdateUser.UpdateRole;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers;

/// <summary>
/// Controller for administrative tasks.
/// </summary>
[Route("api/[controller]")]
[ApiController]
public class AdminController : ControllerBase
{
    private readonly IMediator _mediator;
    private readonly IUserRepository _userRepository;

    /// <summary>
    /// Initializes a new instance of the <see cref="AdminController"/> class.
    /// </summary>
    /// <param name="mediator">The mediator for handling commands and queries.</param>
    /// <param name="userRepository">The repository for user-related operations.</param>
    public AdminController(IMediator mediator, IUserRepository userRepository)
    {
        _mediator = mediator;
        _userRepository = userRepository;
    }

    /// <summary>
    /// Searches for a user by email asynchronously.
    /// </summary>
    /// <param name="email">The email address to search for.</param>
    /// <returns>
    /// An asynchronous task representing the HTTP action result.
    /// If a user with the given email is found, it returns the user's profile as an OK result.
    /// If there is an error or the user is not found, it returns a problem result with an error message.
    /// </returns>
    /// <remarks>
    /// This HTTP GET method allows searching for a user by their email address.
    /// It performs this operation asynchronously and returns an HTTP action result.
    /// If the user is found, it returns the user's profile as an OK result.
    /// If there is an error or the user is not found, it returns a problem result with an error message.
    /// </remarks>
    [HttpGet("SearchUserByEmail/{email}")]
    public async Task<ActionResult<UserProfileResponse>> SearchUserByEmail(string email)
    {
        try
        {
            var user = await _userRepository.GetUserByEmailAsync(email);

            var userProfile = ModelConvertors.ToUserProfileResponse(user);

            return Ok(userProfile);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Changes a user's role asynchronously.
    /// </summary>
    /// <param name="updateCommand">The command to update a user's role.</param>
    /// <returns>
    /// An asynchronous task representing the HTTP action result.
    /// If the operation is successful, it returns a success result as an OK result.
    /// If there is an error, it returns a problem result with an error message.
    /// </returns>
    /// <remarks>
    /// This HTTP PATCH method allows changing a user's role by sending an update command.
    /// It performs this operation asynchronously and returns an HTTP action result.
    /// If the operation is successful, it returns a success result as an OK result.
    /// If there is an error, it returns a problem result with an error message.
    /// </remarks>
    [HttpPatch("ChangeRole")]
    public async Task<ActionResult> ChangeRole([FromBody] UpdateRoleCommand updateCommand)
    {
        try
        {
            var result = await _mediator.Send(updateCommand);

            return Ok(result);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    [HttpPost("PostBanner")]
    public async Task<ActionResult> PostBanner([FromBody] InsertBannerCommand insertCommand)
    {
        try
        {
            var result = await _mediator.Send(insertCommand);

            return Ok(result);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    [HttpDelete("DeleteBanner/{BannerId}")]
    public async Task<ActionResult> DeleteBanner(DeleteBannerCommand deleteCommand)
    {
        try
        {
            var result = await _mediator.Send(deleteCommand);

            return Ok(result);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    [HttpPatch("UpdateBanner")]
    public async Task<ActionResult> UpdateBanner([FromBody] UpdateBannerCommand updateCommand)
    {
        try
        {
            var result = await _mediator.Send(updateCommand);

            return Ok(result);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }
}