﻿using AppDomain.Common.Entities;
using AppDomain.DTOs.Article;
using AppDomain.DTOs.Pagination;
using AppDomain.Interfaces;
using Application.Tasks.Commands.Delete.DeleteArticle;
using Application.Tasks.Commands.Insert.InsertArticle;
using Application.Tasks.Commands.Update.UpdateArticle;
using Application.Tasks.Commands.Update.UpdateArticle.SaveArticle;
using Application.Tasks.Commands.Update.UpdateArticle.VoteArticle;
using Application.Tasks.Queries.ArticleQueries.GetAll;
using Application.Tasks.Queries.ArticleQueries.GetAllForUser;
using Application.Tasks.Queries.ArticleQueries.GetAllSavedForUser;
using Application.Tasks.Queries.ArticleQueries.GetById;
using Application.Tasks.Queries.ArticleQueries.GetForFlow;
using Application.Tasks.Queries.ArticleQueries.GetLastArticles;
using Application.Tasks.Queries.ArticleQueries.GetMoreFrom;
using Application.Tasks.Queries.QuestionQueries.GetCount;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ArticlesController : ControllerBase
{
    private readonly IMediator _mediator;
    private readonly IArticleRepository _repository;

    public ArticlesController(IMediator mediator, IArticleRepository repository)
    {
        _mediator = mediator;
        _repository = repository;
    }

    [HttpGet("GetArticlesForFeed")]
    public async Task<ActionResult<List<GetAllArticleDTO>>> GetArticlesForFeed()
    {
        try
        {
            var articles = await _repository.GetArticlesForFeedAsync();

            return Ok(articles);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    [HttpGet("count")]
    public async Task<ActionResult<int>> GetAllArticleCount()
    {
        try
        {
            var count = await _repository.GetAllArticlesCount();

            return Ok(count);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    [HttpGet("GetLastArticles")]
    public async Task<ActionResult<IEnumerable<GetLastArticlesDTO>>> GetLastArticles(
        GetLastArticlesQuery query
    )
    {
        try
        {
            var articles = await _mediator.Send(query);

            return Ok(articles);
        }
        catch (Exception Ex)
        {
            return Problem(Ex.Message);
        }
    }

    [HttpGet("GetAllArticle")]
    public async Task<ActionResult<PaginatedListDto<GetAllArticleDTO>>> GetAllArticle(
        [FromQuery] GetAllArticleQuery query
    )
    {
        try
        {
            var data = await _mediator.Send(query);

            return Ok(data);
        }
        catch (Exception Ex)
        {
            return Problem(Ex.Message);
        }
    }

    [HttpGet("GetAllSavedArticle")]
    public async Task<ActionResult<PaginatedListDto<GetAllArticleDTO>>> GetAllSavedArticle(
        [FromQuery] GetAllArticleQuery query
    )
    {
        try
        {
            var data = await _repository.GetAllSavedArticle(
                query.SearchQuery,
                query.CategoryIdList,
                query.FlagId,
                query.Sort
            );

            return Ok(data);
        }
        catch (Exception Ex)
        {
            return Problem(Ex.Message);
        }
    }

    [HttpPost("insert")]
    public async Task<ActionResult> InsertArticle([FromBody] InsertArticleCommand command)
    {
        var inserted = await _mediator.Send(command);

        if (inserted is "0")
        {
            GeneralResponce responce = new() { Result = 0, Message = "ArticleFlag is not exist" };
            return NotFound(responce);
        }
        else if (inserted is "1")
        {
            GeneralResponce responce = new() { Result = 0, Message = "Category is not exist" };
            return NotFound(responce);
        }
        else if (inserted is "-2")
        {
            return Forbid();
        }
        else if (inserted is "-1")
        {
            GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
            return Problem(responce.Message);
        }
        else
        {
            GeneralResponce responce =
                new()
                {
                    Result = inserted,
                    Message = $"Article Id: {inserted} , Insert process successfully"
                };
            return Ok(responce);
        }
    }

    [HttpPut("update")]
    public async Task<ActionResult> UpdateArticle([FromBody] UpdateArticleCommand command)
    {
        var updated = await _mediator.Send(command);

        if (updated is 0)
        {
            GeneralResponce responce = new() { Result = 0, Message = "Article is not exist" };
            return NotFound(responce);
        }
        else if (updated is -2)
        {
            return Forbid();
        }
        else if (updated is -1)
        {
            GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
            return Problem(responce.Message);
        }
        else
        {
            GeneralResponce responce =
                new() { Result = 1, Message = "Article update process successfully" };
            return Ok(responce);
        }
    }

    [HttpDelete("delete/{ArticleId}")]
    public async Task<ActionResult> DeleteArticle(DeleteArticleCommand command)
    {
        var deleted = await _mediator.Send(command);

        if (deleted is 0)
        {
            GeneralResponce responce = new() { Result = 0, Message = "Article is not exist" };
            return NotFound(responce);
        }
        else if (deleted is -2)
        {
            return Forbid();
        }
        else if (deleted is -1)
        {
            GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
            return Problem(responce.Message);
        }
        else
        {
            GeneralResponce responce =
                new() { Result = 1, Message = "Delete Article process successfully" };
            return Ok(responce);
        }
    }

    [HttpGet("{ArticleId}")]
    public async Task<ActionResult<GetByIdArticleDTO>> GetByIdArticle(GetByIdArticleQuery query)
    {
        var getData = await _mediator.Send(query);
        if (getData is null)
        {
            GeneralResponce responce = new() { Result = 0, Message = "Article is not exist" };
            return NotFound(responce);
        }
        return Ok(getData);
    }

    [HttpPatch("save/{ArticleId}")]
    public async Task<ActionResult<int>> SaveArticle(SaveArticleCommand command)
    {
        var saved = await _mediator.Send(command);

        if (saved is 0)
        {
            GeneralResponce responce = new() { Result = 0, Message = "Article is not exist" };
            return NotFound(responce);
        }
        else if (saved is -1)
        {
            GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
            return Problem(responce.Message);
        }
        else if (saved is -2)
        {
            return Forbid();
        }
        else
        {
            return Ok(1);
        }
    }

    [HttpPatch("vote/{Status}/{ArticleId}")]
    public async Task<ActionResult<int>> VoteArticle(VoteArticleCommand command)
    {
        var voted = await _mediator.Send(command);

        if (voted is 0)
        {
            GeneralResponce responce = new() { Result = 0, Message = "Article is not exist" };
            return NotFound(responce);
        }
        else if (voted is -1)
        {
            GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
            return Problem(responce.Message);
        }
        else if (voted is -2)
        {
            return Forbid();
        }
        else
        {
            return Ok(1);
        }
    }

    [HttpGet("GetAllUserSavedArticle")]
    public async Task<ActionResult<PaginatedListDto<GetAllArticleDTO>>> GetAllUserSavedArticle(
        [FromQuery] GetAllUserSavedArticleQuery query
    )
    {
        var data = await _mediator.Send(query);

        return Ok(data);
    }

    [HttpGet("GetAllUserArticle")]
    public async Task<ActionResult<PaginatedListDto<GetAllArticleDTO>>> GetAllUserArticle(
        [FromQuery] GetAllArticleForUserQuery query
    )
    {
        var data = await _mediator.Send(query);

        return Ok(data);
    }

    [HttpGet("GetArticlesForFlow")]
    public async Task<ActionResult> GetArticlesForFlow()
    {
        var result = await _mediator.Send(new GetArticlesForFlowQuery());
        if (result is null)
            return Ok(new { hasFollowingUser = false });
        else
            return Ok(new { hasFollowingUser = true, articles = result });
    }

    [HttpGet("GetArticleMoreFrom/{UserId}")]
    public async Task<ActionResult<List<ArticleMoreFromDTO>>> GetArticleMoreFrom(
        GetArticleMoreFromQuery query
    )
    {
        var result = await _mediator.Send(query);

        return Ok(result);
    }
}
