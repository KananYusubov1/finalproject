﻿using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace WebApi.Controllers;

/// <summary>
/// Controller for managing general operations for front-end.
/// </summary>
[ApiController]
[Route("api/[controller]")]
public class CommonController : ControllerBase
{
    [HttpGet("CheckUrl")]
    public async Task<ActionResult> CheckUrl(string url)
    {
        try
        {
            try
            {
                string domain = new Uri(url).Host;

                // Check if the domain resolves to an IP address
                IPHostEntry ipHostEntry = Dns.GetHostEntry(domain);

                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }
}