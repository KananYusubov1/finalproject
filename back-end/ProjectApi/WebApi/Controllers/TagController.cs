﻿using AppDomain.Common.Entities;
using AppDomain.DTOs.Category;
using AppDomain.DTOs.Tags;
using AppDomain.Entities.TagBaseRelated;
using Application.Tasks.Commands.Delete.DeleteArticleFLag;
using Application.Tasks.Commands.Delete.DeleteCategory;
using Application.Tasks.Commands.Delete.DeleteResourceFlag;
using Application.Tasks.Commands.Insert.InsertArticleFlag;
using Application.Tasks.Commands.Insert.InsertCategory;
using Application.Tasks.Commands.Insert.InsertResourceFlag;
using Application.Tasks.Commands.Update.UpdateArticleFlag;
using Application.Tasks.Commands.Update.UpdateCategory;
using Application.Tasks.Commands.Update.UpdateCategory.FollowCategory;
using Application.Tasks.Commands.Update.UpdateResourceFlag;
using Application.Tasks.Queries.ArticleFlagQueries.GetAllArticleFlag;
using Application.Tasks.Queries.ArticleFlagQueries.GetArticleFlagById;
using Application.Tasks.Queries.CategoryQueries.GetAllCategory;
using Application.Tasks.Queries.CategoryQueries.GetAllCategoryBuild;
using Application.Tasks.Queries.CategoryQueries.GetAllFollowed;
using Application.Tasks.Queries.CategoryQueries.GetCategoryById;
using Application.Tasks.Queries.ResourceFlagQueries.GetAllResourceFlag;
using Application.Tasks.Queries.ResourceFlagQueries.GetResourceFlagById;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TagController : ControllerBase
    {
        private readonly IMediator _mediator;

        public TagController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Retrieves all available category build profiles.
        /// </summary>
        /// <returns>A list of category build profiles.</returns>

        [HttpGet("GetAllCategoryBuildProfile")]
        public async Task<ActionResult<List<CategoryBuildProfileDTO>>> GetAllCategoryBuildProfile()
        {
            var result = await _mediator.Send(new GetAllCategoryBuildProfileQuery());

            if (result.Count is 0)
                return NotFound("Category is Empty");

            return Ok(result);
        }

        [HttpGet("GetAllFollowedCategory")]
        public async Task<ActionResult> GetAllFollowedCategory(
            [FromQuery] GetAllFollowedCategoryQuery query
        )
        {
            var data = await _mediator.Send(query);

            if (data is null)
            {
                GeneralResponce responce =
                    new() { Result = 0, Message = "Followed Category is not exist" };
                return NotFound(responce);
            }

            return Ok(data);
        }

        /// <summary>
        /// Retrieves all available categories.
        /// </summary>
        /// <returns>A list of categories.</returns>

        [HttpGet("categories")]
        public async Task<ActionResult<List<TagFullResponseDto>>> GetAllCategory(
            GetAllCategoryQuery query
        )
        {
            var categoryList = await _mediator.Send(query);

            if (categoryList is null || categoryList.Count is 0)
                return NotFound("Category is Empty.");

            return Ok(categoryList);
        }

        /// <summary>
        /// Retrieves a category using its unique identifier.
        /// </summary>
        /// <param name="query">Query containing the category identifier.</param>
        /// <returns>The requested category, or a "Not Found" response if not found.</returns>

        [HttpGet("categories/{CategoryId}")]
        public async Task<ActionResult<TagPreviewResponseDto>> GetCategoryById(
            GetCategoryByIdQuery query
        )
        {
            var categoryGet = await _mediator.Send(query);

            if (categoryGet is null)
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = 0,
                        Message = $"Category is not exist in this id = {query.CategoryId}"
                    };
                return NotFound(responce);
            }
            else
            {
                return Ok(categoryGet);
            }
        }

        /// <summary>
        /// Inserts a new category.
        /// </summary>
        /// <param name="command">The command containing category information.</param>
        /// <returns>A success response with the inserted category ID.</returns>

        [HttpPost("categories/insert")]
        public async Task<ActionResult> InsertCategory([FromBody] InsertCategoryCommand command)
        {
            var insertedId = await _mediator.Send(command);
            if (insertedId is "0")
            {
                GeneralResponce responce =
                    new() { Result = 0, Message = "This Category is exist,Insert process failed" };
                return Conflict(responce);
            }
            else if (insertedId is "-1")
            {
                GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
                return Problem(responce.Message);
            }
            else
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = insertedId,
                        Message = $"Category Id: {insertedId} inserted successfully"
                    };

                return Ok(responce);
            }
        }

        /// <summary>
        /// Updates an existing category.
        /// </summary>
        /// <param name="command">The command for updating category information.</param>
        /// <returns>A response indicating success or a "Not Found" response if the category doesn't exist.</returns>

        [HttpPut("categories/update")]
        public async Task<ActionResult> UpdateCategory([FromBody] UpdateCategoryCommand command)
        {
            var updatedId = await _mediator.Send(command);

            if (updatedId is "0")
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = 0,
                        Message =
                            $"Category Id: {command.UpdateTagDTO.Id} is not exist, Update process failed"
                    };
                return NotFound(responce);
            }
            else if (updatedId is "-1")
            {
                GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
                return Problem(responce.Message);
            }
            else if (updatedId is "-2")
            {
                GeneralResponce responce =
                    new() { Result = -2, Message = "This Category Title is exist" };
                return Conflict(responce);
            }
            else
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = updatedId,
                        Message = $"Category Id: {updatedId}, Update process successfully"
                    };
                return Ok(responce);
            }
        }

        /// <summary>
        /// Deletes a category using its identifier.
        /// </summary>
        /// <param name="command">The command containing the category identifier.</param>
        /// <returns>A response indicating deletion success or a "Not Found" response if the category doesn't exist.</returns>

        [HttpDelete("categories/delete/{CategoryId}")]
        public async Task<ActionResult> DeleteCategory(DeleteCategoryCommand command)
        {
            var deleted = await _mediator.Send(command);

            if (deleted is null)
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = 0,
                        Message =
                            $"Category Id: {command.CategoryId} is not exist, Delete process failed"
                    };
                return NotFound(responce);
            }
            else
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = deleted,
                        Message = $"Category Id: {deleted}, Delete process successfully"
                    };
                return Ok(responce);
            }
        }

        /// <summary>
        /// Increments the usage count of a category list.
        /// </summary>
        /// <param name="command">The command for incrementing the usage count.</param>
        /// <returns>A response indicating the success of the increment operation or a failure response.</returns>

        [HttpPatch("IncrementCategoryListUseCount")]
        public async Task<ActionResult> IncrementCategoryUseCount(
            [FromBody] IncrementCategoryUseCountCommand command
        )
        {
            var result = await _mediator.Send(command);

            if (result is 1)
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = 1,
                        Message = $"CategoryList Increment UseCount process successfully"
                    };
                return Ok(responce);
            }
            else
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = 0,
                        Message = $"CategoryList Increment UseCount process failed"
                    };
                return NotFound(responce);
            }
        }

        /// <summary>
        /// Decrements the usage count of a category list.
        /// </summary>
        /// <param name="command">The command for decrementing the usage count.</param>
        /// <returns>A response indicating the success of the decrement operation or a failure response.</returns>

        [HttpPatch("DecrementCategoryListUseCount")]
        public async Task<ActionResult> DecrementCategoryListUseCount(
            [FromBody] DecrementCategoryListUseCountCommand command
        )
        {
            var result = await _mediator.Send(command);

            if (result is 1)
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = 1,
                        Message = $"CategoryList Decrement UseCount process successfully"
                    };
                return Ok(responce);
            }
            else
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = 0,
                        Message = $"CategoryList Decrement UseCount process failed"
                    };
                return NotFound(responce);
            }
        }

        /// <summary>
        /// Follows a category based on the provided category ID.
        /// </summary>
        /// <param name="command">The command containing the category ID to be followed.</param>
        /// <returns>A response indicating the success of the follow operation.</returns>

        [HttpPatch("FollowCategory/{CategoryId}")]
        public async Task<ActionResult> FollowCategory(FollowCategoryCommand command)
        {
            var followed = await _mediator.Send(command);

            if (followed is 0)
            {
                GeneralResponce responce = new() { Result = 0, Message = "Category is not exist" };
                return NotFound(responce);
            }
            else if (followed is -1)
            {
                GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
                return Problem(responce.Message);
            }
            else
            {
                GeneralResponce responce =
                    new() { Result = 1, Message = "Category followed successfully" };
                return Ok(responce);
            }
        }

        /// <summary>
        /// Retrieves a list of article flags based on the provided query parameters.
        /// </summary>
        /// <param name="query">The query parameters for filtering article flags.</param>
        /// <returns>A list of article flags.</returns>

        [HttpGet("article-flags")]
        public async Task<ActionResult<List<ArticleFlag>>> GetAllArticleFlag(
            GetAllArticleFlagQuery query
        )
        {
            var result = await _mediator.Send(query);

            if (result.Count is 0)
            {
                GeneralResponce responce =
                    new() { Result = 0, Message = "ArticleFlag List is Empty" };
                return NotFound(responce);
            }
            return Ok(result);
        }

        /// <summary>
        /// Retrieves an article flag based on the provided article flag ID.
        /// </summary>
        /// <param name="query">The query parameters specifying the article flag ID.</param>
        /// <returns>The article flag with the specified ID.</returns>

        [HttpGet("article-flags/{ArticleFlagId}")]
        public async Task<ActionResult<TagPreviewResponseDto>> GetArticleFlagById(
            GetArticleFlagByIdQuery query
        )
        {
            var getArticleFlag = await _mediator.Send(query);

            if (getArticleFlag is null)
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = 0,
                        Message = $"ArticleFlag is not exist in this id = {query.ArticleFlagId}"
                    };
                return NotFound(responce);
            }
            else
            {
                return Ok(getArticleFlag);
            }
        }

        /// <summary>
        /// Inserts a new article flag into the system.
        /// </summary>
        /// <param name="command">The command containing the information for the new article flag.</param>
        /// <returns>A response indicating the success of the insertion operation.</returns>

        [HttpPost("article-flags/insert")]
        public async Task<ActionResult<string>> InsertArticleFlag(
            [FromBody] InsertArticleFlagCommand command
        )
        {
            var insertedId = await _mediator.Send(command);

            if (insertedId is "0")
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = 0,
                        Message = "This ArticleFlag is exist,Insert process failed"
                    };
                return Conflict(responce);
            }
            else if (insertedId is "-1")
            {
                GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
                return Problem(responce.Message);
            }
            else
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = insertedId,
                        Message = $"ArticleFlag Id: {insertedId} inserted successfully"
                    };

                return Ok(responce);
            }
        }

        /// <summary>
        /// Updates an existing article flag in the system.
        /// </summary>
        /// <param name="command">The command containing the updated information for the article flag.</param>
        /// <returns>A response indicating the success of the update operation.</returns>

        [HttpPut("article-flags/update")]
        public async Task<ActionResult> UpdateArticleFlag(
            [FromBody] UpdateArticleFlagCommand command
        )
        {
            var updatedId = await _mediator.Send(command);

            if (updatedId is "0")
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = 0,
                        Message =
                            $"ArticleFlag Id: {command.UpdateTagDTO.Id} is not exist, Update process failed"
                    };
                return NotFound(responce);
            }
            else if (updatedId is "-1")
            {
                GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
                return Problem(responce.Message);
            }
            else if (updatedId is "-2")
            {
                GeneralResponce responce =
                    new() { Result = -2, Message = "This ArticleFlag Title is exist" };
                return Conflict(responce);
            }
            else
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = updatedId,
                        Message = $"ArticleFlag Id: {updatedId}, Update process successfully"
                    };
                return Ok(responce);
            }
        }

        /// <summary>
        /// Deletes an article flag from the system based on the provided article flag ID.
        /// </summary>
        /// <param name="command">The command containing the information for deleting the article flag.</param>
        /// <returns>A response indicating the success of the deletion operation.</returns>

        [HttpDelete("article-flags/delete/{ArticleFLagId}")]
        public async Task<ActionResult> DeleteArticleFLag(DeleteArticleFLagCommand command)
        {
            var deleted = await _mediator.Send(command);

            if (deleted is null)
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = 0,
                        Message =
                            $"ArticleFLag Id: {command.ArticleFLagId} is not exist, Delete process failed"
                    };
                return NotFound(responce);
            }
            else
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = deleted,
                        Message = $"ArticleFLag Id: {deleted}, Delete process successfully"
                    };
                return Ok(responce);
            }
        }

        /// <summary>
        /// Increments the usage count of an article flag list in the system.
        /// </summary>
        /// <param name="command">The command containing the information to increment the usage count.</param>
        /// <returns>A response indicating the success of the increment operation.</returns>

        [HttpPatch("IncrementArticleFlagListUseCount")]
        public async Task<ActionResult> IncrementArticleFlagListUseCount(
            [FromBody] IncrementArticleFlagListUseCountCommand command
        )
        {
            var result = await _mediator.Send(command);

            if (result is 1)
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = 1,
                        Message = $"ArticleFlagList Increment UseCount process successfully"
                    };
                return Ok(responce);
            }
            else
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = 0,
                        Message = $"ArticleFlagList Increment UseCount process failed"
                    };
                return NotFound(responce);
            }
        }

        /// <summary>
        /// Decrements the usage count of an article flag list in the system.
        /// </summary>
        /// <param name="command">The command containing the information to decrement the usage count.</param>
        /// <returns>A response indicating the success of the decrement operation.</returns>

        [HttpPatch("DecrementArticleFlagListUseCount")]
        public async Task<ActionResult> DecrementArticleFlagListUseCount(
            [FromBody] DecrementArticleFlagListUseCountCommand command
        )
        {
            var result = await _mediator.Send(command);

            if (result is 1)
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = 1,
                        Message = $"ArticleFlagList Decrement UseCount process successfully"
                    };
                return Ok(responce);
            }
            else
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = 0,
                        Message = $"ArticleFlagList Decrement UseCount process failed"
                    };
                return NotFound(responce);
            }
        }

        /// <summary>
        /// Retrieves a list of resource flags based on the provided query parameters.
        /// </summary>
        /// <param name="query">The query parameters for filtering resource flags.</param>
        /// <returns>A list of resource flags.</returns>

        [HttpGet("resource-flags")]
        public async Task<ActionResult<List<ResourceFlag>>> GetAllResourceFlag(
            GetAllResourceFlagQuery query
        )
        {
            var result = await _mediator.Send(query);

            if (result.Count is 0)
            {
                GeneralResponce responce =
                    new() { Result = 0, Message = "ResourceFlag List is Empty" };
                return NotFound(responce);
            }
            return Ok(result);
        }

        /// <summary>
        /// Retrieves a resource flag based on the provided resource flag ID.
        /// </summary>
        /// <param name="query">The query parameters specifying the resource flag ID.</param>
        /// <returns>The resource flag with the specified ID.</returns>

        [HttpGet("resource-flags/{ResourceFlagId}")]
        public async Task<ActionResult<TagPreviewResponseDto>> GetResourceFlagById(
            GetResourceFlagByIdQuery query
        )
        {
            var getResourceFlag = await _mediator.Send(query);

            if (getResourceFlag is null)
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = 0,
                        Message = $"ResourceFlag is not exist in this id = {query.ResourceFlagId}"
                    };
                return NotFound(responce);
            }
            else
            {
                return Ok(getResourceFlag);
            }
        }

        /// <summary>
        /// Inserts a new resource flag into the system.
        /// </summary>
        /// <param name="command">The command containing the information for the new resource flag.</param>
        /// <returns>A response indicating the success of the insertion operation.</returns>

        [HttpPost("resource-flags/insert")]
        public async Task<ActionResult> InsertResourceFlag(
            [FromBody] InsertResourceFlagCommand command
        )
        {
            var inserted = await _mediator.Send(command);

            if (inserted is "0")
            {
                GeneralResponce responce =
                    new() { Result = 0, Message = "This ResourceFlag is exist" };
                return Conflict(responce);
            }
            else if (inserted is "-1")
            {
                GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
                return Problem(responce.Message);
            }
            else
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = inserted,
                        Message = $"This ResourceFlag Id: {inserted} ,Inserted process successfully"
                    };
                return Ok(responce);
            }
        }

        /// <summary>
        /// Updates an existing resource flag in the system.
        /// </summary>
        /// <param name="command">The command containing the updated information for the resource flag.</param>
        /// <returns>A response indicating the success of the update operation.</returns>

        [HttpPut("resource-flags/update")]
        public async Task<ActionResult> UpdateResourceFlag(
            [FromBody] UpdateResourceFlagCommand command
        )
        {
            var updatedId = await _mediator.Send(command);

            if (updatedId is "0")
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = 0,
                        Message =
                            $"ResourceFlag Id: {updatedId} is not exist, Update process failed"
                    };
                return NotFound(responce);
            }
            else if (updatedId is "-1")
            {
                GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
                return Problem(responce.Message);
            }
            else if (updatedId is "-2")
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = -2,
                        Message = "ResourceFlag title is exist, Update process failed"
                    };
                return Conflict(responce);
            }
            else
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = updatedId,
                        Message = $"ResourceFlag Id: {updatedId}, Update process successfully"
                    };
                return Ok(responce);
            }
        }

        /// <summary>
        /// Deletes a resource flag from the system based on the provided resource flag ID.
        /// </summary>
        /// <param name="command">The command containing the resource flag ID to be deleted.</param>
        /// <returns>A response indicating the success of the deletion operation.</returns>

        [HttpDelete("resource-flags/delete/{ResourceFlagId}")]
        public async Task<ActionResult> DeleteResourceFlag(DeleteResourceFlagCommand command)
        {
            var deleted = await _mediator.Send(command);

            if (deleted is "0")
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = 0,
                        Message =
                            $"ResourceFlag Id: {command.ResourceFlagId} is not exist, Delete process failed"
                    };
                return NotFound(responce);
            }
            else if (deleted is "-1")
            {
                GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
                return Problem(responce.Message);
            }
            else
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = deleted,
                        Message = $"ResourceFlag Id: {deleted}, Delete process successfully"
                    };
                return Ok(responce);
            }
        }

        /// <summary>
        /// Increments the usage count of a resource flag list in the system.
        /// </summary>
        /// <param name="command">The command containing the information to increment the usage count.</param>
        /// <returns>A response indicating the success of the increment operation.</returns>

        [HttpPatch("IncrementResourceFlagListUseCount")]
        public async Task<ActionResult> IncrementResourceFlagListUseCount(
            [FromBody] IncrementResourceFlagListUseCountCommand command
        )
        {
            var result = await _mediator.Send(command);

            if (result is 1)
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = 1,
                        Message = $"ResourceFlagList Increment UseCount process successfully"
                    };
                return Ok(responce);
            }
            else if (result is -1)
            {
                GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
                return Problem(responce.Message);
            }
            else
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = 0,
                        Message = $"ResourceFlagList Increment UseCount process failed"
                    };
                return NotFound(responce);
            }
        }

        /// <summary>
        /// Decrements the usage count of a resource flag list in the system.
        /// </summary>
        /// <param name="command">The command containing the information to decrement the usage count.</param>
        /// <returns>A response indicating the success of the decrement operation.</returns>

        [HttpPatch("DecrementResourceFlagListUseCount")]
        public async Task<ActionResult> DecrementResourceFlagListUseCount(
            [FromBody] DecrementResourceFlagListUseCountCommand command
        )
        {
            var result = await _mediator.Send(command);

            if (result is 1)
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = 1,
                        Message = $"ResourceFlagList Decrement UseCount process successfully"
                    };
                return Ok(responce);
            }
            else if (result is -1)
            {
                GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
                return Problem(responce.Message);
            }
            else
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = 0,
                        Message = $"ResourceFlagList Decrement UseCount process failed"
                    };
                return NotFound(responce);
            }
        }
    }
}
