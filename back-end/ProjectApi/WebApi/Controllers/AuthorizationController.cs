﻿using AppDomain.DTOs.Auth;
using AppDomain.Exceptions.Common;
using AppDomain.Exceptions.OtpExceptions;
using AppDomain.Exceptions.UserExceptions;
using Application.Tasks.Commands.Delete.UserDeletes.DeleteEmailVerification;
using Application.Tasks.Commands.Insert.UserInserts.BuildUser;
using Application.Tasks.Commands.Insert.UserInserts.EnterWithProvider;
using Application.Tasks.Commands.Insert.UserInserts.InsertOTPCode;
using Application.Tasks.Commands.Insert.UserInserts.InsertUser;
using Application.Tasks.Commands.Update.UpdateUser.UpdateToken;
using Application.Tasks.Queries.UserQueries.GetUser;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using System.Data;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;

namespace WebApi.Controllers;

/// <summary>
/// Controller responsible for user authorization operations.
/// </summary>
[ApiController]
[Route("api/auth")]
public class AuthorizationController : ControllerBase
{
    private readonly IMediator _mediator;

    /// <summary>
    /// Initializes a new instance of the <see cref="AuthorizationController"/> class.
    /// </summary>
    /// <param name="mediator">The mediator for handling requests and notifications.</param>
    public AuthorizationController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpGet("logout")]
    public IActionResult Logout()
    {
        Response.Cookies.Delete("authToken");
        //Response.Cookies.Delete("refreshToken");

        HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

        return Ok("Logged out successfully");
    }

    /// <summary>
    /// Enter with thirty party providers such as GitHub,Google etc.
    /// </summary>
    /// <param name="command"></param>
    /// <returns></returns>
    [HttpPost("provider-enter")]
    public async Task<ActionResult<UserAuthDto>> EnterWithProvider(
        [FromBody] EnterWithProviderCommand command
    )
    {
        try
        {
            var authDto = await _mediator.Send(command);
            return Ok(authDto);
        }
        catch (Exception ex)
        {
            return Problem("A problem has occurred please try again later");
        }
    }

    /// <summary>
    /// Registers a new user and returns a <see cref="UserAuthDto"/>.
    /// </summary>
    [HttpPost("register")]
    public async Task<ActionResult<UserAuthDto>> RegisterUser(
        [FromBody] InsertUserCommand insertCommand
    )
    {
        try
        {
            var authDto = await _mediator.Send(insertCommand);

            return Ok(authDto);
        }
        catch (EntityExistException ex)
        {
            return Conflict(ex.Message);
        }
        catch (Exception)
        {
            return Problem("A problem has occurred please try again later");
        }
    }

    /// <summary>
    /// Logs in a user and returns a <see cref="UserAuthDto"/>.
    /// </summary>
    [HttpPost("login")]
    public async Task<ActionResult<UserAuthDto>> LogInUser([FromBody] GetUserQuery loginCommand)
    {
        try
        {
            var authDto = await _mediator.Send(loginCommand);

            return Ok(authDto);
        }
        catch (EntityNotFoundException ex)
        {
            return NotFound(ex.Message);
        }
        catch (UserInvalidPasswordException ex)
        {
            return BadRequest(ex.Message);
        }
        catch (Exception)
        {
            return Problem("A problem has occurred please try again later");
        }
    }

    /// <summary>
    /// Refreshes the authentication token and returns the updated token.
    /// </summary>
    [HttpPost("refresh-token")]
    public async Task<ActionResult<string>> RefreshToken(
        [FromBody] UpdateTokenCommand updateTokenCommand
    )
    {
        try
        {
            var token = await _mediator.Send(updateTokenCommand);

            if (token is null)
                return BadRequest();

            return Ok(token);
        }
        catch (BadHttpRequestException ex)
        {
            return BadRequest(ex.Message);
        }
        catch (Exception)
        {
            return Problem("A problem has occurred please try again later");
        }
    }

    /// <summary>
    /// Sends an OTP code to the user's email for verification.
    /// </summary>
    /// <param name="insertOTPCommand">The command containing data for sending OTP.</param>
    /// <returns>The result of sending the OTP code.</returns>
    [HttpPost("SendOTPCode")]
    public async Task<ActionResult> SendOTPCode([FromBody] InsertOTPCommand insertOTPCommand)
    {
        var result = await _mediator.Send(insertOTPCommand);

        return Ok(result);
    }

    /// <summary>
    /// Verifies the user's email using the provided verification code.
    /// </summary>
    /// <param name="deleteCommand">The command containing the verification code.</param>
    /// <returns>The result of email verification.</returns>
    [HttpPost("VerifyEmail")]
    public async Task<ActionResult> VerifyEmail(
        [FromBody] DeleteEmailVerificationCommand deleteCommand
    )
    {
        try
        {
            var result = await _mediator.Send(deleteCommand);

            if (result)
                return Ok();
            else
                return BadRequest();
        }
        catch (EntityNotFoundException ex)
        {
            return NotFound(ex.Message);
        }
        catch (OtpCodeExpiredException ex)
        {
            return StatusCode(410, ex.Message);
        }
        catch (Exception)
        {
            return Problem("A problem has occurred please try again later");
        }
    }

    /// <summary>
    /// Builds a new user.
    /// </summary>
    /// <param name="buildCommand">The command containing user information.</param>
    /// <returns>The created user's DTO if successful, NotFound if user creation fails.</returns>
    [HttpPost("BuildUser")]
    public async Task<ActionResult<UserBuildResponseDTO>> BuildUser(
        [FromBody] BuildUserCommand buildCommand
    )
    {
        try
        {
            var userDTO = await _mediator.Send(buildCommand);

            return Ok(userDTO);
        }
        catch (EntityNotFoundException ex)
        {
            return NotFound(ex.Message);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }
}
