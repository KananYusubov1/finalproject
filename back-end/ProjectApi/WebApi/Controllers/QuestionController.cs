﻿using AppDomain.Common.Entities;
using AppDomain.DTOs.Pagination;
using AppDomain.DTOs.Question;
using Application.Tasks.Commands.Delete.DeleteQuestion;
using Application.Tasks.Commands.Insert.InsertQuestion;
using Application.Tasks.Commands.Insert.InsertQuestion.SaveQuestion;
using Application.Tasks.Commands.Insert.InsertQuestion.WatchQuestion;
using Application.Tasks.Commands.Update.UpdateQuestion;
using Application.Tasks.Commands.Update.UpdateQuestion.SetCorrectAnswer;
using Application.Tasks.Commands.Update.UpdateQuestion.VoteQuestion;
using Application.Tasks.Queries.ArticleQueries.GetLastQuestions;
using Application.Tasks.Queries.QuestionQueries.GetAll;
using Application.Tasks.Queries.QuestionQueries.GetAllForUser;
using Application.Tasks.Queries.QuestionQueries.GetAllSaved;
using Application.Tasks.Queries.QuestionQueries.GetAllWatched;
using Application.Tasks.Queries.QuestionQueries.GetById;
using Application.Tasks.Queries.QuestionQueries.GetCount;
using Application.Tasks.Queries.QuestionQueries.GetPreview;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers;

[Route("api/questions")]
[ApiController]
public class QuestionController : ControllerBase
{
    private readonly IMediator _mediator;

    public QuestionController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpGet("GetLastQuestions")]
    public async Task<ActionResult<IEnumerable<GetLastQuestionsDTO>>> GetLastQuestions(
        GetLastQuestionsQuery query
    )
    {
        try
        {
            var questions = await _mediator.Send(query);

            return Ok(questions);
        }
        catch (Exception Ex)
        {
            return Problem(Ex.Message);
        }
    }

    [HttpGet("GetAllQuestion")]
    public async Task<ActionResult<PaginatedListDto<GetAllQuestionDTO>>> GetAllQuestion(
        [FromQuery] GetAllQuestionQuery query
    )
    {
        try
        {
            var questions = await _mediator.Send(query);

            return Ok(questions);
        }
        catch (Exception ex)
        {
            return Problem("System Exception");
        }
    }

    [HttpGet("{QuestionId}")]
    public async Task<ActionResult> GetQuestionById(GetQuestionByIdQuery query)
    {
        var getData = await _mediator.Send(query);

        if (getData is null)
        {
            GeneralResponce responce = new() { Result = 0, Message = "Question is not exist" };
            return NotFound(responce);
        }

        return Ok(getData);
    }

    [HttpGet("GetQuestionPreview/{QuestionId}")]
    public async Task<ActionResult<QuestionPreviewDTO>> GetQuestionPreview(GetPreviewQuery query)
    {
        var getting = await _mediator.Send(query);

        if (getting is null)
        {
            GeneralResponce responce = new() { Result = 0, Message = "Question is not exist" };
            return NotFound(responce);
        }
        else
            return Ok(getting);
    }

    [HttpPost("insert")]
    public async Task<ActionResult> InsertQuestion([FromBody] InsertQuestionCommand command)
    {
        var inserted = await _mediator.Send(command);
        if (inserted is "-1")
        {
            GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
            return Problem(responce.Message);
        }
        else if (inserted is "0")
        {
            GeneralResponce responce = new() { Result = 0, Message = "Categories is not exist" };
            return NotFound(responce);
        }
        else
        {
            GeneralResponce responce =
                new()
                {
                    Result = inserted,
                    Message = $"Question Id: {inserted}, Insert process successfully"
                };
            return Ok(responce);
        }
    }

    [HttpPut("update")]
    public async Task<ActionResult> EditQuestion([FromBody] UpdateQuestionCommand command)
    {
        var updated = await _mediator.Send(command);

        if (updated is "0")
        {
            GeneralResponce responce = new() { Result = 0, Message = "Question is not exist" };
            return NotFound(responce);
        }
        else if (updated is "-1")
        {
            GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
            return Problem(responce.Message);
        }
        else
        {
            GeneralResponce responce =
                new()
                {
                    Result = updated,
                    Message = $"Question Id: {updated}, Update process successfully"
                };
            return Ok(responce);
        }
    }

    [HttpDelete("delete/{QuestionId}")]
    public async Task<ActionResult> DeleteQuestion(DeleteQuestionCommand command)
    {
        var deleted = await _mediator.Send(command);
        if (deleted is 0)
        {
            GeneralResponce responce = new() { Result = 0, Message = "Question is not exist" };
            return NotFound(responce);
        }
        else if (deleted is -1)
        {
            GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
            return Problem(responce.Message);
        }
        else
        {
            GeneralResponce response =
                new() { Result = 1, Message = "Delete process successfully" };
            return Ok(response);
        }
    }

    [HttpPatch("SetCorrectAnswer/{QuestionId}/{AnswerId}")]
    public async Task<ActionResult> SetCorrectAnswer(SetCorrectAnswerCommand command)
    {
        var result = await _mediator.Send(command);
        if (result is "0")
        {
            GeneralResponce responce = new() { Result = 0, Message = "Question is not exist" };
            return NotFound(responce);
        }
        else if (result is "1")
        {
            GeneralResponce responce = new() { Result = 1, Message = "Answer is not exist" };
            return NotFound(responce);
        }
        else if (result is "-1")
        {
            GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
            return Problem(responce.Message);
        }
        else
        {
            GeneralResponce responce =
                new()
                {
                    Result = result,
                    Message = $"Question Id: {result} set correct Answer Id: {command.AnswerId}"
                };
            return Ok(responce);
        }
    }

    [HttpPatch("save/{QuestionId}")]
    public async Task<ActionResult> SaveQuestion(SaveQuestionCommand command)
    {
        var saved = await _mediator.Send(command);

        if (saved is 0)
        {
            GeneralResponce responce = new() { Result = 0, Message = "Question is not exist" };
            return NotFound(responce);
        }
        else if (saved is -1)
        {
            GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
            return Problem(responce.Message);
        }
        else
        {
            return Ok(saved);
        }
    }

    [HttpPatch("watch/{QuestionId}")]
    public async Task<ActionResult> WatchQuestion(WatchQuestionCommand command)
    {
        var watched = await _mediator.Send(command);
        if (watched is 0)
        {
            GeneralResponce responce = new() { Result = 0, Message = "Question is not exist" };
            return NotFound(responce);
        }
        else if (watched is -1)
        {
            GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
            return Problem(responce.Message);
        }
        else
        {
            return Ok(watched);
        }
    }

    [HttpPatch("vote/{Status}/{QuestionId}")]
    public async Task<ActionResult> VoteQuestion(VoteQuestionCommand command)
    {
        var data = await _mediator.Send(command);

        if (data is 0)
        {
            GeneralResponce responce =
                new() { Result = 0, Message = "User or Question is not exist" };
            return NotFound(responce);
        }
        else if (data is -1)
        {
            GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
            return Problem(responce.Message);
        }
        else
            return Ok(data);
    }

    [HttpGet("count")]
    public async Task<ActionResult<int>> GetAllQuestionCount()
    {
        var data = await _mediator.Send(new GetAllQuestionCountQuery());

        return Ok(data);
    }

    [HttpGet("GetAllUserSavedQuestion")]
    public async Task<ActionResult<PaginatedListDto<GetAllQuestionDTO>>> GetAllUserSavedQuestion(
        [FromQuery] GetAllSavedQuestionQuery query)
    {
        var result = await _mediator.Send(query);
        return Ok(result);
    }

    [HttpGet("GetAllUserWatchedQuestion")]
    public async Task<ActionResult<PaginatedListDto<GetAllQuestionDTO>>> GetAllUserWatchedQuestion(
        [FromQuery] GetAllWatchedQuestionQuery query)
    {
        var result = await _mediator.Send(query);

        return Ok(result);
    }


    [HttpGet("GetAllUserQuestion")]
    public async Task<ActionResult<PaginatedListDto<GetAllQuestionDTO>>> GetAllUserQuestion(
        [FromQuery] GetAllQuestionByUserQuery query)
    {
        var result = await _mediator.Send(query);

        return Ok(result);
    }
}
