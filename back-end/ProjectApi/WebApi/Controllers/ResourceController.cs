﻿using AppDomain.Common.Entities;
using AppDomain.DTOs.Pagination;
using AppDomain.DTOs.Question;
using AppDomain.DTOs.Resource;
using AppDomain.Exceptions.Common;
using Application.Tasks.Commands.Delete.DeleteResource;
using Application.Tasks.Commands.Insert.InsertResource;
using Application.Tasks.Commands.Update.UpdateResource.ResourceEdit;
using Application.Tasks.Commands.Update.UpdateResource.ResourceVisit;
using Application.Tasks.Commands.Update.UpdateResource.ResourceVote;
using Application.Tasks.Queries.ArticleQueries.GetLastQuestions;
using Application.Tasks.Queries.ArticleQueries.GetLastResources;
using Application.Tasks.Queries.ResourceQueries.GetAllResources;
using Application.Tasks.Queries.ResourceQueries.GetAllUserResource;
using Application.Tasks.Queries.ResourceQueries.GetResource;
using Application.Tasks.Queries.ResourceQueries.GetResourcesCount;
using Application.Tasks.Queries.ResourceQueries.GetSavedAllResources;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers;

[Route("api/explore")]
[ApiController]
public class ResourceController : ControllerBase
{
    private readonly IMediator _mediator;

    public ResourceController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpGet("GetLastResources")]
    public async Task<ActionResult<IEnumerable<GetLastQuestionsDTO>>> GetLastResources(GetLastResourcesQuery query)
    {
        try
        {
            var resources = await _mediator.Send(query);

            return Ok(resources);
        }
        catch (Exception Ex)
        {
            return Problem(Ex.Message);
        }
    }

    [HttpGet("count")]
    public async Task<ActionResult<ResourceResponse>> GetResourceById(
        GetResourcesCountCommand command
    )
    {
        try
        {
            var resource = await _mediator.Send(command);
            return Ok(resource);
        }
        catch (Exception)
        {
            return Problem("A problem acquired please try later");
        }
    }

    [HttpGet("/api/resource/{ResourceId}")]
    public async Task<ActionResult<ResourceResponse>> GetResourceById(GetResourceCommand command)
    {
        try
        {
            var resource = await _mediator.Send(command);
            return Ok(resource);
        }
        catch (EntityNotFoundException)
        {
            return NotFound("Resource not found.");
        }
        catch (Exception)
        {
            return Problem("A problem acquired please try later");
        }
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="query"></param>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<PaginatedListDto<ResourceResponse>>> GetAll(
        [FromQuery] GetAllResourcesQuery query
    )
    {
        try
        {
            var resources = await _mediator.Send(query);

            return Ok(resources);
        }
        catch (Exception ex)
        {
            return Problem("A problem has occurred please try again later");
        }
    }


    [HttpGet("GetAllUserResources")]
    public async Task<ActionResult<List<ResourceResponse>>> GetAllUserResources()
    {
        try
        {
            var data = await _mediator.Send(new GetAllUserResourceQuery());
            return Ok(data);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }


    /// <summary>
    ///
    /// </summary>
    /// <param name="query"></param>
    /// <returns></returns>

    [HttpGet("saved")]
    public async Task<ActionResult<PaginatedListDto<ResourceResponse>>> GetSavedAll(
        [FromQuery] GetAllSavedResourcesQuery query
    )
    {
        try
        {
            var resources = await _mediator.Send(query);

            return Ok(resources);
        }
        catch (Exception ex)
        {
            return Problem("A problem has occurred please try again later");
        }
    }

    /// <summary>
    /// Inserts a new resource into the system.
    /// </summary>
    /// <param name="command">The command containing the information for the new resource.</param>
    /// <returns>A response indicating the success of the insertion operation.</returns>

    [HttpPost("InsertResource")]
    public async Task<ActionResult> InsertResource([FromBody] InsertResourceCommand command)
    {
        var result = await _mediator.Send(command);

        if (result.Equals("0"))
        {
            GeneralResponce response = new GeneralResponce()
            {
                Result = 0,
                Message = "This Resource Url is exist"
            };
            return Conflict(response);
        }
        else if (result.Equals("1"))
        {
            GeneralResponce response = new GeneralResponce()
            {
                Result = 1,
                Message = "CategoryId or ResourceFlagId is not exist"
            };
            return NotFound(response);
        }
        else if (result is "-1")
        {
            GeneralResponce response = new GeneralResponce()
            {
                Result = -1,
                Message = "System Exception"
            };
            return Problem(response.Message);
        }
        else if (result is "forbidden")
        {
            GeneralResponce response = new GeneralResponce()
            {
                Result = -1,
                Message = "access denied for delete resource"
            };
            return Forbid(response.Message);
        }
        else
        {
            return Ok(result);
        }
    }

    /// <summary>
    /// Deletes a resource from the system based on the provided resource ID.
    /// </summary>
    /// <param name="command">The command containing the resource ID to be deleted.</param>
    /// <returns>A response indicating the success of the deletion operation.</returns>

    [HttpDelete("/api/resource/delete/{ResourceId}")]
    public async Task<ActionResult> DeleteResource(DeleteResourceCommand command)
    {
        var result = await _mediator.Send(command);

        if (result.Equals("0"))
        {
            GeneralResponce responce = new() { Result = 0, Message = "Delete process failed" };
            return NotFound(responce);
        }
        else
        {
            GeneralResponce responce =
                new()
                {
                    Result = result,
                    Message = $"Resource Id: {result},Delete process successfully"
                };
            return Ok(responce);
        }
    }

    /// <summary>
    /// Saves a resource with the provided resource ID.
    /// </summary>
    /// <param name="command">The command containing the resource ID to be saved.</param>
    /// <returns>A response indicating the success of the save operation.</returns>

    [HttpPatch("/api/resource/save/{ResourceId}")]
    public async Task<ActionResult> SaveResource(SaveResourceCommand command)
    {
        var result = await _mediator.Send(command);

        GeneralResponce responce;

        switch (result)
        {
            case 0:
                responce = new() { Result = 0, Message = "Save Resource process successfully" };
                return Ok(responce);
            case -1:
                responce = new() { Result = -1, Message = "System Exception" };
                return Problem(responce.Message);
            case -2:
                responce = new() { Result = -2, Message = "This Resource is not exist" };
                return NotFound(responce);
            default:
                return Problem();
        }
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="command"></param>
    /// <returns></returns>
    [HttpPatch("/api/resource/visit/{ResourceId}")]
    public async Task<ActionResult> VisitResource(ResourceVisitCommand command)
    {
        try
        {
            await _mediator.Send(command);
            return Ok();
        }
        catch (Exception)
        {
            return Problem("A problem acquired please try later");
        }
    }

    /// <summary>
    /// Votes on a resource with the specified status and resource ID.
    /// </summary>
    /// <param name="command">The command containing the status and resource ID for voting.</param>
    /// <returns>A response indicating the success of the voting process.</returns>

    [HttpPatch("/api/resource/vote/{Status}/{ResourceId}")]
    public async Task<ActionResult> Vote(VoteResourceCommand command)
    {
        var voted = await _mediator.Send(command);

        if (voted is 0)
        {
            GeneralResponce responce =
                new() { Result = 0, Message = "User or Resource is not exist" };
            return NotFound(responce);
        }
        else if (voted is -1)
        {
            GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
            return Problem(responce.Message);
        }
        else
        {
            GeneralResponce responce =
                new() { Result = 1, Message = "VoteResource process successfully" };
            return Ok(responce);
        }
    }

    [HttpPut("EditResource")]
    public async Task<ActionResult> EditResource([FromBody] EditResourceCommand command)
    {
        var edited = await _mediator.Send(command);

        if (edited is "0")
        {
            GeneralResponce responce = new() { Result = 0, Message = "Resource is not exist" };
            return NotFound(responce);
        }
        else if (edited is "-1")
        {
            GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
            return Problem(responce.Message);
        }
        else
        {
            GeneralResponce responce =
                new()
                {
                    Result = edited,
                    Message = $"Resource Id: {edited}, Edit process successfully"
                };
            return Ok(responce);
        }
    }
}
