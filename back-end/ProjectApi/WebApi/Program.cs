using AppDomain;
using Application;
using FluentValidation.AspNetCore;
using Infrastructure;
using Microsoft.AspNetCore.Rewrite;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDomain();

builder.Services.AddApplication();

builder.Services.AddInfrastructure(builder.Configuration);

builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddFluentValidation();

builder.Services.AddConfigs(builder.Configuration);
builder.Services.AuthenticationAndAuthorization(builder.Configuration);
builder.Services.AddSwagger();
builder.Services.AddMemoryCache();

builder.Services
    .AddControllers()
    .ConfigureApiBehaviorOptions(options =>
    {
        options.SuppressInferBindingSourcesForParameters = true;
    });

builder.Services.AddCors(
    p =>
        p.AddPolicy(
            "corsapp",
            builder =>
            {
                builder
                    .WithOrigins("http://localhost:5173", "https://www.clubrick.net")
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials();

                //builder
                //    .WithOrigins("https://www.clubrick.net")
                //    .AllowAnyMethod()
                //    .AllowAnyHeader()
                //    .AllowCredentials();

                //.WithHeaders("Access-Control-Allow-Headers");

                //builder.Allow
                //builder.WithOrigins("http://localhost:5173").AllowAnyMethod().AllowAnyHeader();
            }
        )
);

var app = builder.Build();

var newUrl = new RewriteOptions().AddRewrite(
    "https://www.admin.clubrick.net",
    "https://www.clubrick.net/manage/admin",
    false
);
app.UseRewriter(newUrl);

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(x => x.EnablePersistAuthorization());
}

app.UseHttpsRedirection();

app.UseCors("corsapp");

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.Run();
