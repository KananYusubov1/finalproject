﻿using AppDomain.DTO;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Insert.CommentInserts.InsertComment;

public class InsertCommentCommandHandler : IRequestHandler<InsertCommentCommand, CommentDTO>
{
    private readonly ICommentRepository _commentRepository;

    public InsertCommentCommandHandler(ICommentRepository commentRepository)
    {
        _commentRepository = commentRepository;
    }

    public async Task<CommentDTO> Handle(
        InsertCommentCommand request,
        CancellationToken cancellationToken
    )
    {
        return await _commentRepository.InsertCommentAsync(
            request.Content,
            request.ContentId
        );
    }
}