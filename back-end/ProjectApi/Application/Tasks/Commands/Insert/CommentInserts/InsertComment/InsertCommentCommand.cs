﻿using AppDomain.DTO;
using MediatR;

namespace Application.Tasks.Commands.Insert.CommentInserts.InsertComment;

public class InsertCommentCommand : IRequest<CommentDTO>
{
    public string Content { get; set; }
    public string ContentId { get; set; }
}