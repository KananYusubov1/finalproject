﻿using FluentValidation;

namespace Application.Tasks.Commands.Insert.CommentInserts.InsertComment;

public class InsertCommentCommandValidator : AbstractValidator<InsertCommentCommand>
{
    public InsertCommentCommandValidator()
    {
        RuleFor(x => x.Content)
            .NotNull();
    }
}