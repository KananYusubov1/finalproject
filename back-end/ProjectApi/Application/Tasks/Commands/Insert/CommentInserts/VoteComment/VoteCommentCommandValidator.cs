﻿using Application.Tasks.Commands.Insert.VoteComment;
using FluentValidation;

namespace Application.Tasks.Commands.Insert.CommentInserts.VoteComment;

public class VoteCommentCommandValidator : AbstractValidator<VoteCommentCommand>
{
    public VoteCommentCommandValidator()
    {
        RuleFor(x => x.Status).NotEmpty().IsInEnum();

        RuleFor(x => x.CommentId).NotEmpty();
    }
}
