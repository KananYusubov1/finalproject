﻿using AppDomain.Enums;
using MediatR;

namespace Application.Tasks.Commands.Insert.VoteComment;

public class VoteCommentCommand : IRequest
{
    public VoteStatus Status { get; set; }
    public string CommentId { get; set; }
}
