﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Insert.InsertContentReport;

public class InsertReportCommandHandler : IRequestHandler<InsertReportCommand, Task>
{
    private readonly IReportRepository _reportRepository;

    public InsertReportCommandHandler(IReportRepository reportRepository)
    {
        _reportRepository = reportRepository;
    }

    public async Task<Task> Handle(InsertReportCommand request, CancellationToken cancellationToken)
    {
        return await _reportRepository.ReportContent(request.ReportDTO);
    }
}