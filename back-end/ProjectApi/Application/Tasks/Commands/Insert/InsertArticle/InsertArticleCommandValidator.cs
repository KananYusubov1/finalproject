﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Insert.InsertArticle
{
    public class InsertArticleCommandValidator : AbstractValidator<InsertArticleCommand>
    {
        public InsertArticleCommandValidator()
        {
            RuleFor(x => x.Body).NotNull().NotEmpty();
            RuleFor(x => x.ReadingTime).NotNull().NotEmpty().GreaterThan(0);
            RuleFor(x => x.FlagId).NotNull().NotEmpty();
            RuleFor(x => x.CategoriesIdList).NotNull().NotEmpty();
            RuleFor(x => x.Header.Title).NotNull().NotEmpty();
        }
    }
}
