﻿using AppDomain.ValueObjects;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Insert.InsertArticle
{
    public class InsertArticleCommand : IRequest<string>
    {
        public string Body { get; set; }
        public Header Header { get; set; }
        public int ReadingTime { get; set; }
        public string FlagId { get; set; }
        public List<string> CategoriesIdList { get; set; }
    }
}
