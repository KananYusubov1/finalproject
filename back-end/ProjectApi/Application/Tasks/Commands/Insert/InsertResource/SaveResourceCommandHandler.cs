﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Insert.InsertResource
{
    public class SaveResourceCommandHandler : IRequestHandler<SaveResourceCommand,int>
    {
        private readonly IResourceRepository _repository;

        private readonly IAuthRepository _authRepository;

        public SaveResourceCommandHandler(IResourceRepository repository, IAuthRepository authRepository)
        {
            _repository = repository;
            _authRepository = authRepository;
        }

        public async Task<int> Handle(SaveResourceCommand request, CancellationToken cancellationToken)
        {
            try
            {
                string token = _authRepository.GetClaimValue("userId");

                var saved = await _repository.SaveResource(request.ResourceId, token);

                return saved;
            }
            catch (Exception)
            {
                return -1;
                throw;
            }
        }
    }
}