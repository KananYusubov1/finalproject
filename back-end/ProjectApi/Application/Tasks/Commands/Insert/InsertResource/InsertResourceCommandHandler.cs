﻿using AppDomain.Entities.ContentRelated;
using AppDomain.Entities.TagBaseRelated;
using AppDomain.Interfaces;
using Application.Services;
using AutoMapper;
using MediatR;

namespace Application.Tasks.Commands.Insert.InsertResource;

public class InsertResourceCommandHandler : IRequestHandler<InsertResourceCommand, string>
{
    private readonly IResourceRepository _repository;

    private readonly IMapper _mapper;

    private readonly IAuthRepository _authRepository;

    public InsertResourceCommandHandler(
        IResourceRepository repository,
        IMapper mapper,
        IAuthRepository authRepository
    )
    {
        _repository = repository;
        _mapper = mapper;
        _authRepository = authRepository;
    }

    public async Task<string> Handle(
        InsertResourceCommand request,
        CancellationToken cancellationToken
    )
    {
        try
        {
            Resource resourceDTO = _mapper.Map<Resource>(request);

            resourceDTO.UserId = _authRepository.GetClaimValue("userId");

            resourceDTO.Id = IDGeneratorService.GetShortUniqueId();

            resourceDTO.Category = new Category() { Id = request.CategoryId };

            resourceDTO.Flag = new ResourceFlag() { Id = request.ResourceFlagId };

            resourceDTO.UpdateTime = DateTime.UtcNow;

            var result = await _repository.InsertResource(resourceDTO);

            return await Task.FromResult(result);
        }
        catch (Exception)
        {
            return "-1";
            throw;
        }
    }
}