﻿using FluentValidation;

namespace Application.Tasks.Commands.Insert.InsertArticleFlag;

public class InsertArticleFlagCommandValidator : AbstractValidator<InsertArticleFlagCommand>
{
    public InsertArticleFlagCommandValidator()
    {
        RuleFor(x => x.InsertTagDTO.Title).NotEmpty().NotNull();
        RuleFor(x => x.InsertTagDTO.AccentColor).NotEmpty().NotNull();
    }
}
