﻿using FluentValidation;

namespace Application.Tasks.Commands.Insert.InsertResourceFlag;

public class InsertResourceFlagCommandValidator : AbstractValidator<InsertResourceFlagCommand>
{
    public InsertResourceFlagCommandValidator()
    {
        RuleFor(x => x.InsertTagDTO.Title).NotNull().NotEmpty();
        RuleFor(x => x.InsertTagDTO.AccentColor).NotNull().NotEmpty();
    }
}
