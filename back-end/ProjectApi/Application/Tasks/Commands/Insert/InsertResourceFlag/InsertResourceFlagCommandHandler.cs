﻿using AppDomain.Entities.TagBaseRelated;
using AppDomain.Interfaces;
using Application.Services;
using AutoMapper;
using MediatR;

namespace Application.Tasks.Commands.Insert.InsertResourceFlag
{
    public class InsertResourceFlagCommandHandler : IRequestHandler<InsertResourceFlagCommand,string>
    {
        private readonly IResourceFlagRepository _repository;

        private readonly IMapper _mapper;

        public InsertResourceFlagCommandHandler(IResourceFlagRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<string> Handle(InsertResourceFlagCommand request, CancellationToken cancellationToken)
        {
            try
            {
                ResourceFlag resourceFlag = _mapper.Map<ResourceFlag>(request.InsertTagDTO);

                resourceFlag.Id = IDGeneratorService.GetShortUniqueId();

                resourceFlag.UseCount = 0;

                var result = await _repository.InsertResourceFlag(resourceFlag);

                return result;
            }
            catch (Exception)
            {
                return "-1";
            }
        }
    }
}