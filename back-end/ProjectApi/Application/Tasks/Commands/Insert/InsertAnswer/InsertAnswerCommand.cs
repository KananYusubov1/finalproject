﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Insert.InsertAnswer
{
    public class InsertAnswerCommand : IRequest<string>
    {
        public string QuestionId { get; set; }
        public string Body { get; set; }
    }
}
