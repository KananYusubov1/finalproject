﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Insert.InsertAnswer
{
    public class InsertAnswerCommandValidator : AbstractValidator<InsertAnswerCommand>
    {
        public InsertAnswerCommandValidator()
        {
            RuleFor(x => x.QuestionId).NotNull().NotEmpty();
            RuleFor(x => x.Body).NotNull().NotEmpty();
        }
    }
}
