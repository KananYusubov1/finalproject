﻿using AppDomain.Entities.ContentRelated;
using AppDomain.Interfaces;
using Application.Services;
using AutoMapper;
using MediatR;

namespace Application.Tasks.Commands.Insert.InsertAnswer
{
    public class InsertAnswerCommandHandler : IRequestHandler<InsertAnswerCommand,string>
    {
        private readonly IAnswerRepository _repository;

        private readonly IAuthRepository _authRepository;

        private readonly IMapper _mapper;

        public InsertAnswerCommandHandler(IAnswerRepository repository, IAuthRepository authRepository, IMapper mapper)
        {
            _repository = repository;
            _authRepository = authRepository;
            _mapper = mapper;
        }

        public async Task<string> Handle(InsertAnswerCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var tokenId = _authRepository.GetClaimValue("userId");

                Answer answer = _mapper.Map<Answer>(request);

                answer.Id = IDGeneratorService.GetShortUniqueId();

                answer.UpdateTime = DateTime.UtcNow;

                answer.VoteCount = 0;

                answer.UserId = tokenId;

                var result = await _repository.InsertAnswer(answer);

                return result;
            }
            catch (Exception)
            {
                return "-1";
            }
        }
    }
}