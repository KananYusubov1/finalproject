﻿using AppDomain.DTOs.Product;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Insert.InsertProduct
{
    public class InsertProductCommand : IRequest
    {
        public InsertProductDTO InsertProductDTO { get; set; }
    }
}
