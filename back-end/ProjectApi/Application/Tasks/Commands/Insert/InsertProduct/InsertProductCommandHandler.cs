﻿using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Insert.InsertProduct
{
    public class InsertProductCommandHandler : IRequestHandler<InsertProductCommand>
    {
        private readonly IProductRepository _repository;

        public InsertProductCommandHandler(IProductRepository repository)
        {
            _repository = repository;
        }

        public Task Handle(InsertProductCommand request, CancellationToken cancellationToken)
        {
            return _repository.InsertProduct(request.InsertProductDTO);
        }
    }
}
