﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Insert.InsertProduct
{
    public class InsertProductCommandValidator : AbstractValidator<InsertProductCommand>
    {
        public InsertProductCommandValidator()
        {
            RuleFor(x => x.InsertProductDTO.Name).NotEmpty();
            RuleFor(x => x.InsertProductDTO.Description).NotEmpty();
            RuleFor(x => x.InsertProductDTO.PhotoPaths)
                .NotNull()
                .NotEmpty()
                .WithMessage("Photo mustn't be empty");
            RuleFor(x => x.InsertProductDTO.Price)
                .GreaterThan(0.0)
                .WithMessage("Price is greater than 0");
            RuleFor(x => x.InsertProductDTO.Stock)
                .GreaterThan(0)
                .WithMessage("Stock is greater than 0");
        }
    }
}
