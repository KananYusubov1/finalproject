﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Insert.InsertBanner;

public class InsertBannerCommandHandler : IRequestHandler<InsertBannerCommand, Task>
{
    private readonly IAdminRepository _adminRepository;

    public InsertBannerCommandHandler(IAdminRepository adminRepository)
    {
        _adminRepository = adminRepository;
    }

    public async Task<Task> Handle(InsertBannerCommand request, CancellationToken cancellationToken)
    {
        return await _adminRepository.PostBannerAsync(request.BannerDTO);
    }
}