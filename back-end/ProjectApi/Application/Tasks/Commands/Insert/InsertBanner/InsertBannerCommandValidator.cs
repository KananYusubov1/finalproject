﻿using FluentValidation;

namespace Application.Tasks.Commands.Insert.InsertBanner;

public class InsertBannerCommandValidator : AbstractValidator<InsertBannerCommand>
{
    public InsertBannerCommandValidator()
    {
        RuleFor(command => command.BannerDTO.Photo).NotEmpty();
        RuleFor(command => command.BannerDTO.Url).NotEmpty();
        RuleFor(command => command.BannerDTO.Title).NotEmpty();
        RuleFor(command => command.BannerDTO.Description).NotEmpty();
        RuleFor(command => command.BannerDTO.CreatedAt).NotEmpty();
        RuleFor(command => command.BannerDTO.ValidUntil).NotEmpty();
    }
}