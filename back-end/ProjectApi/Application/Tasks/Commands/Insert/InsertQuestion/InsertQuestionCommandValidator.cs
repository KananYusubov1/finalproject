﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Insert.InsertQuestion
{
    public class InsertQuestionCommandValidator : AbstractValidator<InsertQuestionCommand>
    {
        public InsertQuestionCommandValidator()
        {
            //RuleFor(x => x.Header).NotNull().NotEmpty();
            RuleFor(x => x.Header.Title).NotNull().NotEmpty();
            RuleFor(x => x.ReadingTime).NotEmpty();
            RuleFor(x => x.Body).NotNull().NotEmpty();
            RuleFor(x => x.CategoriesIdList).NotNull().NotEmpty();
        }
    }
}
