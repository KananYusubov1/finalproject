﻿using AppDomain.Entities.ContentRelated;
using AppDomain.Interfaces;
using Application.Services;
using AutoMapper;
using MediatR;

namespace Application.Tasks.Commands.Insert.InsertQuestion
{
    public class InsertQuestionCommandHandler : IRequestHandler<InsertQuestionCommand,string>
    {
        private readonly IQuestionRepository _questionRepository;

        private readonly IUserRepository _userRepository;

        private readonly IMapper _mapper;

        public InsertQuestionCommandHandler(IQuestionRepository questionRepository, IUserRepository userRepository, IMapper mapper)
        {
            _questionRepository = questionRepository;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<string> Handle(InsertQuestionCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var userId = _userRepository.GetClaimValue("userId");

                Question question = _mapper.Map<Question>(request);

                question.Id = IDGeneratorService.GetShortUniqueId();

                question.UserId = userId;

                question.WatchCount = 0;

                question.UpdateTime = DateTime.UtcNow;

                question.ViewCount = 0;

                question.VoteCount = 0;

                var result = await _questionRepository.InsertQuestion(question,request.CategoriesIdList);

                return result;

            }
            catch (Exception)
            {
                return "-1";
            }
        }
    }
}