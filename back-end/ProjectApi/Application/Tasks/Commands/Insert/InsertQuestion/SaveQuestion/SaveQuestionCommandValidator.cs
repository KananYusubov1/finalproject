﻿using FluentValidation;

namespace Application.Tasks.Commands.Insert.InsertQuestion.SaveQuestion
{
    public class SaveQuestionCommandValidator : AbstractValidator<SaveQuestionCommand>
    {
        public SaveQuestionCommandValidator()
        {
            RuleFor(x => x.QuestionId).NotNull().NotEmpty();
        }
    }
}