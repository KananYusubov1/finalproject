﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Insert.InsertQuestion.SaveQuestion
{
    public class SaveQuestionCommand : IRequest<int>
    {
        public string QuestionId { get; set; }
    }
}
