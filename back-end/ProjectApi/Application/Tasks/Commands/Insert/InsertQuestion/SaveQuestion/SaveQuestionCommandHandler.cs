﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Insert.InsertQuestion.SaveQuestion
{
    public class SaveQuestionCommandHandler : IRequestHandler<SaveQuestionCommand,int>
    {
        private readonly IQuestionRepository _questionRepository;

        private readonly IUserRepository _userRepository;

        public SaveQuestionCommandHandler(IQuestionRepository questionRepository, IUserRepository userRepository)
        {
            _questionRepository = questionRepository;
            _userRepository = userRepository;
        }

        public async Task<int> Handle(SaveQuestionCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var tokenId = _userRepository.GetClaimValue("userId");

                var result = await _questionRepository.SaveQuestion(request.QuestionId, tokenId);

                return result;
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }
}