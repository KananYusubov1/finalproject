﻿using AppDomain.ValueObjects;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Insert.InsertQuestion
{
    public class InsertQuestionCommand : IRequest<string>
    {
        public Header Header { get; set; }
        public string Body { get; set; }
        public int ReadingTime { get; set; }
        public List<string> CategoriesIdList { get; set; }
    }
}
