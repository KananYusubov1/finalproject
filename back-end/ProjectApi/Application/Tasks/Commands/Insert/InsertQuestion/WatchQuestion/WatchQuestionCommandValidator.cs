﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Insert.InsertQuestion.WatchQuestion
{
    public class WatchQuestionCommandValidator : AbstractValidator<WatchQuestionCommand>
    {
        public WatchQuestionCommandValidator()
        {
            RuleFor(x => x.QuestionId).NotNull().NotEmpty();
        }
    }
}
