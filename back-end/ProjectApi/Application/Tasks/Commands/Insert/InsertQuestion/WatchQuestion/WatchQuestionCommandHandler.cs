﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Insert.InsertQuestion.WatchQuestion
{
    public class WatchQuestionCommandHandler : IRequestHandler<WatchQuestionCommand, int>
    {
        private readonly IQuestionRepository _questionRepository;

        private readonly IUserRepository _userRepository;

        public WatchQuestionCommandHandler(IQuestionRepository questionRepository, IUserRepository userRepository)
        {
            _questionRepository = questionRepository;
            _userRepository = userRepository;
        }

        public async Task<int> Handle(WatchQuestionCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var userId = _userRepository.GetClaimValue("userId");

                var result = await _questionRepository.WatchQuestion(request.QuestionId, userId);
                
                return result;
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }
}