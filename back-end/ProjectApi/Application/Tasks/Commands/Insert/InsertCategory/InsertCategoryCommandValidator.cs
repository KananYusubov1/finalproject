﻿using FluentValidation;

namespace Application.Tasks.Commands.Insert.InsertCategory;

public class InsertCategoryCommandValidator : AbstractValidator<InsertCategoryCommand>
{
    public InsertCategoryCommandValidator()
    {
        RuleFor(x => x.InsertTagDTO.AccentColor).NotEmpty();
        RuleFor(x => x.InsertTagDTO.Title).NotEmpty();
    }
}
