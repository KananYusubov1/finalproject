﻿using AppDomain.DTOs.Category;
using MediatR;

namespace Application.Tasks.Commands.Insert.InsertCategory;

public class InsertCategoryCommand : IRequest<string>
{
    public InsertCategoryDTO InsertTagDTO { get; set; }
}
