﻿using AppDomain.DTOs.Auth;
using MediatR;

namespace Application.Tasks.Commands.Insert.UserInserts.InsertUser;

public class InsertUserCommand : IRequest<UserAuthDto>
{
    public InsertPendingUserDTO User { get; set; }
}
