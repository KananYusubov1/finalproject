﻿using FluentValidation;

namespace Application.Tasks.Commands.Insert.UserInserts.FollowUser;
public class FollowUserCommandValidator : AbstractValidator<FollowUserCommand>
{
    public FollowUserCommandValidator()
    {
        RuleFor(user => user.Id)
            .NotEmpty()
            .NotNull();
    }
}