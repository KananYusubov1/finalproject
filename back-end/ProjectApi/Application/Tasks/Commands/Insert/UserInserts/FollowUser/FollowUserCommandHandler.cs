﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Insert.UserInserts.FollowUser;

public class FollowUserCommandHandler : IRequestHandler<FollowUserCommand, Task>
{
    private readonly IUserRepository _userRepository;

    public FollowUserCommandHandler(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<Task> Handle(FollowUserCommand request, CancellationToken cancellationToken)
    {
        return await _userRepository.FollowUserAsync(request.Id);
    }
}