﻿using AppDomain.DTO;
using AppDomain.DTOs.User;
using AppDomain.Entities.UserRelated;
using MediatR;

namespace Application.Tasks.Commands.Insert.UserInserts.FollowUser;

public class FollowUserCommand : IRequest<Task>
{
    public string Id { get; set; }
}