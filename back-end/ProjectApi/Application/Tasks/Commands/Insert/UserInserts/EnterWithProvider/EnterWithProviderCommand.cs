﻿using AppDomain.DTOs.Auth;
using AppDomain.Enums;
using MediatR;

namespace Application.Tasks.Commands.Insert.UserInserts.EnterWithProvider;

public class EnterWithProviderCommand : IRequest<UserAuthDto>
{
    public EnterWithProviderDTO providerEnter { get; set; }
}
