﻿using AppDomain.DTOs.Auth;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Insert.UserInserts.EnterWithProvider;

public class EnterWithProviderCommandHandler
    : IRequestHandler<EnterWithProviderCommand, UserAuthDto>
{
    private readonly IAuthRepository _authRepository;

    public EnterWithProviderCommandHandler(IAuthRepository authRepository)
    {
        _authRepository = authRepository;
    }

    public async Task<UserAuthDto> Handle(
        EnterWithProviderCommand request,
        CancellationToken cancellationToken
    )
    {
        return await _authRepository.EnterWitProvider(request.providerEnter);
    }
}
