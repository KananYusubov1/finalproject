﻿using AppDomain.DTOs.Auth;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Insert.UserInserts.BuildUser;

public class BuildUserCommandHandler : IRequestHandler<BuildUserCommand, UserBuildResponseDTO>
{
    private readonly IAuthRepository _authRepository;

    public BuildUserCommandHandler(IAuthRepository authRepository)
    {
        _authRepository = authRepository;
    }

    public async Task<UserBuildResponseDTO> Handle(
        BuildUserCommand request,
        CancellationToken cancellationToken
    )
    {
        return await _authRepository.BuildUserAsync(request.BuildUser);
    }
}
