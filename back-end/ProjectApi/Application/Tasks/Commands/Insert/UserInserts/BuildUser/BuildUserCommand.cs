﻿using AppDomain.DTOs.Auth;
using MediatR;

namespace Application.Tasks.Commands.Insert.UserInserts.BuildUser;

public class BuildUserCommand : IRequest<UserBuildResponseDTO>
{
    public BuildUserDTO BuildUser { get; set; }
}
