﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Insert.UserInserts.InsertConnectedAccount;

public class InsertConnectedAccountCommandHandler
    : IRequestHandler<InsertConnectedAccountCommand, Task>
{
    private readonly IUserRepository _userRepository;

    public InsertConnectedAccountCommandHandler(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<Task> Handle(
        InsertConnectedAccountCommand request,
        CancellationToken cancellationToken
    )
    {
        return await _userRepository.AddConnectedAccountAsync(
            request.Type,
            request.Secret,
            request.AccountUsername,
            request.Email
        );
    }
}
