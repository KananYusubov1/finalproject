﻿using MediatR;

namespace Application.Tasks.Commands.Insert.UserInserts.InsertOTPCode;

public class InsertOTPCommand : IRequest<Task>
{
    public string ToEmail { get; set; }
    public string? ToName { get; set; } = null;
}