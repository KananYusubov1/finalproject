﻿using FluentValidation;

namespace Application.Tasks.Commands.Insert.UserInserts.InsertOTPCode;
public class InsertOTPCommandValidator : AbstractValidator<InsertOTPCommand>
{
    public InsertOTPCommandValidator()
    {
        RuleFor(user => user.ToEmail)
            .NotEmpty()
            .EmailAddress();
    }
}