﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Insert.UserInserts.InsertOTPCode;

public class InsertOTPCommandHandler : IRequestHandler<InsertOTPCommand, Task>
{
    private readonly IAuthRepository _authRepository;

    public InsertOTPCommandHandler(IAuthRepository authRepository)
    {
        _authRepository = authRepository;
    }

    public async Task<Task> Handle(InsertOTPCommand request, CancellationToken cancellationToken)
    {
        //return await _authRepository.SendOTPCodeAsync(request.Email, request.Name);
        return await _authRepository.SendOTPCodeAsync(request.ToEmail, request.ToName);
    }
}
