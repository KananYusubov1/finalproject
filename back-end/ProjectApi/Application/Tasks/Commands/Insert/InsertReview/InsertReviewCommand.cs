﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Insert.InsertReview
{
    public class InsertReviewCommand : IRequest<string>
    {
        public string ProductId { get; set; }
        public string Body { get; set; }
        public int Rank { get; set; }
    }
}
