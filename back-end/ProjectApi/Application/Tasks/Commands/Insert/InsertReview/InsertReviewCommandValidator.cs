﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Insert.InsertReview
{
    public class InsertReviewCommandValidator : AbstractValidator<InsertReviewCommand>
    {
        public InsertReviewCommandValidator()
        {
            RuleFor(x => x.Rank).GreaterThanOrEqualTo(0);
        }
    }
}
