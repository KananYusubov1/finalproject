﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Insert.InsertNotification;

public class InsertNotificationCommandHandler : IRequestHandler<InsertNotificationCommand, Task>
{
    private readonly INotificationRepository _notificationRepository;

    public InsertNotificationCommandHandler(INotificationRepository notificationRepository)
    {
        _notificationRepository = notificationRepository;
    }

    public async Task<Task> Handle(InsertNotificationCommand request, CancellationToken cancellationToken)
    {
        return await _notificationRepository.PostNotificationAsync(request.NotificationDTO);
    }
}