﻿using AppDomain.DTOs.Notification;
using MediatR;

namespace Application.Tasks.Commands.Insert.InsertNotification;

public class InsertNotificationCommand : IRequest<Task>
{
    public NotificationDTO NotificationDTO { get; set; }
}