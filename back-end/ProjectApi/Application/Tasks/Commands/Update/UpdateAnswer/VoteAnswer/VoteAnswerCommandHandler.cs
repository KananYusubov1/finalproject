﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateAnswer.VoteAnswer;

public class VoteAnswerCommandHandler : IRequestHandler<VoteAnswerCommand,int>
{
    private readonly IAnswerRepository _answerRepository;

    private readonly IAuthRepository _authRepository;

    public VoteAnswerCommandHandler(IAnswerRepository answerRepository, IAuthRepository authRepository)
    {
        _answerRepository = answerRepository;
        _authRepository = authRepository;
    }

    public async Task<int> Handle(VoteAnswerCommand request, CancellationToken cancellationToken)
    {
        try
        {
            int result = -1;

            var tokenId = _authRepository.GetClaimValue("userId");

            if(tokenId != null)
            {
                result = await _answerRepository.VoteAnswer(request.Status,request.AnswerId, tokenId);
            }

            return result;
        }
        catch (Exception)
        {
            return -1;
        }
    }
}