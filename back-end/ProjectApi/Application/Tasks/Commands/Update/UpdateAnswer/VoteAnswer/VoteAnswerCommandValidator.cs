﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Update.UpdateAnswer.VoteAnswer
{
    public class VoteAnswerCommandValidator : AbstractValidator<VoteAnswerCommand>
    {
        public VoteAnswerCommandValidator()
        {
            RuleFor(x => x.AnswerId).NotNull().NotEmpty();
        }
    }
}
