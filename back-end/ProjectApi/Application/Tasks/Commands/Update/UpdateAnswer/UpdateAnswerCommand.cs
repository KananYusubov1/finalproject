﻿using MediatR;

namespace Application.Tasks.Commands.Update.UpdateAnswer;

public class UpdateAnswerCommand : IRequest<string>
{
    public string ContentId { get; set; }
    public string Body { get; set; }
}
