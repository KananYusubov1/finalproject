﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateAnswer
{
    public class UpdateAnswerCommandHandler : IRequestHandler<UpdateAnswerCommand, string>
    {
        private readonly IAnswerRepository _answerRepository;

        public UpdateAnswerCommandHandler(IAnswerRepository answerRepository)
        {
            _answerRepository = answerRepository;
        }

        public async Task<string> Handle(
            UpdateAnswerCommand request,
            CancellationToken cancellationToken
        )
        {
            try
            {
                var result = await _answerRepository.UpdateAnswer(request.ContentId, request.Body);

                return result;
            }
            catch (Exception)
            {
                return "-1";
            }
        }
    }
}
