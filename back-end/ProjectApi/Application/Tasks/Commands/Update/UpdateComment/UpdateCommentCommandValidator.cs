﻿using FluentValidation;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdateComment;

public class UpdateCommentCommandValidator : AbstractValidator<UpdateCommentCommand>
{
    public UpdateCommentCommandValidator()
    {
        RuleFor(x => x.ContentId).NotEmpty().NotNull();

        RuleFor(x => x.Body).NotEmpty().NotNull();
    }
}
