﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdateComment;

public class UpdateCommentCommandHandler : IRequestHandler<UpdateCommentCommand, Task>
{
    private readonly ICommentRepository _commentRepository;

    public UpdateCommentCommandHandler(ICommentRepository commentRepository)
    {
        _commentRepository = commentRepository;
    }

    public async Task<Task> Handle(
        UpdateCommentCommand request,
        CancellationToken cancellationToken
    )
    {
        return await _commentRepository.EditCommentAsync(request.ContentId, request.Body);
    }
}
