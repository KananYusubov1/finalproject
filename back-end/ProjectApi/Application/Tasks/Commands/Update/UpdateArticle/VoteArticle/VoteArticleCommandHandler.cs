﻿using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Update.UpdateArticle.VoteArticle
{
    public class VoteArticleCommandHandler : IRequestHandler<VoteArticleCommand,int>
    {
        private readonly IArticleRepository _articleRepository;

        private readonly IUserRepository _userRepository;

        public VoteArticleCommandHandler(IArticleRepository articleRepository, IUserRepository userRepository)
        {
            _articleRepository = articleRepository;
            _userRepository = userRepository;
        }

        public async Task<int> Handle(VoteArticleCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var tokenUser = await _userRepository.GetCurrentUser();

                if (tokenUser.IsFrozen)
                    return -2;

                var result = await _articleRepository.VoteArticle(request.Status, request.ArticleId, tokenUser.Id);

                return result;
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }
}
