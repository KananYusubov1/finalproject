﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Update.UpdateArticle.VoteArticle
{
    public class VoteArticleCommandValidator : AbstractValidator<VoteArticleCommand>
    {
        public VoteArticleCommandValidator()
        {
            RuleFor(x => x.ArticleId).NotNull().NotEmpty();
        }
    }
}
