﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Update.UpdateArticle.SaveArticle
{
    public class SaveArticleCommandValidator : AbstractValidator<SaveArticleCommand>
    {
        public SaveArticleCommandValidator()
        {
            RuleFor(x => x.ArticleId).NotNull().NotEmpty();
        }
    }
}
