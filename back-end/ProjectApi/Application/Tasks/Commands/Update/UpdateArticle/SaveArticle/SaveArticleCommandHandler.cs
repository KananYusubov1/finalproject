﻿using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Update.UpdateArticle.SaveArticle
{
    public class SaveArticleCommandHandler : IRequestHandler<SaveArticleCommand,int>
    {
        private readonly IArticleRepository _articleRepository;

        private readonly IUserRepository _userRepository;

        public SaveArticleCommandHandler(IArticleRepository articleRepository, IUserRepository userRepository)
        {
            _articleRepository = articleRepository;
            _userRepository = userRepository;
        }

        public async Task<int> Handle(SaveArticleCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var tokenUser = await _userRepository.GetCurrentUser();

                if (tokenUser.IsFrozen)
                    return -2;

                var result = await _articleRepository.SaveArticle(request.ArticleId,tokenUser.Id);

                return result;
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }
}
