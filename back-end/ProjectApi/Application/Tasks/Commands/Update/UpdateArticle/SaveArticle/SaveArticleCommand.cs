﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Update.UpdateArticle.SaveArticle
{
    public class SaveArticleCommand : IRequest<int>
    {
        public string ArticleId { get; set; }
    }
}
