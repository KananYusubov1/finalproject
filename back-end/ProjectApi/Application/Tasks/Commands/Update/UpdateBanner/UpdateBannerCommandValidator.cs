﻿using FluentValidation;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdateBanner;

public class UpdateBannerCommandValidator : AbstractValidator<UpdateBannerCommand>
{
    public UpdateBannerCommandValidator()
    {
        RuleFor(x => x.BannerId).NotEmpty();
    }
}