﻿using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdateBanner;

public class UpdateBannerCommand : IRequest<Task>
{
    public string? BannerId { get; set; }
    public string? Photo { get; set; }
    public string? Url { get; set; }
    public string? Title { get; set; }
    public string? Description { get; set; }
    public DateTime? ValidUntil { get; set; }
}