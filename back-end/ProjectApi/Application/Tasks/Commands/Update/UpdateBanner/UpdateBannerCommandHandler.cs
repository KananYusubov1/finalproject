﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdateBanner;

public class UpdateBannerCommandHandler : IRequestHandler<UpdateBannerCommand, Task>
{
    private readonly IAdminRepository _adminRepository;

    public UpdateBannerCommandHandler(IAdminRepository adminRepository)
    {
        _adminRepository = adminRepository;
    }

    public async Task<Task> Handle(UpdateBannerCommand request, CancellationToken cancellationToken)
    {
        return await _adminRepository.UpdateBannerAsync(
            request.BannerId, request.Photo, request.Url,
            request.Title, request.Description, request.ValidUntil);
    }
}