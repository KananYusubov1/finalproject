﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateResource.ResourceVote
{
    public class VoteResourceCommandHandler : IRequestHandler<VoteResourceCommand,int>
    {
        private readonly IResourceRepository _resourceRepository;

        private readonly IAuthRepository _authRepository;

        public VoteResourceCommandHandler(IResourceRepository resourceRepository, IAuthRepository authRepository)
        {
            _resourceRepository = resourceRepository;
            _authRepository = authRepository;
        }

        public async Task<int> Handle(VoteResourceCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var tokenId = _authRepository.GetClaimValue("userId");

                var result = await _resourceRepository.VoteResource(request.Status,request.ResourceId,tokenId);

                return result;
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }
}