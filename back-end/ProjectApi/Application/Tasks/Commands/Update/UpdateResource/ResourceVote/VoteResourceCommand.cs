﻿using AppDomain.Enums;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Update.UpdateResource.ResourceVote
{
    public class VoteResourceCommand : IRequest<int>
    {
        public VoteStatus Status { get; set; }
        public string ResourceId { get; set; }
    }
}
