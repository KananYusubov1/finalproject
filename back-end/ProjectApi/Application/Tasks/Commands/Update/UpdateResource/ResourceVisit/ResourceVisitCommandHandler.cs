﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateResource.ResourceVisit;

public class ResourceVisitCommandHandler : IRequestHandler<ResourceVisitCommand>
{
    private readonly IResourceRepository _repository;

    public ResourceVisitCommandHandler(IResourceRepository repository)
    {
        _repository = repository;
    }

    public async Task Handle(ResourceVisitCommand request, CancellationToken cancellationToken)
    {
        await _repository.VisitedUrl(request.ResourceId);
    }
}