﻿using MediatR;

namespace Application.Tasks.Commands.Update.UpdateResource.ResourceVisit;

public class ResourceVisitCommand : IRequest
{
    public string ResourceId { get; set; }
}
