﻿using AppDomain.DTOs.Resource;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Update.UpdateResource.ResourceEdit
{
    public class EditResourceCommand : IRequest<string>
    {
        public EditResourceDTO EditResource { get; set; }
    }
}
