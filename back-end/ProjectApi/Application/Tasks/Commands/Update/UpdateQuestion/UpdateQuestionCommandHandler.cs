﻿using AppDomain.DTOs.Question;
using AppDomain.Interfaces;
using AutoMapper;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateQuestion
{
    public class UpdateQuestionCommandHandler : IRequestHandler<UpdateQuestionCommand,string>
    {
        private readonly IQuestionRepository _questionRepository;

        private readonly IMapper _mapper;

        private readonly IUserRepository _userRepository;

        public UpdateQuestionCommandHandler(IQuestionRepository questionRepository, IMapper mapper, IUserRepository userRepository)
        {
            _questionRepository = questionRepository;
            _mapper = mapper;
            _userRepository = userRepository;
        }

        public async Task<string> Handle(UpdateQuestionCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var tokenId = _userRepository.GetClaimValue("userId");

                UpdateQuestionDTO updateQuestionDTO = _mapper.Map<UpdateQuestionDTO>(request);

                var result = await _questionRepository.UpdateQuestion(updateQuestionDTO, tokenId);

                return result;
            }
            catch (Exception)
            {
                return "-1";
            }
        }
    }
}