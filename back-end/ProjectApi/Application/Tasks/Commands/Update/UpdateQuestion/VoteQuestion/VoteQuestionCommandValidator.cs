﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Update.UpdateQuestion.VoteQuestion
{
    public class VoteQuestionCommandValidator : AbstractValidator<VoteQuestionCommand>
    {
        public VoteQuestionCommandValidator()
        {
            RuleFor(x => x.QuestionId).NotNull().NotEmpty();
        }
    }
}
