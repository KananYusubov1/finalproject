﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateQuestion.SetCorrectAnswer
{
    public class SetCorrectAnswerCommandHandler : IRequestHandler<SetCorrectAnswerCommand,string>
    {
        private readonly IQuestionRepository _questionRepository;

        private readonly IUserRepository _userRepository;

        public SetCorrectAnswerCommandHandler(IQuestionRepository questionRepository, IUserRepository userRepository)
        {
            _questionRepository = questionRepository;
            _userRepository = userRepository;
        }

        public async Task<string> Handle(SetCorrectAnswerCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var tokenId = _userRepository.GetClaimValue("userId");

                var result = await _questionRepository.SetCorrectAnswer(request.QuestionId,request.AnswerId,tokenId);

                return result;
            }
            catch (Exception)
            {
                return "-1";
            }
        }
    }
}