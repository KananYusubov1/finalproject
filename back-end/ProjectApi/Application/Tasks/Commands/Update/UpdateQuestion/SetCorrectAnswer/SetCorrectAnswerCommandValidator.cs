﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Update.UpdateQuestion.SetCorrectAnswer
{
    public class SetCorrectAnswerCommandValidator : AbstractValidator<SetCorrectAnswerCommand>
    {
        public SetCorrectAnswerCommandValidator()
        {
            RuleFor(x => x.AnswerId).NotNull().NotEmpty();
            RuleFor(x =>x.QuestionId).NotNull().NotEmpty(); 
        }
    }
}
