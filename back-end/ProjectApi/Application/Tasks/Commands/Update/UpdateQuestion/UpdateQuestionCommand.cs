﻿using AppDomain.ValueObjects;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Update.UpdateQuestion
{
    public class UpdateQuestionCommand : IRequest<string>
    {
        public string Id { get; set; }
        public Header? Header { get; set; }
        public string? Body { get; set; }
    }
}
