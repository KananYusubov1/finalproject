﻿using FluentValidation;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdatePrimaryEmail;

public class UpdatePrimaryEmailCommandValidator : AbstractValidator<UpdatePrimaryEmailCommand>
{
    public UpdatePrimaryEmailCommandValidator()
    {
        RuleFor(c => c.Email).NotEmpty().NotNull().EmailAddress();
    }
}
