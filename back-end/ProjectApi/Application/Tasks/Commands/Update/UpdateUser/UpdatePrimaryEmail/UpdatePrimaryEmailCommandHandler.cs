﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdatePrimaryEmail;

public class UpdatePrimaryEmailCommandHandler : IRequestHandler<UpdatePrimaryEmailCommand, Task>
{
    private readonly IUserRepository _userRepository;

    public UpdatePrimaryEmailCommandHandler(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<Task> Handle(
        UpdatePrimaryEmailCommand request,
        CancellationToken cancellationToken
    )
    {
        return await _userRepository.UpdatePrimaryEmail(request.Email);
    }
}
