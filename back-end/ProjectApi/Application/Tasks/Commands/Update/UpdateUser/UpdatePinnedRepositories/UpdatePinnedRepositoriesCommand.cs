﻿using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdatePinnedRepositories;

public class UpdatePinnedRepositoriesCommand : IRequest<Task>
{
    public string RepositoryId { get; set; }
}