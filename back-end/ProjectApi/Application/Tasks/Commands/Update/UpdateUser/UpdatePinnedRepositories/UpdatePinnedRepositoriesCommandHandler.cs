﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdatePinnedRepositories;

public class UpdatePinnedRepositoriesCommandHandler : IRequestHandler<UpdatePinnedRepositoriesCommand, Task>
{
    private readonly IUserRepository _userRepository;

    public UpdatePinnedRepositoriesCommandHandler(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<Task> Handle(
        UpdatePinnedRepositoriesCommand request,
        CancellationToken cancellationToken
    )
    {
        return await _userRepository.UpdatePinnedRepositoriesAsync(request.RepositoryId);
    }
}