﻿using FluentValidation;

namespace Application.Tasks.Commands.Update.UpdateUser.FreezeUser;

public class FreezeUserCommandValidator : AbstractValidator<FreezeUserCommand>
{
    public FreezeUserCommandValidator()
    {
        RuleFor(x => x.UserId).NotEmpty();

        RuleFor(x => x.FrozenUntil).NotEmpty();
    }
}