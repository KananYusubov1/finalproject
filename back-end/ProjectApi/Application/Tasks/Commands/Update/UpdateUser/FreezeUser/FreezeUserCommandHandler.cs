﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.FreezeUser;

public class FreezeUserCommandHandler : IRequestHandler<FreezeUserCommand, Task>
{
    private readonly IModeratorRepository _moderatorRepository;

    public FreezeUserCommandHandler(IModeratorRepository moderatorRepository)
    {
        _moderatorRepository = moderatorRepository;
    }

    public async Task<Task> Handle(FreezeUserCommand request, CancellationToken cancellationToken)
    {
        return await _moderatorRepository.FreezeUserAsync(request.UserId, request.FrozenUntil);
    }
}