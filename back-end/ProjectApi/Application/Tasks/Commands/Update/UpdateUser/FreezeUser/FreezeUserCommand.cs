﻿using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.FreezeUser;

public class FreezeUserCommand : IRequest<Task>
{
    public string UserId { get; set; }
    public DateTime FrozenUntil { get; set; }
}