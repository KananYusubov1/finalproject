﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdateToken;

public class UpdateTokenCommandHandler : IRequestHandler<UpdateTokenCommand, string>
{
    private readonly IAuthRepository _authRepository;

    public UpdateTokenCommandHandler(IAuthRepository authRepository)
    {
        _authRepository = authRepository;
    }

    public async Task<string> Handle(
        UpdateTokenCommand request,
        CancellationToken cancellationToken
    )
    {
        return await _authRepository.UpdateTokenAsync(request.Email, request.RefreshToken);
    }
}
