﻿using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdateToken;

public class UpdateTokenCommand : IRequest<string>
{
    public string Email { get; set; }
    public string RefreshToken { get; set; }
}