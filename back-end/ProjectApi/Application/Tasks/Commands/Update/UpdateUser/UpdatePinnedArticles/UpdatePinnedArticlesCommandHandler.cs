﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdatePinnedArticles;

public class UpdatePinnedArticlesCommandHandler : IRequestHandler<UpdatePinnedArticlesCommand, Task>
{
    private readonly IUserRepository _userRepository;

    public UpdatePinnedArticlesCommandHandler(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<Task> Handle(UpdatePinnedArticlesCommand request, CancellationToken cancellationToken)
    {
        return await _userRepository.UpdatePinnedArticlesAsync(request.ArticleId);
    }
}