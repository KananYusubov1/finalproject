﻿using FluentValidation;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdatePinnedArticles;

public class UpdatePrimaryEmailCommandValidator : AbstractValidator<UpdatePinnedArticlesCommand>
{
    public UpdatePrimaryEmailCommandValidator()
    {
        RuleFor(x => x.ArticleId).NotEmpty();
    }
}