﻿using AppDomain.DTOs.Settings;
using AppDomain.Responses;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdatePersonalSettings;

public class UpdateProfileSettingsCommand : IRequest<ProfileSettingsDTO>
{
    public ProfileSettingsDTO ProfileSettings { get; set; }
}
