﻿using AppDomain.Interfaces;
using FluentValidation;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdatePersonalSettings;

public class UpdatePersonalInfoCommandValidator : AbstractValidator<UpdateProfileSettingsCommand>
{
    public UpdatePersonalInfoCommandValidator() { }
}
