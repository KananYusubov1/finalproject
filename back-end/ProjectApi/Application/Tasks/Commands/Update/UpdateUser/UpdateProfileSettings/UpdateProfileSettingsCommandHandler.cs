﻿using AppDomain.DTOs.Settings;
using AppDomain.Interfaces;
using AppDomain.Responses;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdatePersonalSettings;

public class UpdateProfileSettingsCommandHandler
    : IRequestHandler<UpdateProfileSettingsCommand, ProfileSettingsDTO>
{
    private readonly IUserRepository _userRepository;

    public UpdateProfileSettingsCommandHandler(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<ProfileSettingsDTO> Handle(
        UpdateProfileSettingsCommand request,
        CancellationToken cancellationToken
    )
    {
        try
        {
            return await _userRepository.UpdateProfileSettingsAsync(request.ProfileSettings);
        }
        catch (Exception)
        {
            throw;
        }
    }
}
