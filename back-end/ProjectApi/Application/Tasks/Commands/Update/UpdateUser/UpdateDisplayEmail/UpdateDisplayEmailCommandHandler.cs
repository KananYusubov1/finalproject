﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdateDisplayEmail;

public class UpdateDisplayEmailCommandHandler : IRequestHandler<UpdateDisplayEmailCommand, Task>
{
    private readonly IUserRepository _userRepository;

    public UpdateDisplayEmailCommandHandler(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<Task> Handle(
        UpdateDisplayEmailCommand request,
        CancellationToken cancellationToken
    )
    {
        return await _userRepository.UpdateDisplayEmailAsync(
            request.Email,
            request.UsePrimaryEmail,
            request.RemoveDisplayEmail
        );
    }
}
