﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdateRole;

public class UpdateRoleCommandHandler : IRequestHandler<UpdateRoleCommand, Task>
{
    private readonly IUserRepository _userRepository;

    public UpdateRoleCommandHandler(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<Task> Handle(UpdateRoleCommand request, CancellationToken cancellationToken)
    {
        return await _userRepository.ChangeRoleAsync(request.Role, request.UserId);
    }
}