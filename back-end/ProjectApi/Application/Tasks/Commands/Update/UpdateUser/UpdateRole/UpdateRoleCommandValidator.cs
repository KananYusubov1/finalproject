﻿using FluentValidation;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdateRole;

public class UpdateRoleCommandValidator : AbstractValidator<UpdateRoleCommand>
{
    public UpdateRoleCommandValidator()
    {
        RuleFor(x => x.UserId).NotEmpty();

        RuleFor(x => x.Role).NotEmpty().IsInEnum();
    }
}