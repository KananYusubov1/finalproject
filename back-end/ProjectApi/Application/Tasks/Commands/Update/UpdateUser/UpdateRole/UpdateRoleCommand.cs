﻿using AppDomain.Enums;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdateRole;

public class UpdateRoleCommand : IRequest<Task>
{
    public string UserId { get; set; }
    public UserRole Role { get; set; }
}