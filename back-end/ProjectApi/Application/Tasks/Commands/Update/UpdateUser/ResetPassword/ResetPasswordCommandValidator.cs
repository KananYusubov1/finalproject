﻿using FluentValidation;

namespace Application.Tasks.Commands.Update.UpdateUser.ResetPassword;

public class ResetPasswordCommandValidator : AbstractValidator<ResetPasswordCommand>
{
    public ResetPasswordCommandValidator()
    {
        RuleFor(u => u.NewPassword)
            .NotEmpty()
            .NotNull()
            .MinimumLength(8) // Minimum length requirement
            .MaximumLength(16) // Maximum length requirement
            .Matches("[A-Z]") // At least one uppercase letter
            .Matches("[a-z]") // At least one lowercase letter
            .Matches("[0-9]") // At least one digit
            .Matches("[^a-zA-Z0-9]"); // At least one special character
    }
}
