﻿using AppDomain.Interfaces;
using Application.Tasks.Commands.Update.UpdateUser.UpdatePassword;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.ResetPassword;

public class ResetPasswordCommandHandler : IRequestHandler<ResetPasswordCommand, Task>
{
    private readonly IUserRepository _userRepository;

    public ResetPasswordCommandHandler(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<Task> Handle(
        ResetPasswordCommand request,
        CancellationToken cancellationToken
    )
    {
        return await _userRepository.ResetPasswordAsync(request.Email, request.NewPassword);
    }
}
