﻿using AppDomain.Responses;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdateCustomization;

public class UpdateCustomizationSettingsCommand : IRequest<CustomizationSettings>
{
    public CustomizationSettings CustomizationSettings { get; set; }
}
