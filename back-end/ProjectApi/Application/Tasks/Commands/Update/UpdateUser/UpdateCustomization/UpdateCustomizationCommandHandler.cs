﻿using AppDomain.Interfaces;
using AppDomain.Responses;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdateCustomization;

public class UpdateCustomizationCommandHandler
    : IRequestHandler<UpdateCustomizationSettingsCommand, CustomizationSettings>
{
    private readonly IUserRepository _userRepository;

    public UpdateCustomizationCommandHandler(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<CustomizationSettings> Handle(
        UpdateCustomizationSettingsCommand request,
        CancellationToken cancellationToken
    )
    {
        return await _userRepository.UpdateCustomizationAsync(request.CustomizationSettings);
    }
}
