﻿using FluentValidation;

namespace Application.Tasks.Commands.Update.UpdateUser.RestoreUser;

public class RestoreUserCommandValidator : AbstractValidator<RestoreUserCommand>
{
    public RestoreUserCommandValidator()
    {
        RuleFor(u => u.Id)
            .NotEmpty();
    }
}