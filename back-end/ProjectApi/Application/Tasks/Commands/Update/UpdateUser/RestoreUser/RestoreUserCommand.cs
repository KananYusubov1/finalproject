﻿using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.RestoreUser;

public class RestoreUserCommand : IRequest<Task>
{
    public string Id { get; set; }
}