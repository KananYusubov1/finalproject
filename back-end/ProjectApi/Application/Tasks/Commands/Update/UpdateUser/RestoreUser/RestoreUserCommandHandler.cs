﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.RestoreUser;

public class RestoreUserCommandHandler : IRequestHandler<RestoreUserCommand, Task>
{
    private readonly IUserRepository _userRepository;

    public RestoreUserCommandHandler(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<Task> Handle(RestoreUserCommand request, CancellationToken cancellationToken)
    {
        return await _userRepository.RestoreUserRelationsAsync(request.Id);
    }
}