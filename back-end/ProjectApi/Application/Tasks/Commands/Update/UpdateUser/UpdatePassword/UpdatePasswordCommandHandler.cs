﻿using AppDomain.DTO;
using AppDomain.Entities.UserRelated;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdatePassword;

public class UpdatePasswordCommandHandler : IRequestHandler<UpdatePasswordCommand, Task>
{
    private readonly IUserRepository _userRepository;

    public UpdatePasswordCommandHandler(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<Task> Handle(
        UpdatePasswordCommand request,
        CancellationToken cancellationToken
    )
    {
        return await _userRepository.UpdatePasswordAsync(
            request.CurrentPassword,
            request.NewPassword
        );
    }
}
