﻿using FluentValidation;

namespace Application.Tasks.Commands.Update.UpdateUser.UnfreezeUser;

public class UnfreezeUserCommandValidator : AbstractValidator<UnfreezeUserCommand>
{
    public UnfreezeUserCommandValidator()
    {
        RuleFor(x => x.UserId).NotEmpty();
    }
}