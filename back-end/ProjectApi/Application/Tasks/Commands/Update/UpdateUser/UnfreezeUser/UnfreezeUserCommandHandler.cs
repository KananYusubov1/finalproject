﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.UnfreezeUser;

public class UnfreezeUserCommandHandler : IRequestHandler<UnfreezeUserCommand, Task>
{
    private readonly IModeratorRepository _moderatorRepository;

    public UnfreezeUserCommandHandler(IModeratorRepository moderatorRepository)
    {
        _moderatorRepository = moderatorRepository;
    }

    public async Task<Task> Handle(UnfreezeUserCommand request, CancellationToken cancellationToken)
    {
        return await _moderatorRepository.UnfreezeUserAsync(request.UserId);
    }
}