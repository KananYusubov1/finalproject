﻿using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.UnfreezeUser;

public class UnfreezeUserCommand : IRequest<Task>
{
    public string UserId { get; set; }
}