﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateArticleFlag
{
    public class IncrementArticleFlagListUseCountCommandHandler : IRequestHandler<IncrementArticleFlagListUseCountCommand,int>
    {
        private readonly IArticleFlagRepository _articleFlagRepository;

        public IncrementArticleFlagListUseCountCommandHandler(IArticleFlagRepository articleFlagRepository)
        {
            _articleFlagRepository = articleFlagRepository;
        }

        public async Task<int> Handle(IncrementArticleFlagListUseCountCommand request, CancellationToken cancellationToken)
        {
            var result = await _articleFlagRepository.IncrementUseCounts(request.ArticleFlagIdList);

            return result;
        }
    }
}