﻿using AppDomain.DTOs.ArticleFlag;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateArticleFlag;

public class UpdateArticleFlagCommand : IRequest<string>
{
    public UpdateArticleFlagDTO UpdateTagDTO { get; set; }
}
