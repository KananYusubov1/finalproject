﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateArticleFlag
{
    public class DecrementArticleFlagListUseCountCommandHandler : IRequestHandler<DecrementArticleFlagListUseCountCommand,int>
    {
        private readonly IArticleFlagRepository _articleFlagRepository;

        public DecrementArticleFlagListUseCountCommandHandler(IArticleFlagRepository articleFlagRepository)
        {
            _articleFlagRepository = articleFlagRepository;
        }

        public Task<int> Handle(DecrementArticleFlagListUseCountCommand request, CancellationToken cancellationToken)
        {
            var result = _articleFlagRepository.DecrementUseCounts(request.ArticleFlagIdList);

            return result;
        }
    }
}