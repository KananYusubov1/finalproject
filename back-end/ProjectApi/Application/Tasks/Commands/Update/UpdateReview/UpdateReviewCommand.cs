﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Update.UpdateReview
{
    public class UpdateReviewCommand : IRequest
    {
        public string ContentId { get; set; }
        public string? Body { get; set; }
        public int? Rank { get; set; }
    }
}
