﻿using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Update.UpdateReview
{
    public class UpdateReviewCommandHandler : IRequestHandler<UpdateReviewCommand>
    {
        private readonly IReviewRepository _repository;

        public UpdateReviewCommandHandler(IReviewRepository repository)
        {
            _repository = repository;
        }

        public Task Handle(UpdateReviewCommand request, CancellationToken cancellationToken)
        {
            return _repository.UpdateReview(request.ContentId, request.Body, request.Rank);
        }
    }
}
