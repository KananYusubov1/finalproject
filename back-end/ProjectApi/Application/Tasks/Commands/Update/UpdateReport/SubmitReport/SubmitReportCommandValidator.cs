﻿using FluentValidation;

namespace Application.Tasks.Commands.Update.SubmitReport;

public class SubmitReportCommandValidator : AbstractValidator<SubmitReportCommand>
{
    public SubmitReportCommandValidator()
    {
        RuleFor(r => r.ReportId)
            .NotEmpty();

        RuleFor(r => r.ReportStatus)
            .NotEmpty()
            .IsInEnum();

        RuleFor(r => r.ModeratorMessage)
            .NotEmpty();
    }
}