﻿using AppDomain.Entities.AdminRelated;
using AppDomain.Enums;
using MediatR;

namespace Application.Tasks.Commands.Update.SubmitReport;

public class SubmitReportCommand : IRequest<Report>
{
    public string ReportId { get; set; }
    public ReportStatus ReportStatus { get; set; }
    public string ModeratorMessage { get; set; }
}