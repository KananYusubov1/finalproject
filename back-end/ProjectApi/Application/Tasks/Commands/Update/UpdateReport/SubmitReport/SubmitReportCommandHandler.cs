﻿using AppDomain.Entities.AdminRelated;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.SubmitReport;

public class SubmitReportCommandHandler : IRequestHandler<SubmitReportCommand, Report>
{
    private readonly IReportRepository _reportRepository;

    public SubmitReportCommandHandler(IReportRepository reportRepository)
    {
        _reportRepository = reportRepository;
    }

    public async Task<Report> Handle(SubmitReportCommand request, CancellationToken cancellationToken)
    {
        return await _reportRepository.SubmitReportAsync(request.ReportId, request.ReportStatus, request.ModeratorMessage);
    }
}