﻿using AppDomain.DTOs.Category;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateCategory;

public class UpdateCategoryCommand : IRequest<string>
{
    public UpdateCategoryDTO UpdateTagDTO { get; set; }
}
