﻿using AppDomain.Entities.TagBaseRelated;
using AppDomain.Interfaces;
using AutoMapper;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateCategory
{
    public class UpdateCategoryCommandHandler : IRequestHandler<UpdateCategoryCommand, string>
    {
        private readonly ICategoryRepository _repository;

        private readonly IMapper _mapper;

        public UpdateCategoryCommandHandler(ICategoryRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<string> Handle(UpdateCategoryCommand request, CancellationToken cancellationToken)
        {
            try
            {
                Category category = _mapper.Map<Category>(request.UpdateTagDTO);

                var result = await _repository.UpdateCategory(category);

                return result;
            }
            catch (Exception)
            {
                return "-1";
            }
        }
    }
}