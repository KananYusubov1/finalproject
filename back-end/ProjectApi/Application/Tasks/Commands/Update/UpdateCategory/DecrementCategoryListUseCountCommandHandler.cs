﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateCategory
{
    public class DecrementCategoryListUseCountCommandHandler : IRequestHandler<DecrementCategoryListUseCountCommand, int>
    {
        private readonly ICategoryRepository _categoryRepository;

        public DecrementCategoryListUseCountCommandHandler(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public async Task<int> Handle(DecrementCategoryListUseCountCommand request, CancellationToken cancellationToken)
        {
            var result = await _categoryRepository.DecrementUseCounts(request.CategoryIdList);

            return result;
        }
    }
}