﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateCategory.FollowCategory
{
    public class FollowCategoryCommandHandler : IRequestHandler<FollowCategoryCommand,int>
    {
        private readonly ICategoryRepository _categoryRepository;

        private readonly IUserRepository _userRepository;

        public FollowCategoryCommandHandler(ICategoryRepository categoryRepository, IUserRepository userRepository)
        {
            _categoryRepository = categoryRepository;
            _userRepository = userRepository;
        }

        public async Task<int> Handle(FollowCategoryCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var tokenId = _userRepository.GetClaimValue("userId");

                var result = await _categoryRepository.Follow(request.CategoryId, tokenId);

                return result;
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }
}