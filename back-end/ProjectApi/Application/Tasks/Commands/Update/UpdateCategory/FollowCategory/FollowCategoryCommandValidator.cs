﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Update.UpdateCategory.FollowCategory
{
    public class FollowCategoryCommandValidator : AbstractValidator<FollowCategoryCommand>
    {
        public FollowCategoryCommandValidator()
        {
            RuleFor(x => x.CategoryId).NotNull().NotEmpty();
        }
    }
}
