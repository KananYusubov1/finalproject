﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateResourceFlag
{
    public class DecrementResourceFlagListUseCountCommandHandler : IRequestHandler<DecrementResourceFlagListUseCountCommand,int>
    {
        private readonly IResourceFlagRepository _resourceFlagRepository;

        public DecrementResourceFlagListUseCountCommandHandler(IResourceFlagRepository resourceFlagRepository)
        {
            _resourceFlagRepository = resourceFlagRepository;
        }

        public async Task<int> Handle(DecrementResourceFlagListUseCountCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var result = await _resourceFlagRepository.DecrementUseCounts(request.ResourceFlagIdList);

                return result;
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }
}