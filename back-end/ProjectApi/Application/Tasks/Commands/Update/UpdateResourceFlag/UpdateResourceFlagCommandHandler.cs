﻿using AppDomain.Entities.TagBaseRelated;
using AppDomain.Interfaces;
using AutoMapper;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateResourceFlag
{
    public class UpdateResourceFlagCommandHandler : IRequestHandler<UpdateResourceFlagCommand,string>
    {
        private readonly IResourceFlagRepository _repository;

        private readonly IMapper _mapper;

        public UpdateResourceFlagCommandHandler(IResourceFlagRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<string> Handle(UpdateResourceFlagCommand request, CancellationToken cancellationToken)
        {
            try
            {
                ResourceFlag resourceFlag = _mapper.Map<ResourceFlag>(request.UpdateTagDTO);

                var result = await _repository.UpdateResourceFlag(resourceFlag);

                return result;
            }
            catch (Exception)
            {
                return "-1";
            }
        }
    }
}