﻿using AppDomain.DTOs.ResourceFlag;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateResourceFlag;

public class UpdateResourceFlagCommand : IRequest<string>
{
    public UpdateResourceFlagDTO UpdateTagDTO { get; set; }
}
