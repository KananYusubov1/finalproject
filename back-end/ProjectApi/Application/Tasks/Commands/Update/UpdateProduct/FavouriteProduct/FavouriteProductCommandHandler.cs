﻿using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Update.UpdateProduct.FavouriteProduct
{
    public class FavouriteProductCommandHandler : IRequestHandler<FavouriteProductCommand>
    {
        private readonly IProductRepository _repository;

        public FavouriteProductCommandHandler(IProductRepository repository)
        {
            _repository = repository;
        }

        public async Task Handle(FavouriteProductCommand request, CancellationToken cancellationToken)
        {
            await _repository.FavouritedProduct(request.ProductId);
        }
    }
}
