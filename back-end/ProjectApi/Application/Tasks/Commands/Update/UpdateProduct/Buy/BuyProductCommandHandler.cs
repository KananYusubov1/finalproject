﻿using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Update.UpdateProduct.Buy
{
    public class BuyProductCommandHandler : IRequestHandler<BuyProductCommand>
    {
        private readonly IProductRepository _productRepository;

        public BuyProductCommandHandler(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public Task Handle(BuyProductCommand request, CancellationToken cancellationToken)
        {
            return _productRepository.BuyProduct(request.ProductId, request.Quantity);
        }
    }
}
