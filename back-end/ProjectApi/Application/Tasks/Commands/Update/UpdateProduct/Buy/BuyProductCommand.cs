﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Update.UpdateProduct.Buy
{
    public class BuyProductCommand : IRequest
    {
        public string ProductId { get; set; }
        public int Quantity { get; set; }
    }
}
