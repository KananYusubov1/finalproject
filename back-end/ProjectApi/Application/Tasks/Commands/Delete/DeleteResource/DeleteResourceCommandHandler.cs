﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Delete.DeleteResource
{
    public class DeleteResourceCommandHandler : IRequestHandler<DeleteResourceCommand, string>
    {
        private readonly IResourceRepository _resourceRepository;

        public DeleteResourceCommandHandler(IResourceRepository resourceRepository)
        {
            _resourceRepository = resourceRepository;
        }

        public async Task<string> Handle(
            DeleteResourceCommand request,
            CancellationToken cancellationToken
        )
        {
            try
            {
                var result = await _resourceRepository.DeleteResource(request.ResourceId);

                return result;
            }
            catch (Exception)
            {
                return "0";
            }
        }
    }
}