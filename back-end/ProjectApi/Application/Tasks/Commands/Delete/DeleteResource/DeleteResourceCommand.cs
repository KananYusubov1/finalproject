﻿using MediatR;

namespace Application.Tasks.Commands.Delete.DeleteResource
{
    public class DeleteResourceCommand : IRequest<string>
    {
        public string ResourceId { get; set; }
    }
}