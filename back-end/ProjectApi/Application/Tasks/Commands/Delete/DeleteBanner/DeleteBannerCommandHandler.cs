﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Delete.DeleteBanner;

public class DeleteBannerCommandHandler : IRequestHandler<DeleteBannerCommand, Task>
{
    private readonly IAdminRepository _adminRepository;

    public DeleteBannerCommandHandler(IAdminRepository adminRepository)
    {
        _adminRepository = adminRepository;
    }

    public async Task<Task> Handle(DeleteBannerCommand request, CancellationToken cancellationToken)
    {
        return await _adminRepository.DeleteBannerAsync(request.BannerId);
    }
}