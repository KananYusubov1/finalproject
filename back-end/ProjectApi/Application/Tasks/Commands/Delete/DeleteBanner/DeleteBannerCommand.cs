﻿using MediatR;

namespace Application.Tasks.Commands.Delete.DeleteBanner;

public class DeleteBannerCommand : IRequest<Task>
{
    public string BannerId { get; set; }
}