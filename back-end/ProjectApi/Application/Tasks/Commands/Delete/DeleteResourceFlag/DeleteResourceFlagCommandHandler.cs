﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Delete.DeleteResourceFlag
{
    public class DeleteResourceFlagCommandHandler : IRequestHandler<DeleteResourceFlagCommand,string>
    {
        private readonly IResourceFlagRepository _resourceFlagRepository;

        public DeleteResourceFlagCommandHandler(IResourceFlagRepository resourceFlagRepository)
        {
            _resourceFlagRepository = resourceFlagRepository;
        }

        public async Task<string> Handle(DeleteResourceFlagCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var result = await _resourceFlagRepository.DeleteResourceFlag(request.ResourceFlagId);

                return result;
            }
            catch (Exception)
            {
                return "-1";
            }
        }
    }
}