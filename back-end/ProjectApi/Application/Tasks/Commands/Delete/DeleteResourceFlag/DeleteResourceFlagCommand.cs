﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Delete.DeleteResourceFlag
{
    public class DeleteResourceFlagCommand : IRequest<string>
    {
        public string ResourceFlagId { get; set; }
    }
}
