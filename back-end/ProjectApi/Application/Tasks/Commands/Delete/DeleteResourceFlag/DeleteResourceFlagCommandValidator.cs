﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Delete.DeleteResourceFlag
{
    public class DeleteResourceFlagCommandValidator : AbstractValidator<DeleteResourceFlagCommand>
    {
        public DeleteResourceFlagCommandValidator()
        {
            RuleFor(x => x.ResourceFlagId).NotNull().NotEmpty();
        }
    }
}
