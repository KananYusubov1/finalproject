﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Delete.DeleteComment;

public class DeleteCommentCommandHandler : IRequestHandler<DeleteCommentCommand, Task>
{
    private readonly ICommentRepository _commentRepository;

    public DeleteCommentCommandHandler(ICommentRepository commentRepository)
    {
        _commentRepository = commentRepository;
    }

    public async Task<Task> Handle(DeleteCommentCommand request,CancellationToken cancellationToken)
    {
        return await _commentRepository.DeleteCommentAsync(request.CommentId);
    }
}