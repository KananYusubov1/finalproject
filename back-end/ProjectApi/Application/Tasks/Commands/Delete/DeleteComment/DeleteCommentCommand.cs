﻿using MediatR;

namespace Application.Tasks.Commands.Delete.DeleteComment;

public class DeleteCommentCommand : IRequest<Task>
{
    public string CommentId { get; set; }

}