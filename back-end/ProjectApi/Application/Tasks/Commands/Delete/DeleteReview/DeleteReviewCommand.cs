﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Delete.DeleteReview
{
    public class DeleteReviewCommand : IRequest
    {
        public string ReviewId { get; set; }
    }
}
