﻿using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Delete.DeleteArticle
{
    public class DeleteArticleCommandHandler : IRequestHandler<DeleteArticleCommand,int>
    {
        private readonly IArticleRepository _articleRepository;

        private readonly IUserRepository _userRepository;

        public DeleteArticleCommandHandler(IArticleRepository articleRepository, IUserRepository userRepository)
        {
            _articleRepository = articleRepository;
            _userRepository = userRepository;
        }

        public async Task<int> Handle(DeleteArticleCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var user = await _userRepository.GetCurrentUser();

                if (user.IsFrozen)
                    return -2;

                var result = await _articleRepository.DeleteArticle(request.ArticleId,user.Id);

                return result;
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }
}
