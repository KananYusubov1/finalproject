﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Delete.DeleteArticle
{
    public class DeleteArticleCommand : IRequest<int>
    {
        public string ArticleId { get; set; }
    }
}
