﻿using AppDomain.Enums;
using MediatR;

namespace Application.Tasks.Commands.Delete.DeleteContent;

public class FreezeContentCommand : IRequest<Task>
{
    public string ContentId { get; set; }
    public ContentType ContentType { get; set; }
    public string ResponsibleModeratorId { get; set; }
}