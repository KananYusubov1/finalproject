﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Delete.DeleteContent;

public class DeleteContentCommandHandler : IRequestHandler<FreezeContentCommand, Task>
{
    private readonly IModeratorRepository _moderatorRepository;

    public DeleteContentCommandHandler(IModeratorRepository moderatorRepository)
    {
        _moderatorRepository = moderatorRepository;
    }

    public async Task<Task> Handle(FreezeContentCommand request, CancellationToken cancellationToken)
    {
        return await _moderatorRepository.FreezeContentAsync(request.ContentId, request.ContentType, request.ResponsibleModeratorId);
    }
}