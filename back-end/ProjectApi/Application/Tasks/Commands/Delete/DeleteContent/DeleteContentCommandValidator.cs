﻿using FluentValidation;

namespace Application.Tasks.Commands.Delete.DeleteContent;

public class DeleteContentCommandValidator : AbstractValidator<FreezeContentCommand>
{
    public DeleteContentCommandValidator()
    {
        RuleFor(x => x.ContentId).NotEmpty();

        RuleFor(x => x.ContentType).IsInEnum().NotEmpty();
    }
}