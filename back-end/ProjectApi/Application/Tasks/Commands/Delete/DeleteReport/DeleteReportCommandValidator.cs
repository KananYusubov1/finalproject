﻿using FluentValidation;

namespace Application.Tasks.Commands.Delete.DeleteReport;

public class DeleteReportCommandValidator : AbstractValidator<DeleteReportCommand>
{
    public DeleteReportCommandValidator()
    {
        RuleFor(x => x.ReportId)
            .NotNull()
            .NotEmpty();
    }
}