﻿using MediatR;

namespace Application.Tasks.Commands.Delete.DeleteReport;

public class DeleteReportCommand : IRequest<Task>
{
    public string ReportId { get; set; }
}