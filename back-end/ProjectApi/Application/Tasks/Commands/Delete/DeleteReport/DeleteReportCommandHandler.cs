﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Delete.DeleteReport;

public class DeleteReportCommandHandler : IRequestHandler<DeleteReportCommand, Task>
{
    private readonly IReportRepository _reportRepository;

    public DeleteReportCommandHandler(IReportRepository reportRepository)
    {
        _reportRepository = reportRepository;
    }

    public async Task<Task> Handle(DeleteReportCommand request, CancellationToken cancellationToken)
    {
        return await _reportRepository.DeleteReportAsync(request.ReportId);
    }
}