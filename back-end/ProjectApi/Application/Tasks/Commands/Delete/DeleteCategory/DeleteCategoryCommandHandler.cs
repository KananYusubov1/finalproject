﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Delete.DeleteCategory
{
    public class DeleteCategoryCommandHandler : IRequestHandler<DeleteCategoryCommand, string>
    {
        private readonly ICategoryRepository _repository;

        public DeleteCategoryCommandHandler(ICategoryRepository repository)
        {
            _repository = repository;
        }

        public async Task<string> Handle(DeleteCategoryCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var result = await _repository.DeleteCategory(request.CategoryId);

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}