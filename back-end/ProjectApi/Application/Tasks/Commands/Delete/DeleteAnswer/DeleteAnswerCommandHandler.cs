﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Delete.DeleteAnswer
{
    public class DeleteAnswerCommandHandler : IRequestHandler<DeleteAnswerCommand,int>
    {
        private readonly IAnswerRepository _answerRepository;

        public DeleteAnswerCommandHandler(IAnswerRepository answerRepository)
        {
            _answerRepository = answerRepository;
        }

        public async Task<int> Handle(DeleteAnswerCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var result = await _answerRepository.DeleteAnswer(request.AnswerId);

                return result;
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }
}