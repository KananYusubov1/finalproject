﻿using MediatR;

namespace Application.Tasks.Commands.Delete.UserDeletes.DeleteEmailVerification;

public class DeleteEmailVerificationCommand : IRequest<bool>
{
    public string Email { get; set; }
    public string OTP { get; set; }
}