﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Delete.UserDeletes.DeleteEmailVerification
{
    public class DeleteEmailVerificationCommandHandler : IRequestHandler<DeleteEmailVerificationCommand, bool>
    {
        private readonly IAuthRepository _authRepository;

        public DeleteEmailVerificationCommandHandler(IAuthRepository authRepository)
        {
            _authRepository = authRepository;
        }

        public async Task<bool> Handle(DeleteEmailVerificationCommand request, CancellationToken cancellationToken)
        {
            var result = await _authRepository.VerifyEmailAsync(request.Email, request.OTP);

            return result;
        }
    }
}