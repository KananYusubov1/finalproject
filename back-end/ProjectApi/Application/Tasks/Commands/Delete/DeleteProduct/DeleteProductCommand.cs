﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Delete.DeleteProduct;

public class DeleteProductCommand : IRequest
{
    public string ProductId { get; set; }
}
