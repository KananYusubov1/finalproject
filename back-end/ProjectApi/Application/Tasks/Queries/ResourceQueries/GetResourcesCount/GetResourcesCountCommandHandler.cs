﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.ResourceQueries.GetResourcesCount;

public class GetResourcesCountCommandHandler : IRequestHandler<GetResourcesCountCommand, int>
{
    private readonly IResourceRepository _resourceRepository;

    public GetResourcesCountCommandHandler(IResourceRepository resourceRepository)
    {
        _resourceRepository = resourceRepository;
    }

    public Task<int> Handle(GetResourcesCountCommand request, CancellationToken cancellationToken)
    {
        return _resourceRepository.GetResourcesCount();
    }
}
