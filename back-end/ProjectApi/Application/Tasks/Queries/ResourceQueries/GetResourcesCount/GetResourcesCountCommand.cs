﻿using MediatR;

namespace Application.Tasks.Queries.ResourceQueries.GetResourcesCount;

public class GetResourcesCountCommand : IRequest<int> { }
