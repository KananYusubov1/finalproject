﻿using AppDomain.DTOs.Resource;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.ArticleQueries.GetLastResources;

public class GetLastResourcesQueryHandler : IRequestHandler<GetLastResourcesQuery, IEnumerable<GetLastResourcesDTO>>
{
    private readonly IResourceRepository _resourceRepository;

    public GetLastResourcesQueryHandler(IResourceRepository resourceRepository)
    {
        _resourceRepository = resourceRepository;
    }

    public async Task<IEnumerable<GetLastResourcesDTO>> Handle(GetLastResourcesQuery request, CancellationToken cancellationToken)
    {
        return await _resourceRepository.GetLastResourcesAsync();
    }
}