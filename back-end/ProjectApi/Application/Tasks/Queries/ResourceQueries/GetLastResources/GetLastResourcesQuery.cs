﻿using AppDomain.DTOs.Resource;
using MediatR;

namespace Application.Tasks.Queries.ArticleQueries.GetLastResources;

public class GetLastResourcesQuery : IRequest<IEnumerable<GetLastResourcesDTO>>
{

}