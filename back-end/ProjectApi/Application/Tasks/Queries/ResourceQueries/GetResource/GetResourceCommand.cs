﻿using AppDomain.DTOs.Resource;
using MediatR;

namespace Application.Tasks.Queries.ResourceQueries.GetResource;

public class GetResourceCommand : IRequest<ResourceResponse>
{
    public string ResourceId { get; set; }
}
