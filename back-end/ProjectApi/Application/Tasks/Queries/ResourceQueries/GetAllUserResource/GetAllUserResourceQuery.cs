﻿using AppDomain.DTOs.Resource;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.ResourceQueries.GetAllUserResource
{
    public class GetAllUserResourceQuery : IRequest<List<ResourceResponse>>
    {

    }
}
