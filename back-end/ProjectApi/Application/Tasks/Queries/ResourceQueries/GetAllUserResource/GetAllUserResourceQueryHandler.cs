﻿using AppDomain.DTOs.Resource;
using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.ResourceQueries.GetAllUserResource
{
    public class GetAllUserResourceQueryHandler : IRequestHandler<GetAllUserResourceQuery, List<ResourceResponse>>
    {
        private readonly IResourceRepository _resourceRepository;

        public GetAllUserResourceQueryHandler(IResourceRepository resourceRepository)
        {
            _resourceRepository = resourceRepository;
        }

        public async Task<List<ResourceResponse>> Handle(GetAllUserResourceQuery request, CancellationToken cancellationToken)
        {
            return await _resourceRepository.GetAllUserResources();
        }
    }
}
