﻿using AppDomain.DTOs.Pagination;
using AppDomain.DTOs.Resource;
using AppDomain.Enums;
using MediatR;

namespace Application.Tasks.Queries.ResourceQueries.GetAllResources;

public class GetAllResourcesQuery : IRequest<PaginatedListDto<ResourceResponse>>
{
    public string? SearchQuery { get; set; }

    public string? CategoryId { get; set; }

    public string? FlagId { get; set; }

    public SortOptions Sort { get; set; } = SortOptions.Relevant;

    public int Page { get; set; } = 1;
}