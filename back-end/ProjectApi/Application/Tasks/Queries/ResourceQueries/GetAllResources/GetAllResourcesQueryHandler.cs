﻿using AppDomain.DTOs.Pagination;
using AppDomain.DTOs.Resource;
using AppDomain.Interfaces;
using Application.Tasks.Queries.ResourceQueries.GetSavedAllResources;
using MediatR;

namespace Application.Tasks.Queries.ResourceQueries.GetAllResources;

public class GetAllResourcesQueryHandler
    : IRequestHandler<GetAllSavedResourcesQuery, PaginatedListDto<ResourceResponse>>
{
    private readonly IResourceRepository _repository;

    public GetAllResourcesQueryHandler(IResourceRepository repository)
    {
        _repository = repository;
    }

    public async Task<PaginatedListDto<ResourceResponse>> Handle(
        GetAllSavedResourcesQuery request,
        CancellationToken cancellationToken
    )
    {
        return await _repository.GetAllSavedResources(
            request.SearchQuery,
            request.CategoryId,
            request.FlagId,
            request.Sort,
            request.Page
        );
    }
}
