﻿using AppDomain.DTOs.Pagination;
using AppDomain.DTOs.Resource;
using AppDomain.Enums;
using AppDomain.Interfaces;
using Application.Tasks.Queries.ResourceQueries.GetAllResources;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.ResourceQueries.GetSavedAllResources;

public class GetAllSavedResourcesQueryHandler
    : IRequestHandler<GetAllResourcesQuery, PaginatedListDto<ResourceResponse>>
{
    private readonly IResourceRepository _repository;

    public GetAllSavedResourcesQueryHandler(IResourceRepository repository)
    {
        _repository = repository;
    }

    public async Task<PaginatedListDto<ResourceResponse>> Handle(
        GetAllResourcesQuery request,
        CancellationToken cancellationToken
    )
    {
        return await _repository.GetAllResources(
            request.SearchQuery,
            request.CategoryId,
            request.FlagId,
            request.Sort,
            request.Page
        );
    }
}
