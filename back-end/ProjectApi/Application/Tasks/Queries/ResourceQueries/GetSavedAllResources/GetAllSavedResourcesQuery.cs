﻿using AppDomain.DTOs.Pagination;
using AppDomain.DTOs.Resource;
using AppDomain.Enums;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.ResourceQueries.GetSavedAllResources;

public class GetAllSavedResourcesQuery : IRequest<PaginatedListDto<ResourceResponse>>
{
    public string? SearchQuery { get; set; }

    public string? CategoryId { get; set; }

    public string? FlagId { get; set; }

    public SortOptions Sort { get; set; } = SortOptions.Relevant;

    public int Page { get; set; } = 1;
}
