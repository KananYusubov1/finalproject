﻿using AppDomain.DTO;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.GetAllComments;

public class GetAllCommentsQueryHandler : IRequestHandler<GetAllCommentsQuery, IEnumerable<CommentDTO>>
{
    private readonly ICommentRepository _commentRepository;

    public GetAllCommentsQueryHandler(ICommentRepository commentRepository)
    {
        _commentRepository = commentRepository;
    }

    public async Task<IEnumerable<CommentDTO>> Handle(GetAllCommentsQuery request, CancellationToken cancellationToken)
    {
        return await _commentRepository.GetAllCommentsAsync(request.ContentId);
    }
}