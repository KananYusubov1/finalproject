﻿using AppDomain.DTO;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.GetAllComments;

public class GetAllCommentsQuery : IRequest<IEnumerable<CommentDTO>>
{
    public string ContentId { get; set; }
}