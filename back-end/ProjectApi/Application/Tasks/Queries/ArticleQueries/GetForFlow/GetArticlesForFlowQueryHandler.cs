﻿using AppDomain.DTOs.Article;
using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.ArticleQueries.GetForFlow
{
    public class GetArticlesForFlowQueryHandler : IRequestHandler<GetArticlesForFlowQuery,List<GetAllArticleDTO>>
    {
        private readonly IArticleRepository _articleRepository;

        public GetArticlesForFlowQueryHandler(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }

        public async Task<List<GetAllArticleDTO>> Handle(GetArticlesForFlowQuery request, CancellationToken cancellationToken)
        {
            var data = await _articleRepository.GetFlow();

            return data;
        }
    }
}
