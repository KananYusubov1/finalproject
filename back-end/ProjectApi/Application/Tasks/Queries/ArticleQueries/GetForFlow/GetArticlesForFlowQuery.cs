﻿using AppDomain.DTOs.Article;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.ArticleQueries.GetForFlow
{
    public class GetArticlesForFlowQuery : IRequest<List<GetAllArticleDTO>>
    {

    }
}
