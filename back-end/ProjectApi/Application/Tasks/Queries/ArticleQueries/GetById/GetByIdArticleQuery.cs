﻿using AppDomain.DTOs.Article;
using MediatR;

namespace Application.Tasks.Queries.ArticleQueries.GetById;

public class GetByIdArticleQuery : IRequest<GetByIdArticleDTO>
{
    public string ArticleId { get; set; }
}