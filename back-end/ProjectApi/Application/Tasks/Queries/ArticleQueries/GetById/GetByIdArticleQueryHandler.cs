﻿using AppDomain.DTOs.Article;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.ArticleQueries.GetById;

public class GetByIdArticleQueryHandler : IRequestHandler<GetByIdArticleQuery,GetByIdArticleDTO>
{
    private readonly IArticleRepository _articleRepository;

    private readonly IUserRepository _userRepository;

    public GetByIdArticleQueryHandler(IArticleRepository articleRepository, IUserRepository userRepository)
    {
        _articleRepository = articleRepository;
        _userRepository = userRepository;
    }

    public async Task<GetByIdArticleDTO> Handle(GetByIdArticleQuery request, CancellationToken cancellationToken)
    {
        try
        {
            var userId = _userRepository.GetClaimValue("userId");

            var result = await _articleRepository.GetByIdArticle(request.ArticleId, userId);

            return result;
        }
        catch (Exception)
        {
            return null;
        }
    }
}