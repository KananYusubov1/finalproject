﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.ArticleQueries.GetById
{
    public class GetByIdArticleQueryValidator : AbstractValidator<GetByIdArticleQuery>
    {
        public GetByIdArticleQueryValidator()
        {
            RuleFor(x => x.ArticleId).NotNull().NotEmpty(); 
        }
    }
}
