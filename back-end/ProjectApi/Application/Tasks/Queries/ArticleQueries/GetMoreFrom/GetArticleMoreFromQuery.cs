﻿using AppDomain.DTOs.Article;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.ArticleQueries.GetMoreFrom
{
    public class GetArticleMoreFromQuery : IRequest<List<ArticleMoreFromDTO>>
    {
        public string UserId { get; set; }
    }
}
