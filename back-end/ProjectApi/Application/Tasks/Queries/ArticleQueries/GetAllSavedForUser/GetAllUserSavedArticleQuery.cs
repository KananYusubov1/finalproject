﻿using AppDomain.DTOs.Article;
using AppDomain.DTOs.Pagination;
using AppDomain.Enums;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.ArticleQueries.GetAllSavedForUser
{
    public class GetAllUserSavedArticleQuery : IRequest<PaginatedListDto<GetAllArticleDTO>>
    {
        public string? SearchQuery { get; set; }
        public List<string>? CategoryIdList { get; set; }
        public string? FlagId { get; set; }
        public SortOptions Sort { get; set; } = SortOptions.Relevant;
        public int Page { get; set; } = 1;
    }
}
