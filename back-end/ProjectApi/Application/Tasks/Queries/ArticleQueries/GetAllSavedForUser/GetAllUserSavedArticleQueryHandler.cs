﻿using AppDomain.DTOs.Article;
using AppDomain.DTOs.Pagination;
using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.ArticleQueries.GetAllSavedForUser
{
    public class GetAllUserSavedArticleQueryHandler : IRequestHandler<GetAllUserSavedArticleQuery,PaginatedListDto<GetAllArticleDTO>>
    {
        private readonly IArticleRepository _articleRepository;

        public GetAllUserSavedArticleQueryHandler(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }

        public async Task<PaginatedListDto<GetAllArticleDTO>> Handle(GetAllUserSavedArticleQuery request, CancellationToken cancellationToken)
        {
            var result = await _articleRepository.GetAllSavedArticleForUser(
                request.SearchQuery,
                request.CategoryIdList,
                request.FlagId,
                request.Sort,
                request.Page);

            return result;
        }
    }
}
