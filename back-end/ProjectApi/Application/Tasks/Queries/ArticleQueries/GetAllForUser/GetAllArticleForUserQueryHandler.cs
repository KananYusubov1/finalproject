﻿using AppDomain.DTOs.Article;
using AppDomain.DTOs.Pagination;
using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.ArticleQueries.GetAllForUser
{
    public class GetAllArticleForUserQueryHandler : IRequestHandler<GetAllArticleForUserQuery,PaginatedListDto<GetAllArticleDTO>>
    {
        private readonly IArticleRepository _articleRepository;

        public GetAllArticleForUserQueryHandler(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }

        public async Task<PaginatedListDto<GetAllArticleDTO>> Handle(GetAllArticleForUserQuery request, CancellationToken cancellationToken)
        {
            var result = await _articleRepository.GetAllUserArticle(
                request.UserId,
                request.SearchQuery,
                request.CategoryIdList,
                request.FlagId,
                request.Sort,
                request.Page);

            return result;
        }
    }
}
