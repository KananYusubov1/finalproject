﻿using AppDomain.DTOs.Article;
using MediatR;

namespace Application.Tasks.Queries.ArticleQueries.GetArticlesForFeed;

public class GetArticlesForFeedQuery : IRequest<List<GetAllArticleDTO>>
{

}