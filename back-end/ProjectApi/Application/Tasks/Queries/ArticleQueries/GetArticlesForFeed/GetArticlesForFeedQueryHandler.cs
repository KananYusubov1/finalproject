﻿using AppDomain.DTOs.Article;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.ArticleQueries.GetArticlesForFeed;

public class GetArticlesForFeedQueryHandler : IRequestHandler<GetArticlesForFeedQuery, List<GetAllArticleDTO>>
{
    private readonly IArticleRepository _articleRepository;

    public GetArticlesForFeedQueryHandler(IArticleRepository articleRepository)
    {
        _articleRepository = articleRepository;
    }

    public async Task<List<GetAllArticleDTO>> Handle(GetArticlesForFeedQuery request, CancellationToken cancellationToken)
    {
        return await _articleRepository.GetArticlesForFeedAsync();
    }
}