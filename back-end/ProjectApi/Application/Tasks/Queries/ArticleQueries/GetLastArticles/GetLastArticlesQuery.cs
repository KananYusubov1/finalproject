﻿using AppDomain.DTOs.Article;
using MediatR;

namespace Application.Tasks.Queries.ArticleQueries.GetLastArticles;

public class GetLastArticlesQuery : IRequest<IEnumerable<GetLastArticlesDTO>>
{

}