﻿using AppDomain.DTOs.Article;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.ArticleQueries.GetLastArticles;

public class GetLastArticlesQueryHandler : IRequestHandler<GetLastArticlesQuery, IEnumerable<GetLastArticlesDTO>>
{
    private readonly IArticleRepository _articleRepository;

    public GetLastArticlesQueryHandler(IArticleRepository articleRepository)
    {
        _articleRepository = articleRepository;
    }

    public async Task<IEnumerable<GetLastArticlesDTO>> Handle(GetLastArticlesQuery request, CancellationToken cancellationToken)
    {
        return await _articleRepository.GetLastArticlesAsync();
    }
}