﻿using AppDomain.DTOs.Tags;
using AppDomain.Entities.TagBaseRelated;
using AppDomain.Interfaces;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.ArticleFlagQueries.GetArticleFlagById
{
    public class GetArticleFlagByIdQueryHandler : IRequestHandler<GetArticleFlagByIdQuery, TagPreviewResponseDto>
    {
        private readonly IArticleFlagRepository _articleFlagRepository;

        private readonly IMapper _mapper;

        public GetArticleFlagByIdQueryHandler(IArticleFlagRepository articleFlagRepository, IMapper mapper)
        {
            _articleFlagRepository = articleFlagRepository;
            _mapper = mapper;
        }

        public async Task<TagPreviewResponseDto> Handle(GetArticleFlagByIdQuery request, CancellationToken cancellationToken)
        {
            var result = await _articleFlagRepository.GetArticleFlagById(request.ArticleFlagId);

            TagPreviewResponseDto tagPreview = _mapper.Map<TagPreviewResponseDto>(result);

            return tagPreview;
        }
    }
}
