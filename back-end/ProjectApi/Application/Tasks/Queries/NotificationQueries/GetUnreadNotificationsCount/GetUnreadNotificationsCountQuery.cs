﻿using MediatR;

namespace Application.Tasks.Queries.NotificationQueries.GetUnreadNotificationsCount;

public class GetUnreadNotificationsCountQuery : IRequest<int>
{

}