﻿using AppDomain.Interfaces;
using AppDomain.Responses;
using MediatR;

namespace Application.Tasks.Queries.NotificationQueries.GetAllBudsNotifications;

public class GetAllBudsNotificationsQueryHandler : IRequestHandler<GetAllBudsNotificationsQuery, IEnumerable<BudsNotificationResponse>>
{
    private readonly INotificationRepository _notificationRepository;

    public GetAllBudsNotificationsQueryHandler(INotificationRepository notificationRepository)
    {
        _notificationRepository = notificationRepository;
    }

    public async Task<IEnumerable<BudsNotificationResponse>> Handle(GetAllBudsNotificationsQuery request, CancellationToken cancellationToken)
    {
        return await _notificationRepository.GetAllBudsRelatedNotificationsAsync();
    }
}