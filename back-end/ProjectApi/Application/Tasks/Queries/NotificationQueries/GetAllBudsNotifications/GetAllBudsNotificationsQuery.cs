﻿using AppDomain.Responses;
using MediatR;

namespace Application.Tasks.Queries.NotificationQueries.GetAllBudsNotifications;

public class GetAllBudsNotificationsQuery : IRequest<IEnumerable<BudsNotificationResponse>>
{

}