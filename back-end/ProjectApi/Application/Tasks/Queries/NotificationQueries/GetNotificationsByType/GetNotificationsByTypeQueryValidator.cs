﻿using FluentValidation;

namespace Application.Tasks.Queries.NotificationQueries.GetNotificationsByType;

public class GetNotificationsByTypeQueryValidator : AbstractValidator<GetNotificationsByTypeQuery>
{
    public GetNotificationsByTypeQueryValidator()
    {
        RuleFor(n => n.Type)
            .IsInEnum();
    }
}