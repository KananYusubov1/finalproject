﻿using AppDomain.Entities.NotificationRelated;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.NotificationQueries.GetNotificationsByType;

public class GetNotificationsByTypeQueryHandler : IRequestHandler<GetNotificationsByTypeQuery, IEnumerable<Notification>>
{
    private readonly INotificationRepository _notificationRepository;

    public GetNotificationsByTypeQueryHandler(INotificationRepository notificationRepository)
    {
        _notificationRepository = notificationRepository;
    }

    public async Task<IEnumerable<Notification>> Handle(GetNotificationsByTypeQuery request, CancellationToken cancellationToken)
    {
        return await _notificationRepository.GetNotificationsByTypeAsync(request.Type);
    }
}