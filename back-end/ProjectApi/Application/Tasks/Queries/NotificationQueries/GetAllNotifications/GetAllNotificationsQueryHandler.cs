﻿using AppDomain.Entities.NotificationRelated;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.NotificationQueries.GetAllNotifications;

public class GetAllNotificationsQueryHandler : IRequestHandler<GetAllNotificationsQuery, IEnumerable<Notification>>
{
    private readonly INotificationRepository _notificationRepository;

    public GetAllNotificationsQueryHandler(INotificationRepository notificationRepository)
    {
        _notificationRepository = notificationRepository;
    }

    public async Task<IEnumerable<Notification>> Handle(GetAllNotificationsQuery request, CancellationToken cancellationToken)
    {
        return await _notificationRepository.GetAllNotificationsAsync();
    }
}