﻿using AppDomain.DTOs.Product;
using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.ProductQueries.GetById
{
    public class GetByIdProductQueryHandler : IRequestHandler<GetByIdProductQuery, GetByIdProductDTO>
    {
        private readonly IProductRepository _productRepository;

        public GetByIdProductQueryHandler(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public async Task<GetByIdProductDTO> Handle(GetByIdProductQuery request, CancellationToken cancellationToken)
        {
            return await _productRepository.GetByIdProduct(request.ProductId);
        }
    }
}
