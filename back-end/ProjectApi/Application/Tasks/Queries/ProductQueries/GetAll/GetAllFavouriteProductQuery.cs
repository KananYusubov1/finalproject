﻿using AppDomain.DTOs.Pagination;
using AppDomain.DTOs.Product;
using AppDomain.Enums;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.ProductQueries.GetAll
{
    public class GetAllFavouriteProductQuery : IRequest<PaginatedListDto<GetAllProductDTO>>
    {
        public string? SearchQuery { get; set; }
        public ProductSortOptions Sort { get; set; } = ProductSortOptions.Newest;
        public int Page { get; set; } = 1;
    }
}
