﻿using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.ProductQueries.GetAllRank
{
    public class GetAllRankCommandHandler : IRequestHandler<GetAllRankCommand, Dictionary<string, int>>
    {
        private readonly IProductRepository _productRepository;

        public GetAllRankCommandHandler(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public async Task<Dictionary<string, int>> Handle(GetAllRankCommand request, CancellationToken cancellationToken)
        {
            return await _productRepository.GetAllRankForProduct(request.ProductId);
        }
    }
}
