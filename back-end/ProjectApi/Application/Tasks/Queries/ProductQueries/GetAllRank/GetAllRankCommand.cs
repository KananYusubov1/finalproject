﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.ProductQueries.GetAllRank
{
    public class GetAllRankCommand : IRequest<Dictionary<string,int>>
    {
        public string ProductId { get; set; }
    }
}
