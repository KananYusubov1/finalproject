﻿using AppDomain.Entities.TagBaseRelated;
using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.ResourceFlagQueries.GetAllResourceFlag
{
    public class GetAllResourceFlagQueryHandler : IRequestHandler<GetAllResourceFlagQuery, List<ResourceFlag>>
    {
        private readonly IResourceFlagRepository _resourceFlagRepository;

        public GetAllResourceFlagQueryHandler(IResourceFlagRepository resourceFlagRepository)
        {
            _resourceFlagRepository = resourceFlagRepository;
        }

        public async Task<List<ResourceFlag>> Handle(GetAllResourceFlagQuery request, CancellationToken cancellationToken)
        {
            var result = await _resourceFlagRepository.GetAllResourceFlag(request.KeyWord);

            return result;
        }
    }
}
