﻿using AppDomain.DTOs.Tags;
using AppDomain.Entities.TagBaseRelated;
using AppDomain.Interfaces;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.ResourceFlagQueries.GetResourceFlagById
{
    public class GetResourceFlagByIdQueryHandler : IRequestHandler<GetResourceFlagByIdQuery, TagPreviewResponseDto>
    {
        private readonly IResourceFlagRepository _resourceFlagRepository;

        private readonly IMapper _mapper;

        public GetResourceFlagByIdQueryHandler(IResourceFlagRepository resourceFlagRepository, IMapper mapper)
        {
            _resourceFlagRepository = resourceFlagRepository;
            _mapper = mapper;
        }

        public async Task<TagPreviewResponseDto> Handle(GetResourceFlagByIdQuery request, CancellationToken cancellationToken)
        {
            var result = await _resourceFlagRepository.GetResourceFlagById(request.ResourceFlagId);

            TagPreviewResponseDto tagPreview = _mapper.Map<TagPreviewResponseDto>(result);

            return tagPreview;
        }
    }
}
