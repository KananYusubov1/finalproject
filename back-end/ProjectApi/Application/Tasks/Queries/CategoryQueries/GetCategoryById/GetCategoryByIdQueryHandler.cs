﻿using AppDomain.DTOs.Tags;
using AppDomain.Interfaces;
using AutoMapper;
using MediatR;

namespace Application.Tasks.Queries.CategoryQueries.GetCategoryById
{
    public class GetCategoryByIdQueryHandler : IRequestHandler<GetCategoryByIdQuery, TagPreviewResponseDto>
    {
        private readonly ICategoryRepository _categoryRepository;

        private readonly IMapper _mapper;

        public GetCategoryByIdQueryHandler(ICategoryRepository categoryRepository, IMapper mapper)
        {
            _categoryRepository = categoryRepository;
            _mapper = mapper;
        }

        public async Task<TagPreviewResponseDto> Handle(GetCategoryByIdQuery request, CancellationToken cancellationToken)
        {
            var findCategory = await _categoryRepository.GetCategoryById(request.CategoryId);

            TagPreviewResponseDto tagPreview = _mapper.Map<TagPreviewResponseDto>(findCategory);

            return await Task.FromResult(tagPreview);
        }
    }
}