﻿using AppDomain.DTOs.Category;
using AppDomain.Interfaces;
using AutoMapper;
using MediatR;

namespace Application.Tasks.Queries.CategoryQueries.GetAllCategoryBuild
{
    public class GetAllCategoryBuildProfileQueryHandler : IRequestHandler<GetAllCategoryBuildProfileQuery, List<CategoryBuildProfileDTO>>
    {
        private readonly IMapper _mapper;

        private readonly ICategoryRepository _categoryRepository;

        public GetAllCategoryBuildProfileQueryHandler(IMapper mapper, ICategoryRepository categoryRepository)
        {
            _mapper = mapper;
            _categoryRepository = categoryRepository;
        }

        public async Task<List<CategoryBuildProfileDTO>> Handle(GetAllCategoryBuildProfileQuery request, CancellationToken cancellationToken)
        {
            var result = await _categoryRepository.GetAllCategory(null);

            List<CategoryBuildProfileDTO> newList = _mapper.Map<List<CategoryBuildProfileDTO>>(result);

            return newList;
        }
    }
}