﻿using AppDomain.DTOs.Tags;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.CategoryQueries.GetAllFollowed
{
    public class GetAllFollowedCategoryQuery : IRequest<List<TagFullResponseDto>>
    {
        public string? KeyWord { get; set; }
    }
}
