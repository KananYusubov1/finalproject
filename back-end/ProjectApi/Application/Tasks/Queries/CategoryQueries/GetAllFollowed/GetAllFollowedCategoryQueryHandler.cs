﻿using AppDomain.DTOs.Tags;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.CategoryQueries.GetAllFollowed;

public class GetAllFollowedCategoryQueryHandler : IRequestHandler<GetAllFollowedCategoryQuery,List<TagFullResponseDto>>
{
    private readonly ICategoryRepository _categoryRepository;

    private readonly IUserRepository _userRepository;

    public GetAllFollowedCategoryQueryHandler(ICategoryRepository categoryRepository, IUserRepository userRepository)
    {
        _categoryRepository = categoryRepository;
        _userRepository = userRepository;
    }

    public async Task<List<TagFullResponseDto>> Handle(GetAllFollowedCategoryQuery request, CancellationToken cancellationToken)
    {
        try
        {
            var userId = _userRepository.GetClaimValue("userId");

            var result = await _categoryRepository.GetAllFollowedCategory(userId, request.KeyWord);

            return result;
        }
        catch (Exception)
        {
            return null;
        }
    }
}