﻿using AppDomain.DTOs.Tags;
using MediatR;

namespace Application.Tasks.Queries.CategoryQueries.GetAllCategory;

public class GetAllCategoryQuery : IRequest<List<TagFullResponseDto>>
{
    public string? KeyWord { get; set; }
}