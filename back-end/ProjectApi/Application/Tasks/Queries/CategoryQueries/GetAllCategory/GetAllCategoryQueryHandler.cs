﻿using AppDomain.DTOs.Tags;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.CategoryQueries.GetAllCategory;

public class GetAllCategoryQueryHandler : IRequestHandler<GetAllCategoryQuery, List<TagFullResponseDto>>
{
    private readonly ICategoryRepository _categoryRepository;

    private readonly IAuthRepository _authRepository;

    public GetAllCategoryQueryHandler(ICategoryRepository categoryRepository, IAuthRepository authRepository)
    {
        _categoryRepository = categoryRepository;
        _authRepository = authRepository;
    }

    public async Task<List<TagFullResponseDto>> Handle(
        GetAllCategoryQuery request,
        CancellationToken cancellationToken
    )
    {
        try
        {
            var userId = _authRepository.GetClaimValue("userId");

            var result = await _categoryRepository.GetAllCategory(userId, request.KeyWord);

            return await Task.FromResult(result);
        }
        catch (Exception)
        {
            return null;
        }
    }
}