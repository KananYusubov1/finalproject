﻿using AppDomain.DTOs.Review;
using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.ReviewQueries.GetAll
{
    public class GetAllReviewQueryHandler : IRequestHandler<GetAllReviewQuery, List<GetAllReviewDTO>>
    {
        private readonly IReviewRepository _repository;

        public GetAllReviewQueryHandler(IReviewRepository repository)
        {
            _repository = repository;
        }

        public async Task<List<GetAllReviewDTO>> Handle(GetAllReviewQuery request, CancellationToken cancellationToken)
        {
            return await _repository.GetAllReviews(request.ProductId);
        }
    }
}
