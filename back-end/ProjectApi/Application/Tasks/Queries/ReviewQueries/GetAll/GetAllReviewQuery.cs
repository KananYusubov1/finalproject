﻿using AppDomain.DTOs.Review;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.ReviewQueries.GetAll
{
    public class GetAllReviewQuery : IRequest<List<GetAllReviewDTO>>
    {
        public string ProductId { get; set; }
    }
}
