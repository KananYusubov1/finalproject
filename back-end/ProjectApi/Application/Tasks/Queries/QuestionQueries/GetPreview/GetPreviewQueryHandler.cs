﻿using AppDomain.DTOs.Question;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.QuestionQueries.GetPreview
{
    public class GetPreviewQueryHandler : IRequestHandler<GetPreviewQuery, QuestionPreviewDTO>
    {
        private readonly IQuestionRepository _questionRepository;

        private readonly IUserRepository _userRepository;

        public GetPreviewQueryHandler(IQuestionRepository questionRepository, IUserRepository userRepository)
        {
            _questionRepository = questionRepository;
            _userRepository = userRepository;
        }

        public async Task<QuestionPreviewDTO> Handle(GetPreviewQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var tokenId = _userRepository.GetClaimValue("userId");

                var result = await _questionRepository.GetQuestionPreview(request.QuestionId, tokenId);

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}