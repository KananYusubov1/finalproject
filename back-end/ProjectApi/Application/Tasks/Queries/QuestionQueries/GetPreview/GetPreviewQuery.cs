﻿using AppDomain.DTOs.Question;
using MediatR;

namespace Application.Tasks.Queries.QuestionQueries.GetPreview;

public class GetPreviewQuery : IRequest<QuestionPreviewDTO>
{
    public string QuestionId { get; set; }
}