﻿using FluentValidation;

namespace Application.Tasks.Queries.QuestionQueries.GetById;

public class GetQuestionByIdQueryValidator : AbstractValidator<GetQuestionByIdQuery>
{
    public GetQuestionByIdQueryValidator()
    {
        RuleFor(x => x.QuestionId).NotNull().NotEmpty();
    }
}