﻿using AppDomain.DTOs.Question;
using MediatR;

namespace Application.Tasks.Queries.QuestionQueries.GetById;

public class GetQuestionByIdQuery : IRequest<GetByIdQuestionDTO>
{
    public string QuestionId { get; set; }
}