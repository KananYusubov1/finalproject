﻿using AppDomain.DTOs.Question;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.QuestionQueries.GetById;

public class GetQuestionByIdQueryHandler : IRequestHandler<GetQuestionByIdQuery,GetByIdQuestionDTO>
{
    private readonly IQuestionRepository _questionRepository;

    private readonly IUserRepository _userRepository;

    public GetQuestionByIdQueryHandler(IQuestionRepository questionRepository, IUserRepository userRepository)
    {
        _questionRepository = questionRepository;
        _userRepository = userRepository;
    }

    public async Task<GetByIdQuestionDTO> Handle(GetQuestionByIdQuery request, CancellationToken cancellationToken)
    {
        try
        {
            var tokenId = _userRepository.GetClaimValue("userId");

            var result = await _questionRepository.GetQuestionById(request.QuestionId, tokenId);

            return result;
        }
        catch (Exception)
        {
            return null;
        }
    }
}