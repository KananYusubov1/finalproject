﻿using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.QuestionQueries.GetCount
{
    public class GetAllQuestionCountQueryHandler : IRequestHandler<GetAllQuestionCountQuery,int>
    {
        private readonly IQuestionRepository _questionRepository;

        public GetAllQuestionCountQueryHandler(IQuestionRepository questionRepository)
        {
            _questionRepository = questionRepository;
        }

        public async Task<int> Handle(GetAllQuestionCountQuery request, CancellationToken cancellationToken)
        {
            var result = await _questionRepository.GetAllQuestionCount();

            return result;
        }
    }
}
