﻿using AppDomain.DTOs.Pagination;
using AppDomain.DTOs.Question;
using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.QuestionQueries.GetAllWatched
{
    public class GetAllWatchedQuestionQueryHandler : IRequestHandler<GetAllWatchedQuestionQuery,PaginatedListDto<GetAllQuestionDTO>>
    {
        private readonly IQuestionRepository _questionRepository;

        public GetAllWatchedQuestionQueryHandler(IQuestionRepository questionRepository)
        {
            _questionRepository = questionRepository;
        }

        public async Task<PaginatedListDto<GetAllQuestionDTO>> Handle(GetAllWatchedQuestionQuery request, CancellationToken cancellationToken)
        {
            var data = await _questionRepository.GetAllWatchedQuestion(
                request.SearchQuery,
                request.CategoryIdList,
                request.Sort,
                request.Page);

            return data;
        }
    }
}
