﻿using AppDomain.DTOs.Pagination;
using AppDomain.DTOs.Question;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.QuestionQueries.GetAll;

public class GetAllQuestionQueryHandler : IRequestHandler<GetAllQuestionQuery, PaginatedListDto<GetAllQuestionDTO>>
{
    private readonly IQuestionRepository _questionRepository;

    public GetAllQuestionQueryHandler(IQuestionRepository questionRepository)
    {
        _questionRepository = questionRepository;
    }

    public Task<PaginatedListDto<GetAllQuestionDTO>> Handle(GetAllQuestionQuery request, CancellationToken cancellationToken)
    {
        return _questionRepository.GetAllQuestion(
            request.SearchQuery,
            request.CategoryIdList,
            request.Sort,
            request.Page);
    }
}