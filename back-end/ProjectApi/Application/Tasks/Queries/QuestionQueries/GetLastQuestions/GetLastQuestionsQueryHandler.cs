﻿using AppDomain.DTOs.Question;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.ArticleQueries.GetLastQuestions;

public class GetLastQuestionsQueryHandler : IRequestHandler<GetLastQuestionsQuery, IEnumerable<GetLastQuestionsDTO>>
{
    private readonly IQuestionRepository _questionRepository;

    public GetLastQuestionsQueryHandler(IQuestionRepository questionRepository)
    {
        _questionRepository = questionRepository;
    }

    public async Task<IEnumerable<GetLastQuestionsDTO>> Handle(GetLastQuestionsQuery request, CancellationToken cancellationToken)
    {
        return await _questionRepository.GetLastQuestionsAsync();
    }
}