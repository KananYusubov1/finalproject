﻿using AppDomain.Entities.AdminRelated;
using AppDomain.Enums;
using MediatR;

namespace Application.Tasks.Queries.ReportQueries.GetAllReportsByStatus;

public class GetAllReportsByStatusQuery : IRequest<IEnumerable<Report>>
{
    public ReportStatus ReportStatus { get; set; }
}