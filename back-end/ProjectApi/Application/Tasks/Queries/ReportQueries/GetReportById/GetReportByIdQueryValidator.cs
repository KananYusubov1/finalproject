﻿using FluentValidation;

namespace Application.Tasks.Queries.ReportQueries.GetReportById;

public class GetReportByIdQueryValidator : AbstractValidator<GetReportByIdQuery>
{
    public GetReportByIdQueryValidator()
    {
        RuleFor(x => x.ReportId).NotEmpty();
    }
}