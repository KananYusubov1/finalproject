﻿using AppDomain.Entities.AdminRelated;
using MediatR;

namespace Application.Tasks.Queries.ReportQueries.GetReportById;

public class GetReportByIdQuery : IRequest<Report>
{
    public string ReportId { get; set; }
}