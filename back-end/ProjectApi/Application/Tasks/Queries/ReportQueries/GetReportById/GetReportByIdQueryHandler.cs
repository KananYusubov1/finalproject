﻿using AppDomain.Entities.AdminRelated;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.ReportQueries.GetReportById;

public class GetReportByIdQueryHandler : IRequestHandler<GetReportByIdQuery, Report>
{
    private readonly IReportRepository _reportRepository;

    public GetReportByIdQueryHandler(IReportRepository reportRepository)
    {
        _reportRepository = reportRepository;
    }

    public async Task<Report> Handle(GetReportByIdQuery request, CancellationToken cancellationToken)
    {
        var report = await _reportRepository.GetReportById(request.ReportId);

        return report;
    }
}
