﻿using AppDomain.Entities.AdminRelated;
using MediatR;

namespace Application.Tasks.Queries.ReportQueries.GetAllReports;

public class GetAllReportsQuery : IRequest<IEnumerable<Report>>
{

}