﻿using AppDomain.Entities.AdminRelated;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.ReportQueries.GetAllReports;

public class GetAllReportsQueryHandler : IRequestHandler<GetAllReportsQuery, IEnumerable<Report>>
{
    private readonly IReportRepository _reportRepository;

    public GetAllReportsQueryHandler(IReportRepository reportRepository)
    {
        _reportRepository = reportRepository;
    }

    public async Task<IEnumerable<Report>> Handle(GetAllReportsQuery request, CancellationToken cancellationToken)
    {
        var reports = await _reportRepository.GetAllReports();

        return reports;
    }
}
