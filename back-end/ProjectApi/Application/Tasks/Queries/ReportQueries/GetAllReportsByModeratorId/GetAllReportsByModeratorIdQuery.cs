﻿using AppDomain.Entities.AdminRelated;
using AppDomain.Enums;
using MediatR;

namespace Application.Tasks.Queries.ReportQueries.GetAllReportsByModeratorId;

public class GetAllReportsByModeratorIdQuery : IRequest<IEnumerable<Report>>
{
    public string ModeratorId { get; set; }
    public ReportStatus ReportStatus { get; set; } = ReportStatus.All;
}