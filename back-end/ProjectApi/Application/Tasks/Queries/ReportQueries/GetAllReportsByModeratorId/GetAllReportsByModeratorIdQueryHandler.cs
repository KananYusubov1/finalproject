﻿using AppDomain.Entities.AdminRelated;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.ReportQueries.GetAllReportsByModeratorId;

public class GetAllReportsByModeratorIdQueryHandler : IRequestHandler<GetAllReportsByModeratorIdQuery, IEnumerable<Report>>
{
    private readonly IReportRepository _reportRepository;

    public GetAllReportsByModeratorIdQueryHandler(IReportRepository reportRepository)
    {
        _reportRepository = reportRepository;
    }

    public async Task<IEnumerable<Report>> Handle(GetAllReportsByModeratorIdQuery request, CancellationToken cancellationToken)
    {
        var reports = await _reportRepository.GetAllReportsByModeratorId(request.ModeratorId, request.ReportStatus);

        return reports;
    }
}
