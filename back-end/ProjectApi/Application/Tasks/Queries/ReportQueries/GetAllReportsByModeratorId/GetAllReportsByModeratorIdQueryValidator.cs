﻿using FluentValidation;

namespace Application.Tasks.Queries.ReportQueries.GetAllReportsByModeratorId;

public class GetAllReportsByModeratorIdQueryValidator : AbstractValidator<GetAllReportsByModeratorIdQuery>
{
    public GetAllReportsByModeratorIdQueryValidator()
    {
        RuleFor(x => x.ModeratorId)
            .NotEmpty();
    }
}