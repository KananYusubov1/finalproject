﻿using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.UserQueries.GetAnswerCount
{
    public class GetAnswerCountQueryHandler : IRequestHandler<GetAnswerCountQuery, int>
    {
        private readonly IUserRepository _userRepository;

        public GetAnswerCountQueryHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<int> Handle(GetAnswerCountQuery request, CancellationToken cancellationToken)
        {
            return await _userRepository.GetUserAnswersCount();
        }
    }
}
