﻿using AppDomain.Interfaces;
using AppDomain.Responses.UserResponses;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.GetFollowings;

public class GetFollowingsQueryHandler : IRequestHandler<GetFollowingsQuery, IEnumerable<UserProfileResponse>>
{
    private readonly IUserRepository _userRepository;

    public GetFollowingsQueryHandler(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<IEnumerable<UserProfileResponse>> Handle(GetFollowingsQuery request, CancellationToken cancellationToken)
    {
        return await _userRepository.GetFollowingsAsync();
    }
}