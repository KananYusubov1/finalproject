﻿using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.UserQueries.GetAllCategoryCount
{
    public class GetAllUserCategoryQueryHandler : IRequestHandler<GetAllUserCategoryQuery, int>
    {
        private readonly IUserRepository _userRepository;

        public GetAllUserCategoryQueryHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<int> Handle(GetAllUserCategoryQuery request, CancellationToken cancellationToken)
        {
            return await _userRepository.GetCategoryFollowedsCount();
        }
    }
}
