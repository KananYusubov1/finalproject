﻿using AppDomain.DTOs.Article;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.GetPinnedArticles;

public class GetRepositoriesQueryHandler : IRequestHandler<GetPinnedArticlesQuery, IEnumerable<GetAllArticleDTO>>
{
    private readonly IUserRepository _userRepository;

    public GetRepositoriesQueryHandler(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<IEnumerable<GetAllArticleDTO>> Handle(GetPinnedArticlesQuery request, CancellationToken cancellationToken)
    {
        return await _userRepository.GetPinnedArticlesAsync(request.UserId);
    }
}