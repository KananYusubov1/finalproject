﻿using AppDomain.DTOs.Article;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.GetPinnedArticles;

public class GetPinnedArticlesQuery : IRequest<IEnumerable<GetAllArticleDTO>>
{
    public string UserId { get; set; }
}