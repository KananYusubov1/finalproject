﻿using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.UserQueries.GetResourceCount
{
    public class GetResourceCountQueryHandler : IRequestHandler<GetResourceCountQuery, int>
    {
        private readonly IUserRepository _userRepository;

        public GetResourceCountQueryHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<int> Handle(GetResourceCountQuery request, CancellationToken cancellationToken)
        {
            return await _userRepository.GetUserResourcesCount();
        }
    }
}
