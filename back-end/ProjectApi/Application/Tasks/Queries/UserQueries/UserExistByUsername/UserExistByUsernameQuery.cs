﻿using MediatR;

namespace Application.Tasks.Queries.UserQueries.UserExistByUsername;

public class UserExistByUsernameQuery : IRequest<bool>
{
    public string Username { get; set; }
}