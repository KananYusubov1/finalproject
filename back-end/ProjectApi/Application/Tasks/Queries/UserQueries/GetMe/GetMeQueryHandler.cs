﻿using AppDomain.Interfaces;
using AppDomain.Responses.UserResponses;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.GetMe;

public class GetMeQueryHandler : IRequestHandler<GetMeQuery, UserPreviewResponse>
{
    private readonly IUserRepository _userRepository;

    public GetMeQueryHandler(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<UserPreviewResponse> Handle(GetMeQuery request, CancellationToken cancellationToken)
    {
        return await _userRepository.GetMe();
    }
}