﻿using FluentValidation;

namespace Application.Tasks.Queries.UserQueries.GetUserBySelector;

public class GetUserBySelectorQueryValidator : AbstractValidator<GetUserBySelectorQuery>
{
    public GetUserBySelectorQueryValidator()
    {
        RuleFor(u => u.Identifier)
            .NotEmpty()
            .NotNull();

        RuleFor(u => u.Selector)
            .IsInEnum()
            .NotNull();
    }
}