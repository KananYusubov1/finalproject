﻿using AppDomain.Entities.UserRelated;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.GetUserBySelector;

public class GetUserBySelectorQueryHandler : IRequestHandler<GetUserBySelectorQuery, User>
{
    private readonly IUserRepository _userRepository;

    public GetUserBySelectorQueryHandler(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<User> Handle(GetUserBySelectorQuery request, CancellationToken cancellationToken)
    {
        return await _userRepository.GetUserBySelectorAsync(request.Selector, request.Identifier);
    }
}