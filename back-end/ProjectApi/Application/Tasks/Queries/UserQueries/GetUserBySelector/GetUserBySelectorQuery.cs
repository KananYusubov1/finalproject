﻿using AppDomain.Entities.UserRelated;
using AppDomain.Enums;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.GetUserBySelector;

public class GetUserBySelectorQuery : IRequest<User>
{
    public Selector Selector { get; set; }
    public string Identifier { get; set; }
}