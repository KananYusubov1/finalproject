﻿using AppDomain.Interfaces;
using AppDomain.Responses;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.GetConnectedEmails;

public class GetConnectedEmailsQueryHandler : IRequestHandler<GetConnectedEmailsQuery, IEnumerable<EmailResponse>>
{
    private readonly IUserRepository _userRepository;

    public GetConnectedEmailsQueryHandler(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<IEnumerable<EmailResponse>> Handle(GetConnectedEmailsQuery request, CancellationToken cancellationToken)
    {
        return await _userRepository.GetConnectedEmailsAsync();
    }
}