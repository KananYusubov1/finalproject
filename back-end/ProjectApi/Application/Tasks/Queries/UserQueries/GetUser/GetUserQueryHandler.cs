﻿using AppDomain.DTOs.Auth;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.GetUser;

public class GetUserQueryHandler : IRequestHandler<GetUserQuery, UserAuthDto>
{
    private readonly IAuthRepository _authRepository;

    public GetUserQueryHandler(IAuthRepository authRepository)
    {
        _authRepository = authRepository;
    }

    public async Task<UserAuthDto> Handle(GetUserQuery request, CancellationToken cancellationToken)
    {
        return await _authRepository.LogInAsync(request.Email, request.Password);
    }
}