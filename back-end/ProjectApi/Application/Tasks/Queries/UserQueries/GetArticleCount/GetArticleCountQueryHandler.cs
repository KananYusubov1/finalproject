﻿using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.UserQueries.GetArticleCount
{
    public class GetArticleCountQueryHandler : IRequestHandler<GetArticleCountQuery, int>
    {
        private readonly IUserRepository _userRepository;

        public GetArticleCountQueryHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<int> Handle(GetArticleCountQuery request, CancellationToken cancellationToken)
        {
            return await _userRepository.GetUserArticlesCount();
        }
    }
}
