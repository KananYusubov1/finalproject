﻿using AppDomain.Responses;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.GetRepositories;

public class GetAllRepositoriesQuery : IRequest<IEnumerable<RepositoryResponse>>
{

}