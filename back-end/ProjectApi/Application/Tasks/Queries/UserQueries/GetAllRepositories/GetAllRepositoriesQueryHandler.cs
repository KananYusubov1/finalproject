﻿using AppDomain.Interfaces;
using AppDomain.Responses;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.GetRepositories;

public class GetRepositoriesQueryHandler : IRequestHandler<GetAllRepositoriesQuery, IEnumerable<RepositoryResponse>>
{
    private readonly IUserRepository _userRepository;

    public GetRepositoriesQueryHandler(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<IEnumerable<RepositoryResponse>> Handle(GetAllRepositoriesQuery request, CancellationToken cancellationToken)
    {
        return await _userRepository.GetAllRepositoriesAsync();
    }
}