﻿using AppDomain.DTOs.Settings;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.GetRepositories;

public class GetProfileSettingsQuery : IRequest<ProfileSettingsDTO>
{
    public string Id { get; set; }
}