﻿using AppDomain.DTOs.Settings;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.GetRepositories;

public class GeProfileSettingsQueryHandler
    : IRequestHandler<GetProfileSettingsQuery, ProfileSettingsDTO>
{
    private readonly IUserRepository _userRepository;

    public GeProfileSettingsQueryHandler(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<ProfileSettingsDTO> Handle(
        GetProfileSettingsQuery request,
        CancellationToken cancellationToken
    )
    {
        return await _userRepository.GetProfileSettingsAsync();
    }
}
