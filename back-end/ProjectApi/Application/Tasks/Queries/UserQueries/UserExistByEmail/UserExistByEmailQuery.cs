﻿using MediatR;

namespace Application.Tasks.Queries.UserQueries.UserExistByEmail;

public class UserExistByEmailQuery : IRequest<bool>
{
    public string Email { get; set; }
}