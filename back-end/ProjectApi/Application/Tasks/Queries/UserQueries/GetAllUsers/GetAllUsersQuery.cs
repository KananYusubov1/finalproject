﻿using AppDomain.Responses.UserResponses;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.GetAllUsers;

public class GetAllUsersQuery : IRequest<IEnumerable<UserProfileResponse>>
{
    public string Search { get; set; }
}