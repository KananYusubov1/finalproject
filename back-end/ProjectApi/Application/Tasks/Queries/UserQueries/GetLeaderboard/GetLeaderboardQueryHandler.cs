﻿using AppDomain.Interfaces;
using AppDomain.Responses;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.GetLeaderboard;

public class GetLeaderboardQueryHandler : IRequestHandler<GetLeaderboardQuery, IEnumerable<LeaderboardResponse>>
{
    private readonly IUserRepository _userRepository;

    public GetLeaderboardQueryHandler(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<IEnumerable<LeaderboardResponse>> Handle(GetLeaderboardQuery request, CancellationToken cancellationToken)
    {
        return _userRepository.GetLeaderboard();
    }
}