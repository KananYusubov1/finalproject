﻿using AppDomain.ValueObjects;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.GetPinnedRepositories;

public class GetPinnedRepositoriesQuery : IRequest<IEnumerable<PinnedRepository>>
{
    public string UserId { get; set; }
}