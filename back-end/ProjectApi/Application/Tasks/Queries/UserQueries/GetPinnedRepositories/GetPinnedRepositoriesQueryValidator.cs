﻿using FluentValidation;

namespace Application.Tasks.Queries.UserQueries.GetPinnedRepositories;

public class GetPinnedRepositoriesQueryValidator : AbstractValidator<GetPinnedRepositoriesQuery>
{
    public GetPinnedRepositoriesQueryValidator()
    {
        RuleFor(u => u.UserId).NotEmpty();
    }
}