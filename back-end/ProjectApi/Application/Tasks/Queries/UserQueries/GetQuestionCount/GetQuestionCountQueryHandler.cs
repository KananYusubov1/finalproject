﻿using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.UserQueries.GetQuestionCount
{
    public class GetQuestionCountQueryHandler : IRequestHandler<GetQuestionCountQuery, int>
    {
        private readonly IUserRepository _userRepository;

        public GetQuestionCountQueryHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<int> Handle(GetQuestionCountQuery request, CancellationToken cancellationToken)
        {
            return await _userRepository.GetUserQuestionsCount();
        }
    }
}
