﻿using AppDomain.DTOs.Answer;
using AppDomain.Entities.ContentRelated;
using MediatR;


namespace Application.Tasks.Queries.AnswerQueries.GetById
{
    public class GetByIdQuery : IRequest<GetByIdAnswerDTO>
    {
        public string AnswerId { get; set; }
    }
}
