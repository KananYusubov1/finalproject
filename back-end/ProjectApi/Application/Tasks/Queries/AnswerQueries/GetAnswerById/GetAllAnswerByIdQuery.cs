﻿using AppDomain.DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.AnswerQueries.GetAnswerById
{
    public class GetAllAnswerByIdQuery : IRequest<List<AnswerDTO>>
    {
        public string QuestionId { get; set; }
    }
}
