﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.AnswerQueries.GetAnswerById
{
    public class GetAllAnswerByIdQueryValidator : AbstractValidator<GetAllAnswerByIdQuery>
    {
        public GetAllAnswerByIdQueryValidator()
        {
            RuleFor(x => x.QuestionId).NotNull().NotEmpty();
        }
    }
}
