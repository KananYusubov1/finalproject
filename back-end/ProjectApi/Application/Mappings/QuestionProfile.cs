﻿using AppDomain.DTOs.Question;
using AppDomain.Entities.ContentRelated;
using Application.Tasks.Commands.Insert.InsertQuestion;
using Application.Tasks.Commands.Update.UpdateQuestion;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Mappings
{
    public class QuestionProfile : Profile
    {
        public QuestionProfile()
        {
            CreateMap<InsertQuestionCommand,Question>().ReverseMap();
            CreateMap<UpdateQuestionCommand,UpdateQuestionDTO>().ReverseMap();
            CreateMap<Question,QuestionPreviewDTO>().ReverseMap();
            CreateMap<Question,GetByIdQuestionDTO>().ReverseMap();
            CreateMap<Question,GetAllQuestionDTO>().ReverseMap();
        }
    }
}
