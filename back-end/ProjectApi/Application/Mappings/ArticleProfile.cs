﻿using AppDomain.DTOs.Article;
using AppDomain.Entities.ContentRelated;
using Application.Tasks.Commands.Update.UpdateArticle;
using AutoMapper;

namespace Application.Mappings
{
    public class ArticleProfile : Profile
    {
        public ArticleProfile()
        {
            CreateMap<UpdateArticleCommand,UpdateArticleDTO>().ReverseMap();
            CreateMap<Article,GetAllArticleDTO>().ReverseMap();
            CreateMap<Article,GetByIdArticleDTO>().ReverseMap();
        }
    }
}