﻿using AppDomain.DTOs.Review;
using AppDomain.Entities.ContentRelated;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Mappings
{
    public class ReviewProfile : Profile
    {
        public ReviewProfile()
        {
            CreateMap<Review,GetAllReviewDTO>().ReverseMap();
        }
    }
}
