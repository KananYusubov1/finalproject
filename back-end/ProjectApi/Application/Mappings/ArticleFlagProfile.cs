﻿using AppDomain.DTOs.ArticleFlag;
using AppDomain.DTOs.Category;
using AppDomain.DTOs.Tags;
using AppDomain.Entities.TagBaseRelated;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Mappings
{
    public class ArticleFlagProfile : Profile
    {
        public ArticleFlagProfile()
        {
   
            CreateMap<InsertArticleFlagDTO, ArticleFlag>().ReverseMap();
            CreateMap<UpdateArticleFlagDTO, ArticleFlag>().ReverseMap();
            CreateMap<ArticleFlag,TagPreviewResponseDto>().ReverseMap();
            CreateMap<ArticleFlag,TagDTO>().ReverseMap();
        }
    }
}
