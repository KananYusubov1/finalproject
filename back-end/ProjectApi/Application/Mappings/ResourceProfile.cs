﻿using AppDomain.DTOs.Resource;
using AppDomain.Entities.ContentRelated;
using Application.Tasks.Commands.Insert.InsertResource;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Mappings
{
    public class ResourceProfile : Profile
    {
        public ResourceProfile()
        {
            CreateMap<InsertResourceCommand,Resource>().ReverseMap();
        }
    }
}
