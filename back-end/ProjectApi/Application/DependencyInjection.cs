﻿using Application.Tasks.Commands.Insert.UserInserts.InsertUser;
using Microsoft.Extensions.DependencyInjection;
using Application.Mappings;
using System.Reflection;
using FluentValidation;

namespace Application;

public static class DependencyInjection
{
    public static IServiceCollection AddApplication(this IServiceCollection services)
    {
        services.AddMediatR(
            c => c.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly())
        );

        services.AddValidatorsFromAssemblyContaining<InsertUserCommand>();

        services.AddAutoMapper(Assembly.GetExecutingAssembly());
        services.AddAutoMapper(typeof(CategoryProfile));
        services.AddAutoMapper(typeof(ArticleFlagProfile));
        services.AddAutoMapper(typeof(ResourceFlagProfile));
        services.AddAutoMapper(typeof(ResourceProfile));
        services.AddAutoMapper(typeof(AnswerProfile));
        services.AddAutoMapper(typeof(QuestionProfile));
        services.AddAutoMapper(typeof(ArticleProfile));
        services.AddAutoMapper(typeof(ProductProfile));
        services.AddAutoMapper(typeof(ReviewProfile));

        return services;
    }
}